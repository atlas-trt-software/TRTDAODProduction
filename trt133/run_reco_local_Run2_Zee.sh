Reco_tf.py \
--inputDRAW_ZEEFile '/eos/user/s/saktas/xAOD_prod/data/data15_13TeV/Zee/data15_13TeV.00284285.physics_Main.merge.DRAW_EGZ.f643_m1453._0156.1' \
--outputAODFile 'test.root' \
--geometryVersion 'ATLAS-R2-2015-03-01-00' \
--conditionsTag 'CONDBR2-BLKPA-RUN2-11' \
--preExec 'all:flags.Tracking.writeExtendedTRT_PRDInfo=True; flags.InDet.doTRTGlobalOccupancy=True; flags.Tracking.doV0Finder=True' \
--steering 'doRAWtoALL' \
--maxEvents '5'
