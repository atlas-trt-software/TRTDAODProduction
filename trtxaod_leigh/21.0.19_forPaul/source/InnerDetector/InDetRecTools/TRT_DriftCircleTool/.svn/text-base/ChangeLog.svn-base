
2016-03-20 peter hansen <phansen@nbi.dk>
	* tag as TRT_DriftCircleTool-04-00-04
	* reduce memory consumption by getting rid of the rdo list in the rio constructor
	* and refraining from creating an rio if quality criteria are not met.
	* new public function bool passValidityGate.
	* general clean-up to increase readability

2016-03-11 Douglas Davis <douglas.davis@cern.ch>
	* src/TRT_DriftCircleTool.cxx - fix gas type checking
	  argon is == Argon or == Dead (not != Good)
	* tag as TRT_DriftCircleTool-04-00-03

2016-01-16 Tatyana Kharlamova <tatyana.kharlamova@cern.ch>
	* src/TRT_DriftCircleTool.cxx - send digit word to drift function tool, for overlay, to tell if it's a mc digit or not (from branch)
	* tag as TRT_DriftCircleTool-04-00-02

2015-09-11 Dimitrii Krasnopevtsev 
	* Adding new mu scaling for error
	* tag as TRT_DriftCircleTool-04-00-01

2013-08-09 Niels van Eldik
	* move to Eigen
	* tag as TRT_DriftCircleTool-04-00-00

2012-11-02 Niels van Eldik
	* reduce malloc
	* tag as TRT_DriftCircleTool-03-00-00

2012-10-15  Walter Lampl
	    CLHEP 2 migration
	    tag as TRT_DriftCircleTool-02-00-06

2012-07-11  Jahred Adelman <jahreda@gmail.com>
	    checkreq warnings
	    tag as TRT_DriftCircleTool-02-00-05

2012-06-27  Jahred Adelman <jahreda@gmail.com>
	    Fix nominal dead straw counting for cosmics too 
	    tag as TRT_DriftCircleTool-02-00-04

2012-06-27  Jahred Adelman <jahreda@gmail.com>
	    Fix nominal dead straw counting
	    tag as TRT_DriftCircleTool-02-00-03

2012-06-26  Jahred Adelman <jahreda@gmail.com>
	    Fix cosmics for new summary service
	    tag as TRT_DriftCircleTool-02-00-02

2012-06-25  Jahred Adelman <jahreda@gmail.com>
	    Update for potential xenon/argon gas
	    tag as TRT_DriftCircleTool-02-00-01

2011-12-14  Jahred Adelman <jahreda@gmail.com>
	    Add option to mask off HT bits when forming DCs
	    Copy to new tag so numbering is in order
	    tag as TRT_DriftCircleTool-02-00-00

2011-12-14  Jahred Adelman <jahreda@gmail.com>
	    Add option to mask off HT bits when forming DCs
	    tag as TRT_DriftCircleTool-01-05-18	
	
2011-06-01  Jahred Adelman <jahreda@gmail.com>
	    add validity interval suppression
	    tag as TRT_DriftCircleTool-01-05-17	

2011-04-27  Esben Klinkby <klinkby@nbi.dk>
	    add dummy HT correction to cosmic interface
	    tag as TRT_DriftCircleTool-01-05-16	
	
2011-04-14  Jahred Adelman <jahreda@gmail.com>
	    update DC creation to use correct LE
	    tag as TRT_DriftCircleTool-01-05-15	
	
2011-04-08  Esben Klinkby <klinkby@nbi.dk>
	    add HT correction from Jon Stahlman
	    tag as TRT_DriftCircleTool-01-05-14	

2010-10-20  Jahred Adelman <jahreda@gmail.com>
            One more fix! Add SimpleOutOfTimePileupSupression flag that is off by default 
            tag as TRT_DriftCircleTool-01-05-13

2010-10-20  Jahred Adelman <jahreda@gmail.com>
            Set cosmics default to off!
            tag as TRT_DriftCircleTool-01-05-12

2010-10-20  Jahred Adelman <jahreda@gmail.com>
            First proposal for 50 ns bunch spacing 
            tag as TRT_DriftCircleTool-01-05-11

2010-08-28  Esben Klinkby <klinkby@phy.duke.edu>
	    add flag: useDriftTimeToTCorrection to cosmics tool
	    tag as TRT_DriftCircleTool-01-05-10	
	
2010-08-25  Esben Klinkby <klinkby@phy.duke.edu>
	    Implement time over threshold corrections.
	    From Jon Stahlman
	    tag as TRT_DriftCircleTool-01-05-09	
	
2010-04-10  Peter Hansen <phansen@nbi.dk>
	    Changed call to TRT_DriftFunctionTool::errorOfDriftRadius
	    so as to take errors from database
	    tag as TRT_DriftCircleTool-01-05-08	
	
2010-01-18  Esben Klinkby <klinkby@phy.duke.edu>
	    Fixed bug 61417.
	    Logic of when to delete driftcircle from bad straw was wrong
	    tag as TRT_DriftCircleTool-01-05-07
 	    

2009-07-14  Peter Hansen <phansen@nbi.dk>
	    Fixed bug 49223.
	    If LE==0 or LE==24 then the error flag is
	    set in the data word and a "tube hit" is made
	    in the TRT_DriftCircle collection.
	    tag as TRT_DriftCircleTool-01-05-06
 	    
2009-05-11  Peter Hansen <phansen@nbi.dk>
	    changed the retrieval of the TRT helper m_trtid
	    tag as TRT_DriftCircleTool-01-05-05                                                                
	     
2009-03-26  Peter Hansen <phansen@nbi.dk>
	    Changed TRT_ConditionsSummaryTool to
	    InDetConditionsSummaryService/IInDetConditionsSvc
            tag as TRT_DriftCircleTool-01-05-04                                                                
	
2009-03-15  Peter Hansen <phansen@nbi.dk>
	    fixed checkreq warning
            tag as TRT_DriftCircleTool-01-05-03                                                                
	     	
2009-01-09  Peter Hansen <phansen@nbi.dk>
	    removed TRTStrawStatusCode from argument to convert(..)
            tag as TRT_DriftCircleTool-01-05-02
	
2008-12-18  Peter Hansen <phansen@nbi.dk>
	    Implemented ImprovingSoftware
            tag as TRT_DriftCircleTool-01-05-01
	
2008-11-20 Christian Schmitt <Christian.Schmitt@cern.ch>
           Change ComTime object to be const
           tag as TRT_DriftCircleTool-01-04-06

2008-06-29 Christian Schmitt <Christian.Schmitt@cern.ch>
           Add possibility to specify a global phase offset (cosmics only)
           tag as TRT_DriftCircleTool-01-04-05

2008-05-07 Peter Hansen <phansen@nbi.dk>
	   use DECLARE_NAMESPACE_TOOL instead of DECLARE_TOOL
	   tag as TRT_DriftCircleTool-01-04-04
	
2008-04-29 Christian Schmitt <Christian.Schmitt@cern.ch>
           Reserve space for vector (bug #36119)
           tag as TRT_DriftCircleTool-01-04-03

2008-03-05 Christian Schmitt <Christian.Schmitt@cern.ch>
           Add possibility to make use of conditions information
           tag as TRT_DriftCircleTool-01-04-02

2008-03-18 Markus Elsing
           migration to new IDC
           tag as TRT_DriftCircleTool-01-04-01

2008-03-05 Christian Schmitt <Christian.Schmitt@cern.ch>
           Don't throw away hits if there is no ComTime object
           tag as TRT_DriftCircleTool-01-03-10

2008-03-04 Christian Schmitt <Christian.Schmitt@cern.ch>
           Change missing ComTime ERROR to DEBUG
           tag as TRT_DriftCircleTool-01-03-09

2007-12-13 Christian Schmitt <Christian.Schmitt@cern.ch>
           Change in TRT Conditions packages
           tag as TRT_DriftCircleTool-01-03-08

2007-08-01 Christian Schmitt <Christian.Schmitt@cern.ch>
           use StrawStatusSummaryTool to mask Modules for Cosmic setup
           tag as TRT_DriftCircleTool-01-03-07

2007-07-16 Christian Schmitt <Christian.Schmitt@cern.ch>
           fix bug #28086 -> validity bit not set correctly
           tag as TRT_DriftCircleTool-01-03-06

2007-07-08  scott s snyder  <snyder@bnl.gov>
           src/TRT_DriftCircleTool.cxx (convert): If we don't push the hit,
	   need to delete tdc rather than loc.
           tag as TRT_DriftCircleTool-01-03-05

2007-07-05 Peter Hansen <phansen@nbi.dk>
	   changed the TRT_DriftCircle constructor to conform with
	   InDetPrepRawData-00-07-01
	   tag as TRT_DriftCircleTool-01-03-04
	
2007-06-26 Peter Hansen <phansen@nbi.dk>
	   added data word to the TRT_DriftCircle constructor
           changed driftTime value to be the real (corrected) one.
	   removed too verbose debug print
	   tag as TRT_DriftCircleTool-01-03-03
	
2007-06-12 Peter Hansen <phansen@nbi.dk>
	   Added doc
	   tag as TRT_DriftCircleTool-01-03-02 
	
2007-05-29 Christian Schmitt <Christian.Schmitt@cern.ch>
           Added Cosmics version that retrieves the event phase information
           from StoreGate to correct the DriftCircles
	   tag as TRT_DriftCircleTool-01-03-01 

2007-05-25 Andreas Wildauer
        * circles now also store the hash id and the index in the coll
        * necessary for proper persistification of EL to IDC on the RIO objects

2006-12-20 Andreas Wildauer <andreas.wildauer@cern.ch>
        * adjusted package to use toolhandle/configurables

2006-08-13 Peter Hansen <phansen@nbi.dk>
	   Use new functionality in TRT_DriftFunctionTool
	   to be informed on the nature of the run.
	   This allows to explicitely restrict changes in trigger
	   timing corrections to CTB data.
	   Only delete DriftCircles in case of bad straws.
	   Tagged as TRT_DriftCircleTool-01-01-12	

2006-07-18 Peter Hansen <phansen@nbi.dk>
	   Replaced trigger pll by the first layer 0 value seen
	   in a rdo collection.
	   Increased the time bins by a factor of two:
	   dt = 2*dt + (pll%2)
	   Tagged as TRT_DriftCircleTool-01-01-10	
	
2006-06-21 Rasmus Mackeprang <rasmus.mackeprang@cern.ch>
	   Improved decision to skip hits.
	   Tagged as TRT_DriftCircleTool-01-01-09	
	
2006-02-13 Rasmus Mackeprang <rasmus.mackeprang@cern.ch>
	   Fixed dependency TrkEventUtils->TrkEventPrimitives
	   Tagged as TRT_DriftCircleTool-01-01-08

2006-02-13 Rasmus Mackeprang <rasmus.mackeprang@cern.ch>
	   Enabled time dependent errors
	   Tagged as TRT_DriftCircleTool-01-01-07
	
2005-04-06 Ashfaq Ahmad <Ashfaq.Ahmad@cern.ch>
	   added code to deal with TRT bad channels
	   access to condition data via TRTStrawStatusTool
	   
2005-02-15 Andreas Salzburger <Andreas.Salzburger@cern.ch>
	   fixing possible memory leak
           renamed Changelog into ChangeLog
	   tagged 01-01-01

2004-11-22 Edward Moyse <edward.moyse@cern.ch>
	   now has "correct" structure with interfaces
	   tagged 01-01-00

2004-03-11 Andreas Salzburger <Andreas.Salzburger@cern.ch>
           adopting to new component library DriftFunctionTool
	   tag TRT_DriftCircleTool-01-00-15

2004-08-11 Andreas Salzburger <Andreas.Salzburger@cern.ch>
           adopting to new component library DriftFunctionTool
	   tag TRT_DriftCircleTool-01-00-14

2004-08-11 Igor gavrilenko <Igor.Gavrilenko@cern.ch>
           transition from Trk to InDet
	   tag TRT_DriftCircleTool-01-00-13

2004-08-11  Andreas Salzburger <Andreas.Salzburger@cern.ch>
           added driftTime to the creation of DriftCircles
	   tag TRT_DriftCircleTool-01-00-12

2004-07-18  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
           corrected requirements file
	   tag TRT_DriftCircleTool-01-00-10

2004-06-30  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
           save pointer to TRT_BaseElement to TRT_DriftCircle 
	   tag TRT_DriftCircleTool-01-00-09

2004-06-15  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
           remove all typedefs
	   tag TRT_DriftCircleTool-01-00-08 

2004-05-14  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
           corrected requirements file
	   tag TRT_DriftCircleTool-01-00-07 

2004-04-07  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
           add Atlas policy for requirements
	   tag TRT_DriftCircleTool-01-00-06 for release 8.1.0

2004-03-16  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
           drop the dependency on InDetRecInput
	   tag TRT_DriftCircleTool-01-00-05 for release 8.0.0

2004-03-12  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
	   tag TRT_DriftCircleTool-01-00-04 for release 8.0.0

2004-03-11  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
	  add parameter Mode for  TRT_DriftCircle production
	  tag TRT_DriftCircleTool-01-00-03 for release 8.0.0

2004-03-08  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
	  correction for new Trk:TRT_DriftCircles constructor
	  tag TRT_DriftCircleTool-01-00-02 for release 8.0.0

2004-03-06  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
	  correction for new Trk:TRT_DriftCircles constructor
	  tag TRT_DriftCircleTool-01-00-01 for release 8.0.0

2004-03-05  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
	  produce transition to new Trk:TRT_DriftCircles
	  tag TRT_DriftCircleTool-01-00-00 for release 8.0.0

2004-03-03  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
	  produce transition to new Trk:TRT_DriftCircles
	  tag TRT_DriftCirlceTool-00-01-00 for release 8.0.0

2003-05-15  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
	  add components directory
	  tag TRT_DriftCirlceTool-00-00-03 for release 6.0.3

2003-04-12  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
	  change tag of InDetRecInput from 02 to 03.
	  tag TRT_DriftCirlceTool-00-00-02 for release 6.0.3

2003-02-20  Igor gavrilenko <Igor.Gavrilenko@cern.ch>
	  tag TRT_DriftCirlceTool-00-00-01 for release 6.0.0
