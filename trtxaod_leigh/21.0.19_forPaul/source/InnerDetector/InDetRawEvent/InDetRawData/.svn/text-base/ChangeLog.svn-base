2016-04-14 Oleg Bulekov Oleg.Bulekov@cern.ch
         * Tagging InDetRawData-01-09-12           
         * fix bug in calculation of ToT

2016-04-13 Oleg Bulekov Oleg.Bulekov@cern.ch
         * Tagging InDetRawData-01-09-11           
         * fix bug in calculation of TE

2016-04-11 Oleg Bulekov Oleg.Bulekov@cern.ch
         * Tagging InDetRawData-01-09-10           
         * modify trailingEdge(), timeOverThreshold() and
         * driftTimeBin() in the TRT_LoLumRawData.cxx

2016-03-19  scott snyder  <snyder@bnl.gov>

	* Tagging InDetRawData-01-09-09.
	* Add InDetRawData::merge, for use from InDetOverlay.

2016-03-20  peter hansen  <phansen@nbi.dk>

	* Tagging InDetRawData-01-09-08.
        *  Equalized driftTimeBin() and timeOverThreshold()
	*  of InDetRawData::TRT_LoLumRawData to the versions in
	*  InDetRecData/TRT_DriftCircle

2015-12-27  scott snyder  <snyder@bnl.gov>

	* Tagging InDetRawData-01-09-07.
	* Comply with ATLAS naming conventions.
	* Tagging InDetRawData-01-09-06.
	* InDetRawData/SCT3_RawData.h, src/SCT3_RawData.cxx: errorHit
	should be passed to ctor as a const pointer.  Add a ctor taking
	the vector via move.

2014-11-21 Shaun Roe
	* fix coverity warnings
	* tag as InDetRawData-01-09-05

2014-04-10  scott snyder  <snyder@bnl.gov>

	* Tagging InDetRawData-01-09-04.
	* Use CONTAINER_IS_IDENTCONT to tag containers that need to use
	IdentContIndexingPolicy.
	* Remove reference to DataModel.

2013-08-14 Niels van Eldik
	* add ostream/MsgStream operators 
	* Tag as InDetRawData-01-09-03

2012-04-18 Rolf Seuster <seuster AT cern.ch>
	* remove non-existing transient data members from selection.xml (needed for lcgcmt_62b)
	* Tag as InDetRawData-01-09-02

2011-04-15 Esben Klinkby, Jahred Adelman
	* After request from Markus, it was decided to move the
	* the functionality of InDetRawData-01-09-00 to TRT_DriftCircleTool
	* (TRT_DriftCircleTool-01-05-13-01).
	* Reverting back to InDetRawData-01-08-05
	* Tag as InDetRawData-01-09-01

2011-04-14 Esben Klinkby, Jahred Adelman
	* Very very important change: Redefining drifttimebin (leading edge) to 
	* be the first 0 to 1 transition rather than the first occurance of 1
	* I.e. valid driftcircle regardless that signal 
	* from previous bunchcrossing migrates into readout window.
	* Very important for 50ns running, where this situation
	* is frequent
	* Tag as InDetRawData-01-09-00

2009-07-22 Paul Keener <Paul.Keener@cern.ch>
	* src/TRT_LoLumRawData.cxx: Change TRT LE, TE, ToT definitions
	* Tag as InDetRawData-01-08-05

2009-07-15 Paul Keener <Paul.Keener@cern.ch>
	* src/TRT_LoLumRawData.cxx: Change TRT LE definition to be first high bit
	* Tag as InDetRawData-01-08-04

2009-01-14  RD Schaffer  <R.D.Schaffer@cern.ch>
	* InDetRawData/selection.xml: fixing dicts
	* tagging InDetRawData-01-08-03


2009-01.09 elsing
	* fix requirements
	* Tag as InDetRawData-01-08-02
	
2008-4-04 Nick Barlow <nbarlow@mail.cern.ch>
	* Modifications to SCT3_RawData class, replace array of 20 floats 
	with a vector to represent errors in individual strips in a
	cluster, remove accessor functions for various types of error that 
	are in the Header rather than the hit (better to track these using
	SCT_ByteStreamErrorsSvc).
	* InDetRawData-01-08-00

2008-03-27 Andreas Wildauer
	* removed TimeRobidCollection.h/.cxx
	* InDetRawData-01-07-00

2008-03-18 Markus Elsing
	* migration to new IDC
	* InDetRawData-01-06-01

2008-01-05  scott snyder  <snyder@bnl.gov>
	* Tagging InDetRawData-01-05-04.
	* InDetRawData/PixelRDO_Container.h: Make sure we have the CLIDs
	for the InDetRawDataCollection's.
	* InDetRawData/SCT_RDO_Container.h: Likewise.

2007-10-09 David Quarrie <David.Quarrie@cern.ch>
	* InDetRawData/InDetRawDataContainer.h: Provide empty implementation of default constructor for asNeeded usage
 	* Tagged as InDetRawData-01-05-03

2007-07-24 Nir Amram <Nir.Amram@cern.ch>
 	* Changed used package AtlasSEAL to AtlasReflex
 	* Tagged as InDetRawData-01-05-02

2007-07-10 Paul Keener <Paul.Keener@cern.ch>
	* Update TRT_LoLumRawData:
	*	Add overloaded highLevel(BX)
	*	Changed both highLevel() methods, firstBinHigh() 
	*	   and lastBinHigh() to inline
	* tagged as InDetRawData-01-05-01

2007-06-28 Paul Keener <Paul.Keener@cern.ch>
	* Update TRT_LoLumRawData:
	*	Correct driftTimeBin() to look for 0->1 transition
	*	Correct timeOverThresh() to look for LE and TE transitions
	*	Add trailingEdge() method
	*	Add firstBinHigh() method
	*	Add lastBinHigh() method
	* tagged as InDetRawData-01-05-00
	
2007-05-28 Kondo Gnanvo <Kondo.Gnanvo@cern.ch>
	* SCT_TB04_RawData.h: New onlineId calculation
	* tagged as InDetRawData-01-04-11

2007-05-24 Paul Keener <Paul.Keener@cern.ch>
	* Fixed bug in TRT RDO calculation of timeOverThreshold
	* tagged as InDetRawData-01-04-10
	
2007-05-22 Florian hirsch <hirsch@cern.ch>
	* added CLASS_DEF for Pixel1 data
	* tagged as InDetRawData-01-04-09

2007-03-13 Daniel Dobos <Daniel.Dobos@cern.ch>
        * changed get methods for Pixel to const
        * Tag InDetRawData-01-04-08

2007-03-06 Attilio Andreazza <Attilio.Andreazza@cern.ch>
        * added L1A to Pixel RDO
        * Tag InDetRawData-01-04-07

2007-02-27 Davide Costanzo <Davide.Costanzo@cern.ch>
	* InDetRawData/selection.xml: remove DataObject
	* Tag InDetRawData-01-04-06

2007-02-22 Kondo Gnanvo <Kondo.Gnanvo@cern.ch>
	* SCT_TB04_RawData.h: New onlineId calculation
	* Tagged tag InDetRawData-01-04-05
	
2007-01-12 Davide Costanzo <davide.costanzo@cern.ch>
	* remove symlinks as for the TRT
        * tag InDetRawData-01-04-04

2007-01-10 Davide Costanzo <davide.costanzo@cern.ch>
	* InDetRawData-01-04-03: Change the definition of the collection 
          of SCT and Pixel to contain abstract classes and not concrete 
	  classes, same as the TRT. The new T/P converters (collected at the same
	  time) take care of filling them

2007-01-04  Andrei Gaponenko <agaponenko@lbl.gov>

	* InDetRawData-01-04-02: Do not disable copy constructor and
	assignment operator for raw datum classes.  The compiler generated
	ones are just fine here, and they are needed by the overlaying code.

2006-12-19  Paul Keener <Paul.Keener@cern.ch>
	* Fix case where we have bad trigger phase data
	* Tagged as InDetRawData-01-04-01
	
2006-09-26  Andrei Gaponenko <agaponenko@lbl.gov>

	* InDetRawData-01-04-00: The first round of clean up: make the
	definition of TRT_RDO_Collection fit its name (it used
	TRT_LoLumRawData).

2006-05-16  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* adding TimeRobidCollection from Kondo
	* Tagged InDetRawData-01-03-17
	
2006-05-16 Kondo Gnanvo <Kondo.Gnanvo@cern.ch>

	* Add TimeRobidCollection class for collection of the 
	Time information for the SCT and TRT mapped with the ROB ID
	* Tagged to InDetRawData-01-03-16
	
2006-04-17  RD Schaffer  <R.D.Schaffer@cern.ch>

	* tagging InDetRawData-01-03-15
	* InDetRawData/InDetRawDataContainer.icc (InDetRawDataContainer):
	fix for gcc344

2006-03-08  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* removing semicolom
	
2006-08-14  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* adding comments in SCT3_RawData.h

2005-08-04  Daniel Dobos <Daniel.Dobos@cern.ch>

        * adding LVL1ID to RDO

2005-06-10  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* adding SCT3_RawData
	
2005-04-22  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* add doc/mainpage.h
	
2005-03-24  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* InDetRawData.cc: initialize m_word in constructor
	
2004-10-10  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* TRT_TB04_RawData: fixed from Yura and Andrei 
	(timeoverThr bin can be >16)
	
2004-10-02  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* SCT_TB04_RawData: add more errors
	
2004-10-09  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* fix TRT_TB04_RawData: use time bins up to 24 rather than just 17
	
2004-07-09  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* add Pixel_TB04_RawData
	
2004-08-09  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* add TRT_TB04_RawData
	
2004-06-07  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* bug fix on SCT_TB04_RawData
	
2004-05-05  Maria Jose Costa <Maria.Jose.Costa@cern.ch>

	* add SCT_TB04_RawData

2004-02-26  Veronique Boisvert  <boisvert@lxplus073.cern.ch>

	* InDetRawData/TRT_RDO_Collection.h, Pixel, SCT: added const
	DataVector for Pool writing

2003-12-11  Veronique Boisvert  <boisvert@lxplus030.cern.ch>

	* added TB03 SCT classes (01-02-00)

2003-11-25  RD Schaffer  <R.D.Schaffer@cern.ch>

	* tagging InDetRawData-01-01-00

	* InDetRawData/InDetRawData.h: moved constructor to public

2003-11-24  RD Schaffer  <R.D.Schaffer@cern.ch>

	* tagging InDetRawData-01-00-08

	* InDetRawData/SCT_RDORawData.h: removed type

	* tagging InDetRawData-01-00-07

	* InDetRawData/InDetRawDataCLASS_DEF.h: modifications for writing
	to pool

2003-11-13  Veronique Boisvert  <boisvert@lxplus067.cern.ch>

	* InDetRawData/InDetRawDataCollection.icc: forgot to remove the type()

2003-11-11  Veronique Boisvert  <boisvert@lxplus031.cern.ch>

	* removed dependencies on Plottable (01-00-05)

2003-08-17  RD Schaffer  <R.D.Schaffer@cern.ch>

	* tagging InDetRawData-01-00-04

2003-08-15  RD Schaffer  <R.D.Schaffer@cern.ch>

	* InDetRawData/InDetRawDataCLASS_DEF.h: split off the three
	containers into separate headers files in order to be able to
	generate converters for them in InDetEventAthenaPool
	NO NEW TAG.
	
	
2003-03-15  Veronique Boisvert  <boisvert@lxplus058.cern.ch>

	* Modified Container and CLASS_DEF for the new way of CLID

2003-02-21  David Candlin  <candlin@lxplus057.cern.ch>

	* cmt/requirements: Add major versions in requirements

2003-02-16  Veronique Boisvert  <boisvert@lxplus045.cern.ch>

	* Changed constructor of Collection for IdentifierHash, added 
	identifyHash and setIdentifier methods... instantiators have to 
	be modified

2003-02-14  Veronique Boisvert  <boisvert@lxplus057.cern.ch>

	* changed containers CLID, cleaned up requirements file

2003-02-13  Veronique Boisvert  <boisvert@lxplus059.cern.ch>

	* added driftTimeBin to TRT classes

2003-02-12  Veronique Boisvert  <boisvert@lxplus059.cern.ch>

	* InDetRawDataCollection.h: changed StoreGate for DataModel 

	* TRT classes: added member function to TRT classes

2002-11-12  Veronique Boisvert  <boisvert@lxplus049.cern.ch>

	* Removed getStrip, added arguments to Container template for
	CLID, added CLID methods, put in typedef in CLASS_DEF to be used
	for all users. Removed newObject, users should use constructor.

2002-11-01  Veronique Boisvert  <boisvert@lxplus040.cern.ch>

	* SCT_RDORawData.h: added getStrip() member function, implements
	in SCT1_RawData.h following Calvet's mask. 

2002-10-29  Veronique Boisvert  <boisvert@lxplus063.cern.ch>

	* Replaced the address of the IDs with a copy of the IDs for
	Collection and RDOs.

2002-10-25  Veronique Boisvert  <boisvert@lxplus008.cern.ch>

	* Added the TRT. Using new InDetDetElemHash function. Removed
	InDetId.h and replaced by Identifier.h. Made word const. Made
	rdoid an &. Added getWord member function to InDetRawData.
	

2002-10-07  Veronique Boisvert  <boisvert@lxplus039.cern.ch>

	* Removed Strategy design. Now have Templates for
	InDetRawDataContainer and Collection. Also have an InDetRawData
	from which Pixel, SCT, TRT inherit from (word is
	protected). Pixel1, etc. inherit from PixelRDORawData (added the
	RDO part because of conflict with SiTracker PixelRawData. 

2002-09-15  Veronique Boisvert  <boisvert@lxplus069.cern.ch>

	* Have only InDetRawData structure along with a strategy design
	(InDetDecoder, List) for the different get functions. Added a
	decoder vector in container to store which get functions are
	available. Added name in the get function classes.
	

2002-09-02  Veronique Boisvert  <boisvert@lxplus033.cern.ch>

	* Made separate structure (Container, Collection, RawData,...) for
	each subdetector

2002-08-28  David Candlin  <candlin@lxplus063.cern.ch>

	* New package
	

