#

Reco_tf.py  \
--inputBSFile='/afs/cern.ch/user/l/lschaef/work/public/data16_13TeV/data16_13TeV.00304337.physics_Main.merge.DRAW_ZMUMU.f716_m1616._0100.1'   \
    --ignoreErrors 'True'  \
    --maxEvents '10' \
    --AMI 'f716' \
    --postExec 'all:conddb.addOverride("/TRT/Calib/ToT/ToTValue","TRTCalibToTDataValue-000-03") ; conddb.addOverride("/TRT/Calib/ToT/ToTVectors","TRTCalibToTDataVectors-000-03"); conddb.addOverride("/TRT/Cond/StatusPermanent", "TRTCondStatusPermanent-RUN2-BLK-UPD4-01-00") ' \
    --outputESDFile=ESD.pool.root 
