#

Reco_tf.py  \
--inputBSFile='/afs/cern.ch/user/l/lschaef/work/public/data16_13TeV/data16_13TeV.00304337.physics_Main.merge.DRAW_ZMUMU.f716_m1616._0100.1'   \
    --ignoreErrors 'True'  \
    --maxEvents '10' \
    --AMI 'f716' \
    --DataRunNumber 323615 \
    --postExec 'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup("/TRT/Cond/StatusHT","<forceRunNumber>323615</forceRunNumber>"); conddb.addMarkup("/TRT/Cond/StatusPermanent","<forceRunNumber>323615</forceRunNumber>"); conddb.addOverride("/TRT/Cond/StatusHT", "TrtStrawStatusHT-RUN2-UPD4-02-00"); conddb.addOverride("/TRT/Cond/StatusPermanent","TRTCondStatusPermanent-RUN2-BLK-UPD4-02-00") ' \
    --preExec  'all:rec.doTrigger.set_Value_and_Lock(False); rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(False);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
    --outputESDFile=InDetDxAOD.pool.root 

#conddb.addOverride("/TRT/Cond/StatusPermanent", "TRTCondStatusPermanent-RUN2-BLK-UPD4-01-01"); 
# 'all:from IOVDbSvc.CondDB import conddb ; conddb.blockFolder("/TRT/Cond/StatusPermanent"); conddb.addFolder("","<dbConnection>sqlite://;schema=mycool.db;dbname=CONDBR2</dbConnection>/TRT/Cond/StatusPermanent<tag>TRTCondStatusPermanent-RUN2-BLK-UPD4-01-01</tag>",force=True) ' \

