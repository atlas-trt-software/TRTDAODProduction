cp ../source/patch_for_OccLayers/*TRT_LocalOccupancy.h ../source/InnerDetector/InDetRecTools/TRT_ElectronPidTools/
cp ../source/patch_for_OccLayers/TRT_LocalOccupancy.cxx ../source/InnerDetector/InDetRecTools/src/
cp ../source/patch_for_OccLayers/TRTOccupancyInclude.cxx ../source/InnerDetector/InDetCalibAlgs/TRT_CalibAlgs/src/

pathena --trf  " Reco_tf.py  \
    --inputBSFile='%IN'   \
    --ignoreErrors 'True'  \
    --skipEvents='%SKIPEVENTS' \
    --maxEvents '500' \
    --AMI 'f716' \
    --postExec 'all:conddb.addOverride(\"/TRT/Calib/ToT/ToTValue\",\"TRTCalibToTDataValue-000-03\") ; conddb.addOverride(\"/TRT/Calib/ToT/ToTVectors\",\"TRTCalibToTDataVectors-000-03\") ' \
    --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
'ESDtoDPD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doPrintConfigurables.set_Value_and_Lock(True); InDetFlags.doPRDFormation.set_Value_and_Lock(True)' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root  "  \
    --official \
    --nEventsPerJob=500 \
    --tmpDir=/tmp/lschaef \
    --inDS=data16_13TeV.00304337.physics_Main.merge.DRAW_ZMUMU.f716_m1616 \
    --outDS=group.det-indet.00304337.physics_Main.merge.TRTxAOD_ZMUMU.f716_m1616_trt094-01

pathena --trf  " Reco_tf.py  \
    --inputBSFile='%IN'   \
    --ignoreErrors 'True'  \
    --skipEvents='%SKIPEVENTS' \
    --maxEvents '500' \
    --AMI 'f701' \
    --postExec 'all:conddb.addOverride(\"/TRT/Calib/ToT/ToTValue\",\"TRTCalibToTDataValue-000-03\") ; conddb.addOverride(\"/TRT/Calib/ToT/ToTVectors\",\"TRTCalibToTDataVectors-000-03\") ' \
    --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
'ESDtoDPD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doPrintConfigurables.set_Value_and_Lock(True); InDetFlags.doPRDFormation.set_Value_and_Lock(True)' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root  "  \
    --official \
    --nEventsPerJob=500 \
    --tmpDir=/tmp/lschaef \
    --inDS=data16_13TeV.00299390.physics_MinBias.daq.RAW \
    --outDS=group.det-indet.00299390.physics_MinBias.daq.TRTxAOD.f701_trt094-01


svn revert InnerDetector/InDetRecTools/TRT_ElectronPidTools/*/*
svn revert InnerDetector/InDetCalibAlgs/TRT_CalibAlgs/src/*