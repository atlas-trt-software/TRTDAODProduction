
pathena --trf  " Reco_tf.py  \
    --inputBSFile='%IN'   \
    --ignoreErrors 'True'  \
    --skipEvents='%SKIPEVENTS' \
    --maxEvents '500' \
    --AMI 'f716' \
    --postExec 'all:conddb.addOverride(\"/TRT/Calib/ToT/ToTValue\",\"TRTCalibToTDataValue-000-03\") ; conddb.addOverride(\"/TRT/Calib/ToT/ToTVectors\",\"TRTCalibToTDataVectors-000-03\"); conddb.blockFolder(\"/TRT/Calib/RT\"); conddb.addFolderWithTag(\"TRT_OFL\",\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-ArLT150-scenario5_00-01\",force=True,forceMC=True); conddb.blockFolder(\"/TRT/Calib/T0\"); conddb.addFolderWithTag(\"TRT_OFL\",\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-ArLT150-scenario5_00-01\",force=True,forceMC=True) ' \
    --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False);InDetDxAODFlags.ThinTrackSelection.set_Value_and_Lock(\"InDetTrackParticles.pt > 0.1*GeV && abs(InDetTrackParticles.eta) < 2.0\") ' \
'ESDtoDPD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doPrintConfigurables.set_Value_and_Lock(True); InDetFlags.doPRDFormation.set_Value_and_Lock(True)' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root  "  \
    --nEventsPerJob=500 \
    --tmpDir=/tmp/lschaef \
    --inDS=data16_13TeV.00304337.physics_Main.merge.DRAW_ZMUMU.f716_m1616 \
    --outDS=user.lschaef.00304337.physics_Main.merge.TRTxAOD_ZMUMU.f716_m1616_trt094_withMCCalib
