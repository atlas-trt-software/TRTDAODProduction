cp ../source/ConfiguredInDetPreProcessingTRT_NoToTCorrections.py ../source/InnerDetector/InDetExample/InDetRecExample/share/ConfiguredInDetPreProcessingTRT.py

pathena --trf  " Reco_tf.py  \
    --inputBSFile='%IN'   \
    --ignoreErrors 'True'  \
    --skipEvents='%SKIPEVENTS' \
    --maxEvents '500' \
    --AMI 'f716' \
    --postExec 'all:conddb.addOverride(\"/TRT/Calib/ToT/ToTValue\",\"TRTCalibToTDataValue-000-03\") ; conddb.addOverride(\"/TRT/Calib/ToT/ToTVectors\",\"TRTCalibToTDataVectors-000-03\") ' \
    --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False)'\
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root  "  \
    --official \
    --nEventsPerJob=500 \
    --tmpDir=/tmp/lschaef \
    --inDS=data16_13TeV.00304337.physics_Main.merge.DRAW_ZMUMU.f716_m1616 \
    --outDS=group.det-indet.00304337.physics_Main.merge.TRTxAOD_ZMUMU.f716_m1616_trt095-00

cp ../source/ConfiguredInDetPreProcessingTRT.py ../source/InnerDetector/InDetExample/InDetRecExample/share/ConfiguredInDetPreProcessingTRT.py
