#
# === original submission
#
# pathena --trf \
#     "Reco_tf.py \
# --inputRDOFile=%IN \
# --outputAODFile=%OUT.AOD.Default.pool.root  \
# --autoConfiguration='everything' \
# --preExec \
# 'rec.doTrigger=False; rec.doTau=False ; from RecExConfig.RecAlgsFlags import recAlgs; recAlgs.doMuonSpShower=False ; rec.doBTagging=False ; recAlgs.doEFlow=False ; recAlgs.doEFlowJet=False ; recAlgs.doMissingET=False ; recAlgs.doMissingETSig=False ; from JetRec.JetRecFlags import jetFlags ; jetFlags.Enabled=False ; from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.egammaTrackSlimmer=False; from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool ' \
# --postExec \
# 'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_mc16_newgeom_noRtOrT0.txt\" ' \
# 'RAWtoESD:ToolSvc.InDetTRT_StandaloneScoringTool.minTRTPrecisionFraction=0.0' " \
# --inDS=mc16_13TeV.423001.ParticleGun_single_photon_egammaET.recon.RDO.e3566_s2955_r8726 \
# --official \
# --outDS=group.det-indet.423001.ParticleGun_single_photon_egammaET.recon.AOD.e3566_s2955_r8726_mc16Errors \
# --extFile=caliboutput_mc16_newgeom_noRtOrT0.txt \
# --nFilesPerJob=1

# pathena --trf \
#     "Reco_tf.py \
# --inputRDOFile=%IN \
# --outputAODFile=%OUT.AOD.Default.pool.root  \
# --autoConfiguration='everything' \
# --preExec \
# 'rec.doTrigger=False; rec.doTau=False ; from RecExConfig.RecAlgsFlags import recAlgs; recAlgs.doMuonSpShower=False ; rec.doBTagging=False ; recAlgs.doEFlow=False ; recAlgs.doEFlowJet=False ; recAlgs.doMissingET=False ; recAlgs.doMissingETSig=False ; from JetRec.JetRecFlags import jetFlags ; jetFlags.Enabled=False ; from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.egammaTrackSlimmer=False; from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool ' \
# --postExec \
# 'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_mc16_newgeom_EG_add_1_5factor_noRtOrT0.txt\" ' \
# 'RAWtoESD:ToolSvc.InDetTRT_StandaloneScoringTool.minTRTPrecisionFraction=0.0' " \
# --inDS=mc16_13TeV.423001.ParticleGun_single_photon_egammaET.recon.RDO.e3566_s2955_r8726 \
# --official \
# --outDS=group.det-indet.423001.ParticleGun_single_photon_egammaET.recon.AOD.e3566_s2955_r8726_mc16Errors1.5 \
# --extFile=caliboutput_mc16_newgeom_EG_add_1_5factor_noRtOrT0.txt \
# --nFilesPerJob=1



#
# === PHF cut = 0
#
pathena --trf \
    "Reco_tf.py \
--inputRDOFile=%IN \
--outputAODFile=%OUT.AOD.Default.pool.root  \
--autoConfiguration='everything' \
--preExec \
'rec.doTrigger=False; rec.doTau=False ; from RecExConfig.RecAlgsFlags import recAlgs; recAlgs.doMuonSpShower=False ; rec.doBTagging=False ; recAlgs.doEFlow=False ; recAlgs.doEFlowJet=False ; recAlgs.doMissingET=False ; recAlgs.doMissingETSig=False ; from JetRec.JetRecFlags import jetFlags ; jetFlags.Enabled=False ; from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.egammaTrackSlimmer=False; from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool ' \
--postExec \
'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_mc16_newgeom_noRtOrT0.txt\" ' \
'RAWtoESD:from AthenaCommon.AppMgr import ToolSvc; ToolSvc.InDetExtenScoringTool.minTRTPrecisionFraction=0.0; ToolSvc.InDetExtenScoringToolPixelPrdAssociation.minTRTPrecisionFraction=0.0; ToolSvc.InDetTRT_StandaloneScoringTool.minTRTPrecisionFraction=0.0' " \
--inDS=mc16_13TeV.423001.ParticleGun_single_photon_egammaET.recon.RDO.e3566_s2955_r8726 \
--official \
--outDS=group.det-indet.423001.ParticleGun_single_photon_egammaET.recon.AOD.e3566_s2955_r8726_mc16Errors_PHF0 \
--extFile=caliboutput_mc16_newgeom_noRtOrT0.txt \
--nFilesPerJob=1

pathena --trf \
    "Reco_tf.py \
--inputRDOFile=%IN \
--outputAODFile=%OUT.AOD.Default.pool.root  \
--autoConfiguration='everything' \
--preExec \
'rec.doTrigger=False; rec.doTau=False ; from RecExConfig.RecAlgsFlags import recAlgs; recAlgs.doMuonSpShower=False ; rec.doBTagging=False ; recAlgs.doEFlow=False ; recAlgs.doEFlowJet=False ; recAlgs.doMissingET=False ; recAlgs.doMissingETSig=False ; from JetRec.JetRecFlags import jetFlags ; jetFlags.Enabled=False ; from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.egammaTrackSlimmer=False; from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool ' \
--postExec \
'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_mc16_newgeom_EG_add_1_5factor_noRtOrT0.txt\" ' \
'RAWtoESD:from AthenaCommon.AppMgr import ToolSvc; ToolSvc.InDetExtenScoringTool.minTRTPrecisionFraction=0.0; ToolSvc.InDetExtenScoringToolPixelPrdAssociation.minTRTPrecisionFraction=0.0; ToolSvc.InDetTRT_StandaloneScoringTool.minTRTPrecisionFraction=0.0' " \
--inDS=mc16_13TeV.423001.ParticleGun_single_photon_egammaET.recon.RDO.e3566_s2955_r8726 \
--official \
--outDS=group.det-indet.423001.ParticleGun_single_photon_egammaET.recon.AOD.e3566_s2955_r8726_mc16Errors1.5_PHF0 \
--extFile=caliboutput_mc16_newgeom_EG_add_1_5factor_noRtOrT0.txt \
--nFilesPerJob=1



