# Reco_tf.py \
# --inputRDOFile='/afs/cern.ch/user/l/lschaef/work/public/mc15_13TeV/RDO.08236526._000316.pool.root.1'   \
# --ignoreErrors True \
# --maxEvents '30' \
# --conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
# --geometryVersion ATLAS-R2-2015-03-01-00 \
# --ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
# --postInclude "RecJobTransforms/UseFrontier.py" \
# --autoConfiguration "everything" \
# --postExec \
# 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];from IOVDbSvc.CondDB import conddb; conddb.addMarkup("/TRT/Cond/StatusHT","<forceRunNumber>286990</forceRunNumber>");conddb.addOverride("/TRT/Cond/StatusHT","TrtStrawStatusHT-MC-run2-scenario5_00-00"); conddb.addOverride("/TRT/Calib/RT","TrtCalibRt-13TeV_MC-scenario5_00-00" ); conddb.addOverride("/TRT/Calib/T0","TrtCalibT0-13TeV_MC-scenario5_00-00"); conddb.addOverride("/TRT/Calib/ToT/ToTValue","TRTCalibToTToTValue-000-03") ; conddb.addOverride("/TRT/Calib/ToT/ToTVectors","TRTCalibToTToTVectors-000-03") ' \
# --preExec \
# 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(True); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# 'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
# 'ESDtoDPD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doPrintConfigurables.set_Value_and_Lock(True); InDetFlags.doPRDFormation.set_Value_and_Lock(True)' \
#     --outputDAOD_IDTRKVALIDFile=InDetDxAOD_yesdriftTimeCorrections.pool.root 

# cp ../source/ConfiguredInDetPreProcessingTRT_NodriftTimeCorrections.py ../source/InnerDetector/InDetExample/InDetRecExample/share/ConfiguredInDetPreProcessingTRT.py

# Reco_tf.py \
# --inputRDOFile='/afs/cern.ch/user/l/lschaef/work/public/mc15_13TeV/RDO.08236526._000316.pool.root.1'   \
# --ignoreErrors True \
# --maxEvents '30' \
# --conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
# --geometryVersion ATLAS-R2-2015-03-01-00 \
# --ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
# --postInclude "RecJobTransforms/UseFrontier.py" \
# --autoConfiguration "everything" \
# --postExec \
# 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];from IOVDbSvc.CondDB import conddb; conddb.addMarkup("/TRT/Cond/StatusHT","<forceRunNumber>286990</forceRunNumber>");conddb.addOverride("/TRT/Cond/StatusHT","TrtStrawStatusHT-MC-run2-scenario5_00-00"); conddb.addOverride("/TRT/Calib/RT","TrtCalibRt-13TeV_MC-scenario5_00-00" ); conddb.addOverride("/TRT/Calib/T0","TrtCalibT0-13TeV_MC-scenario5_00-00"); conddb.addOverride("/TRT/Calib/ToT/ToTValue","TRTCalibToTToTValue-000-03") ; conddb.addOverride("/TRT/Calib/ToT/ToTVectors","TRTCalibToTToTVectors-000-03") ' \
# --preExec \
# 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(True); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# 'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
# 'ESDtoDPD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doPrintConfigurables.set_Value_and_Lock(True); InDetFlags.doPRDFormation.set_Value_and_Lock(True)' \
#     --outputDAOD_IDTRKVALIDFile=InDetDxAOD_nodriftTimeCorrections.pool.root 

cp ../source/ConfiguredInDetPreProcessingTRT_NoToTCorrections.py ../source/InnerDetector/InDetExample/InDetRecExample/share/ConfiguredInDetPreProcessingTRT.py

Reco_tf.py \
--inputRDOFile='/afs/cern.ch/user/l/lschaef/work/public/mc15_13TeV/RDO.08236526._000316.pool.root.1'   \
--ignoreErrors True \
--maxEvents '30' \
--conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
--geometryVersion ATLAS-R2-2015-03-01-00 \
--ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
--postInclude "RecJobTransforms/UseFrontier.py" \
--autoConfiguration "everything" \
--postExec \
'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];from IOVDbSvc.CondDB import conddb; conddb.addMarkup("/TRT/Cond/StatusHT","<forceRunNumber>286990</forceRunNumber>");conddb.addOverride("/TRT/Cond/StatusHT","TrtStrawStatusHT-MC-run2-scenario5_00-00"); conddb.addOverride("/TRT/Calib/RT","TrtCalibRt-13TeV_MC-scenario5_00-00" ); conddb.addOverride("/TRT/Calib/T0","TrtCalibT0-13TeV_MC-scenario5_00-00"); conddb.addOverride("/TRT/Calib/ToT/ToTValue","TRTCalibToTToTValue-000-03") ; conddb.addOverride("/TRT/Calib/ToT/ToTVectors","TRTCalibToTToTVectors-000-03") ' \
--preExec \
'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(True); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
'ESDtoDPD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doPrintConfigurables.set_Value_and_Lock(True); InDetFlags.doPRDFormation.set_Value_and_Lock(True)' \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD_noToTCorrections.pool.root 

cp ../source/ConfiguredInDetPreProcessingTRT_NoHTCorrections.py ../source/InnerDetector/InDetExample/InDetRecExample/share/ConfiguredInDetPreProcessingTRT.py

Reco_tf.py \
--inputRDOFile='/afs/cern.ch/user/l/lschaef/work/public/mc15_13TeV/RDO.08236526._000316.pool.root.1'   \
--ignoreErrors True \
--maxEvents '30' \
--conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
--geometryVersion ATLAS-R2-2015-03-01-00 \
--ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
--postInclude "RecJobTransforms/UseFrontier.py" \
--autoConfiguration "everything" \
--postExec \
'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];from IOVDbSvc.CondDB import conddb; conddb.addMarkup("/TRT/Cond/StatusHT","<forceRunNumber>286990</forceRunNumber>");conddb.addOverride("/TRT/Cond/StatusHT","TrtStrawStatusHT-MC-run2-scenario5_00-00"); conddb.addOverride("/TRT/Calib/RT","TrtCalibRt-13TeV_MC-scenario5_00-00" ); conddb.addOverride("/TRT/Calib/T0","TrtCalibT0-13TeV_MC-scenario5_00-00"); conddb.addOverride("/TRT/Calib/ToT/ToTValue","TRTCalibToTToTValue-000-03") ; conddb.addOverride("/TRT/Calib/ToT/ToTVectors","TRTCalibToTToTVectors-000-03") ' \
--preExec \
'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(True); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
'ESDtoDPD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doPrintConfigurables.set_Value_and_Lock(True); InDetFlags.doPRDFormation.set_Value_and_Lock(True)' \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD_noHTCorrections.pool.root 

cp ../source/ConfiguredInDetPreProcessingTRT.py ../source/InnerDetector/InDetExample/InDetRecExample/share/ConfiguredInDetPreProcessingTRT.py
