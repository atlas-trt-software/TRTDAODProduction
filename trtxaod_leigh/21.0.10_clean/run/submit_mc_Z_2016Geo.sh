# # Zmumu
pathena --trf \
"Reco_tf.py \
    --inputHITSFile %IN \
--ignoreErrors True \
--maxEvents '100' \
--skipEvents='%SKIPEVENTS' \
--DataRunNumber 222525 \
--jobNumber 222525 \
--conditionsTag 'OFLCOND-RUN12-SDR-31' \
--geometryVersion ATLAS-R2-2015-03-01-00 \
--ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
--postInclude 'RecJobTransforms/UseFrontier.py' \
--autoConfiguration 'everything' \
--numberOfCavernBkg 0 \
--inputHighPtMinbiasHitsFile %HIMBIN \
--inputLowPtMinbiasHitsFile %LOMBIN \
--numberOfHighPtMinBias 0.12268057 \
--numberOfLowPtMinBias 39.8773194 \
--pileupFinalBunch 6 \
--postExec \
'all:CfgMgr.MessageSvc().setError+=[\"HepMcParticleLink\"];from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\");conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-Ar_emulation2_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-scenario5_00-00\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/ToT/ToTValue\",\"TRTCalibToTToTValue-000-03\") ; conddb.addOverride(\"/TRT/Calib/ToT/ToTVectors\",\"TRTCalibToTToTVectors-000-03\") ' \
'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools[\"MergeMcEventCollTool\"].DoSlimming=False;job.StandardPileUpToolsAlg.PileUpTools[\"MdtDigitizationTool\"].LastXing=150' \
--preExec \
'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
'HITtoRDO:ScaleTaskLength=0.02' \
'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
'ESDtoAOD:TriggerFlags.AODEDMSet=\"AODSLIM\" ' \
--preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_run222525_v1.py' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
    --official \
    --skipScout \
    --highMinDS=mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS.e3581_s2874 \
    --lowMinDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2874 \
    --nHighMin=1 --nLowMin=1 \
    --nFilesPerJob 1 \
    --nEventsPerJob 100 \
    --site=ANALY_MWT2_SL6,MWT2_UC_LOCALGROUPDISK \
    --inDS mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.HITS.e3601_s2874 \
    --outDS group.det-indet.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.recon.TRTxAOD_emAr2016.e3601_s2874_r7886_trt093-00


# # # Zee
# pathena --trf \
# "Reco_tf.py \
#     --inputHITSFile %IN \
# --ignoreErrors True \
# --maxEvents '100' \
# --skipEvents='%SKIPEVENTS' \
# --DataRunNumber 222525 \
# --jobNumber 222525 \
# --conditionsTag 'OFLCOND-RUN12-SDR-31' \
# --geometryVersion ATLAS-R2-2015-03-01-00 \
# --ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
# --postInclude 'RecJobTransforms/UseFrontier.py' \
# --autoConfiguration 'everything' \
# --numberOfCavernBkg 0 \
# --inputHighPtMinbiasHitsFile %HIMBIN \
# --inputLowPtMinbiasHitsFile %LOMBIN \
# --numberOfHighPtMinBias 0.12268057 \
# --numberOfLowPtMinBias 39.8773194 \
# --pileupFinalBunch 6 \
# --postExec \
# 'all:CfgMgr.MessageSvc().setError+=[\"HepMcParticleLink\"];from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\");conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-Ar_emulation2_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-scenario5_00-00\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/ToT/ToTValue\",\"TRTCalibToTToTValue-000-03\") ; conddb.addOverride(\"/TRT/Calib/ToT/ToTVectors\",\"TRTCalibToTToTVectors-000-03\") ' \
# 'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools[\"MergeMcEventCollTool\"].DoSlimming=False;job.StandardPileUpToolsAlg.PileUpTools[\"MdtDigitizationTool\"].LastXing=150' \
# --preExec \
# 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# 'HITtoRDO:ScaleTaskLength=0.02' \
# 'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
# 'ESDtoAOD:TriggerFlags.AODEDMSet=\"AODSLIM\" ' \
# --preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_run222525_v1.py' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --skipScout \
#     --highMinDS=mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS.e3581_s2874 \
#     --lowMinDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2874 \
#     --nHighMin=1 --nLowMin=1 \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 100 \
#     --site=ANALY_MWT2_SL6,MWT2_UC_LOCALGROUPDISK \
#      --inDS mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_s2874 \
#     --outDS group.det-indet.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.recon.TRTxAOD_emAr2016.e3601_s2874_r7886_trt093-00
