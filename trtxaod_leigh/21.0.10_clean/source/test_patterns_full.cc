
#include <iostream>

#include <stdio.h>
#include <stdlib.h>

unsigned int
read_pattern( int *done, int *blank );

void
write_pattern( unsigned int pattern );
void
write_pattern20( int pattern, bool =false );

unsigned int Vgate( unsigned int pattern );

unsigned int Scheme0( unsigned int pattern );
unsigned int Scheme0p( unsigned int pattern );
unsigned int Scheme1( unsigned int pattern );
unsigned int Scheme2( unsigned int pattern );
unsigned int Scheme3( unsigned int pattern );


int
main( int argc, char *argv[] )
{
  unsigned int pattern;
  int i;

  printf( "\n\n" );
  printf( "Read is the input straw hit pattern.\n" );
  printf( "Vgate indicates whether the Read hit passed the HW Validity Gate\n" );
  printf( "Scheme 1,2,3 are the three different hit transforms under consideration.\n" );
  printf( "Left is earlier in time; right is later in time.\n" );
  printf( " \"_\" marks the divisions between Bunch Crossings.\n" );
  printf( " \"+\" marks the division between HT bit and LT bits.\n" );
  printf( " \".\" is a visual marker between groups of four LT bits.\n\n\n" );

  /*
  for ( i=1; i<0x100000; i++ )
  {
      int pat2 = pat_mod_ar_ptk( i );
      write_pattern( pat2 );
  }

  return 0;
  */

  while ( pattern >= 0 )
  {
    int done=0;
    int blank=0;

    pattern = read_pattern( &done, &blank );
    if ( done )
      break;

    if ( blank )
    {
      printf( "\n\n" );
      continue;
    }

    printf( "Read      => " );
    write_pattern( pattern );

    printf( "Vgate     => " );
    if ( Vgate( pattern ) )
    {
      printf( "PASS\n" );

      unsigned int pat_S0 = Scheme0( pattern << 4 );
      printf( "Scheme 0  => " );
      write_pattern( pat_S0 >> 4 );

      unsigned int pat_S0p = Scheme0p( pattern << 4 );
      printf( "Scheme 0p => " );
      write_pattern( pat_S0p >> 4 );

      unsigned int pat_S1 = Scheme1( pattern << 4 );
      printf( "Scheme 1  => " );
      write_pattern( pat_S1 >> 4 );

      unsigned int pat_S2 = Scheme2( pattern << 4 );
      printf( "Scheme 2  => " );
      write_pattern( pat_S2 >> 4 );

      unsigned int pat_S3 = Scheme3( pattern << 4 );
      printf( "Scheme 3  => " );
      write_pattern( pat_S3 >> 4 );


      printf( "\n" );

    } else
      printf( "FAIL\n\n" );

  }

  return 0;
}


unsigned int
read_pattern( int *done, int *blank )
{
  char *line;
  char l_out[80];
  char *i_ptr;
  char *o_ptr;
  int i, line_len;
  size_t length = 79;

  line = (char *) malloc( length+1 );

  line_len = getline( &line, &length, stdin );
  //    printf( "%d\n", line_len );
  if ( 1 == line_len )
  {
    *blank = 1;
    return 0;
  }
  else if ( line_len <= 0 )
  {
    *done = 1;
    return 0;
  }

  //  printf( "->%s<-\n", line );

  i_ptr = line;
  o_ptr = l_out;

  for ( i=0; i<line_len+1; i++ )
  {
    //    printf( "%x %x %c\n", line, i_ptr, *i_ptr );

    if ( (*i_ptr == '0') || (*i_ptr == '1') || (*i_ptr == '\0') )
    {
      if ( *i_ptr == '\0' )
	;//printf( "Copying null\n" );
      else
	;//printf( "Copying %c\n", *i_ptr );

      *o_ptr = *i_ptr;
      o_ptr++;
    }

    i_ptr++;
  }

  free( line );

  //  printf( "->%s<-\n", l_out );

  if ( (o_ptr == l_out) || (*l_out == '\0' ) )
    return -1;
  else
  {

    unsigned int pattern = strtol( l_out, (char **) NULL, 2 );

    return pattern;
  }
}


void
write_pattern( unsigned int pattern )
{
  int i;
  int max;
  int first=1;

  max = 22;

  for ( i=max; i>=0; i-- )
  {
    if ( (i == 21) || (i == 12) || (i == 3) )
      printf( "+" );
    if ( (i == 17) || ( i == 8 ) )
      printf( "." );
    if ( (i == 13) || ( i == 4 ) )
      printf( "_" );

    printf( "%d", (pattern >> i) & 0x1 );
  }

  printf( "\n" );

  return;
}

void
write_pattern20( int pattern, bool full )
{
  int i;
  int max;
  int first=1;

  if ( full )
    max = 31;
  else
    max = 19;

  for ( i=max; i>=0; i-- )
  {
    if ( full )
    {
      if ( (i == 27) || (i == 19) || (i == 11) || (i==3) )
	printf( "." );
      if ( (i == 23) || (i == 15) || (i==7) )
	printf( "_" );
    } else {
      if ( (pattern >> 20) && first )
      {
	first = 0;
	printf( "1+" );
      }

      if ( (i == 15) || (i==7) )
	printf( "." );
      if ( (i == 11) || (i==3) )
	printf( "_" );
    }


    printf( "%d", (pattern >> i) & 0x1 );
  }

  printf( "\n" );

  return;
}

int
fLE( unsigned int word )
{
  unsigned int mask = 0x400000;   // Start with 2nd bit
  unsigned int m_word_LE = word;
 
  bool SawZero = false;
  int i;
  for(i=2;i<21;++i)
  {
    if      (  (m_word_LE & mask) && SawZero) break;
    else if ( !(m_word_LE & mask) ) SawZero = true;
    mask>>=1;
  }
  if(i==21) i=0;
  return i;
}

int
lTE( unsigned int word )
{
  unsigned int mask = 0x10;   // Start with 1st bit
  unsigned int m_word_TE = word;
 
  bool SawZero = false;
  int i;
  for(i=20;i>0;--i)
  {
    if      (  (m_word_TE & mask) && SawZero) break;
    else if ( !(m_word_TE & mask) ) SawZero = true;
    mask<<=1;
  }

  return i;
}

int
fTE( unsigned int word )
{
  unsigned int m_word_TE = word;
  int LE = fLE( word );

  if ( LE == 0 )
    return 0;

  unsigned int mask = (1 << (20+4-LE));   // Start with 2nd bit

  while ( mask && (m_word_TE & mask) )
  {
    LE++;
    mask>>=1;
  }

  if ( mask )
    return (LE-1);
  else
    return 0;
}


/******************************************
 *
 * HW Validity Gate: 000D.FEE0
 *
 *    0+0000.0011_0+1111.1111_0+1110.0000
 *
 ******************************************/

/*
 * Minimal Transform
 */
unsigned int
Scheme0( unsigned int word )
{
  unsigned int data;
  unsigned int out_data=0;

  //  printf( "%x: %x\n%x: %x\n", word, word & 0x7f, word, word & 0x3f );

  if ( (word & 0x7f) == 0x30 ) 
    word = word & ~0x3f;
  else if ( (word & 0x3f) == 0x10 )
    word = word & ~0x1f;


  data = (word & 0xff) | ((word >> 1) & 0xff00) | ((word >>2) & 0xff0000);

  if ( 0 == data )
    return 0;


  int LE = fLE( data );
  int TE = lTE( data );
  //  printf( "  fLE = %d\t lTE = %d\n", LE, TE );
  //  printf( "0x%08x\n0x00C00000\n\n", data );

  if ( data & 0x00C00000 )  // either of first to bits set
  {
    if ( (0 == LE) || (TE < LE) )
      return ((word & ~0x4000100) | 0x02000000 ); // Set first bit

    out_data = data | 0x00800000;

    unsigned int mask = 0x00400000;
    //    printf( "0x%08x\n0x%08x\n\n", out_data, mask );
    while ( out_data & mask )
    {
      out_data = out_data & ~mask;
      mask = mask >> 1;
      //      printf( "0x%08x\n0x%08x\n\n", out_data, mask );
    }

    data = ((out_data & 0xff0000) << 2) | 
      ((out_data & 0xff00) << 1) | (out_data & 0xff) | (word & 0x0020000);

    return data;
  }
  else
    return ( word & ~0x4000100 );
}


/*
 * Minimal Transform Prime
 */
unsigned int
Scheme0p( unsigned int word )
{
  unsigned int data;
  unsigned int out_data=0;

  //  printf( "%x: %x\n%x: %x\n", word, word & 0x7f, word, word & 0x3f );

  /*
   * Trailing bits manipulation
   *
  if ( (word & 0x7f) == 0x30 ) 
    word = word & ~0x3f;
  else if ( (word & 0x3f) == 0x10 )
    word = word & ~0x1f;
  */

  /*
  printf( "%x: %x %x %x %x %x => ", word,  (word >> 9) & 0x1,
	  (word >> 7) & 0x1,
	  (word >> 6) & 0x1,
	  (word >> 5) & 0x1,
	  (word >> 4) & 0x1
	  );
  */

  if ( ((word >> 7) & 0x1) == 0 )
    word = word & 0xfffffff00;
  else if ( ((word >> 6) & 0x1) == 0 )
    word = word & 0xfffffff80;
  else if ( ((word >> 5) & 0x1) == 0 )
    word = word & 0xfffffffC0;
  else if ( ((word >> 4) & 0x1) == 0 )
    word = word & 0xfffffffe0;

  //  printf( "%x\n", word );

  data = (word & 0xff) | ((word >> 1) & 0xff00) | ((word >>2) & 0xff0000);

  if ( 0 == data )
    return 0;


  int LE = fLE( data );
  int TE = lTE( data );
  //  printf( "  fLE = %d\t lTE = %d\n", LE, TE );
  //  printf( "0x%08x\n0x00C00000\n\n", data );

  if ( data & 0x00C00000 )  // either of first to bits set
  {
    if ( (0 == LE) || (LE > 16) )
      return ((word & ~0x4000100) | 0x02000000 ); // Set first bit

    out_data = data | 0x00800000;                 // Set first bit

    unsigned int mask = 0x00400000;
    //    printf( "0x%08x\n0x%08x\n\n", out_data, mask );
    while ( out_data & mask )
    {
      out_data = out_data & ~mask;
      mask = mask >> 1;
      //      printf( "0x%08x\n0x%08x\n\n", out_data, mask );
    }

    data = ((out_data & 0xff0000) << 2) | 
      ((out_data & 0xff00) << 1) | (out_data & 0xff) | (word & 0x0020000);

    return data;
  }
  else
    return ( word & ~0x4000100 );
}


/*
 * AR+PTK with no special TE treatment
 */
unsigned int
Scheme1( unsigned int word )
{
  unsigned int data;

  data = (word & 0xff) | ((word >> 1) & 0xff00) | ((word >>2) & 0xff0000);

  //  printf( " -> " );
  //  write_pattern20( data );

  if ( data )
  {
    unsigned int out_data=0;
    bool seen_zero;

    int LE = fLE( data );
    int TE = lTE( data );
    //	printf( "  fLE = %d\t lTE = %d\n", LE, TE );

    if ( TE < LE )
      out_data = data;
    else if ( LE == 0 )
      out_data = data; // | 0x00800000;
    else
    {
      //      printf( "%x %x\n", data, data &  0x00C00000  );
      if ( data &  0x00C00000 )        // either of first two bits set
	out_data = 0x00800000;        // Set first bit

      //  printf( "  -> " );
      //  write_pattern( out_data, true );

      if ( !(data & 0x00400000) )
	seen_zero = true;

      unsigned int Lmask = 0x00200000;

      /*
       * Find LE
       */

      // First zero
      while ( (Lmask & data) && !seen_zero && Lmask )
	Lmask = Lmask >> 1;
      if ( Lmask )
      {
	while ( !(Lmask & data) && Lmask )
	  Lmask = Lmask >> 1;
	  

	//	  printf( " Lmask = %x\n", Lmask );

	if ( Lmask != 0x0 )
	{
	  unsigned int Lmask_save = Lmask;
	  
	  unsigned int Tmask = 0x1;

	  while ( (! (Tmask & data)) && (Tmask != 0x0) )
	    Tmask = Tmask << 1;


	  if ( Tmask == 0x0 )
	    std::cout << std::hex << data << std::dec << std::endl;

	  unsigned int Tmask_save = Tmask;

	  out_data = out_data | Lmask | Tmask;

	  while ( (Tmask != Lmask) && (Lmask != 0x0) )
	  {
	    out_data = out_data | Lmask;
	    Lmask = Lmask >> 1;

	  }

	  if ( Lmask == 0x0 )
	    std::cout << std::hex << data << " ( " << Lmask_save << " , " 
		      << Tmask_save << " ) "
		      << " -> " << out_data << std::dec 
		      << std::endl;
	}
      }
    }

    data = ((out_data & 0xff0000) << 2) | 
      ((out_data & 0xff00) << 1) | (out_data & 0xff);

    //    printf( " -> " );
    //    write_pattern20( data );
  }

  /*
   * Reset middle HT bit
   */
  if ( word & 0x0020000 )   // Middle HT bit
    data = data | 0x0020000;

  /*
    cout << std::hex << data << " ( " << Lmask_save << " , " 
    << Tmask_save << " ) "
    << " -> " << out_data << std::dec 
    << std::endl;
  */

  //      printf( "xxx> " );
  //      write_pattern( data, true );

  return data;
}


/*
 * AR+PTK with similar TE treatment
 */
unsigned int
Scheme2( unsigned int word )
{
  unsigned int data;

  data = (word & 0xff) | ((word >> 1) & 0xff00) | ((word >>2) & 0xff0000);

  //  printf( " -> " );
  //  write_pattern20( data );

  if ( data )
  {
    unsigned int out_data=0;
    bool seen_zero;

    //	printf( "  fLE = %d\n", fLE( data ) );
    //	printf( "  lTE = %d\n", lTE( data ) );
    int LE = fLE( data );
    int TE = lTE( data );
    if ( TE < LE )
      out_data = data;
    else if ( (LE == 0) || (TE == 0) )
      out_data = data;
    else
    {
      if ( data & 0x00C00000 )        // either of first two bits set
	out_data = 0x00800000;        // Set first bit

      if ( data & 0x10 )              // either of last two bits set
	out_data = out_data | 0x10;   // Set last bit

      //  printf( "  -> " );
      //  write_pattern( out_data, true );

      for ( int i = LE; i <= TE; i++ )
	out_data = out_data | (1 << (20+4-i));

    }

    data = ((out_data & 0xff0000) << 2) | 
      ((out_data & 0xff00) << 1) | (out_data & 0xff);

    //    printf( " -> " );
    //    write_pattern20( data );
  }

  /*
   * Reset middle HT bit
   */
  if ( word & 0x0020000 )   // Middle HT bit
    data = data | 0x0020000;

  /*
    cout << std::hex << data << " ( " << Lmask_save << " , " 
    << Tmask_save << " ) "
    << " -> " << out_data << std::dec 
    << std::endl;
  */

  //      printf( "xxx> " );
  //      write_pattern( data, true );

  return data;
}



/*
 * First Pulse encoding with similar TE treatment
 */
unsigned int
Scheme3( unsigned int word )
{
  unsigned int data;

  data = (word & 0xff) | ((word >> 1) & 0xff00) | ((word >>2) & 0xff0000);

  //  printf( " -> " );
  //  write_pattern20( data );

  if ( data )
  {
    unsigned int out_data=0;
    bool seen_zero;

    //	printf( "  fLE = %d\n", fLE( data ) );
    //	printf( "  lTE = %d\n", lTE( data ) );
    //  printf( "  fTE = %d\n", fTE( data ) );
    int LE = fLE( data );
    int TE = fTE( data );
    int TEl = lTE( data );
    if ( TEl < LE )
      out_data = data;
    else if ( (LE == 0) || (TE == 0) )
      out_data = data;
    else
    {
      if ( data & 0x00C00000 )        // either of first two bits set
	out_data = 0x00800000;        // Set first bit

      if ( data & 0x10 )              // either of last two bits set
	out_data = out_data | 0x10;   // Set last bit

      //  printf( "  -> " );
      //  write_pattern( out_data, true );

      for ( int i = LE; i <= TE; i++ )
	out_data = out_data | (1 << (20+4-i));

    }

    data = ((out_data & 0xff0000) << 2) | 
      ((out_data & 0xff00) << 1) | (out_data & 0xff);

    //    printf( " -> " );
    //    write_pattern20( data );
  }

  /*
   * Reset middle HT bit
   */
  if ( word & 0x0020000 )   // Middle HT bit
    data = data | 0x0020000;

  /*
    cout << std::hex << data << " ( " << Lmask_save << " , " 
    << Tmask_save << " ) "
    << " -> " << out_data << std::dec 
    << std::endl;
  */

  //      printf( "xxx> " );
  //      write_pattern( data, true );

  return data;
}


unsigned int 
Vgate( unsigned int pattern )
{
  if ( pattern & 0x000DFEE )
    return pattern;
  else
    return 0;
}

// 0+0000.0011_0+1111.1111_0+1110.0000  VG
