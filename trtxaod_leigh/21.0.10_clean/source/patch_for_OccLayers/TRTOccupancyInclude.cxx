/* *******************************************************************

  TRTOccupancyInclude.cxx : Simple code to include Occupancy inside event info xAOD 

* ***************************************************************** */

#include "TRT_CalibAlgs/TRTOccupancyInclude.h"
#include "xAODEventInfo/EventInfo.h"

TRTOccupancyInclude::TRTOccupancyInclude(const std::string& name, ISvcLocator* pSvcLocator) :
  AthAlgorithm   (name, pSvcLocator),
  m_LocalOccTool()
{
  declareProperty("TRT_LocalOccupancyTool", m_LocalOccTool);
}

//---------------------------------------------------------------------

TRTOccupancyInclude::~TRTOccupancyInclude(void)
{}

//--------------------------------------------------------------------------

StatusCode TRTOccupancyInclude::initialize()
{
  if ( m_LocalOccTool.retrieve().isFailure() ){
    ATH_MSG_ERROR(" Failed to retrieve TRT Local Occupancy tool: " << m_LocalOccTool );
    return StatusCode::FAILURE;
  }
  else {
    ATH_MSG_INFO("Retrieved tool " << m_LocalOccTool);
  }

  return StatusCode::SUCCESS;
}

//---------------------------------------------------------------------

StatusCode TRTOccupancyInclude::execute()
{
  msg(MSG::DEBUG) << "execute()" << endreq;

  const xAOD::EventInfo* eventInfo = 0;
  if (evtStore()->retrieve(eventInfo).isFailure()) {
    ATH_MSG_WARNING(" Cannot access to event info.");
    return StatusCode::SUCCESS;
  } 

  std::vector<float> TRTOccu = m_LocalOccTool->GlobalOccupancy( );
  if (TRTOccu.size() > 6) {

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy0("TRTOccGlobal"); 
  decEventInfo_occupancy0( *eventInfo ) = TRTOccu.at(0); 

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy1("TRTOccBarrelC"); 
  decEventInfo_occupancy1( *eventInfo ) = TRTOccu.at(1); 

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy2("TRTOccEndcapAC"); 
  decEventInfo_occupancy2( *eventInfo ) = TRTOccu.at(2); 

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy3("TRTOccEndcapBC"); 
  decEventInfo_occupancy3( *eventInfo ) = TRTOccu.at(3); 

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy4("TRTOccBarrelA"); 
  decEventInfo_occupancy4( *eventInfo ) = TRTOccu.at(4); 

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy5("TRTOccEndcapAA"); 
  decEventInfo_occupancy5( *eventInfo ) = TRTOccu.at(5); 

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy6("TRTOccEndcapBA"); 
  decEventInfo_occupancy6( *eventInfo ) = TRTOccu.at(6); 
  }

  // ========================================== //

  float**  TRTOccWheel_list = m_LocalOccTool->getOccWheel();
  std::vector<std::vector<float>> TRTOccWheel(34, std::vector<float>(32)); 
  for (int i=0; i<34; ++i){
    for (int j=0; j<32; ++j){
      TRTOccWheel[i][j]=TRTOccWheel_list[i][j];
    }
  }
  
  

  if (TRTOccWheel.size() > 33) {
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_0( "TRTOccWheelBC_0_0");     decEventInfo_occwheelBC_0_0(  *eventInfo ) = TRTOccWheel.at(0).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_1( "TRTOccWheelBC_0_1");     decEventInfo_occwheelBC_0_1(  *eventInfo ) = TRTOccWheel.at(0).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_2( "TRTOccWheelBC_0_2");     decEventInfo_occwheelBC_0_2(  *eventInfo ) = TRTOccWheel.at(0).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_3( "TRTOccWheelBC_0_3");     decEventInfo_occwheelBC_0_3(  *eventInfo ) = TRTOccWheel.at(0).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_4( "TRTOccWheelBC_0_4");     decEventInfo_occwheelBC_0_4(  *eventInfo ) = TRTOccWheel.at(0).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_5( "TRTOccWheelBC_0_5");     decEventInfo_occwheelBC_0_5(  *eventInfo ) = TRTOccWheel.at(0).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_6( "TRTOccWheelBC_0_6");     decEventInfo_occwheelBC_0_6(  *eventInfo ) = TRTOccWheel.at(0).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_7( "TRTOccWheelBC_0_7");     decEventInfo_occwheelBC_0_7(  *eventInfo ) = TRTOccWheel.at(0).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_8( "TRTOccWheelBC_0_8");     decEventInfo_occwheelBC_0_8(  *eventInfo ) = TRTOccWheel.at(0).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_9( "TRTOccWheelBC_0_9");     decEventInfo_occwheelBC_0_9(  *eventInfo ) = TRTOccWheel.at(0).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_10("TRTOccWheelBC_0_10");    decEventInfo_occwheelBC_0_10( *eventInfo ) = TRTOccWheel.at(0).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_11("TRTOccWheelBC_0_11");    decEventInfo_occwheelBC_0_11( *eventInfo ) = TRTOccWheel.at(0).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_12("TRTOccWheelBC_0_12");    decEventInfo_occwheelBC_0_12( *eventInfo ) = TRTOccWheel.at(0).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_13("TRTOccWheelBC_0_13");    decEventInfo_occwheelBC_0_13( *eventInfo ) = TRTOccWheel.at(0).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_14("TRTOccWheelBC_0_14");    decEventInfo_occwheelBC_0_14( *eventInfo ) = TRTOccWheel.at(0).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_15("TRTOccWheelBC_0_15");    decEventInfo_occwheelBC_0_15( *eventInfo ) = TRTOccWheel.at(0).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_16("TRTOccWheelBC_0_16");    decEventInfo_occwheelBC_0_16( *eventInfo ) = TRTOccWheel.at(0).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_17("TRTOccWheelBC_0_17");    decEventInfo_occwheelBC_0_17( *eventInfo ) = TRTOccWheel.at(0).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_18("TRTOccWheelBC_0_18");    decEventInfo_occwheelBC_0_18( *eventInfo ) = TRTOccWheel.at(0).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_19("TRTOccWheelBC_0_19");    decEventInfo_occwheelBC_0_19( *eventInfo ) = TRTOccWheel.at(0).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_20("TRTOccWheelBC_0_20");    decEventInfo_occwheelBC_0_20( *eventInfo ) = TRTOccWheel.at(0).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_21("TRTOccWheelBC_0_21");    decEventInfo_occwheelBC_0_21( *eventInfo ) = TRTOccWheel.at(0).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_22("TRTOccWheelBC_0_22");    decEventInfo_occwheelBC_0_22( *eventInfo ) = TRTOccWheel.at(0).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_23("TRTOccWheelBC_0_23");    decEventInfo_occwheelBC_0_23( *eventInfo ) = TRTOccWheel.at(0).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_24("TRTOccWheelBC_0_24");    decEventInfo_occwheelBC_0_24( *eventInfo ) = TRTOccWheel.at(0).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_25("TRTOccWheelBC_0_25");    decEventInfo_occwheelBC_0_25( *eventInfo ) = TRTOccWheel.at(0).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_26("TRTOccWheelBC_0_26");    decEventInfo_occwheelBC_0_26( *eventInfo ) = TRTOccWheel.at(0).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_27("TRTOccWheelBC_0_27");    decEventInfo_occwheelBC_0_27( *eventInfo ) = TRTOccWheel.at(0).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_28("TRTOccWheelBC_0_28");    decEventInfo_occwheelBC_0_28( *eventInfo ) = TRTOccWheel.at(0).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_29("TRTOccWheelBC_0_29");    decEventInfo_occwheelBC_0_29( *eventInfo ) = TRTOccWheel.at(0).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_30("TRTOccWheelBC_0_30");    decEventInfo_occwheelBC_0_30( *eventInfo ) = TRTOccWheel.at(0).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_0_31("TRTOccWheelBC_0_31");    decEventInfo_occwheelBC_0_31( *eventInfo ) = TRTOccWheel.at(0).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_0( "TRTOccWheelBC_1_0");     decEventInfo_occwheelBC_1_0(  *eventInfo ) = TRTOccWheel.at(1).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_1( "TRTOccWheelBC_1_1");     decEventInfo_occwheelBC_1_1(  *eventInfo ) = TRTOccWheel.at(1).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_2( "TRTOccWheelBC_1_2");     decEventInfo_occwheelBC_1_2(  *eventInfo ) = TRTOccWheel.at(1).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_3( "TRTOccWheelBC_1_3");     decEventInfo_occwheelBC_1_3(  *eventInfo ) = TRTOccWheel.at(1).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_4( "TRTOccWheelBC_1_4");     decEventInfo_occwheelBC_1_4(  *eventInfo ) = TRTOccWheel.at(1).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_5( "TRTOccWheelBC_1_5");     decEventInfo_occwheelBC_1_5(  *eventInfo ) = TRTOccWheel.at(1).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_6( "TRTOccWheelBC_1_6");     decEventInfo_occwheelBC_1_6(  *eventInfo ) = TRTOccWheel.at(1).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_7( "TRTOccWheelBC_1_7");     decEventInfo_occwheelBC_1_7(  *eventInfo ) = TRTOccWheel.at(1).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_8( "TRTOccWheelBC_1_8");     decEventInfo_occwheelBC_1_8(  *eventInfo ) = TRTOccWheel.at(1).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_9( "TRTOccWheelBC_1_9");     decEventInfo_occwheelBC_1_9(  *eventInfo ) = TRTOccWheel.at(1).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_10("TRTOccWheelBC_1_10");    decEventInfo_occwheelBC_1_10( *eventInfo ) = TRTOccWheel.at(1).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_11("TRTOccWheelBC_1_11");    decEventInfo_occwheelBC_1_11( *eventInfo ) = TRTOccWheel.at(1).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_12("TRTOccWheelBC_1_12");    decEventInfo_occwheelBC_1_12( *eventInfo ) = TRTOccWheel.at(1).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_13("TRTOccWheelBC_1_13");    decEventInfo_occwheelBC_1_13( *eventInfo ) = TRTOccWheel.at(1).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_14("TRTOccWheelBC_1_14");    decEventInfo_occwheelBC_1_14( *eventInfo ) = TRTOccWheel.at(1).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_15("TRTOccWheelBC_1_15");    decEventInfo_occwheelBC_1_15( *eventInfo ) = TRTOccWheel.at(1).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_16("TRTOccWheelBC_1_16");    decEventInfo_occwheelBC_1_16( *eventInfo ) = TRTOccWheel.at(1).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_17("TRTOccWheelBC_1_17");    decEventInfo_occwheelBC_1_17( *eventInfo ) = TRTOccWheel.at(1).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_18("TRTOccWheelBC_1_18");    decEventInfo_occwheelBC_1_18( *eventInfo ) = TRTOccWheel.at(1).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_19("TRTOccWheelBC_1_19");    decEventInfo_occwheelBC_1_19( *eventInfo ) = TRTOccWheel.at(1).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_20("TRTOccWheelBC_1_20");    decEventInfo_occwheelBC_1_20( *eventInfo ) = TRTOccWheel.at(1).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_21("TRTOccWheelBC_1_21");    decEventInfo_occwheelBC_1_21( *eventInfo ) = TRTOccWheel.at(1).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_22("TRTOccWheelBC_1_22");    decEventInfo_occwheelBC_1_22( *eventInfo ) = TRTOccWheel.at(1).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_23("TRTOccWheelBC_1_23");    decEventInfo_occwheelBC_1_23( *eventInfo ) = TRTOccWheel.at(1).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_24("TRTOccWheelBC_1_24");    decEventInfo_occwheelBC_1_24( *eventInfo ) = TRTOccWheel.at(1).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_25("TRTOccWheelBC_1_25");    decEventInfo_occwheelBC_1_25( *eventInfo ) = TRTOccWheel.at(1).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_26("TRTOccWheelBC_1_26");    decEventInfo_occwheelBC_1_26( *eventInfo ) = TRTOccWheel.at(1).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_27("TRTOccWheelBC_1_27");    decEventInfo_occwheelBC_1_27( *eventInfo ) = TRTOccWheel.at(1).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_28("TRTOccWheelBC_1_28");    decEventInfo_occwheelBC_1_28( *eventInfo ) = TRTOccWheel.at(1).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_29("TRTOccWheelBC_1_29");    decEventInfo_occwheelBC_1_29( *eventInfo ) = TRTOccWheel.at(1).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_30("TRTOccWheelBC_1_30");    decEventInfo_occwheelBC_1_30( *eventInfo ) = TRTOccWheel.at(1).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_1_31("TRTOccWheelBC_1_31");    decEventInfo_occwheelBC_1_31( *eventInfo ) = TRTOccWheel.at(1).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_0( "TRTOccWheelBC_2_0");     decEventInfo_occwheelBC_2_0(  *eventInfo ) = TRTOccWheel.at(2).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_1( "TRTOccWheelBC_2_1");     decEventInfo_occwheelBC_2_1(  *eventInfo ) = TRTOccWheel.at(2).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_2( "TRTOccWheelBC_2_2");     decEventInfo_occwheelBC_2_2(  *eventInfo ) = TRTOccWheel.at(2).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_3( "TRTOccWheelBC_2_3");     decEventInfo_occwheelBC_2_3(  *eventInfo ) = TRTOccWheel.at(2).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_4( "TRTOccWheelBC_2_4");     decEventInfo_occwheelBC_2_4(  *eventInfo ) = TRTOccWheel.at(2).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_5( "TRTOccWheelBC_2_5");     decEventInfo_occwheelBC_2_5(  *eventInfo ) = TRTOccWheel.at(2).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_6( "TRTOccWheelBC_2_6");     decEventInfo_occwheelBC_2_6(  *eventInfo ) = TRTOccWheel.at(2).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_7( "TRTOccWheelBC_2_7");     decEventInfo_occwheelBC_2_7(  *eventInfo ) = TRTOccWheel.at(2).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_8( "TRTOccWheelBC_2_8");     decEventInfo_occwheelBC_2_8(  *eventInfo ) = TRTOccWheel.at(2).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_9( "TRTOccWheelBC_2_9");     decEventInfo_occwheelBC_2_9(  *eventInfo ) = TRTOccWheel.at(2).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_10("TRTOccWheelBC_2_10");    decEventInfo_occwheelBC_2_10( *eventInfo ) = TRTOccWheel.at(2).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_11("TRTOccWheelBC_2_11");    decEventInfo_occwheelBC_2_11( *eventInfo ) = TRTOccWheel.at(2).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_12("TRTOccWheelBC_2_12");    decEventInfo_occwheelBC_2_12( *eventInfo ) = TRTOccWheel.at(2).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_13("TRTOccWheelBC_2_13");    decEventInfo_occwheelBC_2_13( *eventInfo ) = TRTOccWheel.at(2).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_14("TRTOccWheelBC_2_14");    decEventInfo_occwheelBC_2_14( *eventInfo ) = TRTOccWheel.at(2).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_15("TRTOccWheelBC_2_15");    decEventInfo_occwheelBC_2_15( *eventInfo ) = TRTOccWheel.at(2).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_16("TRTOccWheelBC_2_16");    decEventInfo_occwheelBC_2_16( *eventInfo ) = TRTOccWheel.at(2).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_17("TRTOccWheelBC_2_17");    decEventInfo_occwheelBC_2_17( *eventInfo ) = TRTOccWheel.at(2).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_18("TRTOccWheelBC_2_18");    decEventInfo_occwheelBC_2_18( *eventInfo ) = TRTOccWheel.at(2).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_19("TRTOccWheelBC_2_19");    decEventInfo_occwheelBC_2_19( *eventInfo ) = TRTOccWheel.at(2).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_20("TRTOccWheelBC_2_20");    decEventInfo_occwheelBC_2_20( *eventInfo ) = TRTOccWheel.at(2).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_21("TRTOccWheelBC_2_21");    decEventInfo_occwheelBC_2_21( *eventInfo ) = TRTOccWheel.at(2).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_22("TRTOccWheelBC_2_22");    decEventInfo_occwheelBC_2_22( *eventInfo ) = TRTOccWheel.at(2).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_23("TRTOccWheelBC_2_23");    decEventInfo_occwheelBC_2_23( *eventInfo ) = TRTOccWheel.at(2).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_24("TRTOccWheelBC_2_24");    decEventInfo_occwheelBC_2_24( *eventInfo ) = TRTOccWheel.at(2).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_25("TRTOccWheelBC_2_25");    decEventInfo_occwheelBC_2_25( *eventInfo ) = TRTOccWheel.at(2).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_26("TRTOccWheelBC_2_26");    decEventInfo_occwheelBC_2_26( *eventInfo ) = TRTOccWheel.at(2).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_27("TRTOccWheelBC_2_27");    decEventInfo_occwheelBC_2_27( *eventInfo ) = TRTOccWheel.at(2).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_28("TRTOccWheelBC_2_28");    decEventInfo_occwheelBC_2_28( *eventInfo ) = TRTOccWheel.at(2).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_29("TRTOccWheelBC_2_29");    decEventInfo_occwheelBC_2_29( *eventInfo ) = TRTOccWheel.at(2).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_30("TRTOccWheelBC_2_30");    decEventInfo_occwheelBC_2_30( *eventInfo ) = TRTOccWheel.at(2).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBC_2_31("TRTOccWheelBC_2_31");    decEventInfo_occwheelBC_2_31( *eventInfo ) = TRTOccWheel.at(2).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_0( "TRTOccWheelEC_0_0");     decEventInfo_occwheelEC_0_0(  *eventInfo ) = TRTOccWheel.at(3).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_1( "TRTOccWheelEC_0_1");     decEventInfo_occwheelEC_0_1(  *eventInfo ) = TRTOccWheel.at(3).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_2( "TRTOccWheelEC_0_2");     decEventInfo_occwheelEC_0_2(  *eventInfo ) = TRTOccWheel.at(3).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_3( "TRTOccWheelEC_0_3");     decEventInfo_occwheelEC_0_3(  *eventInfo ) = TRTOccWheel.at(3).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_4( "TRTOccWheelEC_0_4");     decEventInfo_occwheelEC_0_4(  *eventInfo ) = TRTOccWheel.at(3).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_5( "TRTOccWheelEC_0_5");     decEventInfo_occwheelEC_0_5(  *eventInfo ) = TRTOccWheel.at(3).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_6( "TRTOccWheelEC_0_6");     decEventInfo_occwheelEC_0_6(  *eventInfo ) = TRTOccWheel.at(3).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_7( "TRTOccWheelEC_0_7");     decEventInfo_occwheelEC_0_7(  *eventInfo ) = TRTOccWheel.at(3).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_8( "TRTOccWheelEC_0_8");     decEventInfo_occwheelEC_0_8(  *eventInfo ) = TRTOccWheel.at(3).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_9( "TRTOccWheelEC_0_9");     decEventInfo_occwheelEC_0_9(  *eventInfo ) = TRTOccWheel.at(3).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_10("TRTOccWheelEC_0_10");    decEventInfo_occwheelEC_0_10( *eventInfo ) = TRTOccWheel.at(3).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_11("TRTOccWheelEC_0_11");    decEventInfo_occwheelEC_0_11( *eventInfo ) = TRTOccWheel.at(3).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_12("TRTOccWheelEC_0_12");    decEventInfo_occwheelEC_0_12( *eventInfo ) = TRTOccWheel.at(3).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_13("TRTOccWheelEC_0_13");    decEventInfo_occwheelEC_0_13( *eventInfo ) = TRTOccWheel.at(3).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_14("TRTOccWheelEC_0_14");    decEventInfo_occwheelEC_0_14( *eventInfo ) = TRTOccWheel.at(3).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_15("TRTOccWheelEC_0_15");    decEventInfo_occwheelEC_0_15( *eventInfo ) = TRTOccWheel.at(3).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_16("TRTOccWheelEC_0_16");    decEventInfo_occwheelEC_0_16( *eventInfo ) = TRTOccWheel.at(3).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_17("TRTOccWheelEC_0_17");    decEventInfo_occwheelEC_0_17( *eventInfo ) = TRTOccWheel.at(3).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_18("TRTOccWheelEC_0_18");    decEventInfo_occwheelEC_0_18( *eventInfo ) = TRTOccWheel.at(3).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_19("TRTOccWheelEC_0_19");    decEventInfo_occwheelEC_0_19( *eventInfo ) = TRTOccWheel.at(3).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_20("TRTOccWheelEC_0_20");    decEventInfo_occwheelEC_0_20( *eventInfo ) = TRTOccWheel.at(3).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_21("TRTOccWheelEC_0_21");    decEventInfo_occwheelEC_0_21( *eventInfo ) = TRTOccWheel.at(3).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_22("TRTOccWheelEC_0_22");    decEventInfo_occwheelEC_0_22( *eventInfo ) = TRTOccWheel.at(3).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_23("TRTOccWheelEC_0_23");    decEventInfo_occwheelEC_0_23( *eventInfo ) = TRTOccWheel.at(3).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_24("TRTOccWheelEC_0_24");    decEventInfo_occwheelEC_0_24( *eventInfo ) = TRTOccWheel.at(3).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_25("TRTOccWheelEC_0_25");    decEventInfo_occwheelEC_0_25( *eventInfo ) = TRTOccWheel.at(3).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_26("TRTOccWheelEC_0_26");    decEventInfo_occwheelEC_0_26( *eventInfo ) = TRTOccWheel.at(3).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_27("TRTOccWheelEC_0_27");    decEventInfo_occwheelEC_0_27( *eventInfo ) = TRTOccWheel.at(3).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_28("TRTOccWheelEC_0_28");    decEventInfo_occwheelEC_0_28( *eventInfo ) = TRTOccWheel.at(3).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_29("TRTOccWheelEC_0_29");    decEventInfo_occwheelEC_0_29( *eventInfo ) = TRTOccWheel.at(3).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_30("TRTOccWheelEC_0_30");    decEventInfo_occwheelEC_0_30( *eventInfo ) = TRTOccWheel.at(3).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_0_31("TRTOccWheelEC_0_31");    decEventInfo_occwheelEC_0_31( *eventInfo ) = TRTOccWheel.at(3).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_0( "TRTOccWheelEC_1_0");     decEventInfo_occwheelEC_1_0(  *eventInfo ) = TRTOccWheel.at(4).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_1( "TRTOccWheelEC_1_1");     decEventInfo_occwheelEC_1_1(  *eventInfo ) = TRTOccWheel.at(4).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_2( "TRTOccWheelEC_1_2");     decEventInfo_occwheelEC_1_2(  *eventInfo ) = TRTOccWheel.at(4).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_3( "TRTOccWheelEC_1_3");     decEventInfo_occwheelEC_1_3(  *eventInfo ) = TRTOccWheel.at(4).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_4( "TRTOccWheelEC_1_4");     decEventInfo_occwheelEC_1_4(  *eventInfo ) = TRTOccWheel.at(4).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_5( "TRTOccWheelEC_1_5");     decEventInfo_occwheelEC_1_5(  *eventInfo ) = TRTOccWheel.at(4).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_6( "TRTOccWheelEC_1_6");     decEventInfo_occwheelEC_1_6(  *eventInfo ) = TRTOccWheel.at(4).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_7( "TRTOccWheelEC_1_7");     decEventInfo_occwheelEC_1_7(  *eventInfo ) = TRTOccWheel.at(4).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_8( "TRTOccWheelEC_1_8");     decEventInfo_occwheelEC_1_8(  *eventInfo ) = TRTOccWheel.at(4).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_9( "TRTOccWheelEC_1_9");     decEventInfo_occwheelEC_1_9(  *eventInfo ) = TRTOccWheel.at(4).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_10("TRTOccWheelEC_1_10");    decEventInfo_occwheelEC_1_10( *eventInfo ) = TRTOccWheel.at(4).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_11("TRTOccWheelEC_1_11");    decEventInfo_occwheelEC_1_11( *eventInfo ) = TRTOccWheel.at(4).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_12("TRTOccWheelEC_1_12");    decEventInfo_occwheelEC_1_12( *eventInfo ) = TRTOccWheel.at(4).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_13("TRTOccWheelEC_1_13");    decEventInfo_occwheelEC_1_13( *eventInfo ) = TRTOccWheel.at(4).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_14("TRTOccWheelEC_1_14");    decEventInfo_occwheelEC_1_14( *eventInfo ) = TRTOccWheel.at(4).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_15("TRTOccWheelEC_1_15");    decEventInfo_occwheelEC_1_15( *eventInfo ) = TRTOccWheel.at(4).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_16("TRTOccWheelEC_1_16");    decEventInfo_occwheelEC_1_16( *eventInfo ) = TRTOccWheel.at(4).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_17("TRTOccWheelEC_1_17");    decEventInfo_occwheelEC_1_17( *eventInfo ) = TRTOccWheel.at(4).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_18("TRTOccWheelEC_1_18");    decEventInfo_occwheelEC_1_18( *eventInfo ) = TRTOccWheel.at(4).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_19("TRTOccWheelEC_1_19");    decEventInfo_occwheelEC_1_19( *eventInfo ) = TRTOccWheel.at(4).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_20("TRTOccWheelEC_1_20");    decEventInfo_occwheelEC_1_20( *eventInfo ) = TRTOccWheel.at(4).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_21("TRTOccWheelEC_1_21");    decEventInfo_occwheelEC_1_21( *eventInfo ) = TRTOccWheel.at(4).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_22("TRTOccWheelEC_1_22");    decEventInfo_occwheelEC_1_22( *eventInfo ) = TRTOccWheel.at(4).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_23("TRTOccWheelEC_1_23");    decEventInfo_occwheelEC_1_23( *eventInfo ) = TRTOccWheel.at(4).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_24("TRTOccWheelEC_1_24");    decEventInfo_occwheelEC_1_24( *eventInfo ) = TRTOccWheel.at(4).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_25("TRTOccWheelEC_1_25");    decEventInfo_occwheelEC_1_25( *eventInfo ) = TRTOccWheel.at(4).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_26("TRTOccWheelEC_1_26");    decEventInfo_occwheelEC_1_26( *eventInfo ) = TRTOccWheel.at(4).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_27("TRTOccWheelEC_1_27");    decEventInfo_occwheelEC_1_27( *eventInfo ) = TRTOccWheel.at(4).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_28("TRTOccWheelEC_1_28");    decEventInfo_occwheelEC_1_28( *eventInfo ) = TRTOccWheel.at(4).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_29("TRTOccWheelEC_1_29");    decEventInfo_occwheelEC_1_29( *eventInfo ) = TRTOccWheel.at(4).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_30("TRTOccWheelEC_1_30");    decEventInfo_occwheelEC_1_30( *eventInfo ) = TRTOccWheel.at(4).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_1_31("TRTOccWheelEC_1_31");    decEventInfo_occwheelEC_1_31( *eventInfo ) = TRTOccWheel.at(4).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_0( "TRTOccWheelEC_2_0");     decEventInfo_occwheelEC_2_0(  *eventInfo ) = TRTOccWheel.at(5).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_1( "TRTOccWheelEC_2_1");     decEventInfo_occwheelEC_2_1(  *eventInfo ) = TRTOccWheel.at(5).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_2( "TRTOccWheelEC_2_2");     decEventInfo_occwheelEC_2_2(  *eventInfo ) = TRTOccWheel.at(5).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_3( "TRTOccWheelEC_2_3");     decEventInfo_occwheelEC_2_3(  *eventInfo ) = TRTOccWheel.at(5).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_4( "TRTOccWheelEC_2_4");     decEventInfo_occwheelEC_2_4(  *eventInfo ) = TRTOccWheel.at(5).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_5( "TRTOccWheelEC_2_5");     decEventInfo_occwheelEC_2_5(  *eventInfo ) = TRTOccWheel.at(5).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_6( "TRTOccWheelEC_2_6");     decEventInfo_occwheelEC_2_6(  *eventInfo ) = TRTOccWheel.at(5).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_7( "TRTOccWheelEC_2_7");     decEventInfo_occwheelEC_2_7(  *eventInfo ) = TRTOccWheel.at(5).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_8( "TRTOccWheelEC_2_8");     decEventInfo_occwheelEC_2_8(  *eventInfo ) = TRTOccWheel.at(5).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_9( "TRTOccWheelEC_2_9");     decEventInfo_occwheelEC_2_9(  *eventInfo ) = TRTOccWheel.at(5).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_10("TRTOccWheelEC_2_10");    decEventInfo_occwheelEC_2_10( *eventInfo ) = TRTOccWheel.at(5).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_11("TRTOccWheelEC_2_11");    decEventInfo_occwheelEC_2_11( *eventInfo ) = TRTOccWheel.at(5).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_12("TRTOccWheelEC_2_12");    decEventInfo_occwheelEC_2_12( *eventInfo ) = TRTOccWheel.at(5).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_13("TRTOccWheelEC_2_13");    decEventInfo_occwheelEC_2_13( *eventInfo ) = TRTOccWheel.at(5).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_14("TRTOccWheelEC_2_14");    decEventInfo_occwheelEC_2_14( *eventInfo ) = TRTOccWheel.at(5).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_15("TRTOccWheelEC_2_15");    decEventInfo_occwheelEC_2_15( *eventInfo ) = TRTOccWheel.at(5).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_16("TRTOccWheelEC_2_16");    decEventInfo_occwheelEC_2_16( *eventInfo ) = TRTOccWheel.at(5).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_17("TRTOccWheelEC_2_17");    decEventInfo_occwheelEC_2_17( *eventInfo ) = TRTOccWheel.at(5).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_18("TRTOccWheelEC_2_18");    decEventInfo_occwheelEC_2_18( *eventInfo ) = TRTOccWheel.at(5).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_19("TRTOccWheelEC_2_19");    decEventInfo_occwheelEC_2_19( *eventInfo ) = TRTOccWheel.at(5).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_20("TRTOccWheelEC_2_20");    decEventInfo_occwheelEC_2_20( *eventInfo ) = TRTOccWheel.at(5).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_21("TRTOccWheelEC_2_21");    decEventInfo_occwheelEC_2_21( *eventInfo ) = TRTOccWheel.at(5).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_22("TRTOccWheelEC_2_22");    decEventInfo_occwheelEC_2_22( *eventInfo ) = TRTOccWheel.at(5).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_23("TRTOccWheelEC_2_23");    decEventInfo_occwheelEC_2_23( *eventInfo ) = TRTOccWheel.at(5).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_24("TRTOccWheelEC_2_24");    decEventInfo_occwheelEC_2_24( *eventInfo ) = TRTOccWheel.at(5).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_25("TRTOccWheelEC_2_25");    decEventInfo_occwheelEC_2_25( *eventInfo ) = TRTOccWheel.at(5).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_26("TRTOccWheelEC_2_26");    decEventInfo_occwheelEC_2_26( *eventInfo ) = TRTOccWheel.at(5).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_27("TRTOccWheelEC_2_27");    decEventInfo_occwheelEC_2_27( *eventInfo ) = TRTOccWheel.at(5).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_28("TRTOccWheelEC_2_28");    decEventInfo_occwheelEC_2_28( *eventInfo ) = TRTOccWheel.at(5).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_29("TRTOccWheelEC_2_29");    decEventInfo_occwheelEC_2_29( *eventInfo ) = TRTOccWheel.at(5).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_30("TRTOccWheelEC_2_30");    decEventInfo_occwheelEC_2_30( *eventInfo ) = TRTOccWheel.at(5).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_2_31("TRTOccWheelEC_2_31");    decEventInfo_occwheelEC_2_31( *eventInfo ) = TRTOccWheel.at(5).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_0( "TRTOccWheelEC_3_0");     decEventInfo_occwheelEC_3_0(  *eventInfo ) = TRTOccWheel.at(6).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_1( "TRTOccWheelEC_3_1");     decEventInfo_occwheelEC_3_1(  *eventInfo ) = TRTOccWheel.at(6).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_2( "TRTOccWheelEC_3_2");     decEventInfo_occwheelEC_3_2(  *eventInfo ) = TRTOccWheel.at(6).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_3( "TRTOccWheelEC_3_3");     decEventInfo_occwheelEC_3_3(  *eventInfo ) = TRTOccWheel.at(6).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_4( "TRTOccWheelEC_3_4");     decEventInfo_occwheelEC_3_4(  *eventInfo ) = TRTOccWheel.at(6).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_5( "TRTOccWheelEC_3_5");     decEventInfo_occwheelEC_3_5(  *eventInfo ) = TRTOccWheel.at(6).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_6( "TRTOccWheelEC_3_6");     decEventInfo_occwheelEC_3_6(  *eventInfo ) = TRTOccWheel.at(6).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_7( "TRTOccWheelEC_3_7");     decEventInfo_occwheelEC_3_7(  *eventInfo ) = TRTOccWheel.at(6).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_8( "TRTOccWheelEC_3_8");     decEventInfo_occwheelEC_3_8(  *eventInfo ) = TRTOccWheel.at(6).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_9( "TRTOccWheelEC_3_9");     decEventInfo_occwheelEC_3_9(  *eventInfo ) = TRTOccWheel.at(6).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_10("TRTOccWheelEC_3_10");    decEventInfo_occwheelEC_3_10( *eventInfo ) = TRTOccWheel.at(6).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_11("TRTOccWheelEC_3_11");    decEventInfo_occwheelEC_3_11( *eventInfo ) = TRTOccWheel.at(6).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_12("TRTOccWheelEC_3_12");    decEventInfo_occwheelEC_3_12( *eventInfo ) = TRTOccWheel.at(6).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_13("TRTOccWheelEC_3_13");    decEventInfo_occwheelEC_3_13( *eventInfo ) = TRTOccWheel.at(6).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_14("TRTOccWheelEC_3_14");    decEventInfo_occwheelEC_3_14( *eventInfo ) = TRTOccWheel.at(6).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_15("TRTOccWheelEC_3_15");    decEventInfo_occwheelEC_3_15( *eventInfo ) = TRTOccWheel.at(6).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_16("TRTOccWheelEC_3_16");    decEventInfo_occwheelEC_3_16( *eventInfo ) = TRTOccWheel.at(6).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_17("TRTOccWheelEC_3_17");    decEventInfo_occwheelEC_3_17( *eventInfo ) = TRTOccWheel.at(6).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_18("TRTOccWheelEC_3_18");    decEventInfo_occwheelEC_3_18( *eventInfo ) = TRTOccWheel.at(6).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_19("TRTOccWheelEC_3_19");    decEventInfo_occwheelEC_3_19( *eventInfo ) = TRTOccWheel.at(6).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_20("TRTOccWheelEC_3_20");    decEventInfo_occwheelEC_3_20( *eventInfo ) = TRTOccWheel.at(6).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_21("TRTOccWheelEC_3_21");    decEventInfo_occwheelEC_3_21( *eventInfo ) = TRTOccWheel.at(6).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_22("TRTOccWheelEC_3_22");    decEventInfo_occwheelEC_3_22( *eventInfo ) = TRTOccWheel.at(6).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_23("TRTOccWheelEC_3_23");    decEventInfo_occwheelEC_3_23( *eventInfo ) = TRTOccWheel.at(6).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_24("TRTOccWheelEC_3_24");    decEventInfo_occwheelEC_3_24( *eventInfo ) = TRTOccWheel.at(6).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_25("TRTOccWheelEC_3_25");    decEventInfo_occwheelEC_3_25( *eventInfo ) = TRTOccWheel.at(6).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_26("TRTOccWheelEC_3_26");    decEventInfo_occwheelEC_3_26( *eventInfo ) = TRTOccWheel.at(6).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_27("TRTOccWheelEC_3_27");    decEventInfo_occwheelEC_3_27( *eventInfo ) = TRTOccWheel.at(6).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_28("TRTOccWheelEC_3_28");    decEventInfo_occwheelEC_3_28( *eventInfo ) = TRTOccWheel.at(6).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_29("TRTOccWheelEC_3_29");    decEventInfo_occwheelEC_3_29( *eventInfo ) = TRTOccWheel.at(6).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_30("TRTOccWheelEC_3_30");    decEventInfo_occwheelEC_3_30( *eventInfo ) = TRTOccWheel.at(6).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_3_31("TRTOccWheelEC_3_31");    decEventInfo_occwheelEC_3_31( *eventInfo ) = TRTOccWheel.at(6).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_0( "TRTOccWheelEC_4_0");     decEventInfo_occwheelEC_4_0(  *eventInfo ) = TRTOccWheel.at(7).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_1( "TRTOccWheelEC_4_1");     decEventInfo_occwheelEC_4_1(  *eventInfo ) = TRTOccWheel.at(7).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_2( "TRTOccWheelEC_4_2");     decEventInfo_occwheelEC_4_2(  *eventInfo ) = TRTOccWheel.at(7).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_3( "TRTOccWheelEC_4_3");     decEventInfo_occwheelEC_4_3(  *eventInfo ) = TRTOccWheel.at(7).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_4( "TRTOccWheelEC_4_4");     decEventInfo_occwheelEC_4_4(  *eventInfo ) = TRTOccWheel.at(7).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_5( "TRTOccWheelEC_4_5");     decEventInfo_occwheelEC_4_5(  *eventInfo ) = TRTOccWheel.at(7).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_6( "TRTOccWheelEC_4_6");     decEventInfo_occwheelEC_4_6(  *eventInfo ) = TRTOccWheel.at(7).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_7( "TRTOccWheelEC_4_7");     decEventInfo_occwheelEC_4_7(  *eventInfo ) = TRTOccWheel.at(7).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_8( "TRTOccWheelEC_4_8");     decEventInfo_occwheelEC_4_8(  *eventInfo ) = TRTOccWheel.at(7).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_9( "TRTOccWheelEC_4_9");     decEventInfo_occwheelEC_4_9(  *eventInfo ) = TRTOccWheel.at(7).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_10("TRTOccWheelEC_4_10");    decEventInfo_occwheelEC_4_10( *eventInfo ) = TRTOccWheel.at(7).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_11("TRTOccWheelEC_4_11");    decEventInfo_occwheelEC_4_11( *eventInfo ) = TRTOccWheel.at(7).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_12("TRTOccWheelEC_4_12");    decEventInfo_occwheelEC_4_12( *eventInfo ) = TRTOccWheel.at(7).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_13("TRTOccWheelEC_4_13");    decEventInfo_occwheelEC_4_13( *eventInfo ) = TRTOccWheel.at(7).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_14("TRTOccWheelEC_4_14");    decEventInfo_occwheelEC_4_14( *eventInfo ) = TRTOccWheel.at(7).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_15("TRTOccWheelEC_4_15");    decEventInfo_occwheelEC_4_15( *eventInfo ) = TRTOccWheel.at(7).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_16("TRTOccWheelEC_4_16");    decEventInfo_occwheelEC_4_16( *eventInfo ) = TRTOccWheel.at(7).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_17("TRTOccWheelEC_4_17");    decEventInfo_occwheelEC_4_17( *eventInfo ) = TRTOccWheel.at(7).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_18("TRTOccWheelEC_4_18");    decEventInfo_occwheelEC_4_18( *eventInfo ) = TRTOccWheel.at(7).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_19("TRTOccWheelEC_4_19");    decEventInfo_occwheelEC_4_19( *eventInfo ) = TRTOccWheel.at(7).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_20("TRTOccWheelEC_4_20");    decEventInfo_occwheelEC_4_20( *eventInfo ) = TRTOccWheel.at(7).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_21("TRTOccWheelEC_4_21");    decEventInfo_occwheelEC_4_21( *eventInfo ) = TRTOccWheel.at(7).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_22("TRTOccWheelEC_4_22");    decEventInfo_occwheelEC_4_22( *eventInfo ) = TRTOccWheel.at(7).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_23("TRTOccWheelEC_4_23");    decEventInfo_occwheelEC_4_23( *eventInfo ) = TRTOccWheel.at(7).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_24("TRTOccWheelEC_4_24");    decEventInfo_occwheelEC_4_24( *eventInfo ) = TRTOccWheel.at(7).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_25("TRTOccWheelEC_4_25");    decEventInfo_occwheelEC_4_25( *eventInfo ) = TRTOccWheel.at(7).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_26("TRTOccWheelEC_4_26");    decEventInfo_occwheelEC_4_26( *eventInfo ) = TRTOccWheel.at(7).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_27("TRTOccWheelEC_4_27");    decEventInfo_occwheelEC_4_27( *eventInfo ) = TRTOccWheel.at(7).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_28("TRTOccWheelEC_4_28");    decEventInfo_occwheelEC_4_28( *eventInfo ) = TRTOccWheel.at(7).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_29("TRTOccWheelEC_4_29");    decEventInfo_occwheelEC_4_29( *eventInfo ) = TRTOccWheel.at(7).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_30("TRTOccWheelEC_4_30");    decEventInfo_occwheelEC_4_30( *eventInfo ) = TRTOccWheel.at(7).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_4_31("TRTOccWheelEC_4_31");    decEventInfo_occwheelEC_4_31( *eventInfo ) = TRTOccWheel.at(7).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_0( "TRTOccWheelEC_5_0");     decEventInfo_occwheelEC_5_0(  *eventInfo ) = TRTOccWheel.at(8).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_1( "TRTOccWheelEC_5_1");     decEventInfo_occwheelEC_5_1(  *eventInfo ) = TRTOccWheel.at(8).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_2( "TRTOccWheelEC_5_2");     decEventInfo_occwheelEC_5_2(  *eventInfo ) = TRTOccWheel.at(8).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_3( "TRTOccWheelEC_5_3");     decEventInfo_occwheelEC_5_3(  *eventInfo ) = TRTOccWheel.at(8).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_4( "TRTOccWheelEC_5_4");     decEventInfo_occwheelEC_5_4(  *eventInfo ) = TRTOccWheel.at(8).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_5( "TRTOccWheelEC_5_5");     decEventInfo_occwheelEC_5_5(  *eventInfo ) = TRTOccWheel.at(8).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_6( "TRTOccWheelEC_5_6");     decEventInfo_occwheelEC_5_6(  *eventInfo ) = TRTOccWheel.at(8).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_7( "TRTOccWheelEC_5_7");     decEventInfo_occwheelEC_5_7(  *eventInfo ) = TRTOccWheel.at(8).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_8( "TRTOccWheelEC_5_8");     decEventInfo_occwheelEC_5_8(  *eventInfo ) = TRTOccWheel.at(8).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_9( "TRTOccWheelEC_5_9");     decEventInfo_occwheelEC_5_9(  *eventInfo ) = TRTOccWheel.at(8).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_10("TRTOccWheelEC_5_10");    decEventInfo_occwheelEC_5_10( *eventInfo ) = TRTOccWheel.at(8).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_11("TRTOccWheelEC_5_11");    decEventInfo_occwheelEC_5_11( *eventInfo ) = TRTOccWheel.at(8).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_12("TRTOccWheelEC_5_12");    decEventInfo_occwheelEC_5_12( *eventInfo ) = TRTOccWheel.at(8).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_13("TRTOccWheelEC_5_13");    decEventInfo_occwheelEC_5_13( *eventInfo ) = TRTOccWheel.at(8).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_14("TRTOccWheelEC_5_14");    decEventInfo_occwheelEC_5_14( *eventInfo ) = TRTOccWheel.at(8).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_15("TRTOccWheelEC_5_15");    decEventInfo_occwheelEC_5_15( *eventInfo ) = TRTOccWheel.at(8).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_16("TRTOccWheelEC_5_16");    decEventInfo_occwheelEC_5_16( *eventInfo ) = TRTOccWheel.at(8).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_17("TRTOccWheelEC_5_17");    decEventInfo_occwheelEC_5_17( *eventInfo ) = TRTOccWheel.at(8).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_18("TRTOccWheelEC_5_18");    decEventInfo_occwheelEC_5_18( *eventInfo ) = TRTOccWheel.at(8).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_19("TRTOccWheelEC_5_19");    decEventInfo_occwheelEC_5_19( *eventInfo ) = TRTOccWheel.at(8).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_20("TRTOccWheelEC_5_20");    decEventInfo_occwheelEC_5_20( *eventInfo ) = TRTOccWheel.at(8).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_21("TRTOccWheelEC_5_21");    decEventInfo_occwheelEC_5_21( *eventInfo ) = TRTOccWheel.at(8).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_22("TRTOccWheelEC_5_22");    decEventInfo_occwheelEC_5_22( *eventInfo ) = TRTOccWheel.at(8).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_23("TRTOccWheelEC_5_23");    decEventInfo_occwheelEC_5_23( *eventInfo ) = TRTOccWheel.at(8).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_24("TRTOccWheelEC_5_24");    decEventInfo_occwheelEC_5_24( *eventInfo ) = TRTOccWheel.at(8).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_25("TRTOccWheelEC_5_25");    decEventInfo_occwheelEC_5_25( *eventInfo ) = TRTOccWheel.at(8).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_26("TRTOccWheelEC_5_26");    decEventInfo_occwheelEC_5_26( *eventInfo ) = TRTOccWheel.at(8).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_27("TRTOccWheelEC_5_27");    decEventInfo_occwheelEC_5_27( *eventInfo ) = TRTOccWheel.at(8).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_28("TRTOccWheelEC_5_28");    decEventInfo_occwheelEC_5_28( *eventInfo ) = TRTOccWheel.at(8).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_29("TRTOccWheelEC_5_29");    decEventInfo_occwheelEC_5_29( *eventInfo ) = TRTOccWheel.at(8).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_30("TRTOccWheelEC_5_30");    decEventInfo_occwheelEC_5_30( *eventInfo ) = TRTOccWheel.at(8).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_5_31("TRTOccWheelEC_5_31");    decEventInfo_occwheelEC_5_31( *eventInfo ) = TRTOccWheel.at(8).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_0( "TRTOccWheelEC_6_0");     decEventInfo_occwheelEC_6_0(  *eventInfo ) = TRTOccWheel.at(9).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_1( "TRTOccWheelEC_6_1");     decEventInfo_occwheelEC_6_1(  *eventInfo ) = TRTOccWheel.at(9).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_2( "TRTOccWheelEC_6_2");     decEventInfo_occwheelEC_6_2(  *eventInfo ) = TRTOccWheel.at(9).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_3( "TRTOccWheelEC_6_3");     decEventInfo_occwheelEC_6_3(  *eventInfo ) = TRTOccWheel.at(9).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_4( "TRTOccWheelEC_6_4");     decEventInfo_occwheelEC_6_4(  *eventInfo ) = TRTOccWheel.at(9).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_5( "TRTOccWheelEC_6_5");     decEventInfo_occwheelEC_6_5(  *eventInfo ) = TRTOccWheel.at(9).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_6( "TRTOccWheelEC_6_6");     decEventInfo_occwheelEC_6_6(  *eventInfo ) = TRTOccWheel.at(9).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_7( "TRTOccWheelEC_6_7");     decEventInfo_occwheelEC_6_7(  *eventInfo ) = TRTOccWheel.at(9).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_8( "TRTOccWheelEC_6_8");     decEventInfo_occwheelEC_6_8(  *eventInfo ) = TRTOccWheel.at(9).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_9( "TRTOccWheelEC_6_9");     decEventInfo_occwheelEC_6_9(  *eventInfo ) = TRTOccWheel.at(9).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_10("TRTOccWheelEC_6_10");    decEventInfo_occwheelEC_6_10( *eventInfo ) = TRTOccWheel.at(9).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_11("TRTOccWheelEC_6_11");    decEventInfo_occwheelEC_6_11( *eventInfo ) = TRTOccWheel.at(9).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_12("TRTOccWheelEC_6_12");    decEventInfo_occwheelEC_6_12( *eventInfo ) = TRTOccWheel.at(9).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_13("TRTOccWheelEC_6_13");    decEventInfo_occwheelEC_6_13( *eventInfo ) = TRTOccWheel.at(9).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_14("TRTOccWheelEC_6_14");    decEventInfo_occwheelEC_6_14( *eventInfo ) = TRTOccWheel.at(9).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_15("TRTOccWheelEC_6_15");    decEventInfo_occwheelEC_6_15( *eventInfo ) = TRTOccWheel.at(9).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_16("TRTOccWheelEC_6_16");    decEventInfo_occwheelEC_6_16( *eventInfo ) = TRTOccWheel.at(9).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_17("TRTOccWheelEC_6_17");    decEventInfo_occwheelEC_6_17( *eventInfo ) = TRTOccWheel.at(9).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_18("TRTOccWheelEC_6_18");    decEventInfo_occwheelEC_6_18( *eventInfo ) = TRTOccWheel.at(9).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_19("TRTOccWheelEC_6_19");    decEventInfo_occwheelEC_6_19( *eventInfo ) = TRTOccWheel.at(9).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_20("TRTOccWheelEC_6_20");    decEventInfo_occwheelEC_6_20( *eventInfo ) = TRTOccWheel.at(9).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_21("TRTOccWheelEC_6_21");    decEventInfo_occwheelEC_6_21( *eventInfo ) = TRTOccWheel.at(9).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_22("TRTOccWheelEC_6_22");    decEventInfo_occwheelEC_6_22( *eventInfo ) = TRTOccWheel.at(9).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_23("TRTOccWheelEC_6_23");    decEventInfo_occwheelEC_6_23( *eventInfo ) = TRTOccWheel.at(9).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_24("TRTOccWheelEC_6_24");    decEventInfo_occwheelEC_6_24( *eventInfo ) = TRTOccWheel.at(9).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_25("TRTOccWheelEC_6_25");    decEventInfo_occwheelEC_6_25( *eventInfo ) = TRTOccWheel.at(9).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_26("TRTOccWheelEC_6_26");    decEventInfo_occwheelEC_6_26( *eventInfo ) = TRTOccWheel.at(9).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_27("TRTOccWheelEC_6_27");    decEventInfo_occwheelEC_6_27( *eventInfo ) = TRTOccWheel.at(9).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_28("TRTOccWheelEC_6_28");    decEventInfo_occwheelEC_6_28( *eventInfo ) = TRTOccWheel.at(9).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_29("TRTOccWheelEC_6_29");    decEventInfo_occwheelEC_6_29( *eventInfo ) = TRTOccWheel.at(9).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_30("TRTOccWheelEC_6_30");    decEventInfo_occwheelEC_6_30( *eventInfo ) = TRTOccWheel.at(9).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_6_31("TRTOccWheelEC_6_31");    decEventInfo_occwheelEC_6_31( *eventInfo ) = TRTOccWheel.at(9).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_0( "TRTOccWheelEC_7_0");     decEventInfo_occwheelEC_7_0(  *eventInfo ) = TRTOccWheel.at(10).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_1( "TRTOccWheelEC_7_1");     decEventInfo_occwheelEC_7_1(  *eventInfo ) = TRTOccWheel.at(10).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_2( "TRTOccWheelEC_7_2");     decEventInfo_occwheelEC_7_2(  *eventInfo ) = TRTOccWheel.at(10).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_3( "TRTOccWheelEC_7_3");     decEventInfo_occwheelEC_7_3(  *eventInfo ) = TRTOccWheel.at(10).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_4( "TRTOccWheelEC_7_4");     decEventInfo_occwheelEC_7_4(  *eventInfo ) = TRTOccWheel.at(10).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_5( "TRTOccWheelEC_7_5");     decEventInfo_occwheelEC_7_5(  *eventInfo ) = TRTOccWheel.at(10).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_6( "TRTOccWheelEC_7_6");     decEventInfo_occwheelEC_7_6(  *eventInfo ) = TRTOccWheel.at(10).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_7( "TRTOccWheelEC_7_7");     decEventInfo_occwheelEC_7_7(  *eventInfo ) = TRTOccWheel.at(10).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_8( "TRTOccWheelEC_7_8");     decEventInfo_occwheelEC_7_8(  *eventInfo ) = TRTOccWheel.at(10).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_9( "TRTOccWheelEC_7_9");     decEventInfo_occwheelEC_7_9(  *eventInfo ) = TRTOccWheel.at(10).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_10("TRTOccWheelEC_7_10");    decEventInfo_occwheelEC_7_10( *eventInfo ) = TRTOccWheel.at(10).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_11("TRTOccWheelEC_7_11");    decEventInfo_occwheelEC_7_11( *eventInfo ) = TRTOccWheel.at(10).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_12("TRTOccWheelEC_7_12");    decEventInfo_occwheelEC_7_12( *eventInfo ) = TRTOccWheel.at(10).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_13("TRTOccWheelEC_7_13");    decEventInfo_occwheelEC_7_13( *eventInfo ) = TRTOccWheel.at(10).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_14("TRTOccWheelEC_7_14");    decEventInfo_occwheelEC_7_14( *eventInfo ) = TRTOccWheel.at(10).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_15("TRTOccWheelEC_7_15");    decEventInfo_occwheelEC_7_15( *eventInfo ) = TRTOccWheel.at(10).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_16("TRTOccWheelEC_7_16");    decEventInfo_occwheelEC_7_16( *eventInfo ) = TRTOccWheel.at(10).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_17("TRTOccWheelEC_7_17");    decEventInfo_occwheelEC_7_17( *eventInfo ) = TRTOccWheel.at(10).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_18("TRTOccWheelEC_7_18");    decEventInfo_occwheelEC_7_18( *eventInfo ) = TRTOccWheel.at(10).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_19("TRTOccWheelEC_7_19");    decEventInfo_occwheelEC_7_19( *eventInfo ) = TRTOccWheel.at(10).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_20("TRTOccWheelEC_7_20");    decEventInfo_occwheelEC_7_20( *eventInfo ) = TRTOccWheel.at(10).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_21("TRTOccWheelEC_7_21");    decEventInfo_occwheelEC_7_21( *eventInfo ) = TRTOccWheel.at(10).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_22("TRTOccWheelEC_7_22");    decEventInfo_occwheelEC_7_22( *eventInfo ) = TRTOccWheel.at(10).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_23("TRTOccWheelEC_7_23");    decEventInfo_occwheelEC_7_23( *eventInfo ) = TRTOccWheel.at(10).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_24("TRTOccWheelEC_7_24");    decEventInfo_occwheelEC_7_24( *eventInfo ) = TRTOccWheel.at(10).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_25("TRTOccWheelEC_7_25");    decEventInfo_occwheelEC_7_25( *eventInfo ) = TRTOccWheel.at(10).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_26("TRTOccWheelEC_7_26");    decEventInfo_occwheelEC_7_26( *eventInfo ) = TRTOccWheel.at(10).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_27("TRTOccWheelEC_7_27");    decEventInfo_occwheelEC_7_27( *eventInfo ) = TRTOccWheel.at(10).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_28("TRTOccWheelEC_7_28");    decEventInfo_occwheelEC_7_28( *eventInfo ) = TRTOccWheel.at(10).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_29("TRTOccWheelEC_7_29");    decEventInfo_occwheelEC_7_29( *eventInfo ) = TRTOccWheel.at(10).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_30("TRTOccWheelEC_7_30");    decEventInfo_occwheelEC_7_30( *eventInfo ) = TRTOccWheel.at(10).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_7_31("TRTOccWheelEC_7_31");    decEventInfo_occwheelEC_7_31( *eventInfo ) = TRTOccWheel.at(10).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_0( "TRTOccWheelEC_8_0");     decEventInfo_occwheelEC_8_0(  *eventInfo ) = TRTOccWheel.at(11).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_1( "TRTOccWheelEC_8_1");     decEventInfo_occwheelEC_8_1(  *eventInfo ) = TRTOccWheel.at(11).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_2( "TRTOccWheelEC_8_2");     decEventInfo_occwheelEC_8_2(  *eventInfo ) = TRTOccWheel.at(11).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_3( "TRTOccWheelEC_8_3");     decEventInfo_occwheelEC_8_3(  *eventInfo ) = TRTOccWheel.at(11).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_4( "TRTOccWheelEC_8_4");     decEventInfo_occwheelEC_8_4(  *eventInfo ) = TRTOccWheel.at(11).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_5( "TRTOccWheelEC_8_5");     decEventInfo_occwheelEC_8_5(  *eventInfo ) = TRTOccWheel.at(11).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_6( "TRTOccWheelEC_8_6");     decEventInfo_occwheelEC_8_6(  *eventInfo ) = TRTOccWheel.at(11).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_7( "TRTOccWheelEC_8_7");     decEventInfo_occwheelEC_8_7(  *eventInfo ) = TRTOccWheel.at(11).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_8( "TRTOccWheelEC_8_8");     decEventInfo_occwheelEC_8_8(  *eventInfo ) = TRTOccWheel.at(11).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_9( "TRTOccWheelEC_8_9");     decEventInfo_occwheelEC_8_9(  *eventInfo ) = TRTOccWheel.at(11).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_10("TRTOccWheelEC_8_10");    decEventInfo_occwheelEC_8_10( *eventInfo ) = TRTOccWheel.at(11).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_11("TRTOccWheelEC_8_11");    decEventInfo_occwheelEC_8_11( *eventInfo ) = TRTOccWheel.at(11).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_12("TRTOccWheelEC_8_12");    decEventInfo_occwheelEC_8_12( *eventInfo ) = TRTOccWheel.at(11).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_13("TRTOccWheelEC_8_13");    decEventInfo_occwheelEC_8_13( *eventInfo ) = TRTOccWheel.at(11).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_14("TRTOccWheelEC_8_14");    decEventInfo_occwheelEC_8_14( *eventInfo ) = TRTOccWheel.at(11).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_15("TRTOccWheelEC_8_15");    decEventInfo_occwheelEC_8_15( *eventInfo ) = TRTOccWheel.at(11).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_16("TRTOccWheelEC_8_16");    decEventInfo_occwheelEC_8_16( *eventInfo ) = TRTOccWheel.at(11).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_17("TRTOccWheelEC_8_17");    decEventInfo_occwheelEC_8_17( *eventInfo ) = TRTOccWheel.at(11).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_18("TRTOccWheelEC_8_18");    decEventInfo_occwheelEC_8_18( *eventInfo ) = TRTOccWheel.at(11).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_19("TRTOccWheelEC_8_19");    decEventInfo_occwheelEC_8_19( *eventInfo ) = TRTOccWheel.at(11).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_20("TRTOccWheelEC_8_20");    decEventInfo_occwheelEC_8_20( *eventInfo ) = TRTOccWheel.at(11).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_21("TRTOccWheelEC_8_21");    decEventInfo_occwheelEC_8_21( *eventInfo ) = TRTOccWheel.at(11).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_22("TRTOccWheelEC_8_22");    decEventInfo_occwheelEC_8_22( *eventInfo ) = TRTOccWheel.at(11).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_23("TRTOccWheelEC_8_23");    decEventInfo_occwheelEC_8_23( *eventInfo ) = TRTOccWheel.at(11).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_24("TRTOccWheelEC_8_24");    decEventInfo_occwheelEC_8_24( *eventInfo ) = TRTOccWheel.at(11).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_25("TRTOccWheelEC_8_25");    decEventInfo_occwheelEC_8_25( *eventInfo ) = TRTOccWheel.at(11).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_26("TRTOccWheelEC_8_26");    decEventInfo_occwheelEC_8_26( *eventInfo ) = TRTOccWheel.at(11).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_27("TRTOccWheelEC_8_27");    decEventInfo_occwheelEC_8_27( *eventInfo ) = TRTOccWheel.at(11).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_28("TRTOccWheelEC_8_28");    decEventInfo_occwheelEC_8_28( *eventInfo ) = TRTOccWheel.at(11).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_29("TRTOccWheelEC_8_29");    decEventInfo_occwheelEC_8_29( *eventInfo ) = TRTOccWheel.at(11).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_30("TRTOccWheelEC_8_30");    decEventInfo_occwheelEC_8_30( *eventInfo ) = TRTOccWheel.at(11).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_8_31("TRTOccWheelEC_8_31");    decEventInfo_occwheelEC_8_31( *eventInfo ) = TRTOccWheel.at(11).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_0( "TRTOccWheelEC_9_0");     decEventInfo_occwheelEC_9_0(  *eventInfo ) = TRTOccWheel.at(12).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_1( "TRTOccWheelEC_9_1");     decEventInfo_occwheelEC_9_1(  *eventInfo ) = TRTOccWheel.at(12).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_2( "TRTOccWheelEC_9_2");     decEventInfo_occwheelEC_9_2(  *eventInfo ) = TRTOccWheel.at(12).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_3( "TRTOccWheelEC_9_3");     decEventInfo_occwheelEC_9_3(  *eventInfo ) = TRTOccWheel.at(12).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_4( "TRTOccWheelEC_9_4");     decEventInfo_occwheelEC_9_4(  *eventInfo ) = TRTOccWheel.at(12).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_5( "TRTOccWheelEC_9_5");     decEventInfo_occwheelEC_9_5(  *eventInfo ) = TRTOccWheel.at(12).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_6( "TRTOccWheelEC_9_6");     decEventInfo_occwheelEC_9_6(  *eventInfo ) = TRTOccWheel.at(12).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_7( "TRTOccWheelEC_9_7");     decEventInfo_occwheelEC_9_7(  *eventInfo ) = TRTOccWheel.at(12).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_8( "TRTOccWheelEC_9_8");     decEventInfo_occwheelEC_9_8(  *eventInfo ) = TRTOccWheel.at(12).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_9( "TRTOccWheelEC_9_9");     decEventInfo_occwheelEC_9_9(  *eventInfo ) = TRTOccWheel.at(12).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_10("TRTOccWheelEC_9_10");    decEventInfo_occwheelEC_9_10( *eventInfo ) = TRTOccWheel.at(12).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_11("TRTOccWheelEC_9_11");    decEventInfo_occwheelEC_9_11( *eventInfo ) = TRTOccWheel.at(12).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_12("TRTOccWheelEC_9_12");    decEventInfo_occwheelEC_9_12( *eventInfo ) = TRTOccWheel.at(12).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_13("TRTOccWheelEC_9_13");    decEventInfo_occwheelEC_9_13( *eventInfo ) = TRTOccWheel.at(12).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_14("TRTOccWheelEC_9_14");    decEventInfo_occwheelEC_9_14( *eventInfo ) = TRTOccWheel.at(12).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_15("TRTOccWheelEC_9_15");    decEventInfo_occwheelEC_9_15( *eventInfo ) = TRTOccWheel.at(12).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_16("TRTOccWheelEC_9_16");    decEventInfo_occwheelEC_9_16( *eventInfo ) = TRTOccWheel.at(12).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_17("TRTOccWheelEC_9_17");    decEventInfo_occwheelEC_9_17( *eventInfo ) = TRTOccWheel.at(12).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_18("TRTOccWheelEC_9_18");    decEventInfo_occwheelEC_9_18( *eventInfo ) = TRTOccWheel.at(12).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_19("TRTOccWheelEC_9_19");    decEventInfo_occwheelEC_9_19( *eventInfo ) = TRTOccWheel.at(12).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_20("TRTOccWheelEC_9_20");    decEventInfo_occwheelEC_9_20( *eventInfo ) = TRTOccWheel.at(12).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_21("TRTOccWheelEC_9_21");    decEventInfo_occwheelEC_9_21( *eventInfo ) = TRTOccWheel.at(12).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_22("TRTOccWheelEC_9_22");    decEventInfo_occwheelEC_9_22( *eventInfo ) = TRTOccWheel.at(12).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_23("TRTOccWheelEC_9_23");    decEventInfo_occwheelEC_9_23( *eventInfo ) = TRTOccWheel.at(12).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_24("TRTOccWheelEC_9_24");    decEventInfo_occwheelEC_9_24( *eventInfo ) = TRTOccWheel.at(12).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_25("TRTOccWheelEC_9_25");    decEventInfo_occwheelEC_9_25( *eventInfo ) = TRTOccWheel.at(12).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_26("TRTOccWheelEC_9_26");    decEventInfo_occwheelEC_9_26( *eventInfo ) = TRTOccWheel.at(12).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_27("TRTOccWheelEC_9_27");    decEventInfo_occwheelEC_9_27( *eventInfo ) = TRTOccWheel.at(12).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_28("TRTOccWheelEC_9_28");    decEventInfo_occwheelEC_9_28( *eventInfo ) = TRTOccWheel.at(12).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_29("TRTOccWheelEC_9_29");    decEventInfo_occwheelEC_9_29( *eventInfo ) = TRTOccWheel.at(12).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_30("TRTOccWheelEC_9_30");    decEventInfo_occwheelEC_9_30( *eventInfo ) = TRTOccWheel.at(12).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_9_31("TRTOccWheelEC_9_31");    decEventInfo_occwheelEC_9_31( *eventInfo ) = TRTOccWheel.at(12).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_0( "TRTOccWheelEC_10_0");     decEventInfo_occwheelEC_10_0(  *eventInfo ) = TRTOccWheel.at(13).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_1( "TRTOccWheelEC_10_1");     decEventInfo_occwheelEC_10_1(  *eventInfo ) = TRTOccWheel.at(13).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_2( "TRTOccWheelEC_10_2");     decEventInfo_occwheelEC_10_2(  *eventInfo ) = TRTOccWheel.at(13).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_3( "TRTOccWheelEC_10_3");     decEventInfo_occwheelEC_10_3(  *eventInfo ) = TRTOccWheel.at(13).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_4( "TRTOccWheelEC_10_4");     decEventInfo_occwheelEC_10_4(  *eventInfo ) = TRTOccWheel.at(13).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_5( "TRTOccWheelEC_10_5");     decEventInfo_occwheelEC_10_5(  *eventInfo ) = TRTOccWheel.at(13).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_6( "TRTOccWheelEC_10_6");     decEventInfo_occwheelEC_10_6(  *eventInfo ) = TRTOccWheel.at(13).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_7( "TRTOccWheelEC_10_7");     decEventInfo_occwheelEC_10_7(  *eventInfo ) = TRTOccWheel.at(13).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_8( "TRTOccWheelEC_10_8");     decEventInfo_occwheelEC_10_8(  *eventInfo ) = TRTOccWheel.at(13).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_9( "TRTOccWheelEC_10_9");     decEventInfo_occwheelEC_10_9(  *eventInfo ) = TRTOccWheel.at(13).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_10("TRTOccWheelEC_10_10");    decEventInfo_occwheelEC_10_10( *eventInfo ) = TRTOccWheel.at(13).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_11("TRTOccWheelEC_10_11");    decEventInfo_occwheelEC_10_11( *eventInfo ) = TRTOccWheel.at(13).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_12("TRTOccWheelEC_10_12");    decEventInfo_occwheelEC_10_12( *eventInfo ) = TRTOccWheel.at(13).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_13("TRTOccWheelEC_10_13");    decEventInfo_occwheelEC_10_13( *eventInfo ) = TRTOccWheel.at(13).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_14("TRTOccWheelEC_10_14");    decEventInfo_occwheelEC_10_14( *eventInfo ) = TRTOccWheel.at(13).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_15("TRTOccWheelEC_10_15");    decEventInfo_occwheelEC_10_15( *eventInfo ) = TRTOccWheel.at(13).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_16("TRTOccWheelEC_10_16");    decEventInfo_occwheelEC_10_16( *eventInfo ) = TRTOccWheel.at(13).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_17("TRTOccWheelEC_10_17");    decEventInfo_occwheelEC_10_17( *eventInfo ) = TRTOccWheel.at(13).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_18("TRTOccWheelEC_10_18");    decEventInfo_occwheelEC_10_18( *eventInfo ) = TRTOccWheel.at(13).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_19("TRTOccWheelEC_10_19");    decEventInfo_occwheelEC_10_19( *eventInfo ) = TRTOccWheel.at(13).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_20("TRTOccWheelEC_10_20");    decEventInfo_occwheelEC_10_20( *eventInfo ) = TRTOccWheel.at(13).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_21("TRTOccWheelEC_10_21");    decEventInfo_occwheelEC_10_21( *eventInfo ) = TRTOccWheel.at(13).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_22("TRTOccWheelEC_10_22");    decEventInfo_occwheelEC_10_22( *eventInfo ) = TRTOccWheel.at(13).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_23("TRTOccWheelEC_10_23");    decEventInfo_occwheelEC_10_23( *eventInfo ) = TRTOccWheel.at(13).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_24("TRTOccWheelEC_10_24");    decEventInfo_occwheelEC_10_24( *eventInfo ) = TRTOccWheel.at(13).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_25("TRTOccWheelEC_10_25");    decEventInfo_occwheelEC_10_25( *eventInfo ) = TRTOccWheel.at(13).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_26("TRTOccWheelEC_10_26");    decEventInfo_occwheelEC_10_26( *eventInfo ) = TRTOccWheel.at(13).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_27("TRTOccWheelEC_10_27");    decEventInfo_occwheelEC_10_27( *eventInfo ) = TRTOccWheel.at(13).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_28("TRTOccWheelEC_10_28");    decEventInfo_occwheelEC_10_28( *eventInfo ) = TRTOccWheel.at(13).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_29("TRTOccWheelEC_10_29");    decEventInfo_occwheelEC_10_29( *eventInfo ) = TRTOccWheel.at(13).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_30("TRTOccWheelEC_10_30");    decEventInfo_occwheelEC_10_30( *eventInfo ) = TRTOccWheel.at(13).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_10_31("TRTOccWheelEC_10_31");    decEventInfo_occwheelEC_10_31( *eventInfo ) = TRTOccWheel.at(13).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_0( "TRTOccWheelEC_11_0");     decEventInfo_occwheelEC_11_0(  *eventInfo ) = TRTOccWheel.at(14).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_1( "TRTOccWheelEC_11_1");     decEventInfo_occwheelEC_11_1(  *eventInfo ) = TRTOccWheel.at(14).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_2( "TRTOccWheelEC_11_2");     decEventInfo_occwheelEC_11_2(  *eventInfo ) = TRTOccWheel.at(14).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_3( "TRTOccWheelEC_11_3");     decEventInfo_occwheelEC_11_3(  *eventInfo ) = TRTOccWheel.at(14).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_4( "TRTOccWheelEC_11_4");     decEventInfo_occwheelEC_11_4(  *eventInfo ) = TRTOccWheel.at(14).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_5( "TRTOccWheelEC_11_5");     decEventInfo_occwheelEC_11_5(  *eventInfo ) = TRTOccWheel.at(14).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_6( "TRTOccWheelEC_11_6");     decEventInfo_occwheelEC_11_6(  *eventInfo ) = TRTOccWheel.at(14).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_7( "TRTOccWheelEC_11_7");     decEventInfo_occwheelEC_11_7(  *eventInfo ) = TRTOccWheel.at(14).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_8( "TRTOccWheelEC_11_8");     decEventInfo_occwheelEC_11_8(  *eventInfo ) = TRTOccWheel.at(14).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_9( "TRTOccWheelEC_11_9");     decEventInfo_occwheelEC_11_9(  *eventInfo ) = TRTOccWheel.at(14).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_10("TRTOccWheelEC_11_10");    decEventInfo_occwheelEC_11_10( *eventInfo ) = TRTOccWheel.at(14).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_11("TRTOccWheelEC_11_11");    decEventInfo_occwheelEC_11_11( *eventInfo ) = TRTOccWheel.at(14).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_12("TRTOccWheelEC_11_12");    decEventInfo_occwheelEC_11_12( *eventInfo ) = TRTOccWheel.at(14).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_13("TRTOccWheelEC_11_13");    decEventInfo_occwheelEC_11_13( *eventInfo ) = TRTOccWheel.at(14).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_14("TRTOccWheelEC_11_14");    decEventInfo_occwheelEC_11_14( *eventInfo ) = TRTOccWheel.at(14).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_15("TRTOccWheelEC_11_15");    decEventInfo_occwheelEC_11_15( *eventInfo ) = TRTOccWheel.at(14).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_16("TRTOccWheelEC_11_16");    decEventInfo_occwheelEC_11_16( *eventInfo ) = TRTOccWheel.at(14).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_17("TRTOccWheelEC_11_17");    decEventInfo_occwheelEC_11_17( *eventInfo ) = TRTOccWheel.at(14).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_18("TRTOccWheelEC_11_18");    decEventInfo_occwheelEC_11_18( *eventInfo ) = TRTOccWheel.at(14).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_19("TRTOccWheelEC_11_19");    decEventInfo_occwheelEC_11_19( *eventInfo ) = TRTOccWheel.at(14).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_20("TRTOccWheelEC_11_20");    decEventInfo_occwheelEC_11_20( *eventInfo ) = TRTOccWheel.at(14).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_21("TRTOccWheelEC_11_21");    decEventInfo_occwheelEC_11_21( *eventInfo ) = TRTOccWheel.at(14).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_22("TRTOccWheelEC_11_22");    decEventInfo_occwheelEC_11_22( *eventInfo ) = TRTOccWheel.at(14).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_23("TRTOccWheelEC_11_23");    decEventInfo_occwheelEC_11_23( *eventInfo ) = TRTOccWheel.at(14).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_24("TRTOccWheelEC_11_24");    decEventInfo_occwheelEC_11_24( *eventInfo ) = TRTOccWheel.at(14).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_25("TRTOccWheelEC_11_25");    decEventInfo_occwheelEC_11_25( *eventInfo ) = TRTOccWheel.at(14).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_26("TRTOccWheelEC_11_26");    decEventInfo_occwheelEC_11_26( *eventInfo ) = TRTOccWheel.at(14).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_27("TRTOccWheelEC_11_27");    decEventInfo_occwheelEC_11_27( *eventInfo ) = TRTOccWheel.at(14).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_28("TRTOccWheelEC_11_28");    decEventInfo_occwheelEC_11_28( *eventInfo ) = TRTOccWheel.at(14).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_29("TRTOccWheelEC_11_29");    decEventInfo_occwheelEC_11_29( *eventInfo ) = TRTOccWheel.at(14).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_30("TRTOccWheelEC_11_30");    decEventInfo_occwheelEC_11_30( *eventInfo ) = TRTOccWheel.at(14).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_11_31("TRTOccWheelEC_11_31");    decEventInfo_occwheelEC_11_31( *eventInfo ) = TRTOccWheel.at(14).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_0( "TRTOccWheelEC_12_0");     decEventInfo_occwheelEC_12_0(  *eventInfo ) = TRTOccWheel.at(15).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_1( "TRTOccWheelEC_12_1");     decEventInfo_occwheelEC_12_1(  *eventInfo ) = TRTOccWheel.at(15).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_2( "TRTOccWheelEC_12_2");     decEventInfo_occwheelEC_12_2(  *eventInfo ) = TRTOccWheel.at(15).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_3( "TRTOccWheelEC_12_3");     decEventInfo_occwheelEC_12_3(  *eventInfo ) = TRTOccWheel.at(15).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_4( "TRTOccWheelEC_12_4");     decEventInfo_occwheelEC_12_4(  *eventInfo ) = TRTOccWheel.at(15).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_5( "TRTOccWheelEC_12_5");     decEventInfo_occwheelEC_12_5(  *eventInfo ) = TRTOccWheel.at(15).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_6( "TRTOccWheelEC_12_6");     decEventInfo_occwheelEC_12_6(  *eventInfo ) = TRTOccWheel.at(15).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_7( "TRTOccWheelEC_12_7");     decEventInfo_occwheelEC_12_7(  *eventInfo ) = TRTOccWheel.at(15).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_8( "TRTOccWheelEC_12_8");     decEventInfo_occwheelEC_12_8(  *eventInfo ) = TRTOccWheel.at(15).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_9( "TRTOccWheelEC_12_9");     decEventInfo_occwheelEC_12_9(  *eventInfo ) = TRTOccWheel.at(15).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_10("TRTOccWheelEC_12_10");    decEventInfo_occwheelEC_12_10( *eventInfo ) = TRTOccWheel.at(15).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_11("TRTOccWheelEC_12_11");    decEventInfo_occwheelEC_12_11( *eventInfo ) = TRTOccWheel.at(15).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_12("TRTOccWheelEC_12_12");    decEventInfo_occwheelEC_12_12( *eventInfo ) = TRTOccWheel.at(15).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_13("TRTOccWheelEC_12_13");    decEventInfo_occwheelEC_12_13( *eventInfo ) = TRTOccWheel.at(15).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_14("TRTOccWheelEC_12_14");    decEventInfo_occwheelEC_12_14( *eventInfo ) = TRTOccWheel.at(15).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_15("TRTOccWheelEC_12_15");    decEventInfo_occwheelEC_12_15( *eventInfo ) = TRTOccWheel.at(15).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_16("TRTOccWheelEC_12_16");    decEventInfo_occwheelEC_12_16( *eventInfo ) = TRTOccWheel.at(15).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_17("TRTOccWheelEC_12_17");    decEventInfo_occwheelEC_12_17( *eventInfo ) = TRTOccWheel.at(15).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_18("TRTOccWheelEC_12_18");    decEventInfo_occwheelEC_12_18( *eventInfo ) = TRTOccWheel.at(15).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_19("TRTOccWheelEC_12_19");    decEventInfo_occwheelEC_12_19( *eventInfo ) = TRTOccWheel.at(15).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_20("TRTOccWheelEC_12_20");    decEventInfo_occwheelEC_12_20( *eventInfo ) = TRTOccWheel.at(15).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_21("TRTOccWheelEC_12_21");    decEventInfo_occwheelEC_12_21( *eventInfo ) = TRTOccWheel.at(15).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_22("TRTOccWheelEC_12_22");    decEventInfo_occwheelEC_12_22( *eventInfo ) = TRTOccWheel.at(15).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_23("TRTOccWheelEC_12_23");    decEventInfo_occwheelEC_12_23( *eventInfo ) = TRTOccWheel.at(15).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_24("TRTOccWheelEC_12_24");    decEventInfo_occwheelEC_12_24( *eventInfo ) = TRTOccWheel.at(15).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_25("TRTOccWheelEC_12_25");    decEventInfo_occwheelEC_12_25( *eventInfo ) = TRTOccWheel.at(15).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_26("TRTOccWheelEC_12_26");    decEventInfo_occwheelEC_12_26( *eventInfo ) = TRTOccWheel.at(15).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_27("TRTOccWheelEC_12_27");    decEventInfo_occwheelEC_12_27( *eventInfo ) = TRTOccWheel.at(15).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_28("TRTOccWheelEC_12_28");    decEventInfo_occwheelEC_12_28( *eventInfo ) = TRTOccWheel.at(15).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_29("TRTOccWheelEC_12_29");    decEventInfo_occwheelEC_12_29( *eventInfo ) = TRTOccWheel.at(15).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_30("TRTOccWheelEC_12_30");    decEventInfo_occwheelEC_12_30( *eventInfo ) = TRTOccWheel.at(15).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_12_31("TRTOccWheelEC_12_31");    decEventInfo_occwheelEC_12_31( *eventInfo ) = TRTOccWheel.at(15).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_0( "TRTOccWheelEC_13_0");     decEventInfo_occwheelEC_13_0(  *eventInfo ) = TRTOccWheel.at(16).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_1( "TRTOccWheelEC_13_1");     decEventInfo_occwheelEC_13_1(  *eventInfo ) = TRTOccWheel.at(16).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_2( "TRTOccWheelEC_13_2");     decEventInfo_occwheelEC_13_2(  *eventInfo ) = TRTOccWheel.at(16).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_3( "TRTOccWheelEC_13_3");     decEventInfo_occwheelEC_13_3(  *eventInfo ) = TRTOccWheel.at(16).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_4( "TRTOccWheelEC_13_4");     decEventInfo_occwheelEC_13_4(  *eventInfo ) = TRTOccWheel.at(16).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_5( "TRTOccWheelEC_13_5");     decEventInfo_occwheelEC_13_5(  *eventInfo ) = TRTOccWheel.at(16).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_6( "TRTOccWheelEC_13_6");     decEventInfo_occwheelEC_13_6(  *eventInfo ) = TRTOccWheel.at(16).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_7( "TRTOccWheelEC_13_7");     decEventInfo_occwheelEC_13_7(  *eventInfo ) = TRTOccWheel.at(16).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_8( "TRTOccWheelEC_13_8");     decEventInfo_occwheelEC_13_8(  *eventInfo ) = TRTOccWheel.at(16).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_9( "TRTOccWheelEC_13_9");     decEventInfo_occwheelEC_13_9(  *eventInfo ) = TRTOccWheel.at(16).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_10("TRTOccWheelEC_13_10");    decEventInfo_occwheelEC_13_10( *eventInfo ) = TRTOccWheel.at(16).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_11("TRTOccWheelEC_13_11");    decEventInfo_occwheelEC_13_11( *eventInfo ) = TRTOccWheel.at(16).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_12("TRTOccWheelEC_13_12");    decEventInfo_occwheelEC_13_12( *eventInfo ) = TRTOccWheel.at(16).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_13("TRTOccWheelEC_13_13");    decEventInfo_occwheelEC_13_13( *eventInfo ) = TRTOccWheel.at(16).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_14("TRTOccWheelEC_13_14");    decEventInfo_occwheelEC_13_14( *eventInfo ) = TRTOccWheel.at(16).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_15("TRTOccWheelEC_13_15");    decEventInfo_occwheelEC_13_15( *eventInfo ) = TRTOccWheel.at(16).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_16("TRTOccWheelEC_13_16");    decEventInfo_occwheelEC_13_16( *eventInfo ) = TRTOccWheel.at(16).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_17("TRTOccWheelEC_13_17");    decEventInfo_occwheelEC_13_17( *eventInfo ) = TRTOccWheel.at(16).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_18("TRTOccWheelEC_13_18");    decEventInfo_occwheelEC_13_18( *eventInfo ) = TRTOccWheel.at(16).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_19("TRTOccWheelEC_13_19");    decEventInfo_occwheelEC_13_19( *eventInfo ) = TRTOccWheel.at(16).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_20("TRTOccWheelEC_13_20");    decEventInfo_occwheelEC_13_20( *eventInfo ) = TRTOccWheel.at(16).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_21("TRTOccWheelEC_13_21");    decEventInfo_occwheelEC_13_21( *eventInfo ) = TRTOccWheel.at(16).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_22("TRTOccWheelEC_13_22");    decEventInfo_occwheelEC_13_22( *eventInfo ) = TRTOccWheel.at(16).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_23("TRTOccWheelEC_13_23");    decEventInfo_occwheelEC_13_23( *eventInfo ) = TRTOccWheel.at(16).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_24("TRTOccWheelEC_13_24");    decEventInfo_occwheelEC_13_24( *eventInfo ) = TRTOccWheel.at(16).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_25("TRTOccWheelEC_13_25");    decEventInfo_occwheelEC_13_25( *eventInfo ) = TRTOccWheel.at(16).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_26("TRTOccWheelEC_13_26");    decEventInfo_occwheelEC_13_26( *eventInfo ) = TRTOccWheel.at(16).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_27("TRTOccWheelEC_13_27");    decEventInfo_occwheelEC_13_27( *eventInfo ) = TRTOccWheel.at(16).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_28("TRTOccWheelEC_13_28");    decEventInfo_occwheelEC_13_28( *eventInfo ) = TRTOccWheel.at(16).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_29("TRTOccWheelEC_13_29");    decEventInfo_occwheelEC_13_29( *eventInfo ) = TRTOccWheel.at(16).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_30("TRTOccWheelEC_13_30");    decEventInfo_occwheelEC_13_30( *eventInfo ) = TRTOccWheel.at(16).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEC_13_31("TRTOccWheelEC_13_31");    decEventInfo_occwheelEC_13_31( *eventInfo ) = TRTOccWheel.at(16).at(31); 





    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_0( "TRTOccWheelBA_0_0");     decEventInfo_occwheelBA_0_0(  *eventInfo ) = TRTOccWheel.at(17).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_1( "TRTOccWheelBA_0_1");     decEventInfo_occwheelBA_0_1(  *eventInfo ) = TRTOccWheel.at(17).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_2( "TRTOccWheelBA_0_2");     decEventInfo_occwheelBA_0_2(  *eventInfo ) = TRTOccWheel.at(17).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_3( "TRTOccWheelBA_0_3");     decEventInfo_occwheelBA_0_3(  *eventInfo ) = TRTOccWheel.at(17).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_4( "TRTOccWheelBA_0_4");     decEventInfo_occwheelBA_0_4(  *eventInfo ) = TRTOccWheel.at(17).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_5( "TRTOccWheelBA_0_5");     decEventInfo_occwheelBA_0_5(  *eventInfo ) = TRTOccWheel.at(17).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_6( "TRTOccWheelBA_0_6");     decEventInfo_occwheelBA_0_6(  *eventInfo ) = TRTOccWheel.at(17).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_7( "TRTOccWheelBA_0_7");     decEventInfo_occwheelBA_0_7(  *eventInfo ) = TRTOccWheel.at(17).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_8( "TRTOccWheelBA_0_8");     decEventInfo_occwheelBA_0_8(  *eventInfo ) = TRTOccWheel.at(17).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_9( "TRTOccWheelBA_0_9");     decEventInfo_occwheelBA_0_9(  *eventInfo ) = TRTOccWheel.at(17).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_10("TRTOccWheelBA_0_10");    decEventInfo_occwheelBA_0_10( *eventInfo ) = TRTOccWheel.at(17).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_11("TRTOccWheelBA_0_11");    decEventInfo_occwheelBA_0_11( *eventInfo ) = TRTOccWheel.at(17).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_12("TRTOccWheelBA_0_12");    decEventInfo_occwheelBA_0_12( *eventInfo ) = TRTOccWheel.at(17).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_13("TRTOccWheelBA_0_13");    decEventInfo_occwheelBA_0_13( *eventInfo ) = TRTOccWheel.at(17).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_14("TRTOccWheelBA_0_14");    decEventInfo_occwheelBA_0_14( *eventInfo ) = TRTOccWheel.at(17).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_15("TRTOccWheelBA_0_15");    decEventInfo_occwheelBA_0_15( *eventInfo ) = TRTOccWheel.at(17).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_16("TRTOccWheelBA_0_16");    decEventInfo_occwheelBA_0_16( *eventInfo ) = TRTOccWheel.at(17).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_17("TRTOccWheelBA_0_17");    decEventInfo_occwheelBA_0_17( *eventInfo ) = TRTOccWheel.at(17).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_18("TRTOccWheelBA_0_18");    decEventInfo_occwheelBA_0_18( *eventInfo ) = TRTOccWheel.at(17).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_19("TRTOccWheelBA_0_19");    decEventInfo_occwheelBA_0_19( *eventInfo ) = TRTOccWheel.at(17).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_20("TRTOccWheelBA_0_20");    decEventInfo_occwheelBA_0_20( *eventInfo ) = TRTOccWheel.at(17).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_21("TRTOccWheelBA_0_21");    decEventInfo_occwheelBA_0_21( *eventInfo ) = TRTOccWheel.at(17).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_22("TRTOccWheelBA_0_22");    decEventInfo_occwheelBA_0_22( *eventInfo ) = TRTOccWheel.at(17).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_23("TRTOccWheelBA_0_23");    decEventInfo_occwheelBA_0_23( *eventInfo ) = TRTOccWheel.at(17).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_24("TRTOccWheelBA_0_24");    decEventInfo_occwheelBA_0_24( *eventInfo ) = TRTOccWheel.at(17).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_25("TRTOccWheelBA_0_25");    decEventInfo_occwheelBA_0_25( *eventInfo ) = TRTOccWheel.at(17).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_26("TRTOccWheelBA_0_26");    decEventInfo_occwheelBA_0_26( *eventInfo ) = TRTOccWheel.at(17).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_27("TRTOccWheelBA_0_27");    decEventInfo_occwheelBA_0_27( *eventInfo ) = TRTOccWheel.at(17).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_28("TRTOccWheelBA_0_28");    decEventInfo_occwheelBA_0_28( *eventInfo ) = TRTOccWheel.at(17).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_29("TRTOccWheelBA_0_29");    decEventInfo_occwheelBA_0_29( *eventInfo ) = TRTOccWheel.at(17).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_30("TRTOccWheelBA_0_30");    decEventInfo_occwheelBA_0_30( *eventInfo ) = TRTOccWheel.at(17).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_0_31("TRTOccWheelBA_0_31");    decEventInfo_occwheelBA_0_31( *eventInfo ) = TRTOccWheel.at(17).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_0( "TRTOccWheelBA_1_0");     decEventInfo_occwheelBA_1_0(  *eventInfo ) = TRTOccWheel.at(18).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_1( "TRTOccWheelBA_1_1");     decEventInfo_occwheelBA_1_1(  *eventInfo ) = TRTOccWheel.at(18).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_2( "TRTOccWheelBA_1_2");     decEventInfo_occwheelBA_1_2(  *eventInfo ) = TRTOccWheel.at(18).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_3( "TRTOccWheelBA_1_3");     decEventInfo_occwheelBA_1_3(  *eventInfo ) = TRTOccWheel.at(18).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_4( "TRTOccWheelBA_1_4");     decEventInfo_occwheelBA_1_4(  *eventInfo ) = TRTOccWheel.at(18).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_5( "TRTOccWheelBA_1_5");     decEventInfo_occwheelBA_1_5(  *eventInfo ) = TRTOccWheel.at(18).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_6( "TRTOccWheelBA_1_6");     decEventInfo_occwheelBA_1_6(  *eventInfo ) = TRTOccWheel.at(18).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_7( "TRTOccWheelBA_1_7");     decEventInfo_occwheelBA_1_7(  *eventInfo ) = TRTOccWheel.at(18).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_8( "TRTOccWheelBA_1_8");     decEventInfo_occwheelBA_1_8(  *eventInfo ) = TRTOccWheel.at(18).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_9( "TRTOccWheelBA_1_9");     decEventInfo_occwheelBA_1_9(  *eventInfo ) = TRTOccWheel.at(18).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_10("TRTOccWheelBA_1_10");    decEventInfo_occwheelBA_1_10( *eventInfo ) = TRTOccWheel.at(18).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_11("TRTOccWheelBA_1_11");    decEventInfo_occwheelBA_1_11( *eventInfo ) = TRTOccWheel.at(18).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_12("TRTOccWheelBA_1_12");    decEventInfo_occwheelBA_1_12( *eventInfo ) = TRTOccWheel.at(18).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_13("TRTOccWheelBA_1_13");    decEventInfo_occwheelBA_1_13( *eventInfo ) = TRTOccWheel.at(18).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_14("TRTOccWheelBA_1_14");    decEventInfo_occwheelBA_1_14( *eventInfo ) = TRTOccWheel.at(18).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_15("TRTOccWheelBA_1_15");    decEventInfo_occwheelBA_1_15( *eventInfo ) = TRTOccWheel.at(18).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_16("TRTOccWheelBA_1_16");    decEventInfo_occwheelBA_1_16( *eventInfo ) = TRTOccWheel.at(18).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_17("TRTOccWheelBA_1_17");    decEventInfo_occwheelBA_1_17( *eventInfo ) = TRTOccWheel.at(18).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_18("TRTOccWheelBA_1_18");    decEventInfo_occwheelBA_1_18( *eventInfo ) = TRTOccWheel.at(18).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_19("TRTOccWheelBA_1_19");    decEventInfo_occwheelBA_1_19( *eventInfo ) = TRTOccWheel.at(18).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_20("TRTOccWheelBA_1_20");    decEventInfo_occwheelBA_1_20( *eventInfo ) = TRTOccWheel.at(18).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_21("TRTOccWheelBA_1_21");    decEventInfo_occwheelBA_1_21( *eventInfo ) = TRTOccWheel.at(18).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_22("TRTOccWheelBA_1_22");    decEventInfo_occwheelBA_1_22( *eventInfo ) = TRTOccWheel.at(18).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_23("TRTOccWheelBA_1_23");    decEventInfo_occwheelBA_1_23( *eventInfo ) = TRTOccWheel.at(18).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_24("TRTOccWheelBA_1_24");    decEventInfo_occwheelBA_1_24( *eventInfo ) = TRTOccWheel.at(18).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_25("TRTOccWheelBA_1_25");    decEventInfo_occwheelBA_1_25( *eventInfo ) = TRTOccWheel.at(18).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_26("TRTOccWheelBA_1_26");    decEventInfo_occwheelBA_1_26( *eventInfo ) = TRTOccWheel.at(18).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_27("TRTOccWheelBA_1_27");    decEventInfo_occwheelBA_1_27( *eventInfo ) = TRTOccWheel.at(18).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_28("TRTOccWheelBA_1_28");    decEventInfo_occwheelBA_1_28( *eventInfo ) = TRTOccWheel.at(18).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_29("TRTOccWheelBA_1_29");    decEventInfo_occwheelBA_1_29( *eventInfo ) = TRTOccWheel.at(18).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_30("TRTOccWheelBA_1_30");    decEventInfo_occwheelBA_1_30( *eventInfo ) = TRTOccWheel.at(18).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_1_31("TRTOccWheelBA_1_31");    decEventInfo_occwheelBA_1_31( *eventInfo ) = TRTOccWheel.at(18).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_0( "TRTOccWheelBA_2_0");     decEventInfo_occwheelBA_2_0(  *eventInfo ) = TRTOccWheel.at(19).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_1( "TRTOccWheelBA_2_1");     decEventInfo_occwheelBA_2_1(  *eventInfo ) = TRTOccWheel.at(19).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_2( "TRTOccWheelBA_2_2");     decEventInfo_occwheelBA_2_2(  *eventInfo ) = TRTOccWheel.at(19).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_3( "TRTOccWheelBA_2_3");     decEventInfo_occwheelBA_2_3(  *eventInfo ) = TRTOccWheel.at(19).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_4( "TRTOccWheelBA_2_4");     decEventInfo_occwheelBA_2_4(  *eventInfo ) = TRTOccWheel.at(19).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_5( "TRTOccWheelBA_2_5");     decEventInfo_occwheelBA_2_5(  *eventInfo ) = TRTOccWheel.at(19).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_6( "TRTOccWheelBA_2_6");     decEventInfo_occwheelBA_2_6(  *eventInfo ) = TRTOccWheel.at(19).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_7( "TRTOccWheelBA_2_7");     decEventInfo_occwheelBA_2_7(  *eventInfo ) = TRTOccWheel.at(19).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_8( "TRTOccWheelBA_2_8");     decEventInfo_occwheelBA_2_8(  *eventInfo ) = TRTOccWheel.at(19).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_9( "TRTOccWheelBA_2_9");     decEventInfo_occwheelBA_2_9(  *eventInfo ) = TRTOccWheel.at(19).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_10("TRTOccWheelBA_2_10");    decEventInfo_occwheelBA_2_10( *eventInfo ) = TRTOccWheel.at(19).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_11("TRTOccWheelBA_2_11");    decEventInfo_occwheelBA_2_11( *eventInfo ) = TRTOccWheel.at(19).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_12("TRTOccWheelBA_2_12");    decEventInfo_occwheelBA_2_12( *eventInfo ) = TRTOccWheel.at(19).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_13("TRTOccWheelBA_2_13");    decEventInfo_occwheelBA_2_13( *eventInfo ) = TRTOccWheel.at(19).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_14("TRTOccWheelBA_2_14");    decEventInfo_occwheelBA_2_14( *eventInfo ) = TRTOccWheel.at(19).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_15("TRTOccWheelBA_2_15");    decEventInfo_occwheelBA_2_15( *eventInfo ) = TRTOccWheel.at(19).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_16("TRTOccWheelBA_2_16");    decEventInfo_occwheelBA_2_16( *eventInfo ) = TRTOccWheel.at(19).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_17("TRTOccWheelBA_2_17");    decEventInfo_occwheelBA_2_17( *eventInfo ) = TRTOccWheel.at(19).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_18("TRTOccWheelBA_2_18");    decEventInfo_occwheelBA_2_18( *eventInfo ) = TRTOccWheel.at(19).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_19("TRTOccWheelBA_2_19");    decEventInfo_occwheelBA_2_19( *eventInfo ) = TRTOccWheel.at(19).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_20("TRTOccWheelBA_2_20");    decEventInfo_occwheelBA_2_20( *eventInfo ) = TRTOccWheel.at(19).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_21("TRTOccWheelBA_2_21");    decEventInfo_occwheelBA_2_21( *eventInfo ) = TRTOccWheel.at(19).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_22("TRTOccWheelBA_2_22");    decEventInfo_occwheelBA_2_22( *eventInfo ) = TRTOccWheel.at(19).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_23("TRTOccWheelBA_2_23");    decEventInfo_occwheelBA_2_23( *eventInfo ) = TRTOccWheel.at(19).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_24("TRTOccWheelBA_2_24");    decEventInfo_occwheelBA_2_24( *eventInfo ) = TRTOccWheel.at(19).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_25("TRTOccWheelBA_2_25");    decEventInfo_occwheelBA_2_25( *eventInfo ) = TRTOccWheel.at(19).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_26("TRTOccWheelBA_2_26");    decEventInfo_occwheelBA_2_26( *eventInfo ) = TRTOccWheel.at(19).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_27("TRTOccWheelBA_2_27");    decEventInfo_occwheelBA_2_27( *eventInfo ) = TRTOccWheel.at(19).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_28("TRTOccWheelBA_2_28");    decEventInfo_occwheelBA_2_28( *eventInfo ) = TRTOccWheel.at(19).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_29("TRTOccWheelBA_2_29");    decEventInfo_occwheelBA_2_29( *eventInfo ) = TRTOccWheel.at(19).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_30("TRTOccWheelBA_2_30");    decEventInfo_occwheelBA_2_30( *eventInfo ) = TRTOccWheel.at(19).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelBA_2_31("TRTOccWheelBA_2_31");    decEventInfo_occwheelBA_2_31( *eventInfo ) = TRTOccWheel.at(19).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_0( "TRTOccWheelEA_0_0");     decEventInfo_occwheelEA_0_0(  *eventInfo ) = TRTOccWheel.at(20).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_1( "TRTOccWheelEA_0_1");     decEventInfo_occwheelEA_0_1(  *eventInfo ) = TRTOccWheel.at(20).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_2( "TRTOccWheelEA_0_2");     decEventInfo_occwheelEA_0_2(  *eventInfo ) = TRTOccWheel.at(20).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_3( "TRTOccWheelEA_0_3");     decEventInfo_occwheelEA_0_3(  *eventInfo ) = TRTOccWheel.at(20).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_4( "TRTOccWheelEA_0_4");     decEventInfo_occwheelEA_0_4(  *eventInfo ) = TRTOccWheel.at(20).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_5( "TRTOccWheelEA_0_5");     decEventInfo_occwheelEA_0_5(  *eventInfo ) = TRTOccWheel.at(20).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_6( "TRTOccWheelEA_0_6");     decEventInfo_occwheelEA_0_6(  *eventInfo ) = TRTOccWheel.at(20).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_7( "TRTOccWheelEA_0_7");     decEventInfo_occwheelEA_0_7(  *eventInfo ) = TRTOccWheel.at(20).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_8( "TRTOccWheelEA_0_8");     decEventInfo_occwheelEA_0_8(  *eventInfo ) = TRTOccWheel.at(20).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_9( "TRTOccWheelEA_0_9");     decEventInfo_occwheelEA_0_9(  *eventInfo ) = TRTOccWheel.at(20).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_10("TRTOccWheelEA_0_10");    decEventInfo_occwheelEA_0_10( *eventInfo ) = TRTOccWheel.at(20).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_11("TRTOccWheelEA_0_11");    decEventInfo_occwheelEA_0_11( *eventInfo ) = TRTOccWheel.at(20).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_12("TRTOccWheelEA_0_12");    decEventInfo_occwheelEA_0_12( *eventInfo ) = TRTOccWheel.at(20).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_13("TRTOccWheelEA_0_13");    decEventInfo_occwheelEA_0_13( *eventInfo ) = TRTOccWheel.at(20).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_14("TRTOccWheelEA_0_14");    decEventInfo_occwheelEA_0_14( *eventInfo ) = TRTOccWheel.at(20).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_15("TRTOccWheelEA_0_15");    decEventInfo_occwheelEA_0_15( *eventInfo ) = TRTOccWheel.at(20).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_16("TRTOccWheelEA_0_16");    decEventInfo_occwheelEA_0_16( *eventInfo ) = TRTOccWheel.at(20).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_17("TRTOccWheelEA_0_17");    decEventInfo_occwheelEA_0_17( *eventInfo ) = TRTOccWheel.at(20).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_18("TRTOccWheelEA_0_18");    decEventInfo_occwheelEA_0_18( *eventInfo ) = TRTOccWheel.at(20).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_19("TRTOccWheelEA_0_19");    decEventInfo_occwheelEA_0_19( *eventInfo ) = TRTOccWheel.at(20).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_20("TRTOccWheelEA_0_20");    decEventInfo_occwheelEA_0_20( *eventInfo ) = TRTOccWheel.at(20).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_21("TRTOccWheelEA_0_21");    decEventInfo_occwheelEA_0_21( *eventInfo ) = TRTOccWheel.at(20).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_22("TRTOccWheelEA_0_22");    decEventInfo_occwheelEA_0_22( *eventInfo ) = TRTOccWheel.at(20).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_23("TRTOccWheelEA_0_23");    decEventInfo_occwheelEA_0_23( *eventInfo ) = TRTOccWheel.at(20).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_24("TRTOccWheelEA_0_24");    decEventInfo_occwheelEA_0_24( *eventInfo ) = TRTOccWheel.at(20).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_25("TRTOccWheelEA_0_25");    decEventInfo_occwheelEA_0_25( *eventInfo ) = TRTOccWheel.at(20).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_26("TRTOccWheelEA_0_26");    decEventInfo_occwheelEA_0_26( *eventInfo ) = TRTOccWheel.at(20).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_27("TRTOccWheelEA_0_27");    decEventInfo_occwheelEA_0_27( *eventInfo ) = TRTOccWheel.at(20).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_28("TRTOccWheelEA_0_28");    decEventInfo_occwheelEA_0_28( *eventInfo ) = TRTOccWheel.at(20).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_29("TRTOccWheelEA_0_29");    decEventInfo_occwheelEA_0_29( *eventInfo ) = TRTOccWheel.at(20).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_30("TRTOccWheelEA_0_30");    decEventInfo_occwheelEA_0_30( *eventInfo ) = TRTOccWheel.at(20).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_0_31("TRTOccWheelEA_0_31");    decEventInfo_occwheelEA_0_31( *eventInfo ) = TRTOccWheel.at(20).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_0( "TRTOccWheelEA_1_0");     decEventInfo_occwheelEA_1_0(  *eventInfo ) = TRTOccWheel.at(21).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_1( "TRTOccWheelEA_1_1");     decEventInfo_occwheelEA_1_1(  *eventInfo ) = TRTOccWheel.at(21).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_2( "TRTOccWheelEA_1_2");     decEventInfo_occwheelEA_1_2(  *eventInfo ) = TRTOccWheel.at(21).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_3( "TRTOccWheelEA_1_3");     decEventInfo_occwheelEA_1_3(  *eventInfo ) = TRTOccWheel.at(21).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_4( "TRTOccWheelEA_1_4");     decEventInfo_occwheelEA_1_4(  *eventInfo ) = TRTOccWheel.at(21).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_5( "TRTOccWheelEA_1_5");     decEventInfo_occwheelEA_1_5(  *eventInfo ) = TRTOccWheel.at(21).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_6( "TRTOccWheelEA_1_6");     decEventInfo_occwheelEA_1_6(  *eventInfo ) = TRTOccWheel.at(21).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_7( "TRTOccWheelEA_1_7");     decEventInfo_occwheelEA_1_7(  *eventInfo ) = TRTOccWheel.at(21).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_8( "TRTOccWheelEA_1_8");     decEventInfo_occwheelEA_1_8(  *eventInfo ) = TRTOccWheel.at(21).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_9( "TRTOccWheelEA_1_9");     decEventInfo_occwheelEA_1_9(  *eventInfo ) = TRTOccWheel.at(21).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_10("TRTOccWheelEA_1_10");    decEventInfo_occwheelEA_1_10( *eventInfo ) = TRTOccWheel.at(21).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_11("TRTOccWheelEA_1_11");    decEventInfo_occwheelEA_1_11( *eventInfo ) = TRTOccWheel.at(21).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_12("TRTOccWheelEA_1_12");    decEventInfo_occwheelEA_1_12( *eventInfo ) = TRTOccWheel.at(21).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_13("TRTOccWheelEA_1_13");    decEventInfo_occwheelEA_1_13( *eventInfo ) = TRTOccWheel.at(21).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_14("TRTOccWheelEA_1_14");    decEventInfo_occwheelEA_1_14( *eventInfo ) = TRTOccWheel.at(21).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_15("TRTOccWheelEA_1_15");    decEventInfo_occwheelEA_1_15( *eventInfo ) = TRTOccWheel.at(21).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_16("TRTOccWheelEA_1_16");    decEventInfo_occwheelEA_1_16( *eventInfo ) = TRTOccWheel.at(21).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_17("TRTOccWheelEA_1_17");    decEventInfo_occwheelEA_1_17( *eventInfo ) = TRTOccWheel.at(21).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_18("TRTOccWheelEA_1_18");    decEventInfo_occwheelEA_1_18( *eventInfo ) = TRTOccWheel.at(21).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_19("TRTOccWheelEA_1_19");    decEventInfo_occwheelEA_1_19( *eventInfo ) = TRTOccWheel.at(21).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_20("TRTOccWheelEA_1_20");    decEventInfo_occwheelEA_1_20( *eventInfo ) = TRTOccWheel.at(21).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_21("TRTOccWheelEA_1_21");    decEventInfo_occwheelEA_1_21( *eventInfo ) = TRTOccWheel.at(21).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_22("TRTOccWheelEA_1_22");    decEventInfo_occwheelEA_1_22( *eventInfo ) = TRTOccWheel.at(21).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_23("TRTOccWheelEA_1_23");    decEventInfo_occwheelEA_1_23( *eventInfo ) = TRTOccWheel.at(21).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_24("TRTOccWheelEA_1_24");    decEventInfo_occwheelEA_1_24( *eventInfo ) = TRTOccWheel.at(21).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_25("TRTOccWheelEA_1_25");    decEventInfo_occwheelEA_1_25( *eventInfo ) = TRTOccWheel.at(21).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_26("TRTOccWheelEA_1_26");    decEventInfo_occwheelEA_1_26( *eventInfo ) = TRTOccWheel.at(21).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_27("TRTOccWheelEA_1_27");    decEventInfo_occwheelEA_1_27( *eventInfo ) = TRTOccWheel.at(21).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_28("TRTOccWheelEA_1_28");    decEventInfo_occwheelEA_1_28( *eventInfo ) = TRTOccWheel.at(21).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_29("TRTOccWheelEA_1_29");    decEventInfo_occwheelEA_1_29( *eventInfo ) = TRTOccWheel.at(21).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_30("TRTOccWheelEA_1_30");    decEventInfo_occwheelEA_1_30( *eventInfo ) = TRTOccWheel.at(21).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_1_31("TRTOccWheelEA_1_31");    decEventInfo_occwheelEA_1_31( *eventInfo ) = TRTOccWheel.at(21).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_0( "TRTOccWheelEA_2_0");     decEventInfo_occwheelEA_2_0(  *eventInfo ) = TRTOccWheel.at(22).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_1( "TRTOccWheelEA_2_1");     decEventInfo_occwheelEA_2_1(  *eventInfo ) = TRTOccWheel.at(22).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_2( "TRTOccWheelEA_2_2");     decEventInfo_occwheelEA_2_2(  *eventInfo ) = TRTOccWheel.at(22).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_3( "TRTOccWheelEA_2_3");     decEventInfo_occwheelEA_2_3(  *eventInfo ) = TRTOccWheel.at(22).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_4( "TRTOccWheelEA_2_4");     decEventInfo_occwheelEA_2_4(  *eventInfo ) = TRTOccWheel.at(22).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_5( "TRTOccWheelEA_2_5");     decEventInfo_occwheelEA_2_5(  *eventInfo ) = TRTOccWheel.at(22).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_6( "TRTOccWheelEA_2_6");     decEventInfo_occwheelEA_2_6(  *eventInfo ) = TRTOccWheel.at(22).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_7( "TRTOccWheelEA_2_7");     decEventInfo_occwheelEA_2_7(  *eventInfo ) = TRTOccWheel.at(22).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_8( "TRTOccWheelEA_2_8");     decEventInfo_occwheelEA_2_8(  *eventInfo ) = TRTOccWheel.at(22).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_9( "TRTOccWheelEA_2_9");     decEventInfo_occwheelEA_2_9(  *eventInfo ) = TRTOccWheel.at(22).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_10("TRTOccWheelEA_2_10");    decEventInfo_occwheelEA_2_10( *eventInfo ) = TRTOccWheel.at(22).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_11("TRTOccWheelEA_2_11");    decEventInfo_occwheelEA_2_11( *eventInfo ) = TRTOccWheel.at(22).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_12("TRTOccWheelEA_2_12");    decEventInfo_occwheelEA_2_12( *eventInfo ) = TRTOccWheel.at(22).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_13("TRTOccWheelEA_2_13");    decEventInfo_occwheelEA_2_13( *eventInfo ) = TRTOccWheel.at(22).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_14("TRTOccWheelEA_2_14");    decEventInfo_occwheelEA_2_14( *eventInfo ) = TRTOccWheel.at(22).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_15("TRTOccWheelEA_2_15");    decEventInfo_occwheelEA_2_15( *eventInfo ) = TRTOccWheel.at(22).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_16("TRTOccWheelEA_2_16");    decEventInfo_occwheelEA_2_16( *eventInfo ) = TRTOccWheel.at(22).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_17("TRTOccWheelEA_2_17");    decEventInfo_occwheelEA_2_17( *eventInfo ) = TRTOccWheel.at(22).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_18("TRTOccWheelEA_2_18");    decEventInfo_occwheelEA_2_18( *eventInfo ) = TRTOccWheel.at(22).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_19("TRTOccWheelEA_2_19");    decEventInfo_occwheelEA_2_19( *eventInfo ) = TRTOccWheel.at(22).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_20("TRTOccWheelEA_2_20");    decEventInfo_occwheelEA_2_20( *eventInfo ) = TRTOccWheel.at(22).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_21("TRTOccWheelEA_2_21");    decEventInfo_occwheelEA_2_21( *eventInfo ) = TRTOccWheel.at(22).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_22("TRTOccWheelEA_2_22");    decEventInfo_occwheelEA_2_22( *eventInfo ) = TRTOccWheel.at(22).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_23("TRTOccWheelEA_2_23");    decEventInfo_occwheelEA_2_23( *eventInfo ) = TRTOccWheel.at(22).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_24("TRTOccWheelEA_2_24");    decEventInfo_occwheelEA_2_24( *eventInfo ) = TRTOccWheel.at(22).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_25("TRTOccWheelEA_2_25");    decEventInfo_occwheelEA_2_25( *eventInfo ) = TRTOccWheel.at(22).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_26("TRTOccWheelEA_2_26");    decEventInfo_occwheelEA_2_26( *eventInfo ) = TRTOccWheel.at(22).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_27("TRTOccWheelEA_2_27");    decEventInfo_occwheelEA_2_27( *eventInfo ) = TRTOccWheel.at(22).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_28("TRTOccWheelEA_2_28");    decEventInfo_occwheelEA_2_28( *eventInfo ) = TRTOccWheel.at(22).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_29("TRTOccWheelEA_2_29");    decEventInfo_occwheelEA_2_29( *eventInfo ) = TRTOccWheel.at(22).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_30("TRTOccWheelEA_2_30");    decEventInfo_occwheelEA_2_30( *eventInfo ) = TRTOccWheel.at(22).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_2_31("TRTOccWheelEA_2_31");    decEventInfo_occwheelEA_2_31( *eventInfo ) = TRTOccWheel.at(22).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_0( "TRTOccWheelEA_3_0");     decEventInfo_occwheelEA_3_0(  *eventInfo ) = TRTOccWheel.at(23).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_1( "TRTOccWheelEA_3_1");     decEventInfo_occwheelEA_3_1(  *eventInfo ) = TRTOccWheel.at(23).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_2( "TRTOccWheelEA_3_2");     decEventInfo_occwheelEA_3_2(  *eventInfo ) = TRTOccWheel.at(23).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_3( "TRTOccWheelEA_3_3");     decEventInfo_occwheelEA_3_3(  *eventInfo ) = TRTOccWheel.at(23).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_4( "TRTOccWheelEA_3_4");     decEventInfo_occwheelEA_3_4(  *eventInfo ) = TRTOccWheel.at(23).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_5( "TRTOccWheelEA_3_5");     decEventInfo_occwheelEA_3_5(  *eventInfo ) = TRTOccWheel.at(23).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_6( "TRTOccWheelEA_3_6");     decEventInfo_occwheelEA_3_6(  *eventInfo ) = TRTOccWheel.at(23).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_7( "TRTOccWheelEA_3_7");     decEventInfo_occwheelEA_3_7(  *eventInfo ) = TRTOccWheel.at(23).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_8( "TRTOccWheelEA_3_8");     decEventInfo_occwheelEA_3_8(  *eventInfo ) = TRTOccWheel.at(23).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_9( "TRTOccWheelEA_3_9");     decEventInfo_occwheelEA_3_9(  *eventInfo ) = TRTOccWheel.at(23).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_10("TRTOccWheelEA_3_10");    decEventInfo_occwheelEA_3_10( *eventInfo ) = TRTOccWheel.at(23).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_11("TRTOccWheelEA_3_11");    decEventInfo_occwheelEA_3_11( *eventInfo ) = TRTOccWheel.at(23).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_12("TRTOccWheelEA_3_12");    decEventInfo_occwheelEA_3_12( *eventInfo ) = TRTOccWheel.at(23).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_13("TRTOccWheelEA_3_13");    decEventInfo_occwheelEA_3_13( *eventInfo ) = TRTOccWheel.at(23).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_14("TRTOccWheelEA_3_14");    decEventInfo_occwheelEA_3_14( *eventInfo ) = TRTOccWheel.at(23).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_15("TRTOccWheelEA_3_15");    decEventInfo_occwheelEA_3_15( *eventInfo ) = TRTOccWheel.at(23).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_16("TRTOccWheelEA_3_16");    decEventInfo_occwheelEA_3_16( *eventInfo ) = TRTOccWheel.at(23).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_17("TRTOccWheelEA_3_17");    decEventInfo_occwheelEA_3_17( *eventInfo ) = TRTOccWheel.at(23).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_18("TRTOccWheelEA_3_18");    decEventInfo_occwheelEA_3_18( *eventInfo ) = TRTOccWheel.at(23).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_19("TRTOccWheelEA_3_19");    decEventInfo_occwheelEA_3_19( *eventInfo ) = TRTOccWheel.at(23).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_20("TRTOccWheelEA_3_20");    decEventInfo_occwheelEA_3_20( *eventInfo ) = TRTOccWheel.at(23).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_21("TRTOccWheelEA_3_21");    decEventInfo_occwheelEA_3_21( *eventInfo ) = TRTOccWheel.at(23).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_22("TRTOccWheelEA_3_22");    decEventInfo_occwheelEA_3_22( *eventInfo ) = TRTOccWheel.at(23).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_23("TRTOccWheelEA_3_23");    decEventInfo_occwheelEA_3_23( *eventInfo ) = TRTOccWheel.at(23).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_24("TRTOccWheelEA_3_24");    decEventInfo_occwheelEA_3_24( *eventInfo ) = TRTOccWheel.at(23).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_25("TRTOccWheelEA_3_25");    decEventInfo_occwheelEA_3_25( *eventInfo ) = TRTOccWheel.at(23).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_26("TRTOccWheelEA_3_26");    decEventInfo_occwheelEA_3_26( *eventInfo ) = TRTOccWheel.at(23).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_27("TRTOccWheelEA_3_27");    decEventInfo_occwheelEA_3_27( *eventInfo ) = TRTOccWheel.at(23).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_28("TRTOccWheelEA_3_28");    decEventInfo_occwheelEA_3_28( *eventInfo ) = TRTOccWheel.at(23).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_29("TRTOccWheelEA_3_29");    decEventInfo_occwheelEA_3_29( *eventInfo ) = TRTOccWheel.at(23).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_30("TRTOccWheelEA_3_30");    decEventInfo_occwheelEA_3_30( *eventInfo ) = TRTOccWheel.at(23).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_3_31("TRTOccWheelEA_3_31");    decEventInfo_occwheelEA_3_31( *eventInfo ) = TRTOccWheel.at(23).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_0( "TRTOccWheelEA_4_0");     decEventInfo_occwheelEA_4_0(  *eventInfo ) = TRTOccWheel.at(24).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_1( "TRTOccWheelEA_4_1");     decEventInfo_occwheelEA_4_1(  *eventInfo ) = TRTOccWheel.at(24).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_2( "TRTOccWheelEA_4_2");     decEventInfo_occwheelEA_4_2(  *eventInfo ) = TRTOccWheel.at(24).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_3( "TRTOccWheelEA_4_3");     decEventInfo_occwheelEA_4_3(  *eventInfo ) = TRTOccWheel.at(24).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_4( "TRTOccWheelEA_4_4");     decEventInfo_occwheelEA_4_4(  *eventInfo ) = TRTOccWheel.at(24).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_5( "TRTOccWheelEA_4_5");     decEventInfo_occwheelEA_4_5(  *eventInfo ) = TRTOccWheel.at(24).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_6( "TRTOccWheelEA_4_6");     decEventInfo_occwheelEA_4_6(  *eventInfo ) = TRTOccWheel.at(24).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_7( "TRTOccWheelEA_4_7");     decEventInfo_occwheelEA_4_7(  *eventInfo ) = TRTOccWheel.at(24).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_8( "TRTOccWheelEA_4_8");     decEventInfo_occwheelEA_4_8(  *eventInfo ) = TRTOccWheel.at(24).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_9( "TRTOccWheelEA_4_9");     decEventInfo_occwheelEA_4_9(  *eventInfo ) = TRTOccWheel.at(24).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_10("TRTOccWheelEA_4_10");    decEventInfo_occwheelEA_4_10( *eventInfo ) = TRTOccWheel.at(24).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_11("TRTOccWheelEA_4_11");    decEventInfo_occwheelEA_4_11( *eventInfo ) = TRTOccWheel.at(24).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_12("TRTOccWheelEA_4_12");    decEventInfo_occwheelEA_4_12( *eventInfo ) = TRTOccWheel.at(24).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_13("TRTOccWheelEA_4_13");    decEventInfo_occwheelEA_4_13( *eventInfo ) = TRTOccWheel.at(24).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_14("TRTOccWheelEA_4_14");    decEventInfo_occwheelEA_4_14( *eventInfo ) = TRTOccWheel.at(24).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_15("TRTOccWheelEA_4_15");    decEventInfo_occwheelEA_4_15( *eventInfo ) = TRTOccWheel.at(24).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_16("TRTOccWheelEA_4_16");    decEventInfo_occwheelEA_4_16( *eventInfo ) = TRTOccWheel.at(24).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_17("TRTOccWheelEA_4_17");    decEventInfo_occwheelEA_4_17( *eventInfo ) = TRTOccWheel.at(24).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_18("TRTOccWheelEA_4_18");    decEventInfo_occwheelEA_4_18( *eventInfo ) = TRTOccWheel.at(24).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_19("TRTOccWheelEA_4_19");    decEventInfo_occwheelEA_4_19( *eventInfo ) = TRTOccWheel.at(24).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_20("TRTOccWheelEA_4_20");    decEventInfo_occwheelEA_4_20( *eventInfo ) = TRTOccWheel.at(24).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_21("TRTOccWheelEA_4_21");    decEventInfo_occwheelEA_4_21( *eventInfo ) = TRTOccWheel.at(24).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_22("TRTOccWheelEA_4_22");    decEventInfo_occwheelEA_4_22( *eventInfo ) = TRTOccWheel.at(24).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_23("TRTOccWheelEA_4_23");    decEventInfo_occwheelEA_4_23( *eventInfo ) = TRTOccWheel.at(24).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_24("TRTOccWheelEA_4_24");    decEventInfo_occwheelEA_4_24( *eventInfo ) = TRTOccWheel.at(24).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_25("TRTOccWheelEA_4_25");    decEventInfo_occwheelEA_4_25( *eventInfo ) = TRTOccWheel.at(24).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_26("TRTOccWheelEA_4_26");    decEventInfo_occwheelEA_4_26( *eventInfo ) = TRTOccWheel.at(24).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_27("TRTOccWheelEA_4_27");    decEventInfo_occwheelEA_4_27( *eventInfo ) = TRTOccWheel.at(24).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_28("TRTOccWheelEA_4_28");    decEventInfo_occwheelEA_4_28( *eventInfo ) = TRTOccWheel.at(24).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_29("TRTOccWheelEA_4_29");    decEventInfo_occwheelEA_4_29( *eventInfo ) = TRTOccWheel.at(24).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_30("TRTOccWheelEA_4_30");    decEventInfo_occwheelEA_4_30( *eventInfo ) = TRTOccWheel.at(24).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_4_31("TRTOccWheelEA_4_31");    decEventInfo_occwheelEA_4_31( *eventInfo ) = TRTOccWheel.at(24).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_0( "TRTOccWheelEA_5_0");     decEventInfo_occwheelEA_5_0(  *eventInfo ) = TRTOccWheel.at(25).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_1( "TRTOccWheelEA_5_1");     decEventInfo_occwheelEA_5_1(  *eventInfo ) = TRTOccWheel.at(25).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_2( "TRTOccWheelEA_5_2");     decEventInfo_occwheelEA_5_2(  *eventInfo ) = TRTOccWheel.at(25).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_3( "TRTOccWheelEA_5_3");     decEventInfo_occwheelEA_5_3(  *eventInfo ) = TRTOccWheel.at(25).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_4( "TRTOccWheelEA_5_4");     decEventInfo_occwheelEA_5_4(  *eventInfo ) = TRTOccWheel.at(25).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_5( "TRTOccWheelEA_5_5");     decEventInfo_occwheelEA_5_5(  *eventInfo ) = TRTOccWheel.at(25).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_6( "TRTOccWheelEA_5_6");     decEventInfo_occwheelEA_5_6(  *eventInfo ) = TRTOccWheel.at(25).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_7( "TRTOccWheelEA_5_7");     decEventInfo_occwheelEA_5_7(  *eventInfo ) = TRTOccWheel.at(25).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_8( "TRTOccWheelEA_5_8");     decEventInfo_occwheelEA_5_8(  *eventInfo ) = TRTOccWheel.at(25).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_9( "TRTOccWheelEA_5_9");     decEventInfo_occwheelEA_5_9(  *eventInfo ) = TRTOccWheel.at(25).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_10("TRTOccWheelEA_5_10");    decEventInfo_occwheelEA_5_10( *eventInfo ) = TRTOccWheel.at(25).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_11("TRTOccWheelEA_5_11");    decEventInfo_occwheelEA_5_11( *eventInfo ) = TRTOccWheel.at(25).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_12("TRTOccWheelEA_5_12");    decEventInfo_occwheelEA_5_12( *eventInfo ) = TRTOccWheel.at(25).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_13("TRTOccWheelEA_5_13");    decEventInfo_occwheelEA_5_13( *eventInfo ) = TRTOccWheel.at(25).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_14("TRTOccWheelEA_5_14");    decEventInfo_occwheelEA_5_14( *eventInfo ) = TRTOccWheel.at(25).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_15("TRTOccWheelEA_5_15");    decEventInfo_occwheelEA_5_15( *eventInfo ) = TRTOccWheel.at(25).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_16("TRTOccWheelEA_5_16");    decEventInfo_occwheelEA_5_16( *eventInfo ) = TRTOccWheel.at(25).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_17("TRTOccWheelEA_5_17");    decEventInfo_occwheelEA_5_17( *eventInfo ) = TRTOccWheel.at(25).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_18("TRTOccWheelEA_5_18");    decEventInfo_occwheelEA_5_18( *eventInfo ) = TRTOccWheel.at(25).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_19("TRTOccWheelEA_5_19");    decEventInfo_occwheelEA_5_19( *eventInfo ) = TRTOccWheel.at(25).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_20("TRTOccWheelEA_5_20");    decEventInfo_occwheelEA_5_20( *eventInfo ) = TRTOccWheel.at(25).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_21("TRTOccWheelEA_5_21");    decEventInfo_occwheelEA_5_21( *eventInfo ) = TRTOccWheel.at(25).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_22("TRTOccWheelEA_5_22");    decEventInfo_occwheelEA_5_22( *eventInfo ) = TRTOccWheel.at(25).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_23("TRTOccWheelEA_5_23");    decEventInfo_occwheelEA_5_23( *eventInfo ) = TRTOccWheel.at(25).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_24("TRTOccWheelEA_5_24");    decEventInfo_occwheelEA_5_24( *eventInfo ) = TRTOccWheel.at(25).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_25("TRTOccWheelEA_5_25");    decEventInfo_occwheelEA_5_25( *eventInfo ) = TRTOccWheel.at(25).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_26("TRTOccWheelEA_5_26");    decEventInfo_occwheelEA_5_26( *eventInfo ) = TRTOccWheel.at(25).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_27("TRTOccWheelEA_5_27");    decEventInfo_occwheelEA_5_27( *eventInfo ) = TRTOccWheel.at(25).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_28("TRTOccWheelEA_5_28");    decEventInfo_occwheelEA_5_28( *eventInfo ) = TRTOccWheel.at(25).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_29("TRTOccWheelEA_5_29");    decEventInfo_occwheelEA_5_29( *eventInfo ) = TRTOccWheel.at(25).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_30("TRTOccWheelEA_5_30");    decEventInfo_occwheelEA_5_30( *eventInfo ) = TRTOccWheel.at(25).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_5_31("TRTOccWheelEA_5_31");    decEventInfo_occwheelEA_5_31( *eventInfo ) = TRTOccWheel.at(25).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_0( "TRTOccWheelEA_6_0");     decEventInfo_occwheelEA_6_0(  *eventInfo ) = TRTOccWheel.at(26).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_1( "TRTOccWheelEA_6_1");     decEventInfo_occwheelEA_6_1(  *eventInfo ) = TRTOccWheel.at(26).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_2( "TRTOccWheelEA_6_2");     decEventInfo_occwheelEA_6_2(  *eventInfo ) = TRTOccWheel.at(26).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_3( "TRTOccWheelEA_6_3");     decEventInfo_occwheelEA_6_3(  *eventInfo ) = TRTOccWheel.at(26).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_4( "TRTOccWheelEA_6_4");     decEventInfo_occwheelEA_6_4(  *eventInfo ) = TRTOccWheel.at(26).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_5( "TRTOccWheelEA_6_5");     decEventInfo_occwheelEA_6_5(  *eventInfo ) = TRTOccWheel.at(26).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_6( "TRTOccWheelEA_6_6");     decEventInfo_occwheelEA_6_6(  *eventInfo ) = TRTOccWheel.at(26).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_7( "TRTOccWheelEA_6_7");     decEventInfo_occwheelEA_6_7(  *eventInfo ) = TRTOccWheel.at(26).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_8( "TRTOccWheelEA_6_8");     decEventInfo_occwheelEA_6_8(  *eventInfo ) = TRTOccWheel.at(26).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_9( "TRTOccWheelEA_6_9");     decEventInfo_occwheelEA_6_9(  *eventInfo ) = TRTOccWheel.at(26).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_10("TRTOccWheelEA_6_10");    decEventInfo_occwheelEA_6_10( *eventInfo ) = TRTOccWheel.at(26).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_11("TRTOccWheelEA_6_11");    decEventInfo_occwheelEA_6_11( *eventInfo ) = TRTOccWheel.at(26).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_12("TRTOccWheelEA_6_12");    decEventInfo_occwheelEA_6_12( *eventInfo ) = TRTOccWheel.at(26).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_13("TRTOccWheelEA_6_13");    decEventInfo_occwheelEA_6_13( *eventInfo ) = TRTOccWheel.at(26).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_14("TRTOccWheelEA_6_14");    decEventInfo_occwheelEA_6_14( *eventInfo ) = TRTOccWheel.at(26).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_15("TRTOccWheelEA_6_15");    decEventInfo_occwheelEA_6_15( *eventInfo ) = TRTOccWheel.at(26).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_16("TRTOccWheelEA_6_16");    decEventInfo_occwheelEA_6_16( *eventInfo ) = TRTOccWheel.at(26).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_17("TRTOccWheelEA_6_17");    decEventInfo_occwheelEA_6_17( *eventInfo ) = TRTOccWheel.at(26).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_18("TRTOccWheelEA_6_18");    decEventInfo_occwheelEA_6_18( *eventInfo ) = TRTOccWheel.at(26).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_19("TRTOccWheelEA_6_19");    decEventInfo_occwheelEA_6_19( *eventInfo ) = TRTOccWheel.at(26).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_20("TRTOccWheelEA_6_20");    decEventInfo_occwheelEA_6_20( *eventInfo ) = TRTOccWheel.at(26).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_21("TRTOccWheelEA_6_21");    decEventInfo_occwheelEA_6_21( *eventInfo ) = TRTOccWheel.at(26).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_22("TRTOccWheelEA_6_22");    decEventInfo_occwheelEA_6_22( *eventInfo ) = TRTOccWheel.at(26).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_23("TRTOccWheelEA_6_23");    decEventInfo_occwheelEA_6_23( *eventInfo ) = TRTOccWheel.at(26).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_24("TRTOccWheelEA_6_24");    decEventInfo_occwheelEA_6_24( *eventInfo ) = TRTOccWheel.at(26).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_25("TRTOccWheelEA_6_25");    decEventInfo_occwheelEA_6_25( *eventInfo ) = TRTOccWheel.at(26).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_26("TRTOccWheelEA_6_26");    decEventInfo_occwheelEA_6_26( *eventInfo ) = TRTOccWheel.at(26).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_27("TRTOccWheelEA_6_27");    decEventInfo_occwheelEA_6_27( *eventInfo ) = TRTOccWheel.at(26).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_28("TRTOccWheelEA_6_28");    decEventInfo_occwheelEA_6_28( *eventInfo ) = TRTOccWheel.at(26).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_29("TRTOccWheelEA_6_29");    decEventInfo_occwheelEA_6_29( *eventInfo ) = TRTOccWheel.at(26).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_30("TRTOccWheelEA_6_30");    decEventInfo_occwheelEA_6_30( *eventInfo ) = TRTOccWheel.at(26).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_6_31("TRTOccWheelEA_6_31");    decEventInfo_occwheelEA_6_31( *eventInfo ) = TRTOccWheel.at(26).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_0( "TRTOccWheelEA_7_0");     decEventInfo_occwheelEA_7_0(  *eventInfo ) = TRTOccWheel.at(27).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_1( "TRTOccWheelEA_7_1");     decEventInfo_occwheelEA_7_1(  *eventInfo ) = TRTOccWheel.at(27).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_2( "TRTOccWheelEA_7_2");     decEventInfo_occwheelEA_7_2(  *eventInfo ) = TRTOccWheel.at(27).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_3( "TRTOccWheelEA_7_3");     decEventInfo_occwheelEA_7_3(  *eventInfo ) = TRTOccWheel.at(27).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_4( "TRTOccWheelEA_7_4");     decEventInfo_occwheelEA_7_4(  *eventInfo ) = TRTOccWheel.at(27).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_5( "TRTOccWheelEA_7_5");     decEventInfo_occwheelEA_7_5(  *eventInfo ) = TRTOccWheel.at(27).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_6( "TRTOccWheelEA_7_6");     decEventInfo_occwheelEA_7_6(  *eventInfo ) = TRTOccWheel.at(27).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_7( "TRTOccWheelEA_7_7");     decEventInfo_occwheelEA_7_7(  *eventInfo ) = TRTOccWheel.at(27).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_8( "TRTOccWheelEA_7_8");     decEventInfo_occwheelEA_7_8(  *eventInfo ) = TRTOccWheel.at(27).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_9( "TRTOccWheelEA_7_9");     decEventInfo_occwheelEA_7_9(  *eventInfo ) = TRTOccWheel.at(27).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_10("TRTOccWheelEA_7_10");    decEventInfo_occwheelEA_7_10( *eventInfo ) = TRTOccWheel.at(27).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_11("TRTOccWheelEA_7_11");    decEventInfo_occwheelEA_7_11( *eventInfo ) = TRTOccWheel.at(27).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_12("TRTOccWheelEA_7_12");    decEventInfo_occwheelEA_7_12( *eventInfo ) = TRTOccWheel.at(27).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_13("TRTOccWheelEA_7_13");    decEventInfo_occwheelEA_7_13( *eventInfo ) = TRTOccWheel.at(27).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_14("TRTOccWheelEA_7_14");    decEventInfo_occwheelEA_7_14( *eventInfo ) = TRTOccWheel.at(27).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_15("TRTOccWheelEA_7_15");    decEventInfo_occwheelEA_7_15( *eventInfo ) = TRTOccWheel.at(27).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_16("TRTOccWheelEA_7_16");    decEventInfo_occwheelEA_7_16( *eventInfo ) = TRTOccWheel.at(27).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_17("TRTOccWheelEA_7_17");    decEventInfo_occwheelEA_7_17( *eventInfo ) = TRTOccWheel.at(27).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_18("TRTOccWheelEA_7_18");    decEventInfo_occwheelEA_7_18( *eventInfo ) = TRTOccWheel.at(27).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_19("TRTOccWheelEA_7_19");    decEventInfo_occwheelEA_7_19( *eventInfo ) = TRTOccWheel.at(27).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_20("TRTOccWheelEA_7_20");    decEventInfo_occwheelEA_7_20( *eventInfo ) = TRTOccWheel.at(27).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_21("TRTOccWheelEA_7_21");    decEventInfo_occwheelEA_7_21( *eventInfo ) = TRTOccWheel.at(27).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_22("TRTOccWheelEA_7_22");    decEventInfo_occwheelEA_7_22( *eventInfo ) = TRTOccWheel.at(27).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_23("TRTOccWheelEA_7_23");    decEventInfo_occwheelEA_7_23( *eventInfo ) = TRTOccWheel.at(27).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_24("TRTOccWheelEA_7_24");    decEventInfo_occwheelEA_7_24( *eventInfo ) = TRTOccWheel.at(27).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_25("TRTOccWheelEA_7_25");    decEventInfo_occwheelEA_7_25( *eventInfo ) = TRTOccWheel.at(27).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_26("TRTOccWheelEA_7_26");    decEventInfo_occwheelEA_7_26( *eventInfo ) = TRTOccWheel.at(27).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_27("TRTOccWheelEA_7_27");    decEventInfo_occwheelEA_7_27( *eventInfo ) = TRTOccWheel.at(27).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_28("TRTOccWheelEA_7_28");    decEventInfo_occwheelEA_7_28( *eventInfo ) = TRTOccWheel.at(27).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_29("TRTOccWheelEA_7_29");    decEventInfo_occwheelEA_7_29( *eventInfo ) = TRTOccWheel.at(27).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_30("TRTOccWheelEA_7_30");    decEventInfo_occwheelEA_7_30( *eventInfo ) = TRTOccWheel.at(27).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_7_31("TRTOccWheelEA_7_31");    decEventInfo_occwheelEA_7_31( *eventInfo ) = TRTOccWheel.at(27).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_0( "TRTOccWheelEA_8_0");     decEventInfo_occwheelEA_8_0(  *eventInfo ) = TRTOccWheel.at(28).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_1( "TRTOccWheelEA_8_1");     decEventInfo_occwheelEA_8_1(  *eventInfo ) = TRTOccWheel.at(28).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_2( "TRTOccWheelEA_8_2");     decEventInfo_occwheelEA_8_2(  *eventInfo ) = TRTOccWheel.at(28).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_3( "TRTOccWheelEA_8_3");     decEventInfo_occwheelEA_8_3(  *eventInfo ) = TRTOccWheel.at(28).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_4( "TRTOccWheelEA_8_4");     decEventInfo_occwheelEA_8_4(  *eventInfo ) = TRTOccWheel.at(28).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_5( "TRTOccWheelEA_8_5");     decEventInfo_occwheelEA_8_5(  *eventInfo ) = TRTOccWheel.at(28).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_6( "TRTOccWheelEA_8_6");     decEventInfo_occwheelEA_8_6(  *eventInfo ) = TRTOccWheel.at(28).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_7( "TRTOccWheelEA_8_7");     decEventInfo_occwheelEA_8_7(  *eventInfo ) = TRTOccWheel.at(28).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_8( "TRTOccWheelEA_8_8");     decEventInfo_occwheelEA_8_8(  *eventInfo ) = TRTOccWheel.at(28).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_9( "TRTOccWheelEA_8_9");     decEventInfo_occwheelEA_8_9(  *eventInfo ) = TRTOccWheel.at(28).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_10("TRTOccWheelEA_8_10");    decEventInfo_occwheelEA_8_10( *eventInfo ) = TRTOccWheel.at(28).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_11("TRTOccWheelEA_8_11");    decEventInfo_occwheelEA_8_11( *eventInfo ) = TRTOccWheel.at(28).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_12("TRTOccWheelEA_8_12");    decEventInfo_occwheelEA_8_12( *eventInfo ) = TRTOccWheel.at(28).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_13("TRTOccWheelEA_8_13");    decEventInfo_occwheelEA_8_13( *eventInfo ) = TRTOccWheel.at(28).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_14("TRTOccWheelEA_8_14");    decEventInfo_occwheelEA_8_14( *eventInfo ) = TRTOccWheel.at(28).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_15("TRTOccWheelEA_8_15");    decEventInfo_occwheelEA_8_15( *eventInfo ) = TRTOccWheel.at(28).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_16("TRTOccWheelEA_8_16");    decEventInfo_occwheelEA_8_16( *eventInfo ) = TRTOccWheel.at(28).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_17("TRTOccWheelEA_8_17");    decEventInfo_occwheelEA_8_17( *eventInfo ) = TRTOccWheel.at(28).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_18("TRTOccWheelEA_8_18");    decEventInfo_occwheelEA_8_18( *eventInfo ) = TRTOccWheel.at(28).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_19("TRTOccWheelEA_8_19");    decEventInfo_occwheelEA_8_19( *eventInfo ) = TRTOccWheel.at(28).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_20("TRTOccWheelEA_8_20");    decEventInfo_occwheelEA_8_20( *eventInfo ) = TRTOccWheel.at(28).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_21("TRTOccWheelEA_8_21");    decEventInfo_occwheelEA_8_21( *eventInfo ) = TRTOccWheel.at(28).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_22("TRTOccWheelEA_8_22");    decEventInfo_occwheelEA_8_22( *eventInfo ) = TRTOccWheel.at(28).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_23("TRTOccWheelEA_8_23");    decEventInfo_occwheelEA_8_23( *eventInfo ) = TRTOccWheel.at(28).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_24("TRTOccWheelEA_8_24");    decEventInfo_occwheelEA_8_24( *eventInfo ) = TRTOccWheel.at(28).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_25("TRTOccWheelEA_8_25");    decEventInfo_occwheelEA_8_25( *eventInfo ) = TRTOccWheel.at(28).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_26("TRTOccWheelEA_8_26");    decEventInfo_occwheelEA_8_26( *eventInfo ) = TRTOccWheel.at(28).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_27("TRTOccWheelEA_8_27");    decEventInfo_occwheelEA_8_27( *eventInfo ) = TRTOccWheel.at(28).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_28("TRTOccWheelEA_8_28");    decEventInfo_occwheelEA_8_28( *eventInfo ) = TRTOccWheel.at(28).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_29("TRTOccWheelEA_8_29");    decEventInfo_occwheelEA_8_29( *eventInfo ) = TRTOccWheel.at(28).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_30("TRTOccWheelEA_8_30");    decEventInfo_occwheelEA_8_30( *eventInfo ) = TRTOccWheel.at(28).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_8_31("TRTOccWheelEA_8_31");    decEventInfo_occwheelEA_8_31( *eventInfo ) = TRTOccWheel.at(28).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_0( "TRTOccWheelEA_9_0");     decEventInfo_occwheelEA_9_0(  *eventInfo ) = TRTOccWheel.at(29).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_1( "TRTOccWheelEA_9_1");     decEventInfo_occwheelEA_9_1(  *eventInfo ) = TRTOccWheel.at(29).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_2( "TRTOccWheelEA_9_2");     decEventInfo_occwheelEA_9_2(  *eventInfo ) = TRTOccWheel.at(29).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_3( "TRTOccWheelEA_9_3");     decEventInfo_occwheelEA_9_3(  *eventInfo ) = TRTOccWheel.at(29).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_4( "TRTOccWheelEA_9_4");     decEventInfo_occwheelEA_9_4(  *eventInfo ) = TRTOccWheel.at(29).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_5( "TRTOccWheelEA_9_5");     decEventInfo_occwheelEA_9_5(  *eventInfo ) = TRTOccWheel.at(29).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_6( "TRTOccWheelEA_9_6");     decEventInfo_occwheelEA_9_6(  *eventInfo ) = TRTOccWheel.at(29).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_7( "TRTOccWheelEA_9_7");     decEventInfo_occwheelEA_9_7(  *eventInfo ) = TRTOccWheel.at(29).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_8( "TRTOccWheelEA_9_8");     decEventInfo_occwheelEA_9_8(  *eventInfo ) = TRTOccWheel.at(29).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_9( "TRTOccWheelEA_9_9");     decEventInfo_occwheelEA_9_9(  *eventInfo ) = TRTOccWheel.at(29).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_10("TRTOccWheelEA_9_10");    decEventInfo_occwheelEA_9_10( *eventInfo ) = TRTOccWheel.at(29).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_11("TRTOccWheelEA_9_11");    decEventInfo_occwheelEA_9_11( *eventInfo ) = TRTOccWheel.at(29).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_12("TRTOccWheelEA_9_12");    decEventInfo_occwheelEA_9_12( *eventInfo ) = TRTOccWheel.at(29).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_13("TRTOccWheelEA_9_13");    decEventInfo_occwheelEA_9_13( *eventInfo ) = TRTOccWheel.at(29).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_14("TRTOccWheelEA_9_14");    decEventInfo_occwheelEA_9_14( *eventInfo ) = TRTOccWheel.at(29).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_15("TRTOccWheelEA_9_15");    decEventInfo_occwheelEA_9_15( *eventInfo ) = TRTOccWheel.at(29).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_16("TRTOccWheelEA_9_16");    decEventInfo_occwheelEA_9_16( *eventInfo ) = TRTOccWheel.at(29).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_17("TRTOccWheelEA_9_17");    decEventInfo_occwheelEA_9_17( *eventInfo ) = TRTOccWheel.at(29).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_18("TRTOccWheelEA_9_18");    decEventInfo_occwheelEA_9_18( *eventInfo ) = TRTOccWheel.at(29).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_19("TRTOccWheelEA_9_19");    decEventInfo_occwheelEA_9_19( *eventInfo ) = TRTOccWheel.at(29).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_20("TRTOccWheelEA_9_20");    decEventInfo_occwheelEA_9_20( *eventInfo ) = TRTOccWheel.at(29).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_21("TRTOccWheelEA_9_21");    decEventInfo_occwheelEA_9_21( *eventInfo ) = TRTOccWheel.at(29).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_22("TRTOccWheelEA_9_22");    decEventInfo_occwheelEA_9_22( *eventInfo ) = TRTOccWheel.at(29).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_23("TRTOccWheelEA_9_23");    decEventInfo_occwheelEA_9_23( *eventInfo ) = TRTOccWheel.at(29).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_24("TRTOccWheelEA_9_24");    decEventInfo_occwheelEA_9_24( *eventInfo ) = TRTOccWheel.at(29).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_25("TRTOccWheelEA_9_25");    decEventInfo_occwheelEA_9_25( *eventInfo ) = TRTOccWheel.at(29).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_26("TRTOccWheelEA_9_26");    decEventInfo_occwheelEA_9_26( *eventInfo ) = TRTOccWheel.at(29).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_27("TRTOccWheelEA_9_27");    decEventInfo_occwheelEA_9_27( *eventInfo ) = TRTOccWheel.at(29).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_28("TRTOccWheelEA_9_28");    decEventInfo_occwheelEA_9_28( *eventInfo ) = TRTOccWheel.at(29).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_29("TRTOccWheelEA_9_29");    decEventInfo_occwheelEA_9_29( *eventInfo ) = TRTOccWheel.at(29).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_30("TRTOccWheelEA_9_30");    decEventInfo_occwheelEA_9_30( *eventInfo ) = TRTOccWheel.at(29).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_9_31("TRTOccWheelEA_9_31");    decEventInfo_occwheelEA_9_31( *eventInfo ) = TRTOccWheel.at(29).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_0( "TRTOccWheelEA_10_0");     decEventInfo_occwheelEA_10_0(  *eventInfo ) = TRTOccWheel.at(30).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_1( "TRTOccWheelEA_10_1");     decEventInfo_occwheelEA_10_1(  *eventInfo ) = TRTOccWheel.at(30).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_2( "TRTOccWheelEA_10_2");     decEventInfo_occwheelEA_10_2(  *eventInfo ) = TRTOccWheel.at(30).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_3( "TRTOccWheelEA_10_3");     decEventInfo_occwheelEA_10_3(  *eventInfo ) = TRTOccWheel.at(30).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_4( "TRTOccWheelEA_10_4");     decEventInfo_occwheelEA_10_4(  *eventInfo ) = TRTOccWheel.at(30).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_5( "TRTOccWheelEA_10_5");     decEventInfo_occwheelEA_10_5(  *eventInfo ) = TRTOccWheel.at(30).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_6( "TRTOccWheelEA_10_6");     decEventInfo_occwheelEA_10_6(  *eventInfo ) = TRTOccWheel.at(30).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_7( "TRTOccWheelEA_10_7");     decEventInfo_occwheelEA_10_7(  *eventInfo ) = TRTOccWheel.at(30).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_8( "TRTOccWheelEA_10_8");     decEventInfo_occwheelEA_10_8(  *eventInfo ) = TRTOccWheel.at(30).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_9( "TRTOccWheelEA_10_9");     decEventInfo_occwheelEA_10_9(  *eventInfo ) = TRTOccWheel.at(30).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_10("TRTOccWheelEA_10_10");    decEventInfo_occwheelEA_10_10( *eventInfo ) = TRTOccWheel.at(30).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_11("TRTOccWheelEA_10_11");    decEventInfo_occwheelEA_10_11( *eventInfo ) = TRTOccWheel.at(30).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_12("TRTOccWheelEA_10_12");    decEventInfo_occwheelEA_10_12( *eventInfo ) = TRTOccWheel.at(30).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_13("TRTOccWheelEA_10_13");    decEventInfo_occwheelEA_10_13( *eventInfo ) = TRTOccWheel.at(30).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_14("TRTOccWheelEA_10_14");    decEventInfo_occwheelEA_10_14( *eventInfo ) = TRTOccWheel.at(30).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_15("TRTOccWheelEA_10_15");    decEventInfo_occwheelEA_10_15( *eventInfo ) = TRTOccWheel.at(30).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_16("TRTOccWheelEA_10_16");    decEventInfo_occwheelEA_10_16( *eventInfo ) = TRTOccWheel.at(30).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_17("TRTOccWheelEA_10_17");    decEventInfo_occwheelEA_10_17( *eventInfo ) = TRTOccWheel.at(30).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_18("TRTOccWheelEA_10_18");    decEventInfo_occwheelEA_10_18( *eventInfo ) = TRTOccWheel.at(30).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_19("TRTOccWheelEA_10_19");    decEventInfo_occwheelEA_10_19( *eventInfo ) = TRTOccWheel.at(30).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_20("TRTOccWheelEA_10_20");    decEventInfo_occwheelEA_10_20( *eventInfo ) = TRTOccWheel.at(30).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_21("TRTOccWheelEA_10_21");    decEventInfo_occwheelEA_10_21( *eventInfo ) = TRTOccWheel.at(30).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_22("TRTOccWheelEA_10_22");    decEventInfo_occwheelEA_10_22( *eventInfo ) = TRTOccWheel.at(30).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_23("TRTOccWheelEA_10_23");    decEventInfo_occwheelEA_10_23( *eventInfo ) = TRTOccWheel.at(30).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_24("TRTOccWheelEA_10_24");    decEventInfo_occwheelEA_10_24( *eventInfo ) = TRTOccWheel.at(30).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_25("TRTOccWheelEA_10_25");    decEventInfo_occwheelEA_10_25( *eventInfo ) = TRTOccWheel.at(30).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_26("TRTOccWheelEA_10_26");    decEventInfo_occwheelEA_10_26( *eventInfo ) = TRTOccWheel.at(30).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_27("TRTOccWheelEA_10_27");    decEventInfo_occwheelEA_10_27( *eventInfo ) = TRTOccWheel.at(30).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_28("TRTOccWheelEA_10_28");    decEventInfo_occwheelEA_10_28( *eventInfo ) = TRTOccWheel.at(30).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_29("TRTOccWheelEA_10_29");    decEventInfo_occwheelEA_10_29( *eventInfo ) = TRTOccWheel.at(30).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_30("TRTOccWheelEA_10_30");    decEventInfo_occwheelEA_10_30( *eventInfo ) = TRTOccWheel.at(30).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_10_31("TRTOccWheelEA_10_31");    decEventInfo_occwheelEA_10_31( *eventInfo ) = TRTOccWheel.at(30).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_0( "TRTOccWheelEA_11_0");     decEventInfo_occwheelEA_11_0(  *eventInfo ) = TRTOccWheel.at(31).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_1( "TRTOccWheelEA_11_1");     decEventInfo_occwheelEA_11_1(  *eventInfo ) = TRTOccWheel.at(31).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_2( "TRTOccWheelEA_11_2");     decEventInfo_occwheelEA_11_2(  *eventInfo ) = TRTOccWheel.at(31).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_3( "TRTOccWheelEA_11_3");     decEventInfo_occwheelEA_11_3(  *eventInfo ) = TRTOccWheel.at(31).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_4( "TRTOccWheelEA_11_4");     decEventInfo_occwheelEA_11_4(  *eventInfo ) = TRTOccWheel.at(31).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_5( "TRTOccWheelEA_11_5");     decEventInfo_occwheelEA_11_5(  *eventInfo ) = TRTOccWheel.at(31).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_6( "TRTOccWheelEA_11_6");     decEventInfo_occwheelEA_11_6(  *eventInfo ) = TRTOccWheel.at(31).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_7( "TRTOccWheelEA_11_7");     decEventInfo_occwheelEA_11_7(  *eventInfo ) = TRTOccWheel.at(31).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_8( "TRTOccWheelEA_11_8");     decEventInfo_occwheelEA_11_8(  *eventInfo ) = TRTOccWheel.at(31).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_9( "TRTOccWheelEA_11_9");     decEventInfo_occwheelEA_11_9(  *eventInfo ) = TRTOccWheel.at(31).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_10("TRTOccWheelEA_11_10");    decEventInfo_occwheelEA_11_10( *eventInfo ) = TRTOccWheel.at(31).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_11("TRTOccWheelEA_11_11");    decEventInfo_occwheelEA_11_11( *eventInfo ) = TRTOccWheel.at(31).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_12("TRTOccWheelEA_11_12");    decEventInfo_occwheelEA_11_12( *eventInfo ) = TRTOccWheel.at(31).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_13("TRTOccWheelEA_11_13");    decEventInfo_occwheelEA_11_13( *eventInfo ) = TRTOccWheel.at(31).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_14("TRTOccWheelEA_11_14");    decEventInfo_occwheelEA_11_14( *eventInfo ) = TRTOccWheel.at(31).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_15("TRTOccWheelEA_11_15");    decEventInfo_occwheelEA_11_15( *eventInfo ) = TRTOccWheel.at(31).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_16("TRTOccWheelEA_11_16");    decEventInfo_occwheelEA_11_16( *eventInfo ) = TRTOccWheel.at(31).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_17("TRTOccWheelEA_11_17");    decEventInfo_occwheelEA_11_17( *eventInfo ) = TRTOccWheel.at(31).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_18("TRTOccWheelEA_11_18");    decEventInfo_occwheelEA_11_18( *eventInfo ) = TRTOccWheel.at(31).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_19("TRTOccWheelEA_11_19");    decEventInfo_occwheelEA_11_19( *eventInfo ) = TRTOccWheel.at(31).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_20("TRTOccWheelEA_11_20");    decEventInfo_occwheelEA_11_20( *eventInfo ) = TRTOccWheel.at(31).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_21("TRTOccWheelEA_11_21");    decEventInfo_occwheelEA_11_21( *eventInfo ) = TRTOccWheel.at(31).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_22("TRTOccWheelEA_11_22");    decEventInfo_occwheelEA_11_22( *eventInfo ) = TRTOccWheel.at(31).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_23("TRTOccWheelEA_11_23");    decEventInfo_occwheelEA_11_23( *eventInfo ) = TRTOccWheel.at(31).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_24("TRTOccWheelEA_11_24");    decEventInfo_occwheelEA_11_24( *eventInfo ) = TRTOccWheel.at(31).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_25("TRTOccWheelEA_11_25");    decEventInfo_occwheelEA_11_25( *eventInfo ) = TRTOccWheel.at(31).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_26("TRTOccWheelEA_11_26");    decEventInfo_occwheelEA_11_26( *eventInfo ) = TRTOccWheel.at(31).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_27("TRTOccWheelEA_11_27");    decEventInfo_occwheelEA_11_27( *eventInfo ) = TRTOccWheel.at(31).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_28("TRTOccWheelEA_11_28");    decEventInfo_occwheelEA_11_28( *eventInfo ) = TRTOccWheel.at(31).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_29("TRTOccWheelEA_11_29");    decEventInfo_occwheelEA_11_29( *eventInfo ) = TRTOccWheel.at(31).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_30("TRTOccWheelEA_11_30");    decEventInfo_occwheelEA_11_30( *eventInfo ) = TRTOccWheel.at(31).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_11_31("TRTOccWheelEA_11_31");    decEventInfo_occwheelEA_11_31( *eventInfo ) = TRTOccWheel.at(31).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_0( "TRTOccWheelEA_12_0");     decEventInfo_occwheelEA_12_0(  *eventInfo ) = TRTOccWheel.at(32).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_1( "TRTOccWheelEA_12_1");     decEventInfo_occwheelEA_12_1(  *eventInfo ) = TRTOccWheel.at(32).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_2( "TRTOccWheelEA_12_2");     decEventInfo_occwheelEA_12_2(  *eventInfo ) = TRTOccWheel.at(32).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_3( "TRTOccWheelEA_12_3");     decEventInfo_occwheelEA_12_3(  *eventInfo ) = TRTOccWheel.at(32).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_4( "TRTOccWheelEA_12_4");     decEventInfo_occwheelEA_12_4(  *eventInfo ) = TRTOccWheel.at(32).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_5( "TRTOccWheelEA_12_5");     decEventInfo_occwheelEA_12_5(  *eventInfo ) = TRTOccWheel.at(32).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_6( "TRTOccWheelEA_12_6");     decEventInfo_occwheelEA_12_6(  *eventInfo ) = TRTOccWheel.at(32).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_7( "TRTOccWheelEA_12_7");     decEventInfo_occwheelEA_12_7(  *eventInfo ) = TRTOccWheel.at(32).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_8( "TRTOccWheelEA_12_8");     decEventInfo_occwheelEA_12_8(  *eventInfo ) = TRTOccWheel.at(32).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_9( "TRTOccWheelEA_12_9");     decEventInfo_occwheelEA_12_9(  *eventInfo ) = TRTOccWheel.at(32).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_10("TRTOccWheelEA_12_10");    decEventInfo_occwheelEA_12_10( *eventInfo ) = TRTOccWheel.at(32).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_11("TRTOccWheelEA_12_11");    decEventInfo_occwheelEA_12_11( *eventInfo ) = TRTOccWheel.at(32).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_12("TRTOccWheelEA_12_12");    decEventInfo_occwheelEA_12_12( *eventInfo ) = TRTOccWheel.at(32).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_13("TRTOccWheelEA_12_13");    decEventInfo_occwheelEA_12_13( *eventInfo ) = TRTOccWheel.at(32).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_14("TRTOccWheelEA_12_14");    decEventInfo_occwheelEA_12_14( *eventInfo ) = TRTOccWheel.at(32).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_15("TRTOccWheelEA_12_15");    decEventInfo_occwheelEA_12_15( *eventInfo ) = TRTOccWheel.at(32).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_16("TRTOccWheelEA_12_16");    decEventInfo_occwheelEA_12_16( *eventInfo ) = TRTOccWheel.at(32).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_17("TRTOccWheelEA_12_17");    decEventInfo_occwheelEA_12_17( *eventInfo ) = TRTOccWheel.at(32).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_18("TRTOccWheelEA_12_18");    decEventInfo_occwheelEA_12_18( *eventInfo ) = TRTOccWheel.at(32).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_19("TRTOccWheelEA_12_19");    decEventInfo_occwheelEA_12_19( *eventInfo ) = TRTOccWheel.at(32).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_20("TRTOccWheelEA_12_20");    decEventInfo_occwheelEA_12_20( *eventInfo ) = TRTOccWheel.at(32).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_21("TRTOccWheelEA_12_21");    decEventInfo_occwheelEA_12_21( *eventInfo ) = TRTOccWheel.at(32).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_22("TRTOccWheelEA_12_22");    decEventInfo_occwheelEA_12_22( *eventInfo ) = TRTOccWheel.at(32).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_23("TRTOccWheelEA_12_23");    decEventInfo_occwheelEA_12_23( *eventInfo ) = TRTOccWheel.at(32).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_24("TRTOccWheelEA_12_24");    decEventInfo_occwheelEA_12_24( *eventInfo ) = TRTOccWheel.at(32).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_25("TRTOccWheelEA_12_25");    decEventInfo_occwheelEA_12_25( *eventInfo ) = TRTOccWheel.at(32).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_26("TRTOccWheelEA_12_26");    decEventInfo_occwheelEA_12_26( *eventInfo ) = TRTOccWheel.at(32).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_27("TRTOccWheelEA_12_27");    decEventInfo_occwheelEA_12_27( *eventInfo ) = TRTOccWheel.at(32).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_28("TRTOccWheelEA_12_28");    decEventInfo_occwheelEA_12_28( *eventInfo ) = TRTOccWheel.at(32).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_29("TRTOccWheelEA_12_29");    decEventInfo_occwheelEA_12_29( *eventInfo ) = TRTOccWheel.at(32).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_30("TRTOccWheelEA_12_30");    decEventInfo_occwheelEA_12_30( *eventInfo ) = TRTOccWheel.at(32).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_12_31("TRTOccWheelEA_12_31");    decEventInfo_occwheelEA_12_31( *eventInfo ) = TRTOccWheel.at(32).at(31); 



    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_0( "TRTOccWheelEA_13_0");     decEventInfo_occwheelEA_13_0(  *eventInfo ) = TRTOccWheel.at(33).at(0); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_1( "TRTOccWheelEA_13_1");     decEventInfo_occwheelEA_13_1(  *eventInfo ) = TRTOccWheel.at(33).at(1); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_2( "TRTOccWheelEA_13_2");     decEventInfo_occwheelEA_13_2(  *eventInfo ) = TRTOccWheel.at(33).at(2);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_3( "TRTOccWheelEA_13_3");     decEventInfo_occwheelEA_13_3(  *eventInfo ) = TRTOccWheel.at(33).at(3);  
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_4( "TRTOccWheelEA_13_4");     decEventInfo_occwheelEA_13_4(  *eventInfo ) = TRTOccWheel.at(33).at(4); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_5( "TRTOccWheelEA_13_5");     decEventInfo_occwheelEA_13_5(  *eventInfo ) = TRTOccWheel.at(33).at(5); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_6( "TRTOccWheelEA_13_6");     decEventInfo_occwheelEA_13_6(  *eventInfo ) = TRTOccWheel.at(33).at(6); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_7( "TRTOccWheelEA_13_7");     decEventInfo_occwheelEA_13_7(  *eventInfo ) = TRTOccWheel.at(33).at(7); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_8( "TRTOccWheelEA_13_8");     decEventInfo_occwheelEA_13_8(  *eventInfo ) = TRTOccWheel.at(33).at(8); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_9( "TRTOccWheelEA_13_9");     decEventInfo_occwheelEA_13_9(  *eventInfo ) = TRTOccWheel.at(33).at(9); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_10("TRTOccWheelEA_13_10");    decEventInfo_occwheelEA_13_10( *eventInfo ) = TRTOccWheel.at(33).at(10); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_11("TRTOccWheelEA_13_11");    decEventInfo_occwheelEA_13_11( *eventInfo ) = TRTOccWheel.at(33).at(11); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_12("TRTOccWheelEA_13_12");    decEventInfo_occwheelEA_13_12( *eventInfo ) = TRTOccWheel.at(33).at(12); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_13("TRTOccWheelEA_13_13");    decEventInfo_occwheelEA_13_13( *eventInfo ) = TRTOccWheel.at(33).at(13); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_14("TRTOccWheelEA_13_14");    decEventInfo_occwheelEA_13_14( *eventInfo ) = TRTOccWheel.at(33).at(14); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_15("TRTOccWheelEA_13_15");    decEventInfo_occwheelEA_13_15( *eventInfo ) = TRTOccWheel.at(33).at(15); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_16("TRTOccWheelEA_13_16");    decEventInfo_occwheelEA_13_16( *eventInfo ) = TRTOccWheel.at(33).at(16); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_17("TRTOccWheelEA_13_17");    decEventInfo_occwheelEA_13_17( *eventInfo ) = TRTOccWheel.at(33).at(17); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_18("TRTOccWheelEA_13_18");    decEventInfo_occwheelEA_13_18( *eventInfo ) = TRTOccWheel.at(33).at(18); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_19("TRTOccWheelEA_13_19");    decEventInfo_occwheelEA_13_19( *eventInfo ) = TRTOccWheel.at(33).at(19); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_20("TRTOccWheelEA_13_20");    decEventInfo_occwheelEA_13_20( *eventInfo ) = TRTOccWheel.at(33).at(20); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_21("TRTOccWheelEA_13_21");    decEventInfo_occwheelEA_13_21( *eventInfo ) = TRTOccWheel.at(33).at(21); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_22("TRTOccWheelEA_13_22");    decEventInfo_occwheelEA_13_22( *eventInfo ) = TRTOccWheel.at(33).at(22); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_23("TRTOccWheelEA_13_23");    decEventInfo_occwheelEA_13_23( *eventInfo ) = TRTOccWheel.at(33).at(23); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_24("TRTOccWheelEA_13_24");    decEventInfo_occwheelEA_13_24( *eventInfo ) = TRTOccWheel.at(33).at(24); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_25("TRTOccWheelEA_13_25");    decEventInfo_occwheelEA_13_25( *eventInfo ) = TRTOccWheel.at(33).at(25); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_26("TRTOccWheelEA_13_26");    decEventInfo_occwheelEA_13_26( *eventInfo ) = TRTOccWheel.at(33).at(26); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_27("TRTOccWheelEA_13_27");    decEventInfo_occwheelEA_13_27( *eventInfo ) = TRTOccWheel.at(33).at(27); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_28("TRTOccWheelEA_13_28");    decEventInfo_occwheelEA_13_28( *eventInfo ) = TRTOccWheel.at(33).at(28); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_29("TRTOccWheelEA_13_29");    decEventInfo_occwheelEA_13_29( *eventInfo ) = TRTOccWheel.at(33).at(29); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_30("TRTOccWheelEA_13_30");    decEventInfo_occwheelEA_13_30( *eventInfo ) = TRTOccWheel.at(33).at(30); 
    static SG::AuxElement::Decorator< float >  decEventInfo_occwheelEA_13_31("TRTOccWheelEA_13_31");    decEventInfo_occwheelEA_13_31( *eventInfo ) = TRTOccWheel.at(33).at(31); 



  }

  return StatusCode::SUCCESS;
}

//---------------------------------------------------------------------

StatusCode TRTOccupancyInclude::finalize()
{
  msg(MSG::INFO) << "finalise()" << endreq;
  return StatusCode::SUCCESS;
}
