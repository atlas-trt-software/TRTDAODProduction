
Reco_tf.py  \
--inputESDFile='./myESD.pool.root' \
    --ignoreErrors 'True'  \
    --maxEvents '10' \
    --AMI 'f716' \
    --postExec 'all:conddb.addOverride("/TRT/Calib/ToT/ToTValue","TRTCalibToTDataValue-000-03") ; conddb.addOverride("/TRT/Calib/ToT/ToTVectors","TRTCalibToTDataVectors-000-03") ' \
    --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False);  InDetDxAODFlags.ThinTrackSelection.set_Value_and_Lock("InDetTrackParticles.pt > 0.1*GeV && abs(InDetTrackParticles.eta) < 2.0") ' \
     --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD.pool.root 

