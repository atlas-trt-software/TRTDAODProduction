# #
# cd $TestArea
# cd ../build
# cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
# cd ../run

pathena --trf " Reco_tf.py  \
 --inputBSFile=%IN \
    --ignoreErrors 'True'  \
    --conditionsTag 'CONDBR2-BLKPA-2016-12' \
    --maxEvents '200' \
--skipEvents '%SKIPEVENTS' \
    --postExec 'all:conddb.addOverride(\"/TRT/Calib/ToT/ToTValue\",\"TRTCalibToTDataValue-000-03\") ; conddb.addOverride(\"/TRT/Calib/ToT/ToTVectors\",\"TRTCalibToTDataVectors-000-03\") ' \
    --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False);  InDetDxAODFlags.ThinTrackSelection.set_Value_and_Lock(\"InDetTrackParticles.pt > 0.1*GeV && abs(InDetTrackParticles.eta) < 2.0\") ' \
     --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
    --official \
    --nFilesPerJob 1 \
    --nEventsPerJob 200 \
    --inDS mc15_valid.00200010.overlay_streamsAll_2016_pp_1.skim.DRAW.r8381 \
    --outDS group.det-indet.00200010.overlay_streamsAll_2016_pp_1.skim.TRTxAOD.r8381_trt103-02 \



cd $TestArea

cd ../run