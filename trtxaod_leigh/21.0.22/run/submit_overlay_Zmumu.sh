
pathena --trf \
"Reco_tf.py \
--inputESDFile %IN \
--ignoreErrors True \
--maxEvents '500' \
--skipEvents='%SKIPEVENTS' \
--conditionsTag 'CONDBR2-BLKPA-2016-12' \
--geometryVersion ATLAS-R2-2015-03-01-00 \
--ignorePatterns 'L1TopoMenuLoader.+ERROR.' \
--postInclude 'all:EventOverlayJobTransforms/Rt_override_CONDBR2-BLKPA-2015-12.py' \
--autoConfiguration \"everything\" \
--preInclude \
'EventOverlayJobTransforms/custom.py,EventOverlayJobTransforms/recotrfpre.py' \
--postExec \
'all:from IOVDbSvc.CondDB import conddb; conddb.blockFolder(\"/TRT/Cond/StatusHT\"); conddb.addFolderWithTag(\"TRT_OFL\",\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\",force=True,forceMC=True); conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\"); conddb.blockFolder(\"/TRT/Calib/MC/RT\");  conddb.addFolderWithTag(\"TRT_OFL\",\"/TRT/Calib/MC/RT\",\"TrtCalibRt-13TeV_MC-ArLT150-scenario5_00-01\",force=True,forceData=True); conddb.blockFolder(\"/TRT/Calib/MC/T0\");  conddb.addFolderWithTag(\"TRT_OFL\",\"/TRT/Calib/MC/T0\",\"TrtCalibT0-13TeV_MC-ArLT150-scenario5_00-01\",force=True,forceData=True) ' \
--preExec \
'all:from AthenaCommon.GlobalFlags  import globalflags;globalflags.isOverlay.set_Value_and_Lock(True);from LArConditionsCommon.LArCondFlags import larCondFlags;larCondFlags.OFCShapeFolder.set_Value_and_Lock(\"4samples1phase\");rec.doTrigger=False; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTruthInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
    --inDS mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.recon.ESD.e3601_d1436_r9576 \
    --outDS group.det-indet.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.TRTxAOD.e3601_d1436_r9576_trt103-02 \
    --official \
    --nFilesPerJob 1 \
    --nEventsPerJob 500