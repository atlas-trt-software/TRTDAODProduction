2015-10-25 Alex Alonso 
  	* Turn on the TRT error scaling by default. 
	* tag as TrkRIO_OnTrackCreator-01-02-02



2015-09-29 Alex Alonso 
  	* Add handle to turn off the TRT scaling 
  	* Off by default. 
	* tag as TrkRIO_OnTrackCreator-01-02-01

2015-01-02 Anthony Morley
  * Allow for mu dependent error scaling in the TRT
	* tag as TrkRIO_OnTrackCreator-01-02-00

2014-11-11 Goetz Gaycken
	* Use different error scaling factors for pixel clusters in the IBL.
	  The error scaling tool now expects additional  conditions data in /Indet/TrkErrorScaling
	  for IBL named "PixPhi IBL" and "PixEta IBL". If this data is not present
	  no error scaling is applied for pixel clusters measured in the IBL.

2014-10-10 Antonio Boveia
        * Added option "overrideDatabaseID" to override database
          lookup for inner detector, replacing any error rescaling
          values found in COOL with values specified with job options
          (overrideScale{Pix,SCT,TRT})
        * replace cout statements with Athena messages
        * tag as TrkRIO_OnTrackCreator-01-00-03
	
2014-09-15 Shaun Roe
	* remove checkreq warning
	* tag as TrkRIO_OnTrackCreator-01-00-02
2013-10-19 Wolfgang Liebig
        * RIO_OnTrackErrorScalingTool: fix filling of symmetric matrix
	* tag as TrkRIO_OnTrackCreator-01-00-01

2013-08-21 Niels van Eldik
	* move to eigen
        * tag as TrkRIO_OnTrackCreator-01-00-00

2011-04-26  Wolfgang Liebig
         * react to coverity's disapproval of sprintf().
         * tag as TrkRIO_OnTrackCreator-00-07-15

2010-12-08 Wolfgang Liebig
         * protect dump function against empty vectors
         * tag as TrkRIO_OnTrackCreator-00-07-14

2010-05-05 Wolfgang Liebig
         * cmt/requirements: stop cmt warning with obsolete pattern
         * tag as TrkRIO_OnTrackCreator-00-07-13

2009-10-10 Wolfgang Liebig
         * src/RIO_OnTrackErrorScalingTool: stop compiler warning related to
           unused call-back macro variable I
         * tag as TrkRIO_OnTrackCreator-00-07-12

2009-10-07 Wolfgang Liebig
         * share/make_MuonTrkError.py: correct cut-and-paste mistake
         * src/RIO_OnTrackErrorScalingTool: downgrade warning when ID folder not found
           (to enable Muon jobs run w/o warning)
         * tag as TrkRIO_OnTrackCreator-00-07-11

2009-09-30 Carl Gwilliam
	 * Do not load or use Pixel/SCT/TRT tools if corresponding subdetector is off
	 * Tag as TrkRIO_OnTrackCreator-00-07-10

2009-04-02 Wolfgang Liebig <http://consult.cern.ch/xwho/people/485812>
         * ROTcreator: retrieve ID helper from detStore, const pointer
         * tag TrkRIO_OnTrackCreator-00-07-09

2009-02-14 Wolfgang Liebig <http://consult.cern.ch/xwho/people/485812>
         * fixes against gcc 3.4 warnings
         * tag TrkRIO_OnTrackCreator-00-07-08

2009-01-21 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * cleanup produced unchecked statuscode: fix it.
         * tag TrkRIO_OnTrackCreator-00-07-07

2009-01-16 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * ErrorScalingTool: adjusted "if msgLvl(VERBOSE) msg()" pattern
	 * RIO_OnTrackCreator: stricter forward declaration
	 * tag TrkRIO_OnTrackCreator-00-07-06
	
2009-01-09 Elsing
         * cleanup
         * tag TrkRIO_OnTrackCreator-00-07-05

2007-11-05  Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * adapt ToolHandle default to InDet tools, which migrate to
           InDet namespace
         * tag TrkRIO_OnTrackCreator-00-07-04

2007-06-21   Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * changes to doxygen comments
         * tag TrkRIO_OnTrackCreator-00-07-03

2007-06-10   Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * adapt error scaling scripts under share/make*.py to COOL200
         * tag TrkRIO_OnTrackCreator-00-07-02

2007-04-05   Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * fix typo, tag TrkRIO_OnTrackCreator-00-07-01

2006-12-07   Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * fix a couple of checkreq warnings -> tag -14

2006-12-04   Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * protect callback registration to first check the existence
           of the IOVDbSvc folder
         * tag TrkRIO_OnTrackCreator-00-06-13

2006-11-30   Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * upgrade code to use database and COOL callback
           (infrastructure provided by R.Hawkings)  

2006-10-27   Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * avoid unwanted correlation term: add constant term
           in quadrature
         * repair code for muons
         * tag TrkRIO_OnTrackCreator-00-06-11

2006-10-25   Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * RIO_OnTrackErrorScalingTool: extend scaling strategy and
           add independent constand term to the scaled error.
           Needs new priv members and change in internal methods.
         * IRIO_OnTrackErrorScalingTool: add distinction between eta
           and phi hits for RPC/TGC/CSC
         * tag TrkRIO_OnTrackCreator-00-06-10

2006-04-26  Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * gcc3.4 compatibility (cmath) - tag 00-06-09

2006-04-05  Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * fix SCT endcap error scaling
         * tag TrkRIO_OnTrackCreator-00-06-07

2006-03-07 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * improve MsgStream usage
         * gcc3.4 compatibility - tag 00-06-06

2005-10-10  Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * adapt error scaling too to 1-dim SCT barrel clusters
         * bug fixes and more output to error scaling tool
         * tag TrkRIO_OnTrackCreator-00-06-05

2005-08-13 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * more careful Dll loading

2005-08-08 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * add python set-up class, adapt doxygen

2005-07-27 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * use TrkEventPrimitives instead of TrkEventUtils

2005-07-07 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * move IAlgTool interface to TrkToolInterfaces

2005-04-12 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * the new checkreq is more picky, have to fix warning.

2005-03-31 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * changed from common SiCluster corrections to separate tools
	   for PixelCluster and SCT_Cluster
	 * bug fixed: TGC wasn't properly identified as MuonCluster
         * ROT_ErrorScaling debugging removed, more documentation
	 * new version TrkRIO_OnTrackCreator-00-05-00

2005-03-23 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
	 * Added error scaling features, namely RIO_OnTrackErrorScalingTool
	   will interface and job options
	 * tag as TrkRIO_OnTrackCreator-00-04-00

2005-03-14 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * Moved interface from dual_use_library to component_library
         * some more documentation in mainpage.h
         * tag as TrkRIO_OnTrackCreator-00-03-00
	
2005-01-20 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
	 * Clean up of text output, i.e. reduce MSG levels. Tag -02
	
2004-12-08 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * changed TGC technology to be a MuonCluster
	 * fix job options to load MuonDriftCircleOnTrack options
           -> tag 00-02-01
	   
2004-09-17 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * removed SiCluster dependency after validating diff kinds of ctb data
           -> 00-02-00, minor-V change due to changed functionality

2004-09-11  Andreas Salzburger < Andreas.Salzburger@cern.ch >
         * update SiCluster to be taken from InnerDetector (temporary solution)
           -> 00-01-05 nightly tag for 9.0.0 

2004-08-30   Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * rmv muon tool warning, reduce use of dyncast to only SiCluster
           -> 00-01-04 for release 8.7.0

2004-07-30   Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * split job options into InDet, Muons, all   ->00-01-03

2004-07-21  Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * hotfix against pixel-ID unusable.

2004-07-20 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * get AtlasDetectorId from helper and ID dictionary
         * prepared for all MuonSpectrometer technologies
         * remove checkreq warning
         * tag 00-01-00 for 8.5.0

2004-06-21 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * tag 00-00-05 for 8.4.0 (python and some more output)

2004-06-02 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>
         * Add job options for python

2004-05-27 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>

         * providing jobOptions to AlgTools was not quite trivial
         * Fall-back to dynamic_cast in case no AtlasID is there.

2004-05-26 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>

         * supplied job options and readme.

2004-05-25 Wolfgang Liebig   <http://consult.cern.ch/xwho/people/485812>

         * fixes in interface class -> derivatives work.
         * clean-up in master, but still can not be loaded.

2004-05-11 Markus Elsing, Wolfgang Liebig

         * Make names more consistent (but also longer), i.e.
           IROT_Creator -> IRIO_OnTrackCreator.
         * comments in doxygen style

2004-05-10 Wolfgang Liebig    <http://consult.cern.ch/xwho/people/485812>

        * Check in the abstract base class and the Master Creator
	  The AlgTool concept allowed two possible designs:
	    a) double inheritance from AlgTool and a pure interface
	       class, IROT_Creator, based on IAlgTool
	    b) inheritance from a base class that inherits itself from
	       AlgTool.
	  Design b) was chosen.

2004-05-10 Frederick Luehring <luehring@indiana.edu>

	* Create package and tag as TrkRIO_OnTrackCreator-00-00-00

