2015-08-14 Leigh Schaefer
	* TRTOccupancyInclude: check vector size
	* tag as TRT_CalibAlgs-00-01-11

2015-08-14 Leigh Schaefer
	* Update Changelog
	* tag as TRT_CalibAlgs-00-01-10

2015-08-14 Leigh Schaefer
	* Change occupancy access to GlobalOccupancy()
	* tag as TRT_CalibAlgs-00-01-09

2015-01-22 Leigh Schaefer and Fred Luehring
	* Fix compile warning in TRTOccupancyInclude.cxx
	* tag as TRT_CalibAlgs-00-01-07

2015-01-21 Leigh Schaefer and Fred Luehring
	* Merge changes to increase local occupancy granularity
	* tag as TRT_CalibAlgs-00-01-06

2015-01-165 Alex Alonso 
	* New alg added for storing Local Occupancy inside xAOD::EventInfo  
	* tag as TRT_CalibAlgs-00-01-05

2014-10-29 Alex Alonso 
	* Clean and xAOD vertex 
	* tag as TRT_CalibAlgs-00-01-04

2014-10-22  Vakho Tsulaia  <tsulaia@cern.ch>

	* Migrating to xAOD::EventInfo

2013-11-15 Shaun Roe
	* Updated to new EDM
	* tag as TRT_CalibAlgs-00-01-02

2013-09-09 Alex Alonso <alonso@nbi.dk>
	* Include the code used for 2013 and track Refitting. 
	* tag as TRT_CalibAlgs-00-01-01

2013-01-10 Alex Alonso <alonso@nbi.dk>
	* Code modification to run with Ar/Xe mixture in the TRT
        * Flag DoArXe has to be setup to True in configfile, otherwise exactely same results as previous code.
        * Requires tag TRT_CalibTools-00-01-00 
	* tag as TRT_CalibAlgs-00-01-00

2012-11-16 Alex Alonso <alonso@nbi.dk>
	* Remove WARNING complain in straw status, unused SC
	* tag as TRT_CalibAlgs-00-00-78

2012-11-3 Alex Alonso <alonso@nbi.dk>
	* Remove WARNING complain 
	* tag as TRT_CalibAlgs-00-00-77

2012-10-22 Alex Alonso <alonso@nbi.dk>
	* CLHEP corrections 
	* tag as TRT_CalibAlgs-00-00-76

2012-08-08 Alex Alonso <alonso@nbi.dk>
	* Some correction for bhadd and other issues.
	* tag as TRT_CalibAlgs-00-00-75

2011-08-03 Alex Alonso <alejandro.alonso@hep.lu.se>
	* Correct some issues with the types for the TrackStateOnSurface
	* tag as TRT_CalibAlgs-00-00-65

2011-01-24 Sasa Fratina <sasa.fratina@cern.ch>
	* use the same default 1.4 mm range for hits and holes
	* tag as TRT_CalibAlgs-00-00-64

2010-12-09 Alex Alonso <alejandro.alonso@hep.lu.se>
	* Fix warning 
	* tag as TRT_CalibAlgs-00-00-63

2010-12-09 Alex Alonso <alejandro.alonso@hep.lu.se>
	* Fix Bug in:TRT_StrawStatus_merge
	* Make Straw Status running again
	* Updates to use latest track selector.
	* tag as TRT_CalibAlgs-00-00-62

2010-12-02 Sasa Fratina <sasa.fratina@cern.ch>
	* add TRT_StrawStatus_merge - merging script
	* tag as TRT_CalibAlgs-00-00-61

2010-12-02 Sasa Fratina <sasa.fratina@cern.ch>
	* clean-up TRT_StrawStatus and add hole counting for efficiency
	* tag as TRT_CalibAlgs-00-00-60 

2010-09-17 Johan Lundquist <johan.lundquist@nbi.dk>
        * fixed problematic VxVertex requirement entry
        * tag as TRT_CalibAlgs-00-00-59

2010-09-17 Johan Lundquist <johan.lundquist@nbi.dk>
        * Added selection for 1 vertex + min 3 tracks
        * tag as TRT_CalibAlgs-00-00-58

2010-06-03 Johan Lundquist <johan.lundquist@nbi.dk>
        * Fixed some warnings                 
        * tag as TRT_CalibAlgs-00-00-57           

2010-06-03 Johan Lundquist <johan.lundquist@nbi.dk>
        * Many different minor updates
        * tag as TRT_CalibAlgs-00-00-56

2010-04-07 Johan Lundquist <johan.lundquist@nbi.dk>
        * Made r or t binning/slizing configurable
        * Made possible to split or combine A and C side
        * Barrel with combined A and C sides runs as a separate calibration job 

2010-01-13 Alex Alonso <alejandro.alonso@hep.lu.se>
        * Missed include files
        * tag as TRT_CalibAlgs-00-00-55

2009-11-14 Alex Alonso <alejandro.alonso@hep.lu.se>
        * Changes in Templates to avoid probles on autosetup. Changes in  makeplots.cpp and bhadd.cpp to include more plots
        * tag as TRT_CalibAlgs-00-00-54


2009-11-14 Alex Alonso <alejandro.alonso@hep.lu.se>
        * Changes in  makeplots.cpp and bhadd.cpp to include new plots
        * tag as TRT_CalibAlgs-00-00-53


2009-08-03 Alex Alonso <alejandro.alonso@hep.lu.se>
        * Changes in the script for calibration report makeplots.cpp
        * Changes RunTRTCalib to allow T0 calibration at layer level, store the tracktuple and run on Cosmic MC
        * tag as TRT_CalibAlgs-00-00-52

2009-07-27 Sasa Fratina <Sasa.Fratina@cern.ch>
	* small chnges to TRT_StrawStatus
	* tag as TRT_CalibAlgs-00-00-51

2009-06-30 Alex Alonso <alejandro.alonso@hep.lu.se>
        * Have the option to use histograms, small changes in the templates, add plots...
        * tag as TRT_CalibAlgs-00-00-50

2009-06-10 Alex Alonso <alejandro.alonso@hep.lu.se>
        * First time using svn, fixing error
        * tag as TRT_CalibAlgs-00-00-48

2009-06-10 Alex Alonso <alejandro.alonso@hep.lu.se>
        * Removed the TRTCalAccumulator from CalibrationMgr
	* tag as TRT_CalibAlgs-00-00-47

2009-05-22 Johan Lundquist <johan.lundquist@nbi.dk>
        * added possibility to use different releases for reconst and calib
        * tool -> svc for CosmicCalibTemplate.py

2009-05-11 Denver Whittington <Denver.Whittington@cern.ch>
	* Updated method to access TRT ID Helper
	* tag as TRT_CalibAlgs-00-00-45
	* note: There seems to have been a mis-counting in the tag version
	        number in this ChangeLog for quite some time. Should be 
	        correct now.

2009-04-02 Johan Lundquist <johan.lundquist@nbi.dk>
	* Removed line
	* tag as TRT_CalibAlgs-00-00-43

2009-04-02 Johan Lundquist <johan.lundquist@nbi.dk>
	* Removed some buggy strange lines
	* tag as TRT_CalibAlgs-00-00-42

2009-03-30 Johan Lundquist <johan.lundquist@nbi.dk>
	* Added some more files
	* tag as TRT_CalibAlgs-00-00-41

2009-03-30 Johan Lundquist <johan.lundquist@nbi.dk>
	* Added call to calibration tool TRTCalibrator
	* tag as TRT_CalibAlgs-00-00-40

2009-03-25 Sasa Fratina <Sasa.Fratina@cern.ch>
	* TRT_ConditionsTools -> Svc
	* tag as TRT_CalibAlgs-00-00-39

2009-03-05 Sasa Fratina <Sasa.Fratina@cern.ch>
	* remove StatNtupleName from share/*py
	* tag as TRT_CalibAlgs-00-00-38

2009-03-04 Sasa Fratina <Sasa.Fratina@cern.ch>
	* remove doStatNtuple from share/*py
	* tag as TRT_CalibAlgs-00-00-37

2009-01-22 Johan Lundquist <johan.lundquist@nbi.dk>
        * Some changes in the calibration code in share/
        * tag as TRT_CalibAlgs-00-00-36

2009-01-16 Sasa Fratina <Sasa.Fratina@cern.ch>
	* reduce number of printouts
	* tag as TRT_CalibAlgs-00-00-35

2009-01-13 Sasa Fratina <Sasa.Fratina@cern.ch>
	* TRTStrawNeighbourTool -> ITRTStrawNeighbourTool 
	* tag as TRT_CalibAlgs-00-00-34

2009-01-12 Sasa Fratina <Sasa.Fratina@cern.ch>
	* fix one print out
	* tag as TRT_CalibAlgs-00-00-33

2009-01-12 Sasa Fratina <Sasa.Fratina@cern.ch>
	* fix last use statements
	* tag as TRT_CalibAlgs-00-00-32

2009-01-12 Sasa Fratina <Sasa.Fratina@cern.ch>
	* remove temp getChip() fix and clean up evtStore and storeGate, now package fully cleaned
	* tag as TRT_CalibAlgs-00-00-31

2009-01-06 Sasa Fratina <Sasa.Fratina@cern.ch>
	* temporarily remove getChip() etc to fix compile problem in MIG nightlies
	* tag as TRT_CalibAlgs-00-00-30

2008-12-18 Sasa Fratina <Sasa.Fratina@cern.ch>
	* fix checkreq warnings and msg() output
	* tag as TRT_CalibAlgs-00-00-29

2008-12-07 Johan Lundquist
        * In TRTCalibrationMgr.cxx removed calls to TRTCalAccumulator
        * In TRTCalibrationMgr.cxx added call to the finalize method of FillAlignTRTHits

2008-12-02 Kirill Prokofiev
        * Fixes for TrackInfo
        * 00-00-27

2008-09-15 Sasa Fratina <Sasa.Fratina@cern.ch>
        * fix components/TRT_CalibAlgs_entries.cxx such that it includesTRT_StrawStatus
        * tag as TRT_CalibAlgs-00-00-26

2008-09-10 Sasa Fratina <Sasa.Fratina@cern.ch>
        * add TRT_StrawStatus algorithm to determine dead and hot straws
        * tag as TRT_CalibAlgs-00-00-24

2008-09-04 Alejandro Alonso <Alejandro.Alonso@hep.lu.se>
	* Modified to run with cosmic data (solenoid on and off)
        * Some new features add to configfile
        * tag as TRT_CalibAlgs-00-00-23

2008-08-13 Alejandro Alonso <Alejandro.Alonso@hep.lu.se>
        * Added InDetFlags to run on ESD and write database files with new InDetRecExample. (Used for FDR2_Followup_2)
        * tag as TRT_CalibAlgs-00-00-22

2008-07-29 Alejandro Alonso <Alejandro.Alonso@hep.lu.se>
        * Fixed the problem to write the database (.db and .pool)
        * tag as TRT_CalibAlgs-00-00-21

2008-07-28 Alejandro Alonso <Alejandro.Alonso@hep.lu.se>
        * Corrections for ESD and txt2db in TRT_RecalibrateRotCreator and CosmicsTRTCalibMgr
        * tag as TRT_CalibAlgs-00-00-20

2008-07-04 Christian Schmitt <Christian.Schmitt@cern.ch>
        * Make the refit a jobOption (needed for ESD)
        * tag as TRT_CalibAlgs-00-00-18

2008-06-25 Christian Schmitt <Christian.Schmitt@cern.ch>
        * Disable the refit of the track (not needed here)
        * tag as TRT_CalibAlgs-00-00-17

2008-06-25 Christian Schmitt <Christian.Schmitt@cern.ch>
        * Adjust script such that it also works for cosmics
        * tag as TRT_CalibAlgs-00-00-16

2008-06-23 Christian Schmitt <Christian.Schmitt@cern.ch>
        * Add script to perform calibrationon on ESDs
        * tag as TRT_CalibAlgs-00-00-15

2008-05-27 Johan Lundquist <johan.lundquist@nbi.dk>
        * Modified RunTRTCalib_FDR1.py to allow for reading calib constants from either DB or textfiles
        * tag as TRT_CalibAlgs-00-00-13

2008-05-23 Christian Schmitt <Christian.Schmitt@cern.ch>
        * Fix the creation of the pool file
        * tag as TRT_CalibAlgs-00-00-12

2008-05-20 Christian Schmitt <Christian.Schmitt@cern.ch>
        * Speedup in finalize()
        * tag as TRT_CalibAlgs-00-00-11

2008-05-20 Christian Schmitt <Christian.Schmitt@cern.ch>
        * Implement functionality to run from ESD instead of BS
        * tag as TRT_CalibAlgs-00-00-10

2008-05-15 Johan Lundquist <johan.lundquist@nbi.dk>
        * Modified for using a new merging procedure
	* forgot the tag
	
2008-05-15 Christian Schmitt <Christian.Schmitt@cern.ch>
        * Fix creation of db and pool file
        * tag as TRT_CalibAlgs-00-00-09

2008-04-22 Alejandro Alonso <Alejandro.Alonso@hep.lu.se>
        * Fix tag problem
        * tag as TRT_CalibAlgs-00-00-08


2008-04-22 Alejandro Alonso <Alejandro.Alonso@hep.lu.se>
        * Fix setT0level in share/TRTCalibration.py. Before: lower level board, now: chip
        * Fix display_rt() and create_summary_hists() in share/TRTCalibration.py
        * tag as TRT_CalibAlgs-00-00-07

2008-03-26 Christian Schmitt <Christian.Schmitt@cern.ch>
        * Add the actual calibration code
        * tag as TRT_CalibAlgs-00-00-06

2008-03-17 Christian Schmitt <Christian.Schmitt@cern.ch>
        * Enable writing of calibration constants into pool file
        * tag as TRT_CalibAlgs-00-00-05

2008-02-13 Christian Schmitt <Christian.Schmitt@cern.ch>
        * Fix unchecked StatusCode
        * tag as TRT_CalibAlgs-00-00-04

2007-10-02 David Quarrie <David.Quarrie@cern.ch>
	* TRT_CalibAlgs/TRTCalibrationMgr.h src/components/TRT_CalibAlgs_entries.cxx: Remove redundant ";"
        * tag as TRT_CalibAlgs-00-00-03

2007-04-12 Christian Schmitt <Christian.Schmitt@cern.ch>
        * migrate to configurables
        * fix checkreq problems
        * tag as TRT_CalibAlgs-00-00-02

2007-04-11 Chafik Driouichi <Chafik.Driouichi@cern.ch>

        * added src share TRT_CalibAlgs 
        * tagged as TRT_CalibAlgs-00-00-01

2007-03-23 Frederick Luehring <Fred.Luehring@cern.ch>

	* Create package and tag as TRT_CalibAlgs-00-00-00

