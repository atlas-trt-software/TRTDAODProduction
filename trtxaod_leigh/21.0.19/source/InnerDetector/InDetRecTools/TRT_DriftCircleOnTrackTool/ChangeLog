2013-08-26 Niels van Eldik
	* Tagging TRT_DriftCircleOnTrackTool-02-00-00
	* migrate to Eigen

2012-11-02 Niels van Eldik
	* Tagging TRT_DriftCircleOnTrackTool-01-00-00
	* reduce malloc

2012-09-29  scott snyder  <snyder@bnl.gov>

	* Tagging TRT_DriftCircleOnTrackTool-00-01-14.
	* Updates for clhep2.

2010-08-25 Esben Klinkby <klinkby@phy.duke.edu>
	 * Make use of time over threshold correction for recalibration
	 * From Jon Stahlman
	 * tag as TRT_DriftCircleOnTrackTool-00-01-13
	
2010-04-13 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
	   add Identifier  for method m_drifttool->errorOfDriftRadius(rawtime-t0,DC->identify());
	   and tag as TRT_DriftCircleOnTrackTool-00-01-12

2009-01-13 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
	   add some includes
	   and tag as TRT_DriftCircleOnTrackTool-00-01-11

2008-12-19 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
	   Improving software quality 
	   and tag as TRT_DriftCircleOnTrackTool-00-01-10

2008-05-20 Christian Schmitt <Christian.Schmitt@cern.ch>
           Make use of ErrorScaling Tool
	   and tag as TRT_DriftCircleOnTrackTool-00-01-09
	
2008-05-20 Christian Schmitt <Christian.Schmitt@cern.ch>
	   Fix missing tool retrieve
           Add possibility to perform TRT calibration from ESD
	   and tag as TRT_DriftCircleOnTrackTool-00-01-08
	
2008-01-08 Christian Schmitt <Christian.Schmitt@cern.ch>
	   constrain ROT objects to active area
	   and tag as TRT_DriftCircleOnTrackTool-00-01-07
	
2007-11-28 Christian Schmitt <Christian.Schmitt@cern.ch>
	   remove dependency on TRT_ConditionsAlgs
	   and tag as TRT_DriftCircleOnTrackTool-00-01-06
	
2007-11-28 Christian Schmitt <Christian.Schmitt@cern.ch>
	   one line bugfix in universal tool
	   and tag as TRT_DriftCircleOnTrackTool-00-01-05

2007-11-28 Christian Schmitt <Christian.Schmitt@cern.ch>
	   add new implementation that creates precision and tube hits
	   and tag as TRT_DriftCircleOnTrackTool-00-01-04

2005-11-16 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
           add possibilty correct drift radius error near wire
	   and tag as TRT_DriftCircleOnTrackTool-00-01-03

2007-11-14 Markus Elsing <Markus.Elsing@cern.ch>
	   drop calling the ID helper to get the hash, take the one from the element.
	   and tag as TRT_DriftCircleOnTrackTool-00-01-02

2007-11-05 Wolfgang Liebig <http://consult.cern.ch/xwho/people/54608>
         * migrated to InDet:: namespace
         * and tag as TRT_DriftCircleOnTrackTool-00-01-01

2007-02-21 Andreas Wildauer
	   migrated to use toolhandle

2006-02-09 Wolfgang Liebig <http://consult.cern.ch/xwho/people/54608>
           change to status and side classes from TrkEventPrimitives
           and tag as TRT_DriftCircleOnTrackTool-00-00-32

2006-02-07 Wolfgang Liebig <http://consult.cern.ch/xwho/people/54608>
           use TRT_Status in new TRT_DriftCircleOnTrack constructor
           and tag as TRT_DriftCircleOnTrackTool-00-00-30
           and fix this tag a second time (00-00-31)

2006-02-02 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
	   correction for new TRT_DriftCircleOnTrack constructor
	   and tag as TRT_DriftCircleOnTrackTool-00-00-29

2006-02-02 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
	   correction for new TRT_DriftCircleOnTrack constructor
	   and tag as TRT_DriftCircleOnTrackTool-00-00-27

2006-02-01 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
	   correction for measurements without drift time information
	   and tag as TRT_DriftCircleOnTrackTool-00-00-26

2006-01-16 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
	   put on HEAD version without condition database
	   and tag as TRT_DriftCircleOnTrackTool-00-00-25

2005-11-12 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
           asmall correction
	   and tag as TRT_DriftCircleOnTrackTool-00-00-24

2005-11-11 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
           add alignment tool 
	   and tag as TRT_DriftCircleOnTrackTool-00-00-23

2005-10-13 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
           correted local center of drift circle position 
	   and tag as TRT_DriftCircleOnTrackTool-00-00-22

2005-09-08 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
           speed up methods correct
	   and tag as TRT_DriftCircleOnTrackTool-00-00-21

2005-08-04 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
	   add new tool for TRT_DriftCircleOnTrack production
	   without drift time information
	   and tag as TRT_DriftCircleOnTrackTool-00-00-20

2005-07-28 Wolfgang Liebig <http://consult.cern.ch/xwho/people/54608>
           fix interface implemetation to return concrete class
           instead of RIO_OnTrack base class

2005-07-27 Wolfgang Liebig <http://consult.cern.ch/xwho/people/54608>
           adapt to EDM math classes being in TrkEventPrimitives

2005-07-07 Wolfgang Liebig <http://consult.cern.ch/xwho/people/54608>
           adapt to IROTcreator having moved to TrkToolInterfaces

2005-05-18 Wolfgang Liebig <http://consult.cern.ch/xwho/people/54608>
           change to new RIO_OnTrack with LocalParameters.

2005-03-31 Wolfgang Liebig <http://consult.cern.ch/xwho/people/54608>
           added making use of error scaling tool
           added doxygen mainpage.h
           tag as TRT_DriftCircleOnTrackTool-00-00-14
	
2005-03-16 Wolfgang Liebig <http://consult.cern.ch/xwho/people/54608>
           Package changed from dual_use to component_library
           tag as TRT_DriftCircleOnTrackTool-00-00-13

2005-03-02 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
	   requirements correction 
	   and tag as TRT_DriftCircleOnTrackTool-00-00-12

2004-09-13 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
	   requirements correction 
	   and tag as TRT_DriftCircleOnTrackTool-00-00-11

2004-09-11 Igor Gavrilenko <Igor.Gavrilenko@cern.ch> 
	   transition from Trk to InDet 
	   and tag as TRT_DriftCircleOnTrackTool-00-00-10

2004-07-08 Markus ELsing <Markus.Elsing@cern.ch>
         * check in L/R solution from Wolfgang

2004-06-30 Igor Gavrilenko <Igor.Gavrilenko@cern.ch>
	   use new pointer to detector element from rio 
	   and tag as  TRT_DriftCircleOnTrackTool-00-00-07

2004-05-20 Igor Gavrilenko <Igor.Gavrilenko@cern.ch>
	   corrected requirements file
	   and tag as  TRT_DriftCircleOnTrackTool-00-00-06

2004-05-14 Igor Gavrilenko <Igor.Gavrilenko@cern.ch>
	   corrected requirements file
	   and tag as  TRT_DriftCircleOnTrackTool-00-00-05

2004-05-13 Igor Gavrilenko <Igor.Gavrilenko@cern.ch>
	   corrected requariments file
	   and tag as  TRT_DriftCircleOnTrackTool-00-00-04

2004-05-12 Igor Gavrilenko <Igor.Gavrilenko@cern.ch>
	   new inheritance from class Trk::IRIO_OnTrackCreator
	   and tag as  TRT_DriftCircleOnTrackTool-00-00-03
	
2004-05-03 Igor Gavrilenko <Igor.Gavrilenko@cern.ch>
	   Change name for files 
	 TRT_DriftCircleTool_entries.cxx to TRT_DriftCircleOnTrackTool_entries.cxx
         TRT_DriftCircleTool_load.cxx    to TRT_DriftCircleOnTrackTool_load.cxx
	   and tag as  TRT_DriftCircleOnTrackTool-00-00-02

2004-04-29 Igor Gavrilenko <Igor.Gavrilenko@cern.ch>
	   First coddes for new packag.
	   and tag as  TRT_DriftCircleOnTrackTool-00-00-01
	
2004-04-28 Frederick Luehring <luehring@indiana.edu>

	* Create package and tag as TRT_DriftCircleOnTrackTool-00-00-00.

