cd $TestArea 

# cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_original.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
svn revert InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
cp TRT_LocalOccupancy_fineGran.cxx InnerDetector/InDetRecTools/TRT_ElectronPidTools/src/TRT_LocalOccupancy.cxx
cp TRT_LocalOccupancy_fineGran.h   InnerDetector/InDetRecTools/TRT_ElectronPidTools/TRT_ElectronPidTools/TRT_LocalOccupancy.h
cp ITRT_LocalOccupancy_fineGran.h   InnerDetector/InDetRecTools/TRT_ElectronPidTools/TRT_ElectronPidTools/ITRT_LocalOccupancy.h
cp TRTOccupancyInclude_fineGran.cxx InnerDetector/InDetCalibAlgs/TRT_CalibAlgs/src/TRTOccupancyInclude.cxx


# # ### DATA ### # #

# ### Zmumu ###

# cd $TestArea
# cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_Z.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
# cd ../build
# cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
# cd ../run
# #    --inputBSFile '/afs/cern.ch/user/l/lschaef/work/public/TestxAODs/group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0/group.det-indet.11017568.EXT0._416084.DRAW_TRT.pool.root' \
# Reco_tf.py \
#     --inputESDFile 'tmp.ESD' \
#     --ignoreErrors True \
#     --maxEvents '20' \
#     --autoConfiguration 'everything' \
#     --AMI 'f716' \
#     --postExec 'all:conddb.addOverride("/TRT/Calib/ToT/ToTValue","TRTCalibToTDataValue-000-03") ; conddb.addOverride("/TRT/Calib/ToT/ToTVectors","TRTCalibToTDataVectors-000-03") ' \
#     --preExec \
# 'all:InDetFlags.doMinBias.set_Value_and_Lock(True); jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#     --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=InDetDxAOD.pool.root


Reco_tf.py \
--inputHITSFile '/afs/cern.ch/user/l/lschaef/work/public/mc15_13TeV/HITS.07586631._016103.pool.root.1' \
--ignoreErrors True \
--maxEvents '20' \
--DataRunNumber 222525 \
--jobNumber 222525 \
--conditionsTag 'OFLCOND-RUN12-SDR-31' \
--geometryVersion ATLAS-R2-2015-03-01-00 \
--autoConfiguration 'everything' \
--ignorePatterns 'ByteStreamInputSvc.+ERROR.+Skipping.+bad.+event|EventSelector.+ERROR.+badFragment.+data.+encountered|ByteStreamInputSvcWARNING.+ERROR.+message.+limit|EventSelector.+WARNING.+ERROR.+message.+limit' \
--postInclude 'RecJobTransforms/UseFrontier.py' \
--postExec \
'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup("/TRT/Cond/StatusHT","<forceRunNumber>286990</forceRunNumber>"); conddb.addOverride("/TRT/Cond/StatusHT","TrtStrawStatusHT-MC-run2-scenario5_00-00"); conddb.addOverride("/TRT/Calib/RT","TrtCalibRt-13TeV_MC-ArLT150-scenario5_00-01" ); conddb.addOverride("/TRT/Calib/T0","TrtCalibT0-13TeV_MC-ArLT150-scenario5_00-01");' \
'e2d:from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=False;ServiceMgr.MuonRPC_CablingSvc.CorrFileName="LVL1confAtlasRUN2_ver016.corr";ServiceMgr.MuonRPC_CablingSvc.ConfFileName="LVL1confAtlasRUN2_ver016.data" ' \
--preExec \
'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25) ; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetFlags.doSlimming.set_Value_and_Lock(False); InDetFlags.doMinBias.set_Value_and_Lock(True);  from GeoModelSvc.GeoModelSvcConf import GeoModelSvc; GeoModelSvc.TRT_VersionOverride="TRT-GEO-03" ' \
'r2e:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
 --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD.pool.root