Reco_tf.py \
--inputHITSFile '/afs/cern.ch/user/l/lschaef/work/public/mc15_13TeV/HITS.08040362._003327.pool.root.1' \
--ignoreErrors True \
--maxEvents '15' \
--DataRunNumber 222525 \
--jobNumber 222525 \
--conditionsTag 'OFLCOND-RUN12-SDR-31' \
--geometryVersion ATLAS-R2-2015-03-01-00 \
--autoConfiguration 'everything' \
--ignorePatterns 'ByteStreamInputSvc.+ERROR.+Skipping.+bad.+event|EventSelector.+ERROR.+badFragment.+data.+encountered|ByteStreamInputSvcWARNING.+ERROR.+message.+limit|EventSelector.+WARNING.+ERROR.+message.+limit' \
--postInclude 'RecJobTransforms/UseFrontier.py' \
--inputHighPtMinbiasHitsFile '/afs/cern.ch/user/l/lschaef/work/public/mc15_13TeV/HITS.07586635._020676.pool.root.1' \
--inputLowPtMinbiasHitsFile '/afs/cern.ch/user/l/lschaef/work/public/mc15_13TeV/HITS.07586631._016103.pool.root.1' \
--numberOfCavernBkg 0 \
--numberOfHighPtMinBias 0.312197744 \
--numberOfLowPtMinBias 59.68780226 \
--pileupFinalBunch 6 \
--postExec \
'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup("/TRT/Cond/StatusHT","<forceRunNumber>286990</forceRunNumber>"); conddb.addOverride("/TRT/Cond/StatusHT","TrtStrawStatusHT-MC-run2-scenario5_00-00"); conddb.addOverride("/TRT/Calib/RT","TrtCalibRt-13TeV_MC-ArLT150-scenario5_00-01" ); conddb.addOverride("/TRT/Calib/T0","TrtCalibT0-13TeV_MC-ArLT150-scenario5_00-01")' \
'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools["MergeMcEventCollTool"].DoSlimming=False; job.StandardPileUpToolsAlg.PileUpTools["MdtDigitizationTool"].LastXing=150; from AthenaCommon.CfgGetter import getPublicTool; getPublicTool("PixelDigitizationTool").ParticleBarcodeVeto=0; getPublicTool("SCT_DigitizationTool").ParticleBarcodeVeto=0;getPublicTool("TRTDigitizationTool").ParticleBarcodeVeto=0; ' \
'r2e:from AthenaCommon.AppMgr import ToolSvc; ToolSvc.InDetExtenScoringTool.minTRTPrecisionFraction=0.3; ToolSvc.InDetExtenScoringToolPixelPrdAssociation.minTRTPrecisionFraction=0.3;xAODMaker__xAODTruthCnvAlg("GEN_AOD2xAOD",WriteInTimePileUpTruth=True); xAODMaker__xAODTruthCnvAlg("GEN_AOD2xAOD",WriteInTimePileUpTruth=True) ' \
'e2d:from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=False;ServiceMgr.MuonRPC_CablingSvc.CorrFileName="LVL1confAtlasRUN2_ver016.corr";ServiceMgr.MuonRPC_CablingSvc.ConfFileName="LVL1confAtlasRUN2_ver016.data";xAODMaker__xAODTruthCnvAlg("GEN_AOD2xAOD",WriteInTimePileUpTruth=True); xAODMaker__xAODTruthCnvAlg("GEN_AOD2xAOD",WriteInTimePileUpTruth=True)  ' \
--preExec \
'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25) ; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetFlags.doSlimming.set_Value_and_Lock(False);  from GeoModelSvc.GeoModelSvcConf import GeoModelSvc; GeoModelSvc.TRT_VersionOverride="TRT-GEO-03" ' \
'HITtoRDO:ScaleTaskLength=0.1; userRunLumiOverride={"run":284500,"startmu":0.0,"endmu":60.0,"stepmu":2.5,"startlb":1,"timestamp": 1446539185} ' \
'r2e:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
--preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_muRange.py' \
 --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD.pool.root