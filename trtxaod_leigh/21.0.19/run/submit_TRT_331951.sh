cd $TestArea
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_Z.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
svn revert InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
cd ../build
cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
cd ../run


pathena --trf \
"Reco_tf.py  \
     --inputBSFile %IN \
     --maxEvents '500' \
     --autoConfiguration 'everything' \
     --skipEvents='%SKIPEVENTS' \
     --AMI f848 \
     --preExec 'from PrimaryDPDMaker.PrimaryDPDFlags import primDPD; primDPD.WriteEventlessFiles.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.useDynamicAlignFolders.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50); TriggerFlags.AODEDMSet=\"AODFULL\";from TrigHLTMonitoring.HLTMonFlags import HLTMonFlags;HLTMonFlags.doGeneral=False;HLTMonFlags.doJet=False' \
     --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
--inDS data17_13TeV.00331951.physics_Main.daq.RAW \
--outDS group.det-indet.00331951.physics_Main.daq.TRTxAOD_Z.f848_trt099-00 \
--nFilesPerJob 1 \
--nEventsPerJob 500 \
--official

cd $TestArea
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_JPSI.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
svn revert InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
cd ../build
cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
cd ../run


pathena --trf \
"Reco_tf.py  \
     --inputBSFile %IN \
     --maxEvents '500' \
     --autoConfiguration 'everything' \
     --skipEvents='%SKIPEVENTS' \
     --AMI f848 \
     --preExec 'from PrimaryDPDMaker.PrimaryDPDFlags import primDPD; primDPD.WriteEventlessFiles.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.useDynamicAlignFolders.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50); TriggerFlags.AODEDMSet=\"AODFULL\";from TrigHLTMonitoring.HLTMonFlags import HLTMonFlags;HLTMonFlags.doGeneral=False;HLTMonFlags.doJet=False' \
     --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
--inDS data17_13TeV.00331951.physics_Main.daq.RAW \
--outDS group.det-indet.00331951.physics_Main.daq.TRTxAOD_JPSI.f848_trt099-00 \
--nFilesPerJob 1 \
--nEventsPerJob 500 \
--official


