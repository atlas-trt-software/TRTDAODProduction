
cd $TestArea
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_original.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
svn revert InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
cd ../build
cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
cd ../run

# pathena --trf  " Reco_tf.py  \
#     --inputBSFile='%IN'   \
#     --ignoreErrors 'True'  \
#     --skipEvents='%SKIPEVENTS' \
#     --maxEvents '500' \
#     --AMI 'f716' \
#     --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False)'\
#       --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root  "  \
#     --official \
#     --nEventsPerJob=500 \
#     --tmpDir=/tmp/lschaef \
#     --inDS=data16_13TeV.00304337.physics_Main.merge.DRAW_EGZ.f716_m1616 \
#     --outDS=group.det-indet.00304337.physics_Main.merge.TRTxAOD_EGZ.f716_m1616_trt098-00


pathena --trf  " Reco_tf.py  \
    --inputBSFile='%IN'   \
    --ignoreErrors 'True'  \
    --skipEvents='%SKIPEVENTS' \
    --maxEvents '500' \
    --AMI 'f716' \
 --postExec \
 'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_data16_newgeom_noRtOrT0.txt\" ' \
    --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False);  from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool '\
      --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root  "  \
    --official \
     --extFile=caliboutput_data16_newgeom_noRtOrT0.txt \
    --nEventsPerJob=500 \
    --tmpDir=/tmp/lschaef \
    --inDS=data16_13TeV.00304337.physics_Main.merge.DRAW_EGZ.f716_m1616 \
    --outDS=group.det-indet.00304337.physics_Main.merge.TRTxAOD_EGZ.f716_m1616_trt098-newErrCalib-00

cd $TestArea
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_original.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
cd ../run

