# ## Zll

# cd $TestArea
# cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_Z.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
# svn revert InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
# cd ../build
# cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
# cd ../run

# pathena --trf \
# "Reco_tf.py \
#     --inputBSFile %IN \
# --ignoreErrors True \
# --maxEvents '200' \
# --skipEvents '%SKIPEVENTS' \
# --autoConfiguration 'everything' \
# --AMI 'f716' \
# --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#     --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 200 \
#     --inDS group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
#     --outDS group.det-indet.00304128.physics_Main.daq.TRTxAOD_Z.f716_trt098-03

# cd $TestArea
# cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_original.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 

## Jpsill

cd $TestArea
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_JPSI.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
svn revert InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
cd ../build
cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
cd ../run

pathena --trf \
"Reco_tf.py \
    --inputBSFile %IN \
--ignoreErrors True \
--maxEvents '200' \
--skipEvents '%SKIPEVENTS' \
--autoConfiguration 'everything' \
--AMI 'f716' \
--preExec \
'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
    --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
    --official \
    --nFilesPerJob 1 \
    --nEventsPerJob 200 \
    --inDS group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
    --outDS group.det-indet.00304128.physics_Main.daq.TRTxAOD_JPSI.f716_trt098-04

cd $TestArea
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_original.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
cd ../run