cd $TestArea
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_original.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
cd ../build
cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
cd ../run

 pathena --trf \
 "Reco_tf.py \
 --inputHITSFile %IN \
 --ignoreErrors True \
 --maxEvents '500' \
 --skipEvents='%SKIPEVENTS' \
 --DataRunNumber 222525 \
 --jobNumber 222525 \
 --conditionsTag 'OFLCOND-RUN12-SDR-31' \
 --geometryVersion ATLAS-R2-2015-03-01-00 \
 --autoConfiguration 'everything' \
 --ignorePatterns 'ByteStreamInputSvc.+ERROR.+Skipping.+bad.+event|EventSelector.+ERROR.+badFragment.+data.+encountered|ByteStreamInputSvcWARNING.+ERROR.+message.+limit|EventSelector.+WARNING.+ERROR.+message.+limit' \
 --postInclude 'RecJobTransforms/UseFrontier.py' \
 --inputHighPtMinbiasHitsFile %HIMBIN \
 --inputLowPtMinbiasHitsFile  %LOMBIN \
 --numberOfCavernBkg 0 \
 --numberOfHighPtMinBias 0.312197744 \
 --numberOfLowPtMinBias 59.68780226 \
 --pileupFinalBunch 6 \
 --postExec \
 'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\"); conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-ArLT150-scenario5_00-01\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-ArLT150-scenario5_00-01\"); conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"calibout_07_fixed.txt\" ' \
 'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools[\"MergeMcEventCollTool\"].DoSlimming=False; job.StandardPileUpToolsAlg.PileUpTools[\"MdtDigitizationTool\"].LastXing=150; from AthenaCommon.CfgGetter import getPublicTool; getPublicTool(\"PixelDigitizationTool\").ParticleBarcodeVeto=0; getPublicTool(\"SCT_DigitizationTool\").ParticleBarcodeVeto=0;getPublicTool(\"TRTDigitizationTool\").ParticleBarcodeVeto=0; ' \
 'r2e:from AthenaCommon.AppMgr import ToolSvc; ToolSvc.InDetExtenScoringTool.minTRTPrecisionFraction=0.3; ToolSvc.InDetExtenScoringToolPixelPrdAssociation.minTRTPrecisionFraction=0.3 ' \
 'e2d:from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=False;ServiceMgr.MuonRPC_CablingSvc.CorrFileName=\"LVL1confAtlasRUN2_ver016.corr\";ServiceMgr.MuonRPC_CablingSvc.ConfFileName=\"LVL1confAtlasRUN2_ver016.data\"  ' \
 --preExec \
 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25) ; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetFlags.doSlimming.set_Value_and_Lock(False);  from GeoModelSvc.GeoModelSvcConf import GeoModelSvc; GeoModelSvc.TRT_VersionOverride=\"TRT-GEO-03\" ' \
 'HITtoRDO:ScaleTaskLength=0.1; userRunLumiOverride={\"run\":284500,\"startmu\":0.0,\"endmu\":60.0,\"stepmu\":2.5,\"startlb\":1,\"timestamp\": 1446539185} ' \
 'r2e:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
 --preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_muRange.py' \
  --steering 'RAWtoESD:in+ESD' \
     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
     --highMinDS=mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS.e3581_s2876 \
     --lowMinDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2876 \
     --nHighMin=1 --nLowMin=1 \
     --extFile=calibout_07_fixed.txt \
     --nFilesPerJob 1 \
     --nEventsPerJob 500 \
     --official \
     --inDS mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.HITS.e3601_s2876 \
     --outDS group.det-indet.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.TRTxAOD.e3601_s2876_r7886_trt099_LDTT-04



# pathena --trf \
# "Reco_tf.py \
# --inputHITSFile %IN \
# --ignoreErrors True \
# --maxEvents '500' \
# --skipEvents='%SKIPEVENTS' \
# --DataRunNumber 222525 \
# --jobNumber 222525 \
# --conditionsTag 'OFLCOND-RUN12-SDR-31' \
# --geometryVersion ATLAS-R2-2015-03-01-00 \
# --autoConfiguration 'everything' \
# --ignorePatterns 'ByteStreamInputSvc.+ERROR.+Skipping.+bad.+event|EventSelector.+ERROR.+badFragment.+data.+encountered|ByteStreamInputSvcWARNING.+ERROR.+message.+limit|EventSelector.+WARNING.+ERROR.+message.+limit' \
# --postInclude 'RecJobTransforms/UseFrontier.py' \
# --inputHighPtMinbiasHitsFile %HIMBIN \
# --inputLowPtMinbiasHitsFile  %LOMBIN \
# --numberOfCavernBkg 0 \
# --numberOfHighPtMinBias 0.312197744 \
# --numberOfLowPtMinBias 59.68780226 \
# --pileupFinalBunch 6 \
# --postExec \
# 'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\"); conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-ArLT150-scenario5_00-01\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-ArLT150-scenario5_00-01\"); conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_mc16_newgeom_12ns_40mum.txt\" ' \
# 'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools[\"MergeMcEventCollTool\"].DoSlimming=False; job.StandardPileUpToolsAlg.PileUpTools[\"MdtDigitizationTool\"].LastXing=150; from AthenaCommon.CfgGetter import getPublicTool; getPublicTool(\"PixelDigitizationTool\").ParticleBarcodeVeto=0; getPublicTool(\"SCT_DigitizationTool\").ParticleBarcodeVeto=0;getPublicTool(\"TRTDigitizationTool\").ParticleBarcodeVeto=0; ' \
# 'r2e:from AthenaCommon.AppMgr import ToolSvc; ToolSvc.InDetExtenScoringTool.minTRTPrecisionFraction=0.3; ToolSvc.InDetExtenScoringToolPixelPrdAssociation.minTRTPrecisionFraction=0.3 ' \
# 'e2d:from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=False;ServiceMgr.MuonRPC_CablingSvc.CorrFileName=\"LVL1confAtlasRUN2_ver016.corr\";ServiceMgr.MuonRPC_CablingSvc.ConfFileName=\"LVL1confAtlasRUN2_ver016.data\"  ' \
# --preExec \
# 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25) ; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetFlags.doSlimming.set_Value_and_Lock(False);  from GeoModelSvc.GeoModelSvcConf import GeoModelSvc; GeoModelSvc.TRT_VersionOverride=\"TRT-GEO-03\" ' \
# 'HITtoRDO:ScaleTaskLength=0.1; userRunLumiOverride={\"run\":284500,\"startmu\":0.0,\"endmu\":60.0,\"stepmu\":2.5,\"startlb\":1,\"timestamp\": 1446539185} ' \
# 'r2e:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
# --preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_muRange.py' \
#  --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --highMinDS=mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS.e3581_s2876 \
#     --lowMinDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2876 \
#     --nHighMin=1 --nLowMin=1 \
#     --extFile=caliboutput_mc16_newgeom_12ns_40mum.txt \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 500 \
#     --official \
#     --inDS mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.HITS.e3601_s2876 \
#     --outDS group.det-indet.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.TRTxAOD.e3601_s2876_r7886_trt099_LDTT-00







 #pathena --trf \
 #"Reco_tf.py \
 #--inputHITSFile %IN \
 #--ignoreErrors True \
 #--maxEvents '500' \
 #--skipEvents='%SKIPEVENTS' \
 #--DataRunNumber 222525 \
 #--jobNumber 222525 \
 #--conditionsTag 'OFLCOND-RUN12-SDR-31' \
 #--geometryVersion ATLAS-R2-2015-03-01-00 \
 #--autoConfiguration 'everything' \
 #--ignorePatterns 'ByteStreamInputSvc.+ERROR.+Skipping.+bad.+event|EventSelector.+ERROR.+badFragment.+data.+encountered|ByteStreamInputSvcWARNING.+ERROR.+message.+limit|EventSelector.+WARNING.+ERROR.+message.+limit' \
 #--postInclude 'RecJobTransforms/UseFrontier.py' \
 #--inputHighPtMinbiasHitsFile %HIMBIN \
 #--inputLowPtMinbiasHitsFile  %LOMBIN \
 #--numberOfCavernBkg 0 \
 #--numberOfHighPtMinBias 0.312197744 \
 #--numberOfLowPtMinBias 59.68780226 \
 #--pileupFinalBunch 6 \
 #--postExec \
 #'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\"); conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-ArLT150-scenario5_00-01\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-ArLT150-scenario5_00-01\"); conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_mc16_newgeom_12ns_80mum.txt\" ' \
 #'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools[\"MergeMcEventCollTool\"].DoSlimming=False; job.StandardPileUpToolsAlg.PileUpTools[\"MdtDigitizationTool\"].LastXing=150; from AthenaCommon.CfgGetter import getPublicTool; getPublicTool(\"PixelDigitizationTool\").ParticleBarcodeVeto=0; getPublicTool(\"SCT_DigitizationTool\").ParticleBarcodeVeto=0;getPublicTool(\"TRTDigitizationTool\").ParticleBarcodeVeto=0; ' \
 #'r2e:from AthenaCommon.AppMgr import ToolSvc; ToolSvc.InDetExtenScoringTool.minTRTPrecisionFraction=0.3; ToolSvc.InDetExtenScoringToolPixelPrdAssociation.minTRTPrecisionFraction=0.3 ' \
 #'e2d:from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=False;ServiceMgr.MuonRPC_CablingSvc.CorrFileName=\"LVL1confAtlasRUN2_ver016.corr\";ServiceMgr.MuonRPC_CablingSvc.ConfFileName=\"LVL1confAtlasRUN2_ver016.data\"  ' \
 #--preExec \
 #'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25) ; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetFlags.doSlimming.set_Value_and_Lock(False);  from GeoModelSvc.GeoModelSvcConf import GeoModelSvc; GeoModelSvc.TRT_VersionOverride=\"TRT-GEO-03\" ' \
 #'HITtoRDO:ScaleTaskLength=0.1; userRunLumiOverride={\"run\":284500,\"startmu\":0.0,\"endmu\":60.0,\"stepmu\":2.5,\"startlb\":1,\"timestamp\": 1446539185} ' \
 #'r2e:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
 #--preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_muRange.py' \
 # --steering 'RAWtoESD:in+ESD' \
 #    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
 #    --highMinDS=mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS.e3581_s2876 \
 #    --lowMinDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2876 \
 #    --nHighMin=1 --nLowMin=1 \
 #    --extFile=caliboutput_mc16_newgeom_12ns_80mum.txt \
 #    --nFilesPerJob 1 \
 #    --nEventsPerJob 500 \
 #    --inDS mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.HITS.e3601_s2876 \
 #    --outDS user.croland.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.TRTxAOD.e3601_s2876_r7886_trt099_LDTT80-00







# pathena --trf \
# "Reco_tf.py \
# --inputHITSFile %IN \
# --ignoreErrors True \
# --maxEvents '500' \
# --skipEvents='%SKIPEVENTS' \
# --DataRunNumber 222525 \
# --jobNumber 222525 \
# --conditionsTag 'OFLCOND-RUN12-SDR-31' \
# --geometryVersion ATLAS-R2-2015-03-01-00 \
# --autoConfiguration 'everything' \
# --ignorePatterns 'ByteStreamInputSvc.+ERROR.+Skipping.+bad.+event|EventSelector.+ERROR.+badFragment.+data.+encountered|ByteStreamInputSvcWARNING.+ERROR.+message.+limit|EventSelector.+WARNING.+ERROR.+message.+limit' \
# --postInclude 'RecJobTransforms/UseFrontier.py' \
# --inputHighPtMinbiasHitsFile %HIMBIN \
# --inputLowPtMinbiasHitsFile  %LOMBIN \
# --numberOfCavernBkg 0 \
# --numberOfHighPtMinBias 0.312197744 \
# --numberOfLowPtMinBias 59.68780226 \
# --pileupFinalBunch 6 \
# --postExec \
# 'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\"); conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-ArLT150-scenario5_00-01\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-ArLT150-scenario5_00-01\"); conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_mc16_newgeom_9ns_40mum.txt\" ' \
# 'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools[\"MergeMcEventCollTool\"].DoSlimming=False; job.StandardPileUpToolsAlg.PileUpTools[\"MdtDigitizationTool\"].LastXing=150; from AthenaCommon.CfgGetter import getPublicTool; getPublicTool(\"PixelDigitizationTool\").ParticleBarcodeVeto=0; getPublicTool(\"SCT_DigitizationTool\").ParticleBarcodeVeto=0;getPublicTool(\"TRTDigitizationTool\").ParticleBarcodeVeto=0; ' \
# 'r2e:from AthenaCommon.AppMgr import ToolSvc; ToolSvc.InDetExtenScoringTool.minTRTPrecisionFraction=0.3; ToolSvc.InDetExtenScoringToolPixelPrdAssociation.minTRTPrecisionFraction=0.3 ' \
# 'e2d:from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=False;ServiceMgr.MuonRPC_CablingSvc.CorrFileName=\"LVL1confAtlasRUN2_ver016.corr\";ServiceMgr.MuonRPC_CablingSvc.ConfFileName=\"LVL1confAtlasRUN2_ver016.data\"  ' \
# --preExec \
# 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25) ; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetFlags.doSlimming.set_Value_and_Lock(False);  from GeoModelSvc.GeoModelSvcConf import GeoModelSvc; GeoModelSvc.TRT_VersionOverride=\"TRT-GEO-03\" ' \
# 'HITtoRDO:ScaleTaskLength=0.1; userRunLumiOverride={\"run\":284500,\"startmu\":0.0,\"endmu\":60.0,\"stepmu\":2.5,\"startlb\":1,\"timestamp\": 1446539185} ' \
# 'r2e:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
# --preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_muRange.py' \
#  --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --highMinDS=mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS.e3581_s2876 \
#     --lowMinDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2876 \
#     --nHighMin=1 --nLowMin=1 \
#     --extFile=caliboutput_mc16_newgeom_9ns_40mum.txt \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 500 \
#     --official \
#     --inDS mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.HITS.e3601_s2876 \
#     --outDS group.det-indet.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.TRTxAOD.e3601_s2876_r7886_trt099_LDTT40-00





# Zee starting from reco
#pathena --trf \
#       "Reco_tf.py \
#--inputRDOFile %IN \
#--ignoreErrors True \
#--maxEvents '100' \
#--skipEvents='%SKIPEVENTS' \
#--conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
#--geometryVersion ATLAS-R2-2015-03-01-00 \
#--ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
#--postInclude \"RecJobTransforms/UseFrontier.py\" \
#--autoConfiguration \"everything\" \
#--postExec \
#'all:CfgMgr.MessageSvc().setError+=[\"HepMcParticleLink\"];from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\");conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-scenario5_00-00\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-scenario5_00-00\"); conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_mc16_newgeom_12ns_40mum.txt\" ' \
#--preExec \
#'all:rec.OutputLevel.set_Value_and_Lock(WARNING); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(False);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False) ; InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
#'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
#    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#    --official \
#    --extFile=caliboutput_mc16_newgeom_12ns_40mum.txt \
#    --nFilesPerJob 1 \
#    --nEventsPerJob 100 \
#    --inDS mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.recon.RDO.e3601_s2876_r7886 \
#    --outDS group.det-indet.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.recon.TRTxAOD.e3601_s2876_r7886_trt098_LDTT-03
