### NEED TO ADD BACK IN WHATEVER SCHEME YOU WANT TO SUBMIT
### AS WELL AS PAUL'S BASIC PATCH


# cp ~keener/public/TRTCompression/TRT_LoLumRawData.cxx ../source/InnerDetector/InDetRawEvent/InDetRawData/src/


# cd ../build
# cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
# cd ../run

# ##  THIS ONE WAS SUBMITTED WITH LOSSLESS
# pathena --trf \
# "Reco_tf.py \
#     --inputRDOFile='%IN' \
# --ignoreErrors True \
# --maxEvents '500' \
# --skipEvents '%SKIPEVENTS' \
# --AMI 'r8905' \
# --autoConfiguration 'everything' \
# --conditionsTag 'default:OFLCOND-MC16-SDR-13' \
# --geometryVersion 'default:ATLAS-R2-2016-00-01-00' \
# --preExec \
# 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(False);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
# --inDS mc16_valid.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.RDO.e3698_s2995_r8905 \
# --outDS group.det-indet.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.TRTxAOD.e3698_s2995_r8905_trt097-01 \
# --official \
# --nEventsPerJob 500 \
# --nFilesPerJob 1

