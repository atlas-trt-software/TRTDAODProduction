
# ## Jpsill

cd $TestArea
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_JPSI.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
svn revert InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
cd ../build
cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
cd ../run

# #### default
pathena --trf \
"Reco_tf.py \
    --inputBSFile %IN \
--ignoreErrors True \
--maxEvents '200' \
--skipEvents '%SKIPEVENTS' \
--autoConfiguration 'everything' \
--AMI 'f716' \
--preExec \
'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
     --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
    --official \
    --nFilesPerJob 1 \
    --nEventsPerJob 200 \
    --site ANALY_MWT2_SL6 \
    --inDS group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
    --outDS group.det-indet.00304128.physics_Main.daq.TRTxAOD_JPSI.f716_trt098-03



# ### with new error scaling

# pathena --trf \
# "Reco_tf.py \
#     --inputBSFile %IN \
# --ignoreErrors True \
# --maxEvents '500' \
# --skipEvents '%SKIPEVENTS' \
# --autoConfiguration 'everything' \
# --AMI 'f716' \
# --postExec \
# 'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_data16_newgeom_noRtOrT0.txt\" ' \
# --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False);  from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#      --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --nFilesPerJob 1 \
#     --extFile=caliboutput_data16_newgeom_noRtOrT0.txt \
#     --nEventsPerJob 500 \
#     --inDS group.det-indet.00303832.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
#     --outDS group.det-indet.00303832.physics_Main.daq.TRTxAOD_JPSI.f716_trt098-newErrCalib-00


# pathena --trf \
# "Reco_tf.py \
#     --inputBSFile %IN \
# --ignoreErrors True \
# --maxEvents '200' \
# --skipEvents '%SKIPEVENTS' \
# --autoConfiguration 'everything' \
# --AMI 'f716' \
# --postExec \
# 'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_data16_newgeom_noRtOrT0.txt\" ' \
# --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False);  from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#      --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 200 \
#     --extFile=caliboutput_data16_newgeom_noRtOrT0.txt \
#     --inDS group.det-indet.00304178.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
#     --outDS group.det-indet.00304178.physics_Main.daq.TRTxAOD_JPSI.f716_trt098-newErrCalib-00


# pathena --trf \
# "Reco_tf.py \
#     --inputBSFile %IN \
# --ignoreErrors True \
# --maxEvents '200' \
# --skipEvents '%SKIPEVENTS' \
# --autoConfiguration 'everything' \
# --AMI 'f716' \
# --postExec \
# 'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_data16_newgeom_noRtOrT0.txt\" ' \
# --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False);  from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#      --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 200 \
#     --extFile=caliboutput_data16_newgeom_noRtOrT0.txt \
#     --inDS group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
#     --outDS group.det-indet.00304128.physics_Main.daq.TRTxAOD_JPSI.f716_trt098-newErrCalib-01




# ### with new error scaling but without slopes

# pathena --trf \
# "Reco_tf.py \
#     --inputBSFile %IN \
# --ignoreErrors True \
# --maxEvents '500' \
# --skipEvents '%SKIPEVENTS' \
# --autoConfiguration 'everything' \
# --AMI 'f716' \
# --postExec \
# 'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_data16_newgeom_noRtOrT0_noSlopes.txt\" ' \
# --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False);  from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#      --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --nFilesPerJob 1 \
#     --extFile=caliboutput_data16_newgeom_noRtOrT0_noSlopes.txt \
#     --nEventsPerJob 500 \
#     --inDS group.det-indet.00303832.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
#     --outDS group.det-indet.00303832.physics_Main.daq.TRTxAOD_JPSI.f716_trt098-newErrCalibNoSlopes-00


# pathena --trf \
# "Reco_tf.py \
#     --inputBSFile %IN \
# --ignoreErrors True \
# --maxEvents '200' \
# --skipEvents '%SKIPEVENTS' \
# --autoConfiguration 'everything' \
# --AMI 'f716' \
# --postExec \
# 'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_data16_newgeom_noRtOrT0_noSlopes.txt\" ' \
# --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False);  from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#      --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 200 \
#     --extFile=caliboutput_data16_newgeom_noRtOrT0_noSlopes.txt \
#     --inDS group.det-indet.00304178.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
#     --outDS group.det-indet.00304178.physics_Main.daq.TRTxAOD_JPSI.f716_trt098-newErrCalibNoSlopes-00



# pathena --trf \
# "Reco_tf.py \
#     --inputBSFile %IN \
# --ignoreErrors True \
# --maxEvents '200' \
# --skipEvents '%SKIPEVENTS' \
# --autoConfiguration 'everything' \
# --AMI 'f716' \
# --postExec \
# 'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_data16_newgeom_noRtOrT0_noSlopes.txt\" ' \
# --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False);  from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#      --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 200 \
#     --extFile=caliboutput_data16_newgeom_noRtOrT0_noSlopes.txt \
#     --inDS group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
#     --outDS group.det-indet.00304128.physics_Main.daq.TRTxAOD_JPSI.f716_trt098-newErrCalibNoSlopes-01



cd $TestArea
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_original.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
cd ../run