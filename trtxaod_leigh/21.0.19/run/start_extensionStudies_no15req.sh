# cd $TestArea 
# cp InDetAmbiScoringTool_no15requirement.cxx InnerDetector/InDetRecTools/InDetTrackScoringTools/src/InDetAmbiScoringTool.cxx
# cd ../build
# cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
# cd ../run



Reco_tf.py \
--inputRDOFile 'tmp.RDO' \
--ignoreErrors True \
--maxEvents '10' \
--DataRunNumber 222525 \
--jobNumber 222525 \
--conditionsTag 'OFLCOND-RUN12-SDR-31' \
--geometryVersion ATLAS-R2-2015-03-01-00 \
--autoConfiguration 'everything' \
--ignorePatterns 'ByteStreamInputSvc.+ERROR.+Skipping.+bad.+event|EventSelector.+ERROR.+badFragment.+data.+encountered|ByteStreamInputSvcWARNING.+ERROR.+message.+limit|EventSelector.+WARNING.+ERROR.+message.+limit' \
--postInclude 'RecJobTransforms/UseFrontier.py' \
--postExec \
'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup("/TRT/Cond/StatusHT","<forceRunNumber>286990</forceRunNumber>"); conddb.addOverride("/TRT/Cond/StatusHT","TrtStrawStatusHT-MC-run2-scenario5_00-00"); conddb.addOverride("/TRT/Calib/RT","TrtCalibRt-13TeV_MC-ArLT150-scenario5_00-01" ); conddb.addOverride("/TRT/Calib/T0","TrtCalibT0-13TeV_MC-ArLT150-scenario5_00-01");' \
'r2e:from AthenaCommon.AppMgr import ToolSvc; ToolSvc.InDetExtenScoringTool.minTRTPrecisionFraction=0.3; ToolSvc.InDetExtenScoringToolPixelPrdAssociation.minTRTPrecisionFraction=0.3 ' \
'e2d:from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=False;ServiceMgr.MuonRPC_CablingSvc.CorrFileName="LVL1confAtlasRUN2_ver016.corr";ServiceMgr.MuonRPC_CablingSvc.ConfFileName="LVL1confAtlasRUN2_ver016.data" ' \
--preExec \
'all:rec.OutputLevel.set_Value_and_Lock(DEBUG); rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(False);  jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25) ; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); from GeoModelSvc.GeoModelSvcConf import GeoModelSvc; GeoModelSvc.TRT_VersionOverride="TRT-GEO-03" ' \
'r2e:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
 --steering 'RAWtoESD:in+ESD' \
    --outputESDFile=ESD.pool.root