cp ../source/PhysicsAnalysis/PrimaryDPDMaker/share/DRAW_TRT.py ../source/PhysicsAnalysis/PrimaryDPDMaker/share/DRAW_ZMUMU.py 

cd ../build
cmake ../source ; make ; source x86_64-slc6-gcc49-opt/setup.sh
cd ../run
## somehow add primDPD.WriteEventlessFiles()=False

Reco_tf.py  \
     --inputBSFile '/afs/cern.ch/user/l/lschaef/work/public/data16_13TeV/data16_13TeV.00304337.physics_Main.merge.DRAW_ZMUMU.f716_m1616._0090.1' \
     --maxEvents '5' \
     --AMI f716 \
     --preExec 'all:from PrimaryDPDMaker.PrimaryDPDFlags import primDPD; primDPD.WriteEventlessFiles.set_Value_and_Lock(False); jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); rec.OutputLevel.set_Value_and_Lock(WARNING)  ' \
               'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
     --outputDRAW_ZMUMUFile DRAW_TRT.pool.root
svn revert ../source/PhysicsAnalysis/PrimaryDPDMaker/share/DRAW_ZMUMU.py 

