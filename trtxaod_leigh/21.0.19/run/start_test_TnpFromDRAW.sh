## Zll

cd $TestArea
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_Z.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
svn revert InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
cd ../build
cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
cd ../run
# --inputBSFile '/afs/cern.ch/user/l/lschaef/work/public/TestxAODs/group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0/group.det-indet.11017568.EXT0._416084.DRAW_TRT.pool.root' \


Reco_tf.py \
--inputESDFile 'tmp.ESD' \
--ignoreErrors True \
--maxEvents '10' \
--autoConfiguration 'everything' \
--AMI 'f716' \
--preExec \
'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
     --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD.pool.root

# Reco_tf.py \
#     --inputBSFile '/afs/cern.ch/user/l/lschaef/work/public/TestxAODs/group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0/group.det-indet.11017568.EXT0._416084.DRAW_TRT.pool.root' \
# --ignoreErrors True \
# --maxEvents '10' \
# --autoConfiguration 'everything' \
# --AMI 'f716' \
# --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#      --steering 'doRAWtoALL' \
#     --outputDAOD_IDTRKVALIDFile=InDetDxAOD_R2A.pool.root

cd $TestArea
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_original.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
cd ../run

