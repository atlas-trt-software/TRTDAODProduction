cd $TestArea 
svn revert InnerDetector/InDetRecTools/TRT_ElectronPidTools/src/TRT_LocalOccupancy.cxx
svn revert InnerDetector/InDetRecTools/TRT_ElectronPidTools/TRT_ElectronPidTools/TRT_LocalOccupancy.h
svn revert InnerDetector/InDetRecTools/TRT_ElectronPidTools/TRT_ElectronPidTools/ITRT_LocalOccupancy.h
svn revert InnerDetector/InDetCalibAlgs/TRT_CalibAlgs/src/TRTOccupancyInclude.cxx
svn revert InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_original.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
cd ../build
cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
cd ../run


# # default

# # # # Jpsimumu
# # pathena --trf \
# #        "Reco_tf.py \
# # --inputHITSFile %IN \
# # --ignoreErrors True \
# # --maxEvents '300' \
# # --skipEvents='%SKIPEVENTS' \
# # --DataRunNumber 222525 \
# # --jobNumber 222525 \
# # --conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
# # --geometryVersion ATLAS-R2-2015-03-01-00 \
# # --ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
# # --postInclude \"RecJobTransforms/UseFrontier.py\" \
# # --autoConfiguration \"everything\" \
# # --numberOfCavernBkg 0 \
# # --inputHighPtMinbiasHitsFile %HIMBIN \
# # --inputLowPtMinbiasHitsFile %LOMBIN \
# # --numberOfHighPtMinBias 0.12268057 \
# # --numberOfLowPtMinBias 39.8773194 \
# # --pileupFinalBunch 6 \
# # --postExec \
# # 'all:CfgMgr.MessageSvc().setError+=[\"HepMcParticleLink\"];from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\");conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-scenario5_00-00\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-scenario5_00-00\") ' \
# # 'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools[\"MergeMcEventCollTool\"].DoSlimming=False;job.StandardPileUpToolsAlg.PileUpTools[\"MdtDigitizationTool\"].LastXing=150' \
# # --preExec \
# # 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# #  'HITtoRDO:ScaleTaskLength=0.1; userRunLumiOverride={\"run\":284500,\"startmu\":0.0,\"endmu\":60.0,\"stepmu\":1.0,\"startlb\":1,\"timestamp\": 1446539185} ' \
# # 'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
# # --preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_run222525_v1.py' \
# #     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
# #     --official \
# #     --nFilesPerJob 1 \
# #     --nEventsPerJob 300 \
# #     --nHighMin=1 --nLowMin=1 \
# #     --highMinDS=mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS.e3581_s2876 \
# #     --lowMinDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2876 \
# #     --inDS mc15_13TeV.300000.Pythia8BPhotospp_A14_CTEQ6L1_pp_Jpsimu2p5mu2p5.simul.HITS.e3989_s2876 \
# #     --outDS group.det-indet.300000.Pythia8BPhotospp_A14_CTEQ6L1_pp_Jpsimu2p5mu2p5.simul.HITS.e3989_s2876_trt096-00




# # new error scaling

# # # Jpsimumu
# pathena --trf \
#        "Reco_tf.py \
# --inputHITSFile %IN \
# --ignoreErrors True \
# --maxEvents '300' \
# --skipEvents='%SKIPEVENTS' \
# --DataRunNumber 222525 \
# --jobNumber 222525 \
# --conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
# --geometryVersion ATLAS-R2-2015-03-01-00 \
# --ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
# --postInclude \"RecJobTransforms/UseFrontier.py\" \
# --autoConfiguration \"everything\" \
# --numberOfCavernBkg 0 \
# --inputHighPtMinbiasHitsFile %HIMBIN \
# --inputLowPtMinbiasHitsFile %LOMBIN \
# --numberOfHighPtMinBias 0.12268057 \
# --numberOfLowPtMinBias 39.8773194 \
# --pileupFinalBunch 6 \
# --postExec \
# 'all:CfgMgr.MessageSvc().setError+=[\"HepMcParticleLink\"];from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\");conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-scenario5_00-00\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-scenario5_00-00\") ; conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_mc16_newgeom_noRtOrT0.txt\" ' \
# 'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools[\"MergeMcEventCollTool\"].DoSlimming=False;job.StandardPileUpToolsAlg.PileUpTools[\"MdtDigitizationTool\"].LastXing=150' \
# --preExec \
# 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);InDetFlags.doSlimming.set_Value_and_Lock(False);  from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool  ' \
#  'HITtoRDO:ScaleTaskLength=0.1; userRunLumiOverride={\"run\":284500,\"startmu\":0.0,\"endmu\":60.0,\"stepmu\":1.0,\"startlb\":1,\"timestamp\": 1446539185} ' \
# 'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
# --preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_run222525_v1.py' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --nFilesPerJob 1 \
#     --extFile=caliboutput_mc16_newgeom_noRtOrT0.txt \
#     --nEventsPerJob 300 \
#     --nHighMin=1 --nLowMin=1 \
#     --highMinDS=mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS.e3581_s2876 \
#     --lowMinDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2876 \
#     --inDS mc15_13TeV.300000.Pythia8BPhotospp_A14_CTEQ6L1_pp_Jpsimu2p5mu2p5.simul.HITS.e3989_s2876 \
#     --outDS group.det-indet.300000.Pythia8BPhotospp_A14_CTEQ6L1_pp_Jpsimu2p5mu2p5.simul.HITS.e3989_s2876_trt096-newErrCalib-00


# # new error scaling but without slopes

# # # Jpsimumu
# pathena --trf \
#        "Reco_tf.py \
# --inputHITSFile %IN \
# --ignoreErrors True \
# --maxEvents '300' \
# --skipEvents='%SKIPEVENTS' \
# --DataRunNumber 222525 \
# --jobNumber 222525 \
# --conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
# --geometryVersion ATLAS-R2-2015-03-01-00 \
# --ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
# --postInclude \"RecJobTransforms/UseFrontier.py\" \
# --autoConfiguration \"everything\" \
# --numberOfCavernBkg 0 \
# --inputHighPtMinbiasHitsFile %HIMBIN \
# --inputLowPtMinbiasHitsFile %LOMBIN \
# --numberOfHighPtMinBias 0.12268057 \
# --numberOfLowPtMinBias 39.8773194 \
# --pileupFinalBunch 6 \
# --postExec \
# 'all:CfgMgr.MessageSvc().setError+=[\"HepMcParticleLink\"];from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\");conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-scenario5_00-00\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-scenario5_00-00\") ; conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_mc16_newgeom_noRtOrT0_noSlopes.txt\" ' \
# 'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools[\"MergeMcEventCollTool\"].DoSlimming=False;job.StandardPileUpToolsAlg.PileUpTools[\"MdtDigitizationTool\"].LastXing=150' \
# --preExec \
# 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);InDetFlags.doSlimming.set_Value_and_Lock(False) ;  from TrkRIO_OnTrackCreator.TrkRIO_OnTrackCreatorConf import Trk__RIO_OnTrackErrorScalingTool; InDetRotErrorScalingTool = Trk__RIO_OnTrackErrorScalingTool(name=\"RIO_OnTrackErrorScalingTool\",doTRTErrorScaling=False); ToolSvc += InDetRotErrorScalingTool ' \
#  'HITtoRDO:ScaleTaskLength=0.1; userRunLumiOverride={\"run\":284500,\"startmu\":0.0,\"endmu\":60.0,\"stepmu\":1.0,\"startlb\":1,\"timestamp\": 1446539185} ' \
# 'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
# --preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_run222525_v1.py' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --nFilesPerJob 1 \
#     --extFile=caliboutput_mc16_newgeom_noRtOrT0_noSlopes.txt \
#     --nEventsPerJob 300 \
#     --nHighMin=1 --nLowMin=1 \
#     --highMinDS=mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS.e3581_s2876 \
#     --lowMinDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2876 \
#     --inDS mc15_13TeV.300000.Pythia8BPhotospp_A14_CTEQ6L1_pp_Jpsimu2p5mu2p5.simul.HITS.e3989_s2876 \
#     --outDS group.det-indet.300000.Pythia8BPhotospp_A14_CTEQ6L1_pp_Jpsimu2p5mu2p5.simul.HITS.e3989_s2876_trt096-newErrCalibNoSlopes-00


# # # # with correct calibration for 150 eV Ar LT setting

# # # Jpsimumu
pathena --trf \
       "Reco_tf.py \
--inputHITSFile %IN \
--ignoreErrors True \
--maxEvents '500' \
--skipEvents='%SKIPEVENTS' \
--DataRunNumber 222525 \
--jobNumber 222525 \
--conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
--geometryVersion ATLAS-R2-2015-03-01-00 \
--ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
--postInclude \"RecJobTransforms/UseFrontier.py\" \
--autoConfiguration \"everything\" \
--numberOfCavernBkg 0 \
--inputHighPtMinbiasHitsFile %HIMBIN \
--inputLowPtMinbiasHitsFile %LOMBIN \
--numberOfHighPtMinBias 0.312197744 \
--numberOfLowPtMinBias 59.68780226 \
--pileupFinalBunch 6 \
--postExec \
'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\"); conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-ArLT150-scenario5_00-01\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-ArLT150-scenario5_00-01\");' \
'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools[\"MergeMcEventCollTool\"].DoSlimming=False;job.StandardPileUpToolsAlg.PileUpTools[\"MdtDigitizationTool\"].LastXing=150' \
--preExec \
'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(True); rec.OutputLevel.set_Value_and_Lock(WARNING); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
'HITtoRDO:ScaleTaskLength=0.1; userRunLumiOverride={\"run\":284500,\"startmu\":0.0,\"endmu\":60.0,\"stepmu\":2.5,\"startlb\":1,\"timestamp\": 1446539185} ' \
'r2e:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
--preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_muRange.py' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
    --official \
    --nFilesPerJob 1 \
    --nEventsPerJob 500 \
    --nHighMin=1 --nLowMin=1 \
    --highMinDS=mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS.e3581_s2876 \
    --lowMinDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2876 \
    --inDS mc15_13TeV.300000.Pythia8BPhotospp_A14_CTEQ6L1_pp_Jpsimu2p5mu2p5.simul.HITS.e3989_s2876 \
    --outDS group.det-indet.300000.Pythia8BPhotospp_A14_CTEQ6L1_pp_Jpsimu2p5mu2p5.recon.TRTxAOD.e3989_s2876_trt099-00

# # # # Jpsiee
# pathena --trf \
#        "Reco_tf.py \
# --inputHITSFile %IN \
# --ignoreErrors True \
# --maxEvents '500' \
# --skipEvents='%SKIPEVENTS' \
# --DataRunNumber 222525 \
# --jobNumber 222525 \
# --conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
# --geometryVersion ATLAS-R2-2015-03-01-00 \
# --ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
# --postInclude \"RecJobTransforms/UseFrontier.py\" \
# --autoConfiguration \"everything\" \
# --numberOfCavernBkg 0 \
# --inputHighPtMinbiasHitsFile %HIMBIN \
# --inputLowPtMinbiasHitsFile %LOMBIN \
# --numberOfHighPtMinBias 0.312197744 \
# --numberOfLowPtMinBias 59.68780226 \
# --pileupFinalBunch 6 \
# --postExec \
# 'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\"); conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-ArLT150-scenario5_00-01\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-ArLT150-scenario5_00-01\");' \
# 'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools[\"MergeMcEventCollTool\"].DoSlimming=False;job.StandardPileUpToolsAlg.PileUpTools[\"MdtDigitizationTool\"].LastXing=150' \
# --preExec \
# 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(True); rec.OutputLevel.set_Value_and_Lock(WARNING); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# 'HITtoRDO:ScaleTaskLength=0.1; userRunLumiOverride={\"run\":284500,\"startmu\":0.0,\"endmu\":60.0,\"stepmu\":2.5,\"startlb\":1,\"timestamp\": 1446539185} ' \
# 'r2e:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
# --preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_muRange.py' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 500 \
#     --nHighMin=1 --nLowMin=1 \
#     --highMinDS=mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS.e3581_s2876 \
#     --lowMinDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2876 \
#     --inDS mc15_13TeV.423200.Pythia8B_A14_CTEQ6L1_Jpsie3e3.simul.HITS.e3869_s2876 \
#     --site=ANALY_MWT2_SL6,MWT2_UC_LOCALGROUPDISK \
#     --outDS group.det-indet.423200.Pythia8B_A14_CTEQ6L1_Jpsie3e3.recon.TRTxAOD.e3869_s2876_trt099-00


