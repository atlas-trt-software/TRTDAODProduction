cd $TestArea 
svn revert InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_Z.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
cd ../build
cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
cd ../run


# pathena --trf \
# "Reco_tf.py \
#     --inputBSFile %IN \
#     --ignoreErrors True \
#     --maxEvents '200' \
#     --skipEvents '%SKIPEVENTS' \
#     --autoConfiguration 'everything' \
#     --AMI 'f716' \
#  --postExec \
#  'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_data16_newgeom_noRtOrT0_lowdt_tubes.txt\" ' \
#     --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#     --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --extFile=caliboutput_data16_newgeom_noRtOrT0_lowdt_tubes.txt \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 200 \
#     --inDS group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
#     --outDS group.det-indet.00304128.physics_Main.daq.TRTxAOD_Z.f716_trt099_LDTT-00



# # new calib file
# pathena --trf \
# "Reco_tf.py \
#     --inputBSFile %IN \
#     --ignoreErrors True \
#     --maxEvents '200' \
#     --skipEvents '%SKIPEVENTS' \
#     --autoConfiguration 'everything' \
#     --AMI 'f716' \
#  --postExec \
#  'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_data16_newgeom_noRtOrT0_lowdt_tubes_new.txt\" ' \
#     --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#     --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --extFile=caliboutput_data16_newgeom_noRtOrT0_lowdt_tubes_new.txt \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 200 \
#     --inDS group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
#     --outDS group.det-indet.00304128.physics_Main.daq.TRTxAOD_Z.f716_trt099_LDTT-01



# # new calib file
# pathena --trf \
# "Reco_tf.py \
#     --inputBSFile %IN \
#     --ignoreErrors True \
#     --maxEvents '200' \
#     --skipEvents '%SKIPEVENTS' \
#     --autoConfiguration 'everything' \
#     --AMI 'f716' \
#  --postExec \
#  'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_data16_newgeom_noRtOrT0_lowdt_tubes_v80mum.txt\" ' \
#     --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#     --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --extFile=caliboutput_data16_newgeom_noRtOrT0_lowdt_tubes_v80mum.txt \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 200 \
#     --inDS group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
#     --outDS group.det-indet.00304128.physics_Main.daq.TRTxAOD_Z.f716_trt099_LDTT80-00


# again new calib file
pathena --trf \
"Reco_tf.py \
    --inputBSFile %IN \
    --ignoreErrors True \
    --maxEvents '200' \
    --skipEvents '%SKIPEVENTS' \
    --autoConfiguration 'everything' \
    --AMI 'f716' \
 --postExec \
 'all:conddb.blockFolder(\"/TRT/Calib/slopes\"); conddb.blockFolder(\"/TRT/Calib/errors2d\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"caliboutput_data16_newgeom_noRtOrT0_9ns_40mum.txt\" ' \
    --preExec \
'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
    --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
    --official \
    --extFile=caliboutput_data16_newgeom_noRtOrT0_9ns_40mum.txt \
    --nFilesPerJob 1 \
    --nEventsPerJob 200 \
    --inDS group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
    --outDS group.det-indet.00304128.physics_Main.daq.TRTxAOD_Z.f716_trt099_LDTT40-00



# pathena --trf \
# "Reco_tf.py \
#     --inputBSFile %IN \
#     --ignoreErrors True \
#     --maxEvents '200' \
#     --skipEvents '%SKIPEVENTS' \
#     --autoConfiguration 'everything' \
#     --AMI 'f716' \
#     --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
#     --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
#     --official \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 200 \
#     --inDS group.det-indet.00304128.physics_Main.daq.DRAW_TRT07.f716_EXT0 \
#     --outDS group.det-indet.00304128.physics_Main.daq.TRTxAOD_Z.f716_trt099Test

cd $TestArea
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_original.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
