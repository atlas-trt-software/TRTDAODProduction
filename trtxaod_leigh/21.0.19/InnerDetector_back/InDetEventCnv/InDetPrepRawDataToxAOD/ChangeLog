2017-02-24 Alex Kastanas
  * Improved performance of the broken clusters tagging
  * Fix for intialisation ordering warnings during compilation
  * Tagging as InDetPrepRawDataToxAOD-00-01-44-01

2016-10-03 Alex Kastanas
  * Added a SiHit-to-cluster matching scheme based on SDO barcodes, rather than geometric matching
  * Added a flag for clusters with truth particles contributing that also contribute to other clusters
  * Tagging as InDetPrepRawDataToxAOD-00-01-39-01

2016-08-22 Alex Alonso
	* New sequencer for the ID xAOD
	* Tagging as InDetPrepRawDataToxAOD-00-01-43

2016-08-19 Alex Alonso
	* Change the Split track collection name, as it overlapes with muon
	track collection
	* Tag as InDetPrepRawDataToxAOD-00-01-42

2016-07-14 Leigh Schaefer <leigh.schaefer@cern.ch>
	* Remove initialization of TRT dE/dx tool from share/InDetDxAOD.py since this tool is no longer included in DerivationFrameworkInDet
	* Change TRT_DriftCircles.localXError to store uncertainty rather than covariance in src/TRT_PrepDataToxAOD.cxx
	* Tag as InDetPrepRawDataToxAOD-00-01-40

2016-07-06 Soshi Tsuno
  * Allow to monitor calibration constant / HV / DCS status.

2016-05-07 Goetz Gaycken (obo Leigh Schaefer)
	*  Allow to identify emulated  Krypton and Argon.

2016-04-13 Susumu Oda <Susumu.Oda@cern.ch>
	* Changed the order of algorithms to run RDO/PRD to xAOD tools
	  only events are accepeted.
	* Tagging as InDetPrepRawDataToxAOD-00-01-37

2016-04-12 Susumu Oda <Susumu.Oda@cern.ch>
	* Changed names of algorithms and tools in share/SCTxAOD.py
	  to avoid coflict with share/InDetDxAOD.py.
	* Move the prescale tool in share/SCTxAOD.py to preselection.
	* Tagging as InDetPrepRawDataToxAOD-00-01-36

2016-04-12 Susumu Oda <Susumu.Oda@cern.ch>
	* Add a prescale factor for reduction file size to share/SCTxAOD.py.
	* Add python/SCTxAODJobProperties.py to configure the prescale factor.
	* Tagging as InDetPrepRawDataToxAOD-00-01-35

2016-03-17 Kilian Rosbach <Kilian.Rosbach@cern.ch>
	* Testing changes related to ATLASRECTS-2860 from yesterday
	* Tagging as InDetPrepRawDataToxAOD-00-01-34

2016-03-16 Simon Viel
        * Re-commit Kilian Rosbach's changes on top of the latest
        * NOT tagged

2016-03-16 Simon Viel
        * On behalf of Weiming Yao and Peilian Liu
        * Add pseudo-tracking (aka truth-tracking) in share/InDetDxAOD.py
        * NOT including Kilian Rosbach's latest commit
        * Tagged InDetPrepRawDataToxAOD-00-01-33

2016-03-16 Kilian Rosbach <Kilian.Rosbach@cern.ch>
	* Adding SCT_RawDataToxAOD algorithm (ATLASRECTS-2860)

2016-03-10 Susumu Oda <Susumu.Oda@cern.ch>
	* Use m_SCTHelper->strip(sctRdo->identify()) instead of sctRdo->getStrip()
	  in src/SCT_PrepDataToxAOD.cxx.
	  https://its.cern.ch/jira/browse/ATLASSIM-1907
	* Tagged InDetPrepRawDataToxAOD-00-01-32

2016-01-19 Nick Barlow
	* A couple more flags in InDetDxAODJobProperties.py allowing dumping of RDO information for Pix and SCT clusters to be turned on and off.
	* Tagged InDetPrepRawDataToxAOD-00-01-31

2016-01-18 Nick Barlow
	* Add python/InDetDxAODJobProperties.py, containing preExec-configurable flags for the DxAOD, and modify InDetDxAOD.py to use them.
	* Tagged InDetPrepRawDataToxAOD-00-01-29

2016-01-14 Goetz Gaycken
	* Fix AuxStore type name clashes by renaming some decorations.
	* Determin TRT gas type and add it as decoration instead of isArgon
	  flag.

2015-09-24 Nick Barlow <nick.barlow@cern.ch>
	* Modify SCT_PrepRawDataToxAOD.cxx to also add auxdata for timing info on SCT clusters.
	* Tagged InDetPrepRawDataToxAOD-00-01-27

2015-09-03 Nick Barlow <nick.barlow@cern.ch>
	* Add jobOptions file share/SCTxAOD.py to make cut-down version of DAOD_IDTRKVALID.
	* Tagged InDetPrepRawDataToxAOD-00-01-26

2015-08-12 Simone Pagan Griso <simone.pagan.griso@cern.ch>
	* Removed TRT hits until we filter them with higher-pT tracks
	* Added metadata needed for trigger decision
	* Tagged InDetPrepRawDataToxAOD-00-01-25

2015-06-20 Simone Pagan Griso <simone.pagan.griso@cern.ch>
	* Removed VERBOSE output left from debugging

2015-06-20 Simone Pagan Griso <simone.pagan.griso@cern.ch>
	* Adding thinning of hits on track and ability to thin tracks
	* Switched on by default hits thinning and TRT hits on track
	* Tagged InDetPrepRawDataToxAOD-00-01-24

2015-06-04 Olivier Arnaez <olivier.arnaez@cern.ch>
        * Turning on dumpUnassociatedHits and dumpLArCollisionTime by default
        * Tagged InDetPrepRawDataToxAOD-00-01-23

2015-06-03 Olivier Arnaez <olivier.arnaez@cern.ch>
	* Turning off dumpUnassociatedHits and dumpLArCollisionTime by default (short-term patch, needs to be reverted)
	* Tagged InDetPrepRawDataToxAOD-00-01-22

2015-06-02 Olivier Arnaez <olivier.arnaez@cern.ch>
	* Removing xAOD::TrigT2MbtsBitsContainer,HLT_T2Mbts,HLT_T2MbtsAux.,HLT_xAOD__TrigT2MbtsBitsContainer_T2MbtsAux. from the output
	* Adding TileCellContainer#MBTSContainer to the output stream (for MBTS offline timing information)
	* Tagged InDetPrepRawDataToxAOD-00-01-21

2015-05-15 Simone Pagan Griso <simone.pagan.griso@cern.ch>
	* Added split tracks creation for cosmic reconstruction
	* Tagged InDetPrepRawDataToxAOD-00-01-20

2015-05-26 Olivier Arnaez <olivier.arnaez@cern.ch>
	* Adding basic trigger info (xAOD::TrigT2MbtsBitsContainer,xTrigDecision,BCM_RDOs,TrigNavigation,TrigConfKeys,HLTResult_HLT,xTrigDecisionAux.,TrigNavigationAux.,HLT_T2Mbts,HLT_T2MbtsAux.,HLT_xAOD__TrigT2MbtsBitsContainer_T2MbtsAux.)
	* Tagged InDetPrepRawDataToxAOD-00-01-19

2015-05-22 Olivier Arnaez <olivier.arnaez@cern.ch>
	* Adding LArCollisionTime decoration
	* Tagged InDetPrepRawDataToxAOD-00-01-18

2015-05-22 Olivier Arnaez <olivier.arnaez@cern.ch>
	* Adding unassociated hits decorations
	* Tagged InDetPrepRawDataToxAOD-00-01-17
2015-04-30 Simone Pagan Griso <simone.pagan.griso@cern.ch>
	* Fix python to determine if is simulation
	* Tagged InDetPrepRawDataToxAOD-00-01-16
2015-04-30 Simone Pagan Griso <simone.pagan.griso@cern.ch>
	* Restructure to be more robust against missing info
	* Corrected few small bugs
	* Tuned to a reasonable default and improved readibility of share/InDetDxAOD.py jobOptions
	* Tagged InDetPrepRawDataToxAOD-00-01-15
2015-04-29 Shaun Roe
  * fixed editing error
  * tag as InDetPrepRawDataToxAOD-00-01-14
2015-04-27 Shaun Roe
  * Fix coverity issues:
28774 18/12/2014 (Medium) Uninitialized pointer field :/InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/PixelPrepDataToxAOD.cxx in function "PixelPrepDataToxAOD"
28766 18/12/2014 (Medium) Unchecked dynamic_cast :/InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/PixelPrepDataToxAOD.cxx in function "addNNInformation"
28767 18/12/2014 (Medium) Unchecked dynamic_cast :/InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/PixelPrepDataToxAOD.cxx in function "addNNTruthInfo"
28768 18/12/2014 (Medium) Unchecked dynamic_cast :/InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/PixelPrepDataToxAOD.cxx in function "getCellIdWeightedPosition"
28769 18/12/2014 (Medium) Using invalid iterator :/InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/PixelPrepDataToxAOD.cxx in function "addNNInformation"
28773 18/12/2014 (Medium) Uninitialized pointer field :/InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/SCT_PrepDataToxAOD.cxx in function "SCT_PrepDataToxAOD"
28772 18/12/2014 (Medium) Uninitialized pointer field :/InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/TRT_PrepDataToxAOD.cxx in function "TRT_PrepDataToxAOD"

  * tag as InDetPrepRawDataToxAOD-00-01-13

2015-04-15 Anthony Morley
  * Fix coverity issue 28771
  * Tagged as InDetPrepRawDataToxAOD-00-01-12

2015-02-04 Nick Styles
  * Changes from J. Catmore for stream name
  * Tagged as InDetPrepRawDataToxAOD-00-01-11

2015-03-29 Silvia Miglioranzi <Silvia.Miglioranzi@cern.ch>
  * share/InDetDxAOD.py
    added call to Charge->ToT pix conversion alg
  * Tag InDetPrepRawDataToxAOD-00-01-10

2015-03-09 Simone Pagan Griso
  * Fixed previous bug introduced with SLHC flag
  * Tag InDetPrepRawDataToxAOD-00-01-08

2015-03-06 Nick Barlow
  * Modified src/SCT_PrepDataToxAOD.cxx to add SCT cluster size
  * Tag InDetPrepRawDataToxAOD-00-01-07
	
2015-03-04 Hideyude Oide
  * Modified PixelPrepDataToxAOD.cxx to change col/row variable names to eta/phi_pixel_index
  * Tag InDetPrepRawDataToxAOD-00-01-06

2015-03-03 Simone Pagan Griso obo Susumu Oda
  * Protect share/InDetDxAOD.py excluding TRT in the case of HL-LHC samples
  * Tag InDetPrepRawDataToxAOD-00-01-05

2015-02-21 Daiki Hayakawa
  * Modify share/InDetDxAOD.py to prevent crashing
  * Tag InDetPrepRawDataToxAOD-00-01-04

2015-02-21 Daiki Hayakawa <daiki.hayakawa@cern.ch>
  * Modify share/InDetDxAOD.py about EventInfoBSErrorDecorator
  * Tag InDetPrepRawDataToxAOD-00-01-03

2015-02-05 Anthony Morley
  * Correct the silly mistake i made
  * Tag InDetPrepRawDataToxAOD-00-01-02

2015-02-05 Anthony Morley
  * Update python config
  * Tag InDetPrepRawDataToxAOD-00-01-01

2014-12-04 Anthony Morley
  * Change xAOD class names
  * Tag InDetPrepRawDataToxAOD-00-01-00

2014-11-26 Anthony Morley
  * Add additional information to the NN input
  * Tag InDetPrepRawDataToxAOD-00-00-05

2014-11-13 Anthony Morley
  * Protect against silling things is the pixel code
  * Add joboptions
  * Tag InDetPrepRawDataToxAOD-00-00-04

2014-10-06 Anthony Morley
  * Correct some of the pixel plots
  * Tag InDetPrepRawDataToxAOD-00-00-03

2014-10-02 Simone Pagan Griso
	* Add switch for truth info, protect in case of data
	* Tag InDetPrepRawDataToxAOD-00-00-02

2014-08-13 Anthony Morley
  * Add Pixels and SCT
	* Add some additional information to the TRT
	* Tagged as InDetPrepRawDataToxAOD-00-00-01

2014-08-11 Alex Alonso <Alejandro.Alonso at cern.ch>
	* Initial implementation. First for TRT.
	* Code "inspired" on: MuonSpectrometer/MuonCnv/MuonPrepRawDataToxAOD
	* Tagged as InDetPrepRawDataToxAOD-00-00-00 
