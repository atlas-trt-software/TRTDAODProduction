2016-04-15 Leigh Schaefer
	* Store eProbabilityToT in PID output vector, instead of dEdx
	* Tag as TRT_ElectronPidTools-01-02-07

2016-04-11 Leigh Schaefer
	* Update TRT_LocalOccupancy retrieval of TRT_StrawStatusSummarySvc to work with TRT_GeoModel-00-02-70
	* Tag as TRT_ElectronPidTools-01-02-06

2016-04-10 Leigh Schaefer
	* Fix cmake compile warnings
	* Tag as TRT_ElectronPidTools-01-02-05

2016-04-05 Leigh Schaefer
	* Move trackhit array to header
	* This is better for malloc
	* Tag as TRT_ElectronPidTools-01-02-04

2016-04-04 Leigh Schaefer
	* Bug fix for online occupancy calculation in endcap A, up to date with trunk
	* MUST BE USED WITH TRT_ConditionsServices-01-01-08 OR LATER!
	* Tag as TRT_ElectronPidTools-01-02-03

2016-03-21 Jared Vasquez
  * Removed disabling of ZR correction factor (will fix in DB)

2016-03-19 Jared Vasquez
	* Merged branch back to trunk and tagged
	* Removed documents (outdated)
	* Disabled corrections of ZR & TW without track parameters (Validated)
	* Disabled ZR correction factor ( should have been done before, Validated ) 
	* Tag as TRT_ElectronPidTools-01-02-01

2016-03-18 Jared Vasquez
	* Fixed bug in usedHits
	* Renamed sum_ToT_by_sum_L to dEdx  
	* Removed unused GetToT and GetD functions from header
	* Removed brem probability and job options from code

2016-03-17 Jared Vasquez
	* Implemented ToT probability from ToT Tool (Validated)
	* Added Comments for Alex

2016-03-17 Leigh Schaefer
	* Big fix for estimating Kr as Ar

2016-03-16 Jared Vasquez
	* Removed ToT Calculator (Validated)
	* Removed extremely outdated code from comments

2016-03-12 Leigh Schaefer
	* Estimate pHT and CF of Krypton straws as if they were Ar
	* Tag as TRT_ElectronPidTools-01-01-20

2016-03-03 Leigh Schaefer
	* Estimate pHT and CF of Krypton straws as if they were Xe
	* Tag as TRT_ElectronPidTools-01-01-19

2016-01-29 Leigh Schaefer
	* Add possibilities EmulatedArgon(6) and EmulatedKrypton(7) for compatibility with upcoming changes to getStatusHT
	* Tag as TRT_ElectronPidTools-01-01-18

2016-01-12 Leigh Schaefer
	* Add back in deprecated OccupancyUsedInPID to trunk
	* Tag as TRT_ElectronPidTools-01-01-17

2016-01-07 Leigh Schaefer
	* Estimate EB,Ar straws as EA,Ar for pHT and as EB,Xe for CF
	* This does not include Kr->Ar mimicking but is otherwise the same as TRT_ElectronPidTools-01-01-15-03
	* Also clean up, update Changelog
	* Tag as TRT_ElectronPidTools-01-01-16

2016-01-03 Leigh Schaefer
	* THIS IS ONLY FOR USE IN ARGON MIMICKING STUDIES!
	* Estimate EB,Ar straws as EA,Ar for pHT and as EB,Xe for CF
	* Tag as TRT_ElectronPidTools-01-01-15-03 (not committed to trunk)

2015-12-21 Leigh Schaefer
	* Add back in deprecated OccupancyUsedInPID
	* Tag as TRT_ElectronPidTools-01-01-15-02

2015-12-18 Leigh Schaefer
	* THIS IS ONLY FOR USE IN ARGON MIMICKING STUDIES!
	* Mimic Argon in Krypton straws
	* Tag as TRT_ElectronPidTools-01-01-15-01 (not committed to trunk)

2015-12-18 Leigh Schaefer
	* Fix mistakes in previous commit--intentions were to commit what is described below
	* Tag as TRT_ElectronPidTools-01-01-15


2015-12-18 Leigh Schaefer
	* THIS IS ONLY FOR USE IN ARGON MIMICKING STUDIES!
	* Mimic Argon in Krypton straws
	* Tag as TRT_ElectronPidTools-01-01-14-01

2015-12-18 Leigh Schaefer
	* Hardcode upper and lower limits for electron HT probabilities
	* LowerLimit = 0.0, UpperLimit = 0.0 (strange, will be fixed for release 21)
	* Allow ToT limits to be set by blob
	* Tag as TRT_ElectronPidTools-01-01-14

2015-12-07 Leigh Schaefer
	* HACK to make Kr straws be interpreted as Ar
	* This is for digitization studies
	* Tag as TRT_ElectronPidTools-01-01-13-01 (not committed to trunk)

2015-12-07 Leigh Schaefer
	* Hardcode upper and lower limits for electron probabilities
	* LowerLimit = 0.02, UpperLimit = 0.98
	* This removes the limits' dependence on Blob which is not always initialized
	* Tag as TRT_ElectronPidTools-01-01-13

2015-11-24 Leigh Schaefer
	* Reintroduce option to use occupancy in PID or not (default: true)
	* For validation of HLT use
	* If not using occupancy, fix to 35%
	* Tag as TRT_ElectronPidTools-01-01-11-01

2015-11-06 Leigh Schaefer
	* Remove deprecated flag "OccupancyUsedInPID" from TRT_ElectronPidToolRun2
	* Tag as TRT_ElectronPidTools-01-01-12

2015-10-19 Alex Alonso 
	* Add public interface for Fast Digi. 
	* Tag as TRT_ElectronPidTools-01-01-11

2015-10-19 Leigh Schaefer
	* Update T0 lookup so online RDOs match offline DCs exactly in local occupancy tool
	* Tag as TRT_ElectronPidTools-01-01-10

2015-10-13 Leigh Schaefer
	* change TRT_LocalOccupancy calculation to count Drift Circles in offline calculation, RDOs with validity gate in online calculation.
	* add configurable options to set validity gate, and to decide whether to do T0 lookup for validity gate.
	* Tags as TRT_ElectronPidTools-01-01-09

2015-09-30 Alex Alonso 
	* Add back the removed flag, as dummy
	* THIS HAS TO BE REVERTED ONCE JIRI ACCEPT IT IN THE TRIGGER.
	* Tag as TRT_ElectronPidTools-01-01-08

2015-09-29 Leigh Schaefer
	* Add configurable for TRTStrawStatusSummarySvc in TRT_LocalOccupancy.cxx
	* Tag as TRT_ElectronPidTools-01-01-07

2015-09-08 Jared Vasquez
  * Fixed issue in 2D fit function.

2015-09-07 Leigh Schaefer
	* Get rid of some redundant occupancy arrays for online / offline use
	* Add documentation to TRT_ElectronPidToolRun2.h to explain inclusion of track occupancy in PID vector.
	* Tag as TRT_ElectronPidTools-01-01-04

2015-09-01 Leigh Schaefer
	* Add local occupancy to PIDvalues vector, return value from electronProbability(track)
	* Fix LocalOccupancy(const Trk::Track track) to LocalOccupancy(const Trk::Track& track)
	* Tag as TRT_ElectronPidTools-01-01-03

2015-08-17 Alex Alonso 
	* Add Argon and Krypton correction. Ready to read/write to db 
	* Tag as TRT_ElectronPidTools-01-01-02

2015-08-14 Leigh Schaefer
	* use wider validity gate with no T0 shift for occupancy calculation
	* cache online RDO counts to speed things up
	* Tag as TRT_ElectronPidTools-01-01-01

2015-08-13 Jared Vasquez
	* Added new fit function and commneted MC15 derived correction factors
	* Tag as TRT_ElectronPidTools-01-01-00

2015-08-07 Leigh Schaefer
	* occupancy calculated using RDO with validity gate, rather than DC
	* same validity gate as TRT_DriftCircleTool, so offline RDO and DC count should be the same
	* change name of BeginEvent() to StartEvent() for use in HLT
	* update online occ calculation to match offline
	* Tag as TRT_ElectronPidTools-01-00-31

2015-07-03 Andrew Beddall
	* src/TRT_ElectronPidToolRun2.cxx forgot to remove cout statements
	* Tag as TRT_ElectronPidTools-01-00-30

2015-07-03 Andrew Beddall
        * src/TRT_ElectronPidToolRun2.cxx
          Removed 180 lines of commented code inside electronProbability_old().
          Removed unused "define PI".
        * src/TRT_ElectronPidToolRun2_HTcalculation.cxx
          added DEBUG output to check PID calculations.
        * Tag as TRT_ElectronPidTools-01-00-29

2015-07-02 Andrew Beddall
        * src/TRT_ElectronPidToolRun2.cxx
          Pick up Krypton 'GasType' if supported by getStatusHT
          This is backward-compatible.
        * Tag as TRT_ElectronPidTools-01-00-28

2015-06-27 Alex Alonso 
	* Implementation to read the HT parameters from DB. 
	* Also, script to produce COOL file in place
	* Tag as TRT_ElectronPidTools-01-00-27

2015-06-26 Alex Alonso 
	* Clean the way to store the variables and access them. Bin dependence and so on.
	* Baseline for the DB implementation.
	* Tag as TRT_ElectronPidTools-01-00-26

2015-06-05 Troels Petersen
	* Update of the pHT calculation to use 2D fits of gamma+occupancy with reasonable constants for 2015 data.
	* Update of correction functions (latest from data) with change in range and binning for endcap ZR: 630-1030mm in 40 bins.
	* Tag as TRT_ElectronPidTools-01-00-25

2015-06-11 Leigh Schaefer
	* src/TRT_LocalOccupancy.cxx
	  LocalOccupancy(track) returns a float rather than a std::vector<float>
	  BeginEvent() and LocalOccupancy(track) work for offline and trigger environment
	* src/TRT_ElectronPidToolRun2.cxx
	  change the way LocalOccupancy is accessed, to be compatible with changes above
	* Note this must be used with TRT_ConditionsServices-01-00-00-06 or later
	* Tag as TRT_ElectronPidTools-01-00-24

2015-06-05 Andrew Beddall
        * src/BaseTRTPIDCalculator.cxx
          PrintBlob() changed from ERROR to DEBUG
        * src/TRT_ElectronPidToolRun2.cxx 
          added an INFO to see the state of m_OccupancyUsedInPID.
	* Tag as TRT_ElectronPidTools-01-00-23

2015-06-05 Troels Petersen
	* Update of the pHT calculation to use only HT Middle Bit
	* Update of the pHT calculation to use 2D fits of gamma+occupancy
	* Update to have "space" for Krypton variables.
	* Tag as TRT_ElectronPidTools-01-00-22

2015-06-01 Leigh Schaefer
	* Fix silly bug I introduced in previous tag
	* Tag as TRT_ElectronPidTools-01-00-21

2015-06-01 Leigh Schaefer
	* Create new class for trigger local occupancy
	* Clean up other bits of local occupancy code
	* Tag as TRT_ElectronPidTools-01-00-20

2015-04-12 Leigh Schaefer
	* move array creation from initialize() to constructor
	* Fix localOccupancy(track) calculation:
	* change array structure of m_occ_local from [10][32] to [6][32]
	* correspondingly change calculation of elements within that array
        
        * Requires TRT_ConditionsServices-01-00-00-06
	* Tag as TRT_ElectronPidTools-01-00-19

2015-03-02 Alex Alonso 
	* Fix inic of TRT_Local Occupancy in case PID does not use it 
	* Tag as TRT_ElectronPidTools-01-00-18

2015-02-28 Alex Alonso 
	* Printout fixed 
	* Tag as TRT_ElectronPidTools-01-00-17

2015-02-28 Alex Alonso 
	* Clean a bit to prepare the DB read. 
	* Tag as TRT_ElectronPidTools-01-00-16


2015-02-17 Alex Alonso 
	* Compute the number of active straws in the
	TRT_StrawStatusSummaryService 
	* Tag as TRT_ElectronPidTools-01-00-15

2015-02-17 Alex Alonso 
	* Use TRT Detector Manager to speed up TRT Local Occ. calculation
	* Tag as TRT_ElectronPidTools-01-00-14


2015-02-14 Troels Petersen
	* Change level of pHT warnings to DEBUG for suspecious (but OK) values.
	* Tag as TRT_ElectronPidTools-01-00-13

2015-02-09 Troels Petersen
	* Change level of pHT warnings to DEBUG for suspecious (but OK) values.
	* New switch for including occupancy or not in PID calculation.
	* Tag as TRT_ElectronPidTools-01-00-11

2015-02-04 Alex Alonso 
	* FixCov: 28938, 29026 
	* Tag as TRT_ElectronPidTools-01-00-10

2015-01-22 Leigh.Schaefer@cern.ch & Fred.Luehring@cern.ch
	* Turn off check for hit in middle 25 ns BX-- TRT_LocalOccupancy::isMiddleBXOn(unsigned int word)
	* Change WARNING about occupancy == 0, now this is printed only if number hits/straws is more than 0.01
	* Tag as TRT_ElectronPidTools-01-00-10

2015-01-22 Leigh.Schaefer@cern.ch & Fred.Luehring@cern.ch
	* Fix compile warning in TRT_LocalOccupancy.cxx
	* Change WARNING about occupancy == 0, now this is printed only if number hits is more than 10
	* Tag as TRT_ElectronPidTools-01-00-09

2015-01-21 Leigh.Schaefer@cern.ch & Fred.Luehring@cern.ch
	* Add changes for finer granularity in local occupancy tool.
	* Tag as TRT_ElectronPidTools-01-00-08

2015-01-18 Alejandro.Alonso@cern.ch
        * Fix Coverity Reports 
	* Tag as TRT_ElectronPidTools-01-00-07

2014-12-02 Alejandro.Alonso@cern.ch
        * Fix Coverity Reports 
	* Tag as TRT_ElectronPidTools-01-00-06

2014-12-02 Alejandro.Alonso@cern.ch
        * Add function to get global event occupancy.  
	* Tag as TRT_ElectronPidTools-01-00-05


2014-12-02 Andrew.Bedddall@cern.ch
        * src/TRT_ElectronPidToolRun2.cxx
          Fixed bug in the below "Replaced PIDvalues[3]"
          Fixed (harmless) compile warning 

	* Tag as TRT_ElectronPidTools-01-00-04

2014-11-28 Troels Petersen
        * Onset curves tuned and calculated in three "TRT parts" (Barrel, EC-A, EC-B)
          instead of in five eta regions.
        * Replaced PIDvalues[3] = "prob_El_Brem" with "pHTel_prod"; requested by egamma.  
          src/TRT_ElectronPidToolRun2.cxx, src/TRT_ElectronPidToolRun2_HTcalculation.cxx
          TRT_ElectronPidTools/TRT_ElectronPidToolRun2_HTcalculation.h

	* Tag as TRT_ElectronPidTools-01-00-03

2014-09-18 Troels Petersen
        * Make a tag of latest tool - ready for Egamma production (V2) - fixed some wrong parameters for more optimal output!
	* Tag as TRT_ElectronPidTools-01-00-02

2014-09-17 Troels Petersen
        * Make a tag of latest tool - ready for Egamma production!
	* Tag as TRT_ElectronPidTools-01-00-00-03
	* Tag as TRT_ElectronPidTools-01-00-01 (Andrew: copied to the trunk)

2014-09-04 Anthony Morley
        * Make a tag of latest  Run2 tool
	*  Tag as TRT_ElectronPidTools-01-00-00-01

2013-08-27 Niels van Eldik
	* move to Eigen
	* Tag as TRT_ElectronPidTools-01-00-00

2012-08-28 Markus Juengst <ralph.markus.jungst@cern.ch>
	* clhep2 patch by Scott Snyder
	* Tag as TRT_ElectronPidTools-00-01-08

2012-04-13 Markus Juengst <ralph.markus.jungst@cern.ch>
	* commented line removed to prevent LXR to flag it
	* Tag as TRT_ElectronPidTools-00-01-07

2012-02-27 Markus Juengst <ralph.markus.jungst@cern.ch>
	* fix vector size for return values according to Savanna bug #91955
	* Tag as TRT_ElectronPidTools-00-01-06

2012-02-08 Markus Juengst <ralph.markus.jungst@cern.ch>
	* fix in requirements file (for checkreq warnings)
	* Tag as TRT_ElectronPidTools-00-01-05

2012-01-20 Markus Juengst <ralph.markus.jungst@cern.ch>
	* TOT probability based on TRT_ToT_Tool
	* corrections availlable now for data and MC
	* new flag to distinguish between data and MC running
	* old functionality stored in <name>_old for comparison plots
	* Tag as TRT_ElectronPidTools-00-01-04
	

2012-01-19 Jahred Adelman <jahreda@gmail.com>
	* Fix Coverity 20147 (Savannah 90570)
	* Tag as TRT_ElectronPidTools-00-01-03

2010-10-15 Simon Heisterkamp <simon.heisterkamp@cern.ch>
	* Changed a message in the TRT_ElectronPidTool.cxx from INFO to DEBUG level to avoid confusion. Also changed the wording of the message.
	* confirmed that Savannah bug #73874 is unrelated to the Pid Tool
	* TRT_ElectronPidTools-00-01-02
2010-08-27 Simon Heisterkamp <Simon.Heisterkamp@cern.ch>
	* changed the folder names to prevent clashes lith the old database entries
	* TRT_ElectronPidTools-00-01-01
2010-08-20 Simon Heisterkamp <Simon.Heisterkamp@cern.ch>
	* Almost complete rewrite since 00-00-22. 
	* Procedures were streamlined, 
	* Superfluous methods were removed. 
	* HT and ToT calculations were capsulated within dedicated daughter classes
	* ToT calculations completely changed and uptated
	* doc/Desctiption.tex was added with extensive documentation
	* DatabaseTools was added which contains tools needed for updating the Cool database
	* Cool database frormat changed, Tool now incompatible with old calibration tags
	* TRT_ElectronPidTools-00-01-00
	
2010-06-04 Simon Heisterkamp
        * readability cleanup. No change in functionality
	* TRT_ElectronPidTools-00-00-22

2010-06-04 Esben Klinkby
        * get rid of compile warning
	* TRT_ElectronPidTools-00-00-21

2010-01-19 Esben Klinkby
        * Urgent bug fix slc5 (bug #61512). Return vector of electronProbability() 
	* forced to have default size. 
	* TRT_ElectronPidTools-00-00-20
	
2009-04-01 Wolfgang Liebig
        * more adaptation: retrieve TRT ID helper directly from detStore
        * and clean up dependency of mgr
        * TRT_ElectronPidTools-00-00-18, -19

2009-03-30 Wolfgang Liebig
        * tag falls back on my feet: needs update to requirements
        * TRT_ElectronPidTools-00-00-17

2009-03-29 Wolfgang Liebig
        * adapt to the way how KalmanFitter et al set the new bremfit properties in TrackInfo
        * TRT_ElectronPidTools-00-00-16

2008-11-03 Kirill Prokofiev
        * Initial TrackInfo chages
        * TRT_ElectronPidTools-00-00-13

2008-07-07 Christian Schmitt <Christian.Schmitt@cern.ch>
        Fix uninitialized variable (bug #38580)
	Tag as TRT_ElectronPidTools-00-00-10
	
008-02-25 Andreas Wildauer
        * tagged Troels electron pid update as
	  TRT_ElectronPidTool-00-00-01

2007-11-20 Troels Petersen <Troels.Petersen@cern.ch>
	First import of package.
2007-10-17 Troels Petersen <Troels.Petersen@cern.ch>
	Setup of TRT_ElectronPidTool (empty) using SetupUtility
