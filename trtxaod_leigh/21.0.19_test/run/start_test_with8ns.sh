#

# Reco_tf.py \
# --inputRDOFile='/afs/cern.ch/user/l/lschaef/work/public/mc15_13TeV/RDO.08236526._000316.pool.root.1'   \
# --ignoreErrors True \
# --maxEvents '10' \
# --conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
# --geometryVersion ATLAS-R2-2015-03-01-00 \
# --ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
# --postInclude "RecJobTransforms/UseFrontier.py" \
# --autoConfiguration "everything" \
# --postExec \
# 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];from IOVDbSvc.CondDB import conddb; conddb.addMarkup("/TRT/Cond/StatusHT","<forceRunNumber>286990</forceRunNumber>");conddb.addOverride("/TRT/Cond/StatusHT","TrtStrawStatusHT-MC-run2-scenario5_00-00"); conddb.blockFolder("/TRT/Calib/RT"); conddb.blockFolder("/TRT/Calib/T0"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile="const0ns.txt" ' \
# --preExec \
# 'all:rec.Commissioning.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(False); rec.doEgamma.set_Value_and_Lock(False); rec.doTau.set_Value_and_Lock(False); from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
# 'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
#     --outputESDFile=ESD_0ns.pool.root \
#     --outputDAOD_IDTRKVALIDFile=InDetDxAOD_0ns.pool.root

# mv log.RAWtoESD log.RAWtoESD_0ns 
# mv log.ESDtoDPD log.ESDtoDPD_0ns 



# change TRT_DriftFunctionTool to not apply the 8ns shift: 
#

Reco_tf.py \
--inputRDOFile='/afs/cern.ch/user/l/lschaef/work/public/mc15_13TeV/RDO.08236526._000316.pool.root.1'   \
--ignoreErrors True \
--maxEvents '10' \
--conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
--geometryVersion ATLAS-R2-2015-03-01-00 \
--ignorePatterns 'Py:TrigConf2COOLLib.py.+ERROR.===================================+' \
--postInclude "RecJobTransforms/UseFrontier.py" \
--autoConfiguration "everything" \
--postExec \
'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];from IOVDbSvc.CondDB import conddb; conddb.addMarkup("/TRT/Cond/StatusHT","<forceRunNumber>286990</forceRunNumber>");conddb.addOverride("/TRT/Cond/StatusHT","TrtStrawStatusHT-MC-run2-scenario5_00-00"); conddb.blockFolder("/TRT/Calib/RT"); conddb.blockFolder("/TRT/Calib/T0"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile="const-8ns.txt" ' \
--preExec \
'all:rec.Commissioning.set_Value_and_Lock(True); rec.doTrigger.set_Value_and_Lock(False); rec.doEgamma.set_Value_and_Lock(False); rec.doTau.set_Value_and_Lock(False); from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' \
    --outputESDFile=ESD_-8ns.pool.root \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD_-8ns.pool.root

mv log.RAWtoESD log.RAWtoESD_-8ns 
mv log.ESDtoDPD log.ESDtoDPD_-8ns 