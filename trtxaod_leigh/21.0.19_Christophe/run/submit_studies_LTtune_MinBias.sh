cd $TestArea
emacs InnerDetector/InDetDigitization/TRT_Digitization/src/TRTDigSettings.cxx


cd ../build
cmake ../source ; make
source x86_64-slc6-gcc49-opt/setup.sh
cd ../run

# 70 eV
pathena --trf \
       "Reco_tf.py \
--inputHITSFile %IN \
--ignoreErrors True \
--maxEvents '500' \
--skipEvents='%SKIPEVENTS' \
--DataRunNumber 222525 \
--jobNumber 222525 \
--conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
--geometryVersion ATLAS-R2-2015-03-01-00 \
--ignorePatterns 'ByteStreamInputSvc.+ERROR.+Skipping.+bad.+event|EventSelector.+ERROR.+badFragment.+data.+encountered|ByteStreamInputSvcWARNING.+ERROR.+message.+limit|EventSelector.+WARNING.+ERROR.+message.+limit' \
--postInclude \"RecJobTransforms/UseFrontier.py\" \
--autoConfiguration \"everything\" \
--postExec \
'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\");conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-scenario5_00-00\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-scenario5_00-00\") ' \
'e2d:from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=False;ServiceMgr.MuonRPC_CablingSvc.CorrFileName=\"LVL1confAtlasRUN2_ver016.corr\";ServiceMgr.MuonRPC_CablingSvc.ConfFileName=\"LVL1confAtlasRUN2_ver016.data\" ' \
--preExec \
'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25) ; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(False);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); from GeoModelSvc.GeoModelSvcConf import GeoModelSvc; GeoModelSvc.TRT_VersionOverride=\"TRT-GEO-03\" ' \
'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
--outputRDOFile=%OUT.RDO.pool.root " \
    --official \
    --inDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2876 \
    --nFilesPerJob 1 \
    --nEventsPerJob 500 \
    --site=ANALY_MWT2_SL6,MWT2_UC_LOCALGROUPDISK \
    --allowTaskDuplication \
    --outDS group.det-indet.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.RDO.e3581_s2876_trt096_Ar70_RT



cd $TestArea
svn revert InnerDetector/InDetDigitization/TRT_Digitization/src/TRTDigSettings.cxx



# cd $TestArea
# emacs InnerDetector/InDetDigitization/TRT_Digitization/src/TRTDigSettings.cxx


# cd ../build
# cmake ../source ; make
# source x86_64-slc6-gcc49-opt/setup.sh
# cd ../run

# # 250 eV
# pathena --trf \
#        "Reco_tf.py \
# --inputHITSFile %IN \
# --ignoreErrors True \
# --maxEvents '500' \
# --skipEvents='%SKIPEVENTS' \
# --DataRunNumber 222525 \
# --jobNumber 222525 \
# --conditionsTag 'default:OFLCOND-MC15c-SDR-09' \
# --geometryVersion ATLAS-R2-2015-03-01-00 \
# --ignorePatterns 'ByteStreamInputSvc.+ERROR.+Skipping.+bad.+event|EventSelector.+ERROR.+badFragment.+data.+encountered|ByteStreamInputSvcWARNING.+ERROR.+message.+limit|EventSelector.+WARNING.+ERROR.+message.+limit' \
# --postInclude \"RecJobTransforms/UseFrontier.py\" \
# --autoConfiguration \"everything\" \
# --postExec \
# 'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\");conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-scenario5_00-00\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-scenario5_00-00\") ' \
# 'e2d:from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=False;ServiceMgr.MuonRPC_CablingSvc.CorrFileName=\"LVL1confAtlasRUN2_ver016.corr\";ServiceMgr.MuonRPC_CablingSvc.ConfFileName=\"LVL1confAtlasRUN2_ver016.data\" ' \
# --preExec \
# 'all:jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25) ; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(False);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); from GeoModelSvc.GeoModelSvcConf import GeoModelSvc; GeoModelSvc.TRT_VersionOverride=\"TRT-GEO-03\" ' \
# 'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
# --outputRDOFile=%OUT.RDO.pool.root " \
#     --official \
#     --inDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2876 \
#     --nFilesPerJob 1 \
#     --nEventsPerJob 500 \
#     --site=ANALY_MWT2_SL6,MWT2_UC_LOCALGROUPDISK \
#     --outDS group.det-indet.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.RDO.e3581_s2876_trt096_Ar250_RT

