cd $TestArea
cp ConfiguredInDetPreProcessingTRT.py   InnerDetector/InDetExample/InDetRecExample/share/ConfiguredInDetPreProcessingTRT.py

cd $TestArea/../build
cmake ../source ; make
source x86_64-slc6-gcc49-opt/setup.sh
cd ../run

pathena --trf \
"Reco_tf.py \
    --inputBSFile %IN \
--ignoreErrors True \
--maxEvents '500' \
--skipEvents '%SKIPEVENTS' \
--autoConfiguration 'everything' \
--AMI 'f698' \
--preExec \
'all:rec.doTrigger.set_Value_and_Lock(False); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
    --nFilesPerJob 1 \
    --nEventsPerJob 500 \
    --official \
    --inDS data16_13TeV.00298687.physics_Main.merge.DRAW_ZMUMU.f698_m1453 \
    --outDS group.det-indet.00298687.physics_Main.merge.TRTxAOD_ZMUMU.f698_m1453_trt096_noSWVG-03

svn revert ../source/InnerDetector/InDetExample/InDetRecExample/share/ConfiguredInDetPreProcessingTRT.py
