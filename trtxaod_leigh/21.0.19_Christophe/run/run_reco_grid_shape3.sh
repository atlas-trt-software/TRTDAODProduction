cd $TestArea 
cp ~croland/public/TRTElectronicsProcessing-shape_3.cxx InnerDetector/InDetDigitization/TRT_Digitization/src/TRTElectronicsProcessing.cxx
svn revert InnerDetector/InDetExample/InDetRecExample/share/ConfiguredInDetPreProcessingTRT.py
cd ../build
cmake ../source ; make ;  source x86_64-slc6-gcc49-opt/setup.sh
cd ../run

pathena --trf \
"Reco_tf.py \
--inputHITSFile %IN \
--ignoreErrors True \
--maxEvents '500' \
--skipEvents='%SKIPEVENTS' \
--DataRunNumber 222525 \
--jobNumber 222525 \
--conditionsTag 'OFLCOND-RUN12-SDR-31' \
--geometryVersion ATLAS-R2-2015-03-01-00 \
--ignorePatterns 'ByteStreamInputSvc.+ERROR.+Skipping.+bad.+event|EventSelector.+ERROR.+badFragment.+data.+encountered|ByteStreamInputSvcWARNING.+ERROR.+message.+limit|EventSelector.+WARNING.+ERROR.+message.+limit' \
--postInclude 'RecJobTransforms/UseFrontier.py' \
--autoConfiguration 'everything' \
--inputHighPtMinbiasHitsFile %HIMBIN \
--inputLowPtMinbiasHitsFile  %LOMBIN \
--numberOfCavernBkg 0 \
--numberOfHighPtMinBias 0.12268057 \
--numberOfLowPtMinBias 39.8773194 \
--pileupFinalBunch 6 \
--postExec \
'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\"); conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run2-scenario5_00-00\"); conddb.addOverride(\"/TRT/Calib/RT\",\"TrtCalibRt-13TeV_MC-scenario5_00-00\" ); conddb.addOverride(\"/TRT/Calib/T0\",\"TrtCalibT0-13TeV_MC-scenario5_00-00\");' \
'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools[\"MergeMcEventCollTool\"].DoSlimming=False; job.StandardPileUpToolsAlg.PileUpTools[\"MdtDigitizationTool\"].LastXing=150 ' \
'e2d:from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=False;ServiceMgr.MuonRPC_CablingSvc.CorrFileName=\"LVL1confAtlasRUN2_ver016.corr\";ServiceMgr.MuonRPC_CablingSvc.ConfFileName=\"LVL1confAtlasRUN2_ver016.data\" ' \
--preExec \
'all:rec.doTrigger.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); jobproperties.Beam.bunchSpacing.set_Value_and_Lock(25) ; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); from GeoModelSvc.GeoModelSvcConf import GeoModelSvc; GeoModelSvc.TRT_VersionOverride=\"TRT-GEO-03\" ' \
'HITtoRDO:ScaleTaskLength=0.1' \
'r2e:from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50) ' \
--preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_run222525_v1.py' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
    --highMinDS=mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS.e3581_s2876 \
    --lowMinDS=mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS.e3581_s2876 \
    --nHighMin=1 --nLowMin=1 \
    --nFilesPerJob 1 \
    --nEventsPerJob 500 \
    --inDS mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.HITS.e3601_s2876 \
    --outDS user.lschaef.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.TRTxAOD.e3601_s2876_shaping_width_3

