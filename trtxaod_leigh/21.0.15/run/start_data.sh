Reco_tf.py  \
    --inputBSFile='/afs/cern.ch/user/l/lschaef/work/public/data16_13TeV/data16_13TeV.00304337.physics_Main.merge.DRAW_ZMUMU.f716_m1616._0100.1'   \
    --ignoreErrors 'True'  \
    --maxEvents '10' \
    --AMI 'f716' \
    --postExec 'all:conddb.addOverride("/TRT/Calib/ToT/ToTValue","TRTCalibToTDataValue-000-03") ; conddb.addOverride("/TRT/Calib/ToT/ToTVectors","TRTCalibToTDataVectors-000-03"); conddb.blockFolder("/TRT/Calib/slopes"); conddb.blockFolder("/TRT/Calib/errors2d"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile="caliboutput_data16_newgeom_noRtOrT0.txt" ' \
    --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False)' \
'ESDtoDPD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doPrintConfigurables.set_Value_and_Lock(True); InDetFlags.doPRDFormation.set_Value_and_Lock(True)' \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD.pool.root 