2017-02-05 Leigh Schaefer <leigh.schaefer@cern.ch>
	* src/TrackStateOnSurfaceDecorator.cxx: Add some variables for TRTxAODs (dE/dx, track error, geometry of hits)
	* These variables will not be included if dumpTrtInfo is False.
	* Tagging as DerivationFrameworkInDet-00-00-68

2016-12-13 Susumu Oda <Susumu.Oda@cern.ch>
	* Add protection to src/TrackStateOnSurfaceDecorator.cxx
	  to avoid segfault. ATLASRECTS-3745
	* Tag DerivationFrameworkInDet-00-00-67

2016-09-26 Jake Searcy <jsearcy@umich.edu> 
	 * Add diTau trak Slimming 	
	 * tag DerivationFrameworkInDet-00-00-66

2016-09-26 Jake Searcy <jsearcy@umich.edu> 
	 * Fix some coverity errors 	
	 * tag DerivationFrameworkInDet-00-00-65
	
2016-09-06 Ben Nachman <bnachman@cern.ch> 
	 * removed large R jets from IDTIDE1 	

2016-08-28 Ben Nachman <bnachman@cern.ch> 
	 * added truth information to IDTIDE1 and removed R=0.3 track jets and the trigger navigation.
	 * tag DerivationFrameworkInDet-00-00-62

2016-08-19 Alex Alonso 
	* add protecting against cosmics to prevent use of PrimaryVertices 
	* tag DerivationFrameworkInDet-00-00-61

2016-08-10 Stefan Gadatsch <stefan.gadatsch@cern.ch>
	* Use RunSkimmingFirst option for IDTIDE kernel
	* tag DerivationFrameworkInDet-00-00-60

2016-06-24 Walter Lampl <walter.lampl@ cern.ch>
	* AuxDyn exclusions for RAWtoALL
	* tag DerivationFrameworkInDet-00-00-59

2016-06-17 Walter Lampl <walter.lampl@ cern.ch>
	* fix for RAWtoALL
	* tag DerivationFrameworkInDet-00-00-58

2016-06-15 Jake Searcy
  * Fix Coverity errors and compiler warnings
  * Tagging DerivationFrameworkInDet-00-00-57

2016-05-06 James Catmore 
  * Updates to TauTrackParticleThinning from Justin Griffiths
  * Tagging DerivationFrameworkInDet-00-00-55

2016-04-25 Goetz Gaycken (obo Leigh Catherine Schaefer)
	* Removing redundant TRT dEdx and used hits decorations
	  (partially addresses ATLASRECTS-2838).

2016-04-18 James Catmore
  * Adding EventInfoPixelDecorator
  * Tagging DerivationFrameworkInDet-00-00-54

2016-04-16 James Catmore
  * Track measurement thinning tool
  * Protection against no PVs (non-collision)
  * Tagging DerivationFrameworkInDet-00-00-53

2016-04-05 Jake Searcy
  * Adding more protection to MuonTrackParticleThinning
  * Prevents wrong index coming from  muons which point to track particles in InDetForwardTrackParticles instead of InDetTrackParticles in case where a slectoin string is used
  * Tagging DerivationFrameworkInDet-00-00-52

2015-10-09 Jake Searcy
  * Adding protection to MuonTrackParticleThinning
  * Prevents wrong index coming from  muons which point to track particles in InDetForwardTrackParticles instead of InDetTrackParticles
  * Tagging DerivationFrameworkInDet-00-00-51

2015-10-09 Roland Jansky
  * Adding EventShape to IDTIDE
  * Tagging DerivationFrameworkInDet-00-00-50

2015-10-09 Anthony Morley
  * Change TRT dedx tool interface to be compatible with 20.7 onwards 
  * Tagging DerivationFrameworkInDet-00-00-48

2015-10-09 Anthony Morley
  * Correct unbiased redsidual calculation
  * Tagging DerivationFrameworkInDet-00-00-47

2015-09-22 Paul Laycock <paul.james.laycock@cern.ch>

        * Updated EGammaPrepRawData, downgrade even more INFO messages to DEBUG
        * Tagging DerivationFrameworkInDet-00-00-46

2015-09-21 Paul Laycock <paul.james.laycock@cern.ch>

        * Updated EGammaTracksThinning and EGammaPrepRawData, downgrade more INFO messages to DEBUG
        * Tagging DerivationFrameworkInDet-00-00-45

2015-09-21 Goetz Gaycken <goetz@cern.ch>

        * Updated TrackStateOnSurfaceDecorator to fix 3MB/event memory leak
        * Tagging DerivationFrameworkInDet-00-00-44

2015-09-21 Paul Laycock <paul.james.laycock@cern.ch>

        * Updated EGammaTracksThinning and EGammaPrepRawData, downgrade INFO messages to DEBUG
        * Tagging DerivationFrameworkInDet-00-00-43

2015-08-28 Paul Laycock <paul.james.laycock@cern.ch>

        * Adding EGammaTracksThinning and EGammaPrepRawData class
        * Tagging DerivationFrameworkInDet-00-00-42

2015-07-18 James Catmore <james.catmore@cern.ch>

	* Fixing copy/paste errors from COVERITY
	* Tagging DerivationFrameworkInDet-00-00-41

2015-07-26 Roland Jansky
	* Fixed missing collections (Aux items)
	* Tagging DerivationFrameworkInDet-00-00-40

2015-07-20 Simone Pagan Griso
	* Fixed TRT hits collection for thinning
	* Tagging DerivationFrameworkInDet-00-00-38

2015-07-19 Simone Pagan Griso (obo Soshi Tsuno)
	* Updated IDTIDE1.py jobOptions for DAOD_IDTIDE production
	* Tagging DerivationFrameworkInDet-00-00-37
	* Note: DerivationFrameworkInDet-00-00-36 is the same but did not update ChangeLog

2015-07-16 Simone Pagan Griso <simone.pagan.griso@cern.ch>
	* Added cluster thinning to the track particle.
	* Updated TIDE performance DPD jobOptions
	* Tagging DerivationFrameworkInDet-00-00-35

2015-06-10 James Catmore <james.catmore@cern.ch>

	* Adding slimming helper to IDTR1
	* Tagging as DerivationFrameworkInDet-00-00-34

2015-07-08 Simone Pagan Griso <simone.pagan.griso@cern.ch>
	* Improved IDTR1 for studies needed on first data.
	* Tagging as DerivationFrameworkInDet-00-00-33

2015-06-03 Olivier Arnaez <olivier.arnaez@cern.ch>
	* UnassociatedHitsDecorator.cxx: adding protection against failing getter
	* LArCollisionTimeDecorator.cxx: adding protection against missing LArCollisionTime
	* Tagging as DerivationFrameworkInDet-00-00-32

2015-06-03 James Catmore <james.catmore@cern.ch>

	* Removing wildcards
	* Tagging as DerivationFrameworkInDet-00-00-31

2015-05-22 Olivier Arnaez <olivier.arnaez@cern.ch>
	* Adding LArCollisionTime decoration class: LArCollisionTimeDecorator
	* Tagging as DerivationFrameworkInDet-00-00-30

2015-05-22 Olivier Arnaez <olivier.arnaez@cern.ch>
	* Adding unassociated hits decoration classes: IUnassociatedHitsGetterTool, UnassociatedHitsGetterTool, UnassociatedHitsDecorator and MinBiasPRDAssociation
	* Tagging as DerivationFrameworkInDet-00-00-29

2015-05-08 Roland Jansky <roland.jansky@cern.ch>
  * Changed behaviour of TrackStateOnSurfaceDecorator when no TrackParameters are available (because of slimming) - now still sets TrackMeasurementValidationLink etc. but doesn't fill any derived quantities
  * Tagged as DerivationFrameworkInDet-00-00-28

2015-04-30 Simone Pagan Griso <simone.pagan.griso@cern.ch>
  * Restructure code to be more robust against missing information
  * Fixed few minor bugs
  * Better code readibility
  * Tagged as DerivationFrameworkInDet-00-00-27

2015-03-23 James Catmore
  * Fixing coverity bug 28989
  * Tagged as DerivationFrameworkInDet-00-00-26

2015-03-12 Aleksei Antonov
  * Added ToT_dEdx and ToT_usedHits to the TrackStateOnSurfaceDectorator for TRT
  * Tagged as DerivationFrameworkInDet-00-00-25

2015-03-09 Daiki Hayakawa
  * Fixed bugs in EventInfoBSDecorator
  * Tagged as DerivationFrameworkInDet-00-00-24

2015-02-21 Daiki Hayakawa <daiki.hayakawa@cern.ch>
  * Add EventInfoBSDecorator for SCT ByteStreame error information
	* Tagged as DerivationFrameworkInDet-00-00-23

2015-02-04 Anthony Morley
  * Add holes to the TrackStateOnSurfaceDectorator
  * Tagged as DerivationFrameworkInDet-00-00-22

2014-12-05 James Catmore
  * Changed names in job options
  * Tagged as DerivationFrameworkInDet-00-00-21

2014-12-04 Anthony Morley
  * Change some xAOD Tracking class names
  * Tagged as DerivationFrameworkInDet-00-00-20

2014-11-05 Anthony Morley
  * Update TrackParameterforTruthParticles
  * Tagged as DerivationFrameworkInDet-00-00-19

2014-11-05 Anthony Morley
  * Add TrackParameterforTruthParticles
  * Tagged as DerivationFrameworkInDet-00-00-18

2014-10-22 Simone Pagan Griso
	* Added TRT phase decoration for Tracking DxAOD
	Tagged as DerivationFrameworkInDet-00-00-17

2014-10-02 James Catmore <james.catmore@cern.ch>

	* Adding protection against invalid element links
	* Tagged as DerivationFrameworkInDet-00-00-16

2014-09-03 Anthony Morley
  * Correct a feature of TrackStateOnSurfaceDectorator
  * Tagged as DerivationFrameworkInDet-00-00-15

2014-09-25 James Catmore <james.catmore@cern.ch>

	* Fixing bug in photon conversion thinning
	* Switching to isValid for muon links
	* Tagged as DerivationFrameworkInDet-00-00-14

2014-09-03 Anthony Morley
  * Updates to TrackStateOnSurfaceDectorator
  * Tagged as DerivationFrameworkInDet-00-00-13

2014-09-06 James Catmore <james.catmore@cern.ch>

	* Fixing protection against undefined  muon track links (can be -1)
	* No tag yet

2014-09-04 James Catmore <james.catmore@cern.ch>

	* Fixing error in tracks-in-cone calculation (thinning)
	* No tag yet

2014-09-03 Anthony Morley
  * Update TrackStateOnSurfaceDectorator to use new xAOD objects

2014-08-15 Anthony Morley 
  * Add TrackStateOnSurfaceDectorator


2014-08-15 James Catmore <james.catmore@cern.ch>

	* Fixing wrong accessor to ID tracks from jets
	* Tagged as DerivationFrameworkInDet-00-00-12

2014-08-14 James Catmore <james.catmore@cern.ch>

	* Name change
	* Tagged as DerivationFrameworkInDet-00-00-11

2014-08-13 James Catmore <james.catmore@cern.ch>

	* Re-organising e-gamma TP thinning, adding AND option
	* Tagged as DerivationFrameworkInDet-00-00-10

2014-08-02 James Catmore <james.catmore@cern.ch>

	* Adding TP thinning-within-cone for muons, electrons, taus
	* Tagged as DerivationFrameworkInDet-00-00-09

2014-07-29 James Catmore <james.catmore@cern.ch>

	* Adding TrackParametersAtPV
	* Tagged as DerivationFrameworkInDet-00-00-08

2014-07-28 James Catmore <james.catmore@cern.ch>

	* Fixing initialisation ordering warnings
	* Tagged as DerivationFrameworkInDet-00-00-07

2014-07-25 James Catmore <james.catmore@cern.ch>

	* Splitting up thinning tools
	* Tagged as DerivationFrameworkInDet-00-00-06

2014-07-22 James Catmore <james.catmore@cern.ch>
	
	* Adding jets to thinning
	* Tagged as DerivationFrameworkInDet-00-00-05

2014-07-20 James Catmore <james.catmore@cern.ch>

	* Tagging recent changes
	* Tagged as DerivationFrameworkInDet-00-00-04

2014-07-16 James Catmore <james.catmore@cern.ch>

	* First attempt at adding thinning service to thinning tool
	* No tag

2014-07-15 James Catmore <james.catmore@cern.ch>

	* Thinning now run (doesn't yet thin but sets up thinning mask)
	* No tag

2014-07-15 James Catmore <james.catmore@cern.ch>

	* Thinning now compiles but doesn't run
	* No tag

2014-07-15 James Catmore <james.catmore@cern.ch>

	* Adding first attempt at Thinning tool - does not compile
	* No tag

2014-07-08 James Catmore <james.catmore@cern.ch>

	* Fixing incorrect instantiation of result object
	* Tagged as DerivationFrameworkInDet-00-00-03

2014-06-23 James Catmore <james.catmore@cern.ch>

	* Adding decoration
	* Tagged as DerivationFrameworkInDet-00-00-01

2014-06-23 James Catmore <james.catmore@cern.ch>

	* Initial upload
	* Tagged as DerivationFrameworkInDet-00-00-00

