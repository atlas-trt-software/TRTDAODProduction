
Reco_tf.py  \
     --inputBSFile ../../21.0.19/run/data17_13TeV.00331951.physics_Main.daq.RAW/data17_13TeV.00331951.physics_Main.daq.RAW._lb0136._SFO-2._0001.data \
     --maxEvents '5' \
     --autoConfiguration 'everything' \
     --AMI f848 \
     --preExec 'from PrimaryDPDMaker.PrimaryDPDFlags import primDPD; primDPD.WriteEventlessFiles.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.useDynamicAlignFolders.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50); TriggerFlags.AODEDMSet="AODFULL";from TrigHLTMonitoring.HLTMonFlags import HLTMonFlags;HLTMonFlags.doGeneral=False;HLTMonFlags.doJet=False' \
     --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD.pool.root
