cd ../athena
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_Z.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
cd ../build
cmake ../athena/Projects/WorkDir/ ; make ; source x86_64-slc6-gcc62-opt/setup.sh 
cd ../run


pathena --trf \
"Reco_tf.py  \
     --inputBSFile %IN \
    --autoConfiguration 'everything' \
     --skipEvents='%SKIPEVENTS' \
     --AMI f848 \
     --preExec 'from PrimaryDPDMaker.PrimaryDPDFlags import primDPD; primDPD.WriteEventlessFiles.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.useDynamicAlignFolders.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50); TriggerFlags.AODEDMSet=\"AODFULL\";from TrigHLTMonitoring.HLTMonFlags import HLTMonFlags;HLTMonFlags.doGeneral=False;HLTMonFlags.doJet=False; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
     --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
--inDS data17_13TeV.00331951.physics_Main.daq.RAW \
--outDS group.det-indet.00331951.physics_Main.daq.TRTxAOD_Z.f848_trt104-05 \
--nGBPerJob 5 \
--official

cd ../athena
cp InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD_JPSI.py InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/share/InDetDxAOD.py 
cd ../build
cmake ../athena/Projects/WorkDir/ ; make ; source x86_64-slc6-gcc62-opt/setup.sh 
cd ../run


pathena --trf \
"Reco_tf.py  \
     --inputBSFile %IN \
     --autoConfiguration 'everything' \
     --skipEvents='%SKIPEVENTS' \
     --AMI f848 \
     --preExec 'from PrimaryDPDMaker.PrimaryDPDFlags import primDPD; primDPD.WriteEventlessFiles.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.useDynamicAlignFolders.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.SCTxAODJobProperties import SCTxAODFlags;SCTxAODFlags.Prescale.set_Value_and_Lock(50); TriggerFlags.AODEDMSet=\"AODFULL\";from TrigHLTMonitoring.HLTMonFlags import HLTMonFlags;HLTMonFlags.doGeneral=False;HLTMonFlags.doJet=False; from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
     --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
--inDS data17_13TeV.00331951.physics_Main.daq.RAW \
--outDS group.det-indet.00331951.physics_Main.daq.TRTxAOD_JPSI.f848_trt104-05 \
--nGBPerJob 5 \
--official


