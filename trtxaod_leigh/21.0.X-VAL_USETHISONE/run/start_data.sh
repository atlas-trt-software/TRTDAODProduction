#

Reco_tf.py  \
    --inputBSFile='/afs/cern.ch/user/l/lschaef/work/public/data16_13TeV/data16_13TeV.00304337.physics_Main.merge.DRAW_ZMUMU.f716_m1616._0100.1'   \
    --ignoreErrors 'True'  \
    --maxEvents '10' \
    --AMI 'f716' \
    --postExec 'all:conddb.addOverride("/TRT/Calib/ToT/ToTValue","TRTCalibToTDataValue-000-03") ; conddb.addOverride("/TRT/Calib/ToT/ToTVectors","TRTCalibToTDataVectors-000-03") ' \
    --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False); InDetDxAODFlags.ThinTrackSelection.set_Value_and_Lock("InDetTrackParticles.pt > 0.1*GeV && abs(InDetTrackParticles.eta) < 2.0")  ' \
     --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD_newdata.pool.root 


#

# Reco_tf.py  \
#     --inputBSFile='/afs/cern.ch/user/l/lschaef/work/public/data16_13TeV/data16_13TeV.00304337.physics_Main.merge.DRAW_ZMUMU.f716_m1616._0100.1'   \
#     --ignoreErrors 'True'  \
#     --maxEvents '10' \
#     --AMI 'f716' \
#     --postExec 'all:conddb.addOverride("/TRT/Calib/ToT/ToTValue","TRTCalibToTDataValue-000-03") ; conddb.addOverride("/TRT/Calib/ToT/ToTVectors","TRTCalibToTDataVectors-000-03") ' \
#     --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False); InDetDxAODFlags.ThinTrackSelection.set_Value_and_Lock("InDetTrackParticles.pt > 0.1*GeV && abs(InDetTrackParticles.eta) < 2.0"); InDetDxAODFlags.TrtJpsiSelection.set_Value_and_Lock(True) ' \
#      --steering 'RAWtoESD:in+ESD' \
#     --outputDAOD_IDTRKVALIDFile=InDetDxAOD.pool.root 
