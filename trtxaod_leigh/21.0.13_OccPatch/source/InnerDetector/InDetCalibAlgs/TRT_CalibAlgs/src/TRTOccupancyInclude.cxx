/* *******************************************************************

  TRTOccupancyInclude.cxx : Simple code to include Occupancy inside event info xAOD 

* ***************************************************************** */

#include "TRT_CalibAlgs/TRTOccupancyInclude.h"
#include "xAODEventInfo/EventInfo.h"

TRTOccupancyInclude::TRTOccupancyInclude(const std::string& name, ISvcLocator* pSvcLocator) :
  AthAlgorithm   (name, pSvcLocator),
  m_LocalOccTool()
{
  declareProperty("TRT_LocalOccupancyTool", m_LocalOccTool);
}

//---------------------------------------------------------------------

TRTOccupancyInclude::~TRTOccupancyInclude(void)
{}

//--------------------------------------------------------------------------

StatusCode TRTOccupancyInclude::initialize()
{
  if ( m_LocalOccTool.retrieve().isFailure() ){
    ATH_MSG_ERROR(" Failed to retrieve TRT Local Occupancy tool: " << m_LocalOccTool );
    return StatusCode::FAILURE;
  }
  else {
    ATH_MSG_INFO("Retrieved tool " << m_LocalOccTool);
  }

  return StatusCode::SUCCESS;
}

//---------------------------------------------------------------------

StatusCode TRTOccupancyInclude::execute()
{
  msg(MSG::DEBUG) << "execute()" << endreq;

  const xAOD::EventInfo* eventInfo = 0;
  if (evtStore()->retrieve(eventInfo).isFailure()) {
    ATH_MSG_WARNING(" Cannot access to event info.");
    return StatusCode::SUCCESS;
  } 

  std::vector<float> TRTOccu = m_LocalOccTool->GlobalOccupancy( );
  if (TRTOccu.size() > 6) {

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy0("TRTOccGlobal"); 
  decEventInfo_occupancy0( *eventInfo ) = TRTOccu.at(0); 

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy1("TRTOccBarrelC"); 
  decEventInfo_occupancy1( *eventInfo ) = TRTOccu.at(1); 

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy2("TRTOccEndcapAC"); 
  decEventInfo_occupancy2( *eventInfo ) = TRTOccu.at(2); 

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy3("TRTOccEndcapBC"); 
  decEventInfo_occupancy3( *eventInfo ) = TRTOccu.at(3); 

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy4("TRTOccBarrelA"); 
  decEventInfo_occupancy4( *eventInfo ) = TRTOccu.at(4); 

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy5("TRTOccEndcapAA"); 
  decEventInfo_occupancy5( *eventInfo ) = TRTOccu.at(5); 

  static SG::AuxElement::Decorator< float >  decEventInfo_occupancy6("TRTOccEndcapBA"); 
  decEventInfo_occupancy6( *eventInfo ) = TRTOccu.at(6); 
  }

  // ========================================== //

  std::vector<float>  TRTOccWheel = m_LocalOccTool->getOccWheelTotal();
  if (TRTOccWheel.size() > 33) {
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel0("TRTOccWheelBC_0"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel1("TRTOccWheelBC_1"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel2("TRTOccWheelBC_2"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel3("TRTOccWheelEC_0"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel4("TRTOccWheelEC_1"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel5("TRTOccWheelEC_2"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel6("TRTOccWheelEC_3"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel7("TRTOccWheelEC_4"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel8("TRTOccWheelEC_5"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel9("TRTOccWheelEC_6"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel10("TRTOccWheelEC_7"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel11("TRTOccWheelEC_8"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel12("TRTOccWheelEC_9"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel13("TRTOccWheelEC_10"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel14("TRTOccWheelEC_11"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel15("TRTOccWheelEC_12"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel16("TRTOccWheelEC_13"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel17("TRTOccWheelBA_0"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel18("TRTOccWheelBA_1"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel19("TRTOccWheelBA_2"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel20("TRTOccWheelEA_0"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel21("TRTOccWheelEA_1"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel22("TRTOccWheelEA_2"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel23("TRTOccWheelEA_3"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel24("TRTOccWheelEA_4"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel25("TRTOccWheelEA_5"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel26("TRTOccWheelEA_6"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel27("TRTOccWheelEA_7"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel28("TRTOccWheelEA_8"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel29("TRTOccWheelEA_9"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel30("TRTOccWheelEA_10"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel31("TRTOccWheelEA_11"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel32("TRTOccWheelEA_12"); 
  static SG::AuxElement::Decorator< float >  decEventInfo_occwheel33("TRTOccWheelEA_13"); 

  decEventInfo_occwheel0( *eventInfo ) = TRTOccWheel.at(0); 
  decEventInfo_occwheel1( *eventInfo ) = TRTOccWheel.at(1); 
  decEventInfo_occwheel2( *eventInfo ) = TRTOccWheel.at(2); 
  decEventInfo_occwheel3( *eventInfo ) = TRTOccWheel.at(3); 
  decEventInfo_occwheel4( *eventInfo ) = TRTOccWheel.at(4); 
  decEventInfo_occwheel5( *eventInfo ) = TRTOccWheel.at(5); 
  decEventInfo_occwheel6( *eventInfo ) = TRTOccWheel.at(6); 
  decEventInfo_occwheel7( *eventInfo ) = TRTOccWheel.at(7); 
  decEventInfo_occwheel8( *eventInfo ) = TRTOccWheel.at(8); 
  decEventInfo_occwheel9( *eventInfo ) = TRTOccWheel.at(9); 
  decEventInfo_occwheel10( *eventInfo ) = TRTOccWheel.at(10); 
  decEventInfo_occwheel11( *eventInfo ) = TRTOccWheel.at(11); 
  decEventInfo_occwheel12( *eventInfo ) = TRTOccWheel.at(12); 
  decEventInfo_occwheel13( *eventInfo ) = TRTOccWheel.at(13); 
  decEventInfo_occwheel14( *eventInfo ) = TRTOccWheel.at(14); 
  decEventInfo_occwheel15( *eventInfo ) = TRTOccWheel.at(15); 
  decEventInfo_occwheel16( *eventInfo ) = TRTOccWheel.at(16); 
  decEventInfo_occwheel17( *eventInfo ) = TRTOccWheel.at(17); 
  decEventInfo_occwheel18( *eventInfo ) = TRTOccWheel.at(18); 
  decEventInfo_occwheel19( *eventInfo ) = TRTOccWheel.at(19); 
  decEventInfo_occwheel20( *eventInfo ) = TRTOccWheel.at(20); 
  decEventInfo_occwheel21( *eventInfo ) = TRTOccWheel.at(21); 
  decEventInfo_occwheel22( *eventInfo ) = TRTOccWheel.at(22); 
  decEventInfo_occwheel23( *eventInfo ) = TRTOccWheel.at(23); 
  decEventInfo_occwheel24( *eventInfo ) = TRTOccWheel.at(24); 
  decEventInfo_occwheel25( *eventInfo ) = TRTOccWheel.at(25); 
  decEventInfo_occwheel26( *eventInfo ) = TRTOccWheel.at(26); 
  decEventInfo_occwheel27( *eventInfo ) = TRTOccWheel.at(27); 
  decEventInfo_occwheel28( *eventInfo ) = TRTOccWheel.at(28); 
  decEventInfo_occwheel29( *eventInfo ) = TRTOccWheel.at(29); 
  decEventInfo_occwheel30( *eventInfo ) = TRTOccWheel.at(30); 
  decEventInfo_occwheel31( *eventInfo ) = TRTOccWheel.at(31); 
  decEventInfo_occwheel32( *eventInfo ) = TRTOccWheel.at(32); 
  decEventInfo_occwheel33( *eventInfo ) = TRTOccWheel.at(33); 

  }

  else {
    std::cout<<"LEIGH THE VECTOR SIZE WASN'T BIG ENOUGH"<<std::endl;
  }

  return StatusCode::SUCCESS;
}

//---------------------------------------------------------------------

StatusCode TRTOccupancyInclude::finalize()
{
  msg(MSG::INFO) << "finalise()" << endreq;
  return StatusCode::SUCCESS;
}
