
/*
 * Change "undef" to "define" to select the straw hit transform scheme
 */

#undef SCHEME1
#define SCHEME2
#undef SCHEME3





///////////////////////////////////////////////////////////////////
// TRT_LoLumRawData.cxx
//   Implementation file for class TRT_LoLumRawData
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////
// Version 1.0 13/08/2002 Veronique Boisvert
// Implementation provided by A. Zalite, February 2003
///////////////////////////////////////////////////////////////////

#include <new>
#include "InDetRawData/TRT_LoLumRawData.h"
#include "InDetRawData/TRT_RDORawData.h"

unsigned int f( const unsigned int word );

// default constructor
TRT_LoLumRawData::TRT_LoLumRawData() :
  TRT_RDORawData(Identifier(), 0) //call base-class constructor
{}

// Constructor with parameters:
TRT_LoLumRawData::TRT_LoLumRawData(const Identifier rdoId,
                           const unsigned int word) :
  TRT_RDORawData( rdoId, f(word)) //call base-class constructor
{}

// Destructor:
//should I be destructing something here?
TRT_LoLumRawData::~TRT_LoLumRawData()
{}


  // Time over threshold in ns:
double TRT_LoLumRawData::timeOverThreshold() const
{
  double binWidth = 3.125;

  int LE = driftTimeBin( );
  int TE = trailingEdge( );

  if ( (0 == LE) || (24 == LE) || (24 == TE) || (0 == TE) || (23 == LE) )
    {
      return 0;
    }

  double time = (double) (TE - LE + 1) * binWidth;
  
  return time;

}

// Position of first low-to-high bit transition
int TRT_LoLumRawData::driftTimeBin() const
{
  unsigned  mask = 0x02000000;
  unsigned  m_word_LE = m_word>>6;
  m_word_LE = m_word_LE<<6;
 
  mask >>=1;
  bool SawZero = false;
  int i;
  for(i=1;i<18;++i)
  { 
    if      (  (m_word_LE & mask) && SawZero) break;
    else if ( !(m_word_LE & mask) ) SawZero = true; 
    mask>>=1;
    if(i==7 || i==15) mask>>=1;
  }
  if(i==18) i=0;
  return i;
}

// Position of first low-to-high bit transition moving from the right
// or 24 if no transition is found
int TRT_LoLumRawData::trailingEdge() const
{
  unsigned mask = 0x00000001;
  unsigned mask_word = 0x0001fff0; // 11111111 1 11110000   
  unsigned mask_last_bit =0x10; //10000
  
  unsigned m_word_TE = m_word & mask_word;
  
  bool SawZero=true;
  bool SawZero1=false;
  bool SawZero2=false;
  bool SawUnit1=false;
  int i=0;
  int j=0;
  int k=0;
  
  if(m_word_TE & mask_last_bit) 
  {
  
	for (j = 0; j < 11; ++j)
	{
		mask_last_bit=mask_last_bit<<1;
		
		if(j==3) mask_last_bit=mask_last_bit<<1;
		
		if ( !(m_word_TE & mask_last_bit) )
		{
			SawZero2 = true;
			break;			
		}
	}
	
	if(SawZero2 == false) return 19;

	if(SawZero2 == true){
		for (k = j+1; k < 11; ++k)
		{
			mask_last_bit=mask_last_bit<<1;

			if(k==3) mask_last_bit=mask_last_bit<<1;

			if ( m_word_TE & mask_last_bit )
			{
				SawUnit1 = true;
				break;					
			}
		} 
	}
	
	if(SawUnit1 == false && SawZero2 == true) return 19;
	
  }
  
  //+++++++++++++++++++++++++++++++++++++
  
  for (i = 0; i < 24; ++i)
  {
  
	if(!(m_word_TE & mask) && i>3)
	{
	  SawZero1 = true;
	}
    if(SawZero1){  
		if ( (m_word_TE & mask) && SawZero )
			break;
		else if ( !(m_word_TE & mask) )
			SawZero = true;
    }
    mask <<= 1;
    if (i == 7 || i == 15)
      mask <<= 1;
  }
 
  if ( 24 == i )
    return i;

  return (23 - i);

}

unsigned int Scheme1( unsigned int pattern );
unsigned int Scheme2( unsigned int pattern );
unsigned int Scheme3( unsigned int pattern );


unsigned int f( const unsigned int word )
{

#ifdef SCHEME1
  if ( word & 0x000DFEE0 )
    return Scheme1( word );
  else
    return 0;
#endif

#ifdef SCHEME2
  if ( word & 0x000DFEE0 )
    return Scheme2( word );
  else
    return 0;
#endif

#ifdef SCHEME3
  if ( word & 0x000DFEE0 )
    return Scheme3( word );
  else
    return 0;
#endif

  if ( word & 0x000DFEE0 )
    return word;
  else
    return 0;

}


int
fLE( unsigned int word )
{
  unsigned int mask = 0x400000;   // Start with 2nd bit
  unsigned int m_word_LE = word;
 
  bool SawZero = false;
  int i;
  for(i=2;i<21;++i)
  {
    if      (  (m_word_LE & mask) && SawZero) break;
    else if ( !(m_word_LE & mask) ) SawZero = true;
    mask>>=1;
  }
  if(i==21) i=0;
  return i;
}

int
lTE( unsigned int word )
{
  unsigned int mask = 0x10;   // Start with 1st bit
  unsigned int m_word_TE = word;
 
  bool SawZero = false;
  int i;
  for(i=20;i>0;--i)
  {
    if      (  (m_word_TE & mask) && SawZero) break;
    else if ( !(m_word_TE & mask) ) SawZero = true;
    mask<<=1;
  }

  return i;
}

int
fTE( unsigned int word )
{
  unsigned int m_word_TE = word;
  int LE = fLE( word );

  if ( LE == 0 )
    return 0;

  unsigned int mask = (1 << (20+4-LE));   // Start with 2nd bit

  while ( mask && (m_word_TE & mask) )
  {
    LE++;
    mask>>=1;
  }

  if ( mask )
    return (LE-1);
  else
    return 0;
}


/******************************************
 *
 * HW Validity Gate: 000D.FEE0
 *
 *    0+0000.0011_0+1111.1111_0+1110.0000
 *
 ******************************************/

/*
 * AR+PTK with no special TE treatment
 */
unsigned int
Scheme1( unsigned int word )
{
  unsigned int data;

  data = (word & 0xff) | ((word >> 1) & 0xff00) | ((word >>2) & 0xff0000);

  //  printf( " -> " );
  //  write_pattern20( data );

  if ( data )
  {
    unsigned int out_data=0;
    bool seen_zero;

    int LE = fLE( data );
    int TE = lTE( data );
    //	printf( "  fLE = %d\t lTE = %d\n", LE, TE );

    if ( TE < LE )
      out_data = data;
    else if ( LE == 0 )
      out_data = data; // | 0x00800000;
    else
    {
      //      printf( "%x %x\n", data, data &  0x00C00000  );
      if ( data &  0x00C00000 )        // either of first two bits set
	out_data = 0x00800000;        // Set first bit

      //  printf( "  -> " );
      //  write_pattern( out_data, true );

      if ( !(data & 0x00400000) )
	seen_zero = true;

      unsigned int Lmask = 0x00200000;

      /*
       * Find LE
       */

      // First zero
      while ( (Lmask & data) && !seen_zero && Lmask )
	Lmask = Lmask >> 1;
      if ( Lmask )
      {
	while ( !(Lmask & data) && Lmask )
	  Lmask = Lmask >> 1;
	  

	//	  printf( " Lmask = %x\n", Lmask );

	if ( Lmask != 0x0 )
	{
	  //	  unsigned int Lmask_save = Lmask;
	  
	  unsigned int Tmask = 0x1;

	  while ( (! (Tmask & data)) && (Tmask != 0x0) )
	    Tmask = Tmask << 1;


	  //	  if ( Tmask == 0x0 )
	  //	    std::cout << std::hex << data << std::dec << std::endl;

	  //	  unsigned int Tmask_save = Tmask;

	  out_data = out_data | Lmask | Tmask;

	  while ( (Tmask != Lmask) && (Lmask != 0x0) )
	  {
	    out_data = out_data | Lmask;
	    Lmask = Lmask >> 1;

	  }

	  //	  if ( Lmask == 0x0 )
	  //	    std::cout << std::hex << data << " ( " << Lmask_save << " , " 
	  //		      << Tmask_save << " ) "
	  //		      << " -> " << out_data << std::dec 
	  //		      << std::endl;
	}
      }
    }

    data = ((out_data & 0xff0000) << 2) | 
      ((out_data & 0xff00) << 1) | (out_data & 0xff);

    //    printf( " -> " );
    //    write_pattern20( data );
  }

  /*
   * Reset middle HT bit
   */
  if ( word & 0x0020000 )   // Middle HT bit
    data = data | 0x0020000;

  /*
    cout << std::hex << data << " ( " << Lmask_save << " , " 
    << Tmask_save << " ) "
    << " -> " << out_data << std::dec 
    << std::endl;
  */

  //      printf( "xxx> " );
  //      write_pattern( data, true );

  return data;
}


/*
 * AR+PTK with similar TE treatment
 */
unsigned int
Scheme2( unsigned int word )
{
  unsigned int data;

  data = (word & 0xff) | ((word >> 1) & 0xff00) | ((word >>2) & 0xff0000);

  //  printf( " -> " );
  //  write_pattern20( data );

  if ( data )
  {
    unsigned int out_data=0;
    //    bool seen_zero;

    //	printf( "  fLE = %d\n", fLE( data ) );
    //	printf( "  lTE = %d\n", lTE( data ) );
    int LE = fLE( data );
    int TE = lTE( data );
    if ( TE < LE )
      out_data = data;
    else if ( (LE == 0) || (TE == 0) )
      out_data = data;
    else
    {
      if ( data & 0x00C00000 )        // either of first two bits set
	out_data = 0x00800000;        // Set first bit

      if ( data & 0x10 )              // either of last two bits set
	out_data = out_data | 0x10;   // Set last bit

      //  printf( "  -> " );
      //  write_pattern( out_data, true );

      for ( int i = LE; i <= TE; i++ )
	out_data = out_data | (1 << (20+4-i));

    }

    data = ((out_data & 0xff0000) << 2) | 
      ((out_data & 0xff00) << 1) | (out_data & 0xff);

    //    printf( " -> " );
    //    write_pattern20( data );
  }

  /*
   * Reset middle HT bit
   */
  if ( word & 0x0020000 )   // Middle HT bit
    data = data | 0x0020000;

  /*
    cout << std::hex << data << " ( " << Lmask_save << " , " 
    << Tmask_save << " ) "
    << " -> " << out_data << std::dec 
    << std::endl;
  */

  //      printf( "xxx> " );
  //      write_pattern( data, true );

  return data;
}



/*
 * First Pulse encoding with similar TE treatment
 */
unsigned int
Scheme3( unsigned int word )
{
  unsigned int data;

  data = (word & 0xff) | ((word >> 1) & 0xff00) | ((word >>2) & 0xff0000);

  //  printf( " -> " );
  //  write_pattern20( data );

  if ( data )
  {
    unsigned int out_data=0;
    //    bool seen_zero;

    //	printf( "  fLE = %d\n", fLE( data ) );
    //	printf( "  lTE = %d\n", lTE( data ) );
    //  printf( "  fTE = %d\n", fTE( data ) );
    int LE = fLE( data );
    int TE = fTE( data );
    int TEl = lTE( data );
    if ( TEl < LE )
      out_data = data;
    else if ( (LE == 0) || (TE == 0) )
      out_data = data;
    else
    {
      if ( data & 0x00C00000 )        // either of first two bits set
	out_data = 0x00800000;        // Set first bit

      if ( data & 0x10 )              // either of last two bits set
	out_data = out_data | 0x10;   // Set last bit

      //  printf( "  -> " );
      //  write_pattern( out_data, true );

      for ( int i = LE; i <= TE; i++ )
	out_data = out_data | (1 << (20+4-i));

    }

    data = ((out_data & 0xff0000) << 2) | 
      ((out_data & 0xff00) << 1) | (out_data & 0xff);

    //    printf( " -> " );
    //    write_pattern20( data );
  }

  /*
   * Reset middle HT bit
   */
  if ( word & 0x0020000 )   // Middle HT bit
    data = data | 0x0020000;

  /*
    cout << std::hex << data << " ( " << Lmask_save << " , " 
    << Tmask_save << " ) "
    << " -> " << out_data << std::dec 
    << std::endl;
  */

  //      printf( "xxx> " );
  //      write_pattern( data, true );

  return data;
}
