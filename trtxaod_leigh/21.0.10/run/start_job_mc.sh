Reco_tf.py \
    --inputRDOFile='/afs/cern.ch/user/l/lschaef/work/public/mc15_13TeV/RDO.08226377._000209.pool.root.1'   \
--ignoreErrors True \
--conditionsTag 'OFLCOND-MC16-SDR-10' \
--maxEvents '10' \
--autoConfiguration 'everything' \
--postExec 'from IOVDbSvc.CondDB import conddb; conddb.addMarkup("/TRT/Cond/StatusHT","<forceRunNumber>286990</forceRunNumber>");conddb.addOverride("/TRT/Cond/StatusHT","TrtStrawStatusHT-MC-run2-scenario5_00-00"); conddb.addOverride("/TRT/Calib/RT","TrtCalibRt-13TeV_MC-scenario5_00-00" ); conddb.addOverride("/TRT/Calib/T0","TrtCalibT0-13TeV_MC-scenario5_00-00") ' \
--preExec \
'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD.pool.root

