cd $TestArea
cp TRT_LoLumRawData_Scheme0.cxx InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx

cd $TestArea/../build
cmake ../source ; make
source x86_64-slc6-gcc49-opt/setup.sh
cd ../run

 Reco_tf.py  \
    --inputDRAW_EMUFile='/afs/cern.ch/user/l/lschaef/work/public/data16_13TeV/data16_13TeV.00304337.physics_Main.merge.DRAW_ZMUMU.f716_m1616._0100.1' \
     --autoConfiguration 'everything' \
    --ignoreErrors 'True'  \
    --maxEvents '30' \
    --AMI 'f716' \
    --preExec  'all:rec.doCalo.set_Value_and_Lock(False); rec.doMuon.set_Value_and_Lock(False); rec.doEgamma.set_Value_and_Lock(False); rec.doJetMissingETTag.set_Value_and_Lock(False); rec.doTau.set_Value_and_Lock(False); rec.doTau.set_Value_and_Lock(False); InDetFlags.doTRTStandalone=False; InDetFlags.doTRTSeededTrackFinder=False; InDetFlags.doTRTExtensionNew=False  ' \
    --outputESDFile=ESD_data_noTRT_scheme0.pool.root  \
|& tee output_data_noTRT_scheme0

cd $TestArea
cp TRT_LoLumRawData.cxx InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
cd -

#InDetFlags.doTRTStandalone.set_Value_and_Lock(False); InDetFlags.doTRTSeededTrackFinder.set_Value_and_Lock(False); InDetFlags.doExtensionProcessor.set_Value_and_Lock(False); InDetFlags.doTRTExtension.set_Value_and_Lock(False); InDetFlags.doTRTExtensionNew.set_Value_and_Lock(False); 

#