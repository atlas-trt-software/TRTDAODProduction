cd $TestArea
cp TRT_LoLumRawData_Scheme0.cxx InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx

cp ConfiguredInDetPreProcessingTRT_noSWVG.py InnerDetector/InDetExample/InDetRecExample/share/ConfiguredInDetPreProcessingTRT.py

cd $TestArea/../build
cmake ../source ; make
source x86_64-slc6-gcc49-opt/setup.sh
cd ../run

 Reco_tf.py  \
    --inputDRAW_EMUFile='/afs/cern.ch/user/l/lschaef/work/public/data16_13TeV/data16_13TeV.00304337.physics_Main.merge.DRAW_ZMUMU.f716_m1616._0100.1' \
     --autoConfiguration 'everything' \
    --ignoreErrors 'True'  \
    --maxEvents '30' \
    --AMI 'f716' \
    --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ;from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD_data_scheme0_noSWVG.pool.root  \
|& tee output_data_scheme0_noSWVG

cd $TestArea
cp TRT_LoLumRawData.cxx InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
svn revert InnerDetector/InDetExample/InDetRecExample/share/ConfiguredInDetPreProcessingTRT.py
cd -