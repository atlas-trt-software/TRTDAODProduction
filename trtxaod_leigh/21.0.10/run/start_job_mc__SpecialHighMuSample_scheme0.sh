cd $TestArea
cp TRT_LoLumRawData_Scheme0.cxx InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx

cd $TestArea/../build
cmake ../source ; make
source x86_64-slc6-gcc49-opt/setup.sh
cd ../run
#
Reco_tf.py \
    --inputRDOFile='/afs/cern.ch/user/l/lschaef/work/public/mc16_13TeV/mc16_valid.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.RDO.e3698_s2995_r8905/RDO.10230967._000016.pool.root.1' \
--ignoreErrors True \
--maxEvents '30' \
--AMI 'r8905' \
--autoConfiguration 'everything' \
--conditionsTag 'default:OFLCOND-MC16-SDR-13' \
--geometryVersion 'default:ATLAS-R2-2016-00-01-00' \
--preExec \
'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True); InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True);  InDetFlags.doSlimming.set_Value_and_Lock(False) ' \
    --steering 'RAWtoESD:in+ESD' \
    --outputDAOD_IDTRKVALIDFile=InDetDxAOD_HighMuMC_scheme0.pool.root

cd $TestArea
cp TRT_LoLumRawData.cxx InnerDetector/InDetRawEvent/InDetRawData/src/TRT_LoLumRawData.cxx
cd -