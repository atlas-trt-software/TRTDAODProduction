cp TrackStateOnSurfaceDecorator_TrackHitPosition.cxx PhysicsAnalysis/DerivationFramework/DerivationFrameworkInDet/src/TrackStateOnSurfaceDecorator.cxx
cp requirements_TrackHitPosition PhysicsAnalysis/DerivationFramework/DerivationFrameworkInDet/cmt/requirements

cp ./TRT_PrepDataToxAOD_StrawPhi.cxx InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/TRT_PrepDataToxAOD.cxx 
cp ./TRT_PrepDataToxAOD_StrawPhi.h InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/TRT_PrepDataToxAOD.h



 Reco_tf.py  \
    --inputBSFile='/afs/cern.ch/user/l/lschaef/work/public/data16_13TeV/data16_13TeV.00304337.physics_Main.merge.DRAW_ZMUMU.f716_m1616._0100.1'   \
    --ignoreErrors 'True'  \
    --maxEvents '5' \
    --AMI 'f716' \
    --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False)'\
    --postExec 'all:from IOVDbSvc.CondDB import conddb;conddb.blockFolder("/TRT/Calib/ToT/ToTVectors");conddb.blockFolder("/TRT/Calib/ToT/ToTValue");conddb.addFolder("","<dbConnection>sqlite://;schema=./TRTToTData2016PileUp.db;dbname=CONDBR2</dbConnection>/TRT/Calib/ToT/ToTVectors <tag>TRTCalibToTDataVectors-000-04</tag>",force=True);conddb.addFolder("","<dbConnection>sqlite://;schema=./TRTToTData2016PileUp.db;dbname=CONDBR2</dbConnection>/TRT/Calib/ToT/ToTValue <tag>TRTCalibToTDataValue-000-04</tag>",force=True) ' \
    --outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root  

cp TrackStateOnSurfaceDecorator_NOTrackHitPosition.cxx PhysicsAnalysis/DerivationFramework/DerivationFrameworkInDet/src/TrackStateOnSurfaceDecorator.cxx
cp requirements_NOTrackHitPosition PhysicsAnalysis/DerivationFramework/DerivationFrameworkInDet/cmt/requirements

cp ./TRT_PrepDataToxAOD_NOStrawPhi.cxx InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/TRT_PrepDataToxAOD.cxx 
cp ./TRT_PrepDataToxAOD_NOStrawPhi.h InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/TRT_PrepDataToxAOD.h

