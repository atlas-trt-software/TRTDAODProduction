pathena --trf \
"Reco_tf.py \
--inputHITSFile='%IN' \
--ignoreErrors True \
--maxEvents '300' \
--skipEvents='%SKIPEVENTS' \
--DataRunNumber 286990 \
--jobNumber 286990 \
--conditionsTag 'OFLCOND-MC16-SDR-25' \
--geometryVersion 'ATLAS-R2-2016-01-00-01' \
--ignorePatterns 'ByteStreamInputSvc.+ERROR.+Skipping.+bad.+event|EventSelector.+ERROR.+badFragment.+data.+encountered|ByteStreamInputSvcWARNING.+ERROR.+message.+limit|EventSelector.+WARNING.+ERROR.+message.+limit' \
--postInclude 'PyJobTransforms/UseFrontier.py' \
--autoConfiguration 'everything' \
--inputHighPtMinbiasHitsFile %HIMBIN \
--inputLowPtMinbiasHitsFile %LOMBIN \
--numberOfCavernBkg 0 \
--numberOfHighPtMinBias 0.312197744 \
--numberOfLowPtMinBias 59.68780226 \
--pileupFinalBunch 6 \
--postExec \
'all:from IOVDbSvc.CondDB import conddb; conddb.addMarkup(\"/TRT/Cond/StatusHT\",\"<forceRunNumber>286990</forceRunNumber>\"); conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run3-baseline_00-00\"); from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc ; TRTCalibDBSvc=TRT_CalDbSvc(); ServiceMgr += TRTCalibDBSvc;TRTCalibDBSvc.calibTextFile=\"dbconst.txt\"; conddb.blockFolder(\"/TRT/Calib/RT\") ; conddb.blockFolder(\"/TRT/Calib/T0\");' \
--preExec \
'HITtoRDO:ScaleTaskLength=0.06;userRunLumiOverride={\"run\":310000,\"startmu\":0.0,\"endmu\":60.0,\"stepmu\":2.5,\"startlb\":1,\"timestamp\": 1446539185};' \
'all:rec.doTrigger.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(WARNING); from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True); InDetFlags.doSlimming.set_Value_and_Lock(False); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); from GeoModelSvc.GeoModelSvcConf import GeoModelSvc; GeoModelSvc.TRT_VersionOverride=\"TRT-GEO-03\" ' \
--preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInlcude.PileUpBunchTrainsMC16c_2017_Config1.py,RunDependentSimData/configLumi_muRange.py' \
--outputDAOD_IDTRKVALIDFile=%OUT.InDetDxAOD.pool.root " \
 --highMinDS=group.det-indet.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.simul.HITS_run3baseline_geom_01_EXT0 \
 --lowMinDS=group.det-indet.mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS_run3baseline_geom_EXT0,group.det-indet.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS_run3baseline_geom_2_EXT0 \
 --nHighMin=1 --nLowMin=1 \
 --nEventsPerJob 300 \
 --official \
 --extFile dbconst.txt \
 --inDS group.det-indet.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.HITS_run3baseline_geom_EXT0 \
 --outDS group.det-indet.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.simul.TRTxaod_run3baseline_geom_trt119

