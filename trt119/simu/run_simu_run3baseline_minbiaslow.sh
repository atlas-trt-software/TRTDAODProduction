pathena --trf "Sim_tf.py \
  --inputEvgenFile '%IN' \
  --maxEvents '200' \
  --randomSeed %RNDM:100 \
  --geometryVersion ATLAS-R2-2016-01-00-01_VALIDATION \
  --conditionsTag OFLCOND-MC16-SDR-14 \
  --DataRunNumber 286990 \
  --postInclude 'RecJobTransforms/UseFrontier.py' \
  --preInclude 'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py' \
  --postExec \
  'from IOVDbSvc.CondDB import conddb; conddb.addOverride(\"/TRT/Cond/StatusHT\",\"TrtStrawStatusHT-MC-run3-baseline_00-00\");' \
  --preExec \
  'EVNTtoHITS:simFlags.SimBarcodeOffset.set_Value_and_Lock(200000)' \
  'EVNTtoHITS:simFlags.TRTRangeCut=30.0;simFlags.TightMuonStepping=True' \
  --outputHitsFile %OUT.HIST.root " \
  --nEventsPerJob 200 \
  --official \
  --osMatching \
  --inDS mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.evgen.EVNT.e3581 \
  --outDS group.det-indet.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.simul.HITS_run3baseline_geom

