####################################################
### Setup:
###     sequencer
###     derivation framework / event selection
####################################################

# Sequence
from AthenaCommon.AlgSequence import AlgSequence 
topSequence = AlgSequence() 
DRAW_tnp_Seq = CfgMgr.AthSequencer("DRAW_tnp_Seq")

triggersZE = [
    # Single electron
    'HLT_e24_lhmedium_L1EM18VH',
    'HLT_e24_lhmedium_L1EM20VH',
    'HLT_e60_lhmedium',
    'HLT_e120_lhloose',
    'HLT_e5_lhvloose',
    'HLT_e12_lhvloose_L1EM10VH',
    'HLT_e12_lhvloose_nod0_L1EM10VH',
    'HLT_e15_lhvloose_L1EM7',
    'HLT_e20_lhvloose',
    'HLT_e20_lhvloose_L1EM12',
    'HLT_e20_lhvloose_nod0',
    'HLT_e24_lhmedium_nod0_L1EM20VH',
    'HLT_e24_lhtight_nod0_ivarloose',
    'HLT_e26_lhtight_nod0_ivarloose',
    'HLT_e26_lhtight_smooth_ivarloose',
    'HLT_e28_lhmedium_nod0_L1EM20VH',
    'HLT_e28_lhtight_nod0_ivarloose',
    'HLT_e28_lhtight_smooth_ivarloose',
    'HLT_e50_lhvloose_L1EM15',
    'HLT_e60_medium',
    'HLT_e60_lhmedium_nod0',
    'HLT_e140_lhloose_nod0',
    'HLT_e300_etcut']
expression_trigZE = ' || '.join(triggersZE)

triggersZM = [
    # Single muon
    'HLT_mu26_imedium',
    'HLT_mu26_ivarmedium',
    'HLT_mu28_imedium',
    'HLT_mu28_ivarmedium',
    'HLT_mu40',
    'HLT_mu50' 
   ]
expression_trigZM = ' || '.join(triggersZM)



# Zee TnP
requirement_Zee_tag = '(Electrons.Tight || Electrons.LHTight) && Electrons.pt > 24.5*GeV'
requirement_Zee_probe = 'Electrons.pt > 6.5*GeV'

from DerivationFrameworkTools.DerivationFrameworkToolsConf import DerivationFramework__InvariantMassTool
ZeeMassTool = DerivationFramework__InvariantMassTool( name = "ZeeMassTool",
                                                      ObjectRequirements = requirement_Zee_tag,
                                                      SecondObjectRequirements = requirement_Zee_probe,
                                                      StoreGateEntryName = "Zee_DiElectronMass",
                                                      MassHypothesis = 0.511*MeV,
                                                      SecondMassHypothesis = 0.511*MeV,
                                                      ContainerName = "Electrons",
                                                      SecondContainerName = "Electrons")

ToolSvc+=ZeeMassTool
expression_Zee = 'count(Zee_DiElectronMass > 70.0*GeV && Zee_DiElectronMass < 110.0*GeV)>=1'

# Zmumu TnP
requirement_Zmm_tag   = 'Muons.ptcone40/Muons.pt < 0.3 && Muons.pt > 10.*GeV'
requirement_Zmm_probe = 'Muons.ptcone40/Muons.pt < 0.3 && Muons.pt > 4.5*GeV'

ZmmMassTool = DerivationFramework__InvariantMassTool( name = "ZmmMassTool",
                                                         ObjectRequirements = requirement_Zmm_tag,
                                                         SecondObjectRequirements = requirement_Zmm_probe,
                                                         StoreGateEntryName = "Zmm_DiMuonMass",
                                                         MassHypothesis = 105.66*MeV,
                                                         SecondMassHypothesis = 105.66*MeV,
                                                         ContainerName = "Muons",
                                                         SecondContainerName = "Muons")
ToolSvc+=ZmmMassTool
expression_Zmm = 'count(Zmm_DiMuonMass > 70.0*GeV && Zmm_DiMuonMass < 110.0*GeV)>=1'
expression_Z = '( ' + expression_Zee + ' && ( ' + expression_trigZE + ' ) ) || ( ' + expression_Zmm + ' && ( ' + expression_trigZM + ' ) )'


# # Event selection tool
from DerivationFrameworkTools.DerivationFrameworkToolsConf import DerivationFramework__xAODStringSkimmingTool
# Z_SkimmingTool = DerivationFramework__xAODStringSkimmingTool(name = "Z_SkimmingTool",
#                                                                     expression = expression_Z)

# ToolSvc += Z_SkimmingTool
# print Z_SkimmingTool


# JPSIee TnP

triggersJE = [
    # Di-electron
    'HLT_e9_etcut_e5_lhtight_nod0_Jpsiee',
    'HLT_e14_etcut_e5_lhtight_nod0_Jpsiee',
    'HLT_e5_lhtight_nod0_e4_etcut',
    'HLT_e5_lhtight_nod0_e4_etcut_Jpsiee',
    'HLT_e9_lhtight_nod0_e4_etcut_Jpsiee',
    'HLT_e14_lhtight_nod0_e4_etcut_Jpsiee',
    ]
expression_trigJE = ' || '.join(triggersJE)

triggersJM = [
    # Di-muon
    'HLT_2mu4',
    'HLT_2mu6',
    'HLT_2mu6_bJpsimumu',
    'HLT_2mu6_bJpsimumu_delayed',
    'HLT_2mu6_bJpsimumu_Lxy0_delayed',
    'HLT_2mu10_bJpsimumu',
    'HLT_2mu10_bJpsimumu_delayed',
    'HLT_2mu10_bJpsimumu_noL2',
    'HLT_2mu14',
    'HLT_2mu14_nomucomb',
    'HLT_2mu15'
    ]
expression_trigJM = ' || '.join(triggersJM)


requirement_JPSIee_tag = '(Electrons.Tight || Electrons.LHTight) && Electrons.pt > 4.5*GeV'
requirement_JPSIee_probe = 'Electrons.pt > 4.5*GeV'

JPSIeeMassTool = DerivationFramework__InvariantMassTool( name = "JPSIeeMassTool",
                                                      ObjectRequirements = requirement_JPSIee_tag,
                                                      SecondObjectRequirements = requirement_JPSIee_probe,
                                                      StoreGateEntryName = "JPSIee_DiElectronMass",
                                                      MassHypothesis = 0.511*MeV,
                                                      SecondMassHypothesis = 0.511*MeV,
                                                      ContainerName = "Electrons",
                                                      SecondContainerName = "Electrons")

ToolSvc+=JPSIeeMassTool
expression_JPSIee = '(count(JPSIee_DiElectronMass > 2.0*GeV && JPSIee_DiElectronMass < 4.0*GeV)>=1)'

# JPSImumu TnP
requirement_JPSImm_tag = '(Muons.ptcone40/Muons.pt < 0.3 && Muons.pt > 4.5*GeV)'
requirement_JPSImm_probe = '(Muons.ptcone40/Muons.pt < 0.3 && Muons.pt > 4.5*GeV)'

JPSImmMassTool = DerivationFramework__InvariantMassTool( name = "JPSImmMassTool",
                                                         ObjectRequirements = requirement_JPSImm_tag,
                                                         SecondObjectRequirements = requirement_JPSImm_probe,
                                                         StoreGateEntryName = "JPSImm_DiMuonMass",
                                                         MassHypothesis = 105.66*MeV,
                                                         SecondMassHypothesis = 105.66*MeV,
                                                         ContainerName = "Muons",
                                                         SecondContainerName = "Muons")
ToolSvc+=JPSImmMassTool
expression_JPSImm = '(count(JPSImm_DiMuonMass > 2.0*GeV && JPSImm_DiMuonMass < 4.0*GeV)>=1)'
expression_JPSI = '( ' + expression_JPSIee + ' && ( ' + expression_trigJE + ' ) ) || ( ' + expression_JPSImm + ' && ( ' + expression_trigJM + ' ) )'


expression = expression_Z + '||' + expression_JPSI
# # Event selection tool
# JPSI_SkimmingTool = DerivationFramework__xAODStringSkimmingTool(name = "JPSI_SkimmingTool",
#                                                                     expression = expression_JPSI)
# ToolSvc += JPSI_SkimmingTool
# print JPSI_SkimmingTool


# Event selection tool
TRT_SkimmingTool = DerivationFramework__xAODStringSkimmingTool(name = "TRT_SkimmingTool",
                                                                    expression = expression)
ToolSvc += TRT_SkimmingTool
print TRT_SkimmingTool

# Kernel algorithm
from DerivationFrameworkCore.DerivationFrameworkCoreConf import DerivationFramework__DerivationKernel
DRAW_tnp_Seq += CfgMgr.DerivationFramework__DerivationKernel("DRAW_tnpKernel",
                                                               AugmentationTools = [ZeeMassTool,ZmmMassTool,JPSIeeMassTool,JPSImmMassTool],
                                                               SkimmingTools = [TRT_SkimmingTool]
#                                                               SkimmingTools = [Z_SkimmingTool,JPSI_SkimmingTool]
                                                               )
topSequence += DRAW_tnp_Seq 

##################
### Output stream
##################
from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
from D2PDMaker.D2PDHelpers import buildFileName
from PrimaryDPDMaker.PrimaryDPDFlags import primDPD
streamName = "DRAW_TRT" #primDPD.WriteRAWPerfDPD_ZMUMU.StreamName
fileName   = buildFileName( primDPD.WriteRAWPerfDPD_ZMUMU )
#streamName = primDPD.WriteDRAW_tnp.StreamName
#fileName   = buildFileName( primDPD.WriteDRAW_tnp )
# # Remove the .pool.root ending in the file name, this is a RAW file!
# if fileName.endswith(".pool.root") :
#     fileName = fileName.rstrip(".pool.root")
#     pass
StreamDRAW_tnp = MSMgr.NewByteStream( streamName, fileName )
StreamDRAW_tnp.AddRequireAlgs(["DRAW_tnpKernel"])

# Don't write an output RAW file if it is empty
StreamDRAW_tnp.bsOutputSvc.WriteEventlessFiles = primDPD.WriteEventlessFiles()


#########################################
# Add the containers to the output stream
#########################################
from PrimaryDPDMaker import PrimaryDPD_OutputDefinitions as dpdOutput

# Take everything from the input
ExcludeList=[]
dpdOutput.addAllItemsFromInputExceptExcludeList(streamName,ExcludeList)

