pathena --trf \
"Reco_tf.py  \
  --inputBSFile='%IN'  \
  --ignoreErrors 'True'  \
  --skipEvents='%SKIPEVENTS' \
  --maxEvents '250' \
  --AMI 'f943' \
  --preExec  'all:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags ; InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(False); InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);  InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpTriggerInfo.set_Value_and_Lock(True); InDetFlags.doTRTGlobalOccupancy.set_Value_and_Lock(True) ; InDetFlags.doSlimming.set_Value_and_Lock(False); InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False); rec.OutputLevel.set_Value_and_Lock(ERROR); rec.doTau.set_Value_and_Lock(False); rec.doBTagging.set_Value_and_Lock(False); rec.doPerfMon.set_Value_and_Lock(True);' \
  --postExec 'all:conddb.addOverride(\"/TRT/Calib/ToT/ToTValue\",\"TRTCalibToTDataValue-000-03\") ; conddb.addOverride(\"/TRT/Calib/ToT/ToTVectors\",\"TRTCalibToTDataVectors-000-03\")' \
  --steering 'RAWtoESD:in+ESD' \
  --outputDAOD_IDTRKVALIDFile %OUT.InDetDxAOD.pool.root " \
  --nEventsPerJob=250 \
  --official \
  --inDS=data18_13TeV.00355053.physics_MinBias.daq.RAW \
  --outDS=group.det-indet.data18_13TeV.00355053.physics_MinBias.daq.xAOD.f943_trt107
