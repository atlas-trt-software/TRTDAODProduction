#No Trig GRID, 100K events, errDC override
pathena --trf \
"Reco_tf.py \
--inputHITSFile='%IN' \
--outputAODFile='%OUT.InDetDxAOD.pool.root' \
--maxEvents '10000' \
--preInclude 'all:Campaigns.MC23a' \
--postExec 'RAWtoALL:from IOVDbSvc.IOVDbSvcConfig import addOverride;cfg.merge(addOverride(flags, \"/TRT/Calib/errors2d\", \"TRTCaliberrors2d_dt_lay_run2_sf175_00-02\"))' \
--steering 'RAWtoESD:in+ESD' \
--AMI=r15132" \
--nEventsPerJob=10000 \
--nJobs=10 \
--nFilesPerJob=1 \
--official \
--nCore=8 \
--inDS=mc23_13p6TeV.801278.Py8EG_A14NNPDF23LO_perf_JF17.merge.HITS.e8514_e8528_s4162_s4114 \
--outDS=group.det-indet.801278.Py8EG_A14NNPDF23LO_perf_JF17.merge.HITS.e8514_e8528_s4162_s4114.TRTxAOD_M2323E1_2n_100K

