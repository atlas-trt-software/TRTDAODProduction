#No Trig local, 100K events, errDC override
Reco_tf.py \
--inputHITSFile='/eos/user/s/saktas/xAOD_prod/data/mc23_13p6TeV.801278.Py8EG_A14NNPDF23LO_perf_JF17.merge.HITS.e8514_e8528_s4162_s4114/HITS.33933542._001819.pool.root.1' \
--outputAODFile='MC.xAOD.pool.root' \
--maxEvents='100' \
--postExec='RAWtoALL:from IOVDbSvc.IOVDbSvcConfig import addOverride;cfg.merge(addOverride(flags, "/TRT/Calib/errors2d", "TRTCaliberrors2d_dt_lay_run2_sf175_00-02"))' \
--preInclude='all:Campaigns.MC23a' \
--steering='RAWtoESD:in+ESD' \
--AMI=r15132
