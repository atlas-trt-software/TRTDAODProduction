## TRTDAODProduction

# Documentation

See here: [https://twiki.cern.ch/twiki/bin/viewauth/Atlas/TrtxAODProduction](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/TrtxAODProduction)

Check also the README files in the trt\* folders. They provide simple instructions on how to run the job.

# Scripts for producing TRT Derivations.

Each trt\* folders correspond to a TRT tag, the complete list of tags can be found here: [https://twiki.cern.ch/twiki/bin/viewauth/Atlas/TrtxAOD#TRT\_production\_tags](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/TrtxAOD#TRT_production_tags)

