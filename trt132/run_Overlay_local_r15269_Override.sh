Reco_tf.py \
--inputHITSFile '/eos/user/s/saktas/xAOD_prod/data/mc23_13p6TeV/HITS_merge.e8514_e8528_s4159_s4114/HITS_Zmumu.34124629._005124.pool.root.1' \
--inputRDO_BKGFile '/eos/user/s/saktas/xAOD_prod/data/mc23_13p6TeV/newRDO_background_single_nu_Pt50.merge.RDO.e8514_e8528_s4153_d1909_d1908/RDO.36806752._000001.pool.root.1' \
--outputAODFile='MC.xAOD.pool.root' \
--postExec='Overlay:trt=cfg.getEventAlgo("TRT_OverlayDigitization").DigitizationTool;trt.Override_highThresholdBarShortArgon=0.002271;trt.Override_highThresholdBarLongArgon=0.002061;trt.Override_highThresholdECAwheels=0.004941;trt.Override_highThresholdECBwheels=0.004868;trt.Override_highThresholdECAwheelsArgon=0.002168;trt.Override_highThresholdECBwheelsArgon=0.002089;trtOverlayAlg=cfg.getEventAlgo("TRTOverlay");trtOverlayAlg.TRT_HT_OccupancyCorrectionBarrel=0.110;trtOverlayAlg.TRT_HT_OccupancyCorrectionEndcap=0.090;trtOverlayAlg.TRT_HT_OccupancyCorrectionBarrelNoE=0.060;trtOverlayAlg.TRT_HT_OccupancyCorrectionEndcapNoE=0.050;trtOverlayAlg.TRT_HT_OccupancyCorrectionBarrelAr=0.080;trtOverlayAlg.TRT_HT_OccupancyCorrectionEndcapAr=0.081;trtOverlayAlg.TRT_HT_OccupancyCorrectionBarrelArNoE=0.073;trtOverlayAlg.TRT_HT_OccupancyCorrectionEndcapArNoE=0.082' \
--athenaopts "Overlay: --threads=8" \
--steering='doOverlay' \
--maxEvents '10000' \
--AMI=r15269
