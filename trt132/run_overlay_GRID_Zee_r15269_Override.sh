#No Trig , override GRID
pathena --trf \
"Reco_tf.py \
--inputHITSFile='%IN' \
--outputAODFile='%OUT.InDetDxAOD.pool.root' \
--inputRDO_BKGFile='%MININ' \
--maxEvents='10000' \
--postExec='Overlay:trt=cfg.getEventAlgo(\"TRT_OverlayDigitization\").DigitizationTool;trt.Override_highThresholdBarShortArgon=0.002271;trt.Override_highThresholdBarLongArgon=0.002061;trt.Override_highThresholdECAwheels=0.004941;trt.Override_highThresholdECBwheels=0.004868;trt.Override_highThresholdECAwheelsArgon=0.002168;trt.Override_highThresholdECBwheelsArgon=0.002089;trtOverlayAlg=cfg.getEventAlgo(\"TRTOverlay\");trtOverlayAlg.TRT_HT_OccupancyCorrectionBarrel=0.110;trtOverlayAlg.TRT_HT_OccupancyCorrectionEndcap=0.090;trtOverlayAlg.TRT_HT_OccupancyCorrectionBarrelNoE=0.060;trtOverlayAlg.TRT_HT_OccupancyCorrectionEndcapNoE=0.050;trtOverlayAlg.TRT_HT_OccupancyCorrectionBarrelAr=0.100;trtOverlayAlg.TRT_HT_OccupancyCorrectionEndcapAr=0.101;trtOverlayAlg.TRT_HT_OccupancyCorrectionBarrelArNoE=0.088;trtOverlayAlg.TRT_HT_OccupancyCorrectionEndcapArNoE=0.102' \
'RAWtoALL:from IOVDbSvc.IOVDbSvcConfig import addOverride;cfg.merge(addOverride(flags, \"/TRT/Calib/errors2d\", \"TRTCaliberrors2d_dt_lay_run2_sf175_00-02\"))' \
--runNumber='601189' \
--steering='doOverlay' \
--AMI=r15269" \
--nEventsPerJob=10000 \
--nJobs=1 \
--nFilesPerJob=1 \
--official \
--nCore=8 \
--minDS=mc23_13p6TeV.900149.PG_single_nu_Pt50.merge.RDO.e8514_e8528_s4153_d1909_d1908_sub0005_rnd601190 \
--inDS=mc23_13p6TeV:mc23_13p6TeV.601189.PhPy8EG_AZNLO_Zee.merge.HITS.e8514_e8528_s4159_s4114_tid33674699_00 \
--outDS=group.det-indet.mc23_13p6TeV.601189.PhPy8EG_AZNLO_Zee.merge.HITS.e8514_e8528_s4159_s4114.TRTxAOD_10k
