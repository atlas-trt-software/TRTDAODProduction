Reco_tf.py \
--inputBSFile '/eos/user/s/saktas/xAOD_prod/data/user.saktas.data22_13p6TeV.00427394.physics_Main.daq.RAW/data22_13p6TeV.00427394.physics_Main.daq.RAW._lb0888._SFO-11._0001.data' \
--outputAODFile='InDetDxAOD.pool.root' \
--runNumber=427394 \
--perfmon='fastmonmt' \
--postExec 'with open("config.pkl", "wb") as f: cfg.store(f)' \
--maxEvents '50' \
--AMI=r15020 
