Reco_tf.py \
--inputHITSFile '/eos/user/s/saktas/xAOD_prod/data/mc23_13p6TeV/HITS_merge.e8514_e8528_s4162_s4114/HITS.Zee.33839833._003676.pool.root.1' \
--inputRDO_BKGFile '/eos/user/s/saktas/xAOD_prod/data/mc23_13p6TeV/RDO_background_PG_single_nu_Pt50.merge.e8514_e8528_s4112_d1865_d1858/RDO.33108932._005760.pool.root.1' \
--outputAODFile=InDetDxAOD.pool.root \
--athenaopts "Overlay: --threads=8" \
--steering='doOverlay' \
--maxEvents '10000' \
--AMI=r15022
