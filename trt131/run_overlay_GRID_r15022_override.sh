#No Trig , override GRID
pathena --trf \
"Reco_tf.py \
--inputHITSFile='%IN' \
--outputAODFile='%OUT.InDetDxAOD.pool.root' \
--postExec 'Overlay:trt=cfg.getEventAlgo(\"TRT_OverlayDigitization\").DigitizationTool;trt.Override_highThresholdBarShortArgon=0.002271;trt.Override_highThresholdBarLongArgon=0.002061;trt.Override_highThresholdECAwheels=0.004941;trt.Override_highThresholdECBwheels=0.004868;trt.Override_highThresholdECAwheelsArgon=0.002168;trt.Override_highThresholdECBwheelsArgon=0.002089' \
--athenaopts \"Overlay: --threads=8\" \
--steering='doOverlay' \
--maxEvents '10000' \
--AMI=r15022" \
--official \
--nEventsPerJob=10000 \
--nJobs=1 \
--nFilesPerJob=1 \
--nCore=8 \
--inDS=mc23_13p6TeV.601190.PhPy8EG_AZNLO_Zmumu.merge.HITS.e8514_e8528_s4162_s4114 \
--outDS=group.det-indet.mc23_13p6TeV.601190.PhPy8EG_AZNLO_Zmumu.merge.HITS.e8514_e8528_s4162_s4114.TRTxAOD_Overlay_Override_10k_TEST
